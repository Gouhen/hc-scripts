;#RequireAdmin
#include <AutoItConstants.au3>
#include <MsgBoxConstants.au3>
#include <GUIConstantsEx.au3>
#include <StaticConstants.au3>
#include <WindowsConstants.au3>


;bay buttons
Global Const $bay_button[2] = [1053, 725]
Global Const $auto_bay_button[4] = [1338, 774, 0x61CF00, 0x65D700]
Global Const $auto_bay_confirm[4] = [766, 574, 0x6ED217, 0x73DB18]
Global Const $bay_quest_button[3] = [425, 801, 0x63BE21]
Global Const $deploy_bests_button[2] = [611, 459]
Global Const $confirm_button[2] = [698, 797]
Global Const $no_spell_confirm_button[2] = [632, 578]

Global Const $mercenary_button[3] = [454, 763, 0x29CFE4]

Global Const $bay_missing_warrior[3] = [845, 573, 0xF43F18];red button : not enough warriors
Global Const $use_next_warrior[2] = [165, 610] ; if squad is incomplete, add the next one
Global Const $confirm_new_warrior_button[4] = [276, 704, 0x70D231];green button. Else bay is over ; changed coordinates to a larger same-color area
Global Const $bay_get_reward[5] = [514, 737, 0x51C30F, 0x4FC30D, 0x52C310] ; avant : 0x4FC30D, 0x52C310
Global Const $bay_get_reward_confirm[2] = [765, 781]
Global Const $grayed_new_warrior[3] = [121, 576, 0xAAAAAA] ;gray : warrior is dead


Func Bay()
	If initBlueStacks() Then
		_Function_Finished()
		Return
	EndIf

	; si on est dans le château
	;	on va dans la baie
	; si on est dans la baie
	;	on lance la baie
	;si la baie est lancée
	;	loop
	;sinon
	;	backToCastle
	;tant que baie pas lancée


	Local $firstOpening = True
	Local $bayLaunched = False

	Do
		If is_castle_screen() Then
			ConsoleWrite ("castle screen" & @CRLF)
			If open_map($firstOpening) Then
				_Function_Finished()
				Return
			EndIf
			$firstOpening = False

			If open_bay() Then
				_Function_Finished()
				Return
			EndIf

			; attendre bay screen

		EndIf

		If is_bay_screen() Then
			ConsoleWrite ("bay screen" & @CRLF)
			If launch_bay() Then
				_Function_Finished()
				Return
			EndIf

			;attendre bay stage

		EndIf

		If is_bay_stage() Then
			ConsoleWrite ("bay stage" & @CRLF)
			$bayLaunched = True
			If AutoBayLoop() Then		; <= fonction principale
				_Function_Finished()
				Return
			EndIf
		EndIf

		If $bayLaunched == False Then
			If back_to_castle() Then
				_Function_Finished()
				Return
			EndIf
		EndIf
	Until $bayLaunched == True

	ConsoleWrite("Fin"& @CRLF)
	_Function_Finished()
EndFunc

Func ContinueBay()

	If initBlueStacks() Then
		_Function_Finished()
		Return
	EndIf

	BayLoop()
EndFunc


Func AutoBayLoop()
	ConsoleWrite ("bay loop" & @CRLF)
	;si le bouton auto est vert, appuyer sur le bouton auto.
	;si ça râle, rajouter des combattants
	;si y'a le bouton de récompense, lancer la séquence de fin
	;si y'a plus de bouton (là, je sais pas comment), perdre
	Do
		$color = PixelGetColor($auto_bay_button[0], $auto_bay_button[1])

		;si bouton vert, on clique. Si erreur, on ajoute des combattants
		If $color == $auto_bay_button[2] or $color == $auto_bay_button[3] Then

			MouseClick("primary", $auto_bay_button[0],  $auto_bay_button[1], 1)

			If _Interrupt_Sleep(2000) Then
				Return True
			Endif

			; if red button, clic no, add fighters
			;ConsoleWrite("Couleur en ("&$bay_missing_warrior[0]&", "&$bay_missing_warrior[1]&") = " & Hex(PixelGetColor($bay_missing_warrior[0], $bay_missing_warrior[1]), 6) & @CRLF)
			If (PixelGetColor($bay_missing_warrior[0], $bay_missing_warrior[1]) == $bay_missing_warrior[2]) Then
				ConsoleWrite ("Il manque des guerriers" & @CRLF)
				MouseClick("primary", $bay_missing_warrior[0],  $bay_missing_warrior[1], 1)
				If _Interrupt_Sleep(1000) Then
					Return True
				EndIf

				Local $count_new_warriors = 0
				While ($count_new_warriors < 6 )
					$count_new_warriors = $count_new_warriors +1
					MouseClick("primary", $use_next_warrior[0],  $use_next_warrior[1], 1)

					If _Interrupt_Sleep(1000) Then
						Return True
					EndIf
					$color = PixelGetColor($confirm_new_warrior_button[0], $confirm_new_warrior_button[1])
					If $color == $confirm_new_warrior_button[2] or $color == $confirm_new_warrior_button[3] Then
						MouseClick("primary", $confirm_new_warrior_button[0],  $confirm_new_warrior_button[1], 1)

						If _Interrupt_Sleep(1000) Then
							Return True
						EndIf
					EndIf
				WEnd

				;attack
				MouseClick("primary", $auto_bay_button[0],  $auto_bay_button[1], 1)
				If _Interrupt_Sleep(2000) Then
					Return True
				Endif

			EndIf

			$confirmColor = PixelGetColor($auto_bay_confirm[0], $auto_bay_confirm[1])
			If $confirmColor == $auto_bay_confirm[2] or $confirmColor == $auto_bay_confirm[3] Then
				ConsoleWrite ("Confirmer" & @CRLF)
				MouseClick("primary", $auto_bay_confirm[0],  $auto_bay_confirm[1], 1)
				If _Interrupt_Sleep(2000) Then
					Return True
				Endif
			EndIf
		EndIf

		; si pas bouton vert, on ne fait rien, jusqu'à ce qu'on voit le bouton de fin de bait

		;si bouton "fin de baie", on sort
		$btncolor = PixelGetColor($bay_get_reward[0], $bay_get_reward[1])
		If ($btncolor == $bay_get_reward[2] or $btncolor == $bay_get_reward[3] or $btncolor == $bay_get_reward[4]) Then
			ConsoleWrite ("Fin de la baie" & @CRLF)
			ExitLoop
		Endif

		If _Interrupt_Sleep(1000) Then
			Return True
		EndIf

	Until 0 ; todo mettre un timer. todo 2 : arrêter la baie si tout le monde est mort, aussi.

	$btncolor = PixelGetColor($bay_get_reward[0], $bay_get_reward[1])
	$warriorbtncolor = PixelGetColor($grayed_new_warrior[0], $grayed_new_warrior[1])
	If ($btncolor == $bay_get_reward[2] or $btncolor == $bay_get_reward[3] or $btncolor == $bay_get_reward[4] or $warriorbtncolor == $grayed_new_warrior[2]) Then
		ConsoleWrite ("Fin de la baie" & @CRLF)
		MouseClick("primary", $bay_get_reward[0],  $bay_get_reward[1], 1)

		If _Interrupt_Sleep(1000) Then
			_Function_Finished()
			return
		EndIf
		MouseClick("primary", $bay_get_reward_confirm[0],  $bay_get_reward_confirm[1], 1)

		If _Interrupt_Sleep(1000) Then
			_Function_Finished()
			return
		EndIf
		MouseClick("primary", $bay_get_reward[0],  $bay_get_reward[1], 1)

		If _Interrupt_Sleep(1000) Then
			_Function_Finished()
			return
		EndIf
	EndIf

	If _Interrupt_Sleep(2000) Then
		_Function_Finished()
		return
	EndIf

	Return False
EndFunc

Func BayLoop()

	Local $count = 0;
	Do
		$count = $count+1
		ConsoleWrite ("Attaque " & $count & @CRLF)
		;double clic pour supprimer le "tremblement"
		MouseClick("primary", $fight_button[0],  $fight_button[1], 1)
		If _Interrupt_Sleep(500) Then
			_Function_Finished()
			return
		EndIf

		MouseClick("primary", $fight_button[0],  $fight_button[1], 1)
		If _Interrupt_Sleep(2000) Then
			_Function_Finished()
			return
		Endif

		; if red button, clic no, add fighters
		;ConsoleWrite("Couleur en ("&$bay_missing_warrior[0]&", "&$bay_missing_warrior[1]&") = " & Hex(PixelGetColor($bay_missing_warrior[0], $bay_missing_warrior[1]), 6) & @CRLF)
		If (PixelGetColor($bay_missing_warrior[0], $bay_missing_warrior[1]) == $bay_missing_warrior[2]) Then
			ConsoleWrite ("Il manque des guerriers" & @CRLF)
			MouseClick("primary", $bay_missing_warrior[0],  $bay_missing_warrior[1], 1)
			If _Interrupt_Sleep(1000) Then
				_Function_Finished()
				return
			EndIf

			Local $count_new_warriors = 0
			While ($count_new_warriors < 6 )
				$count_new_warriors = $count_new_warriors +1
				MouseClick("primary", $use_next_warrior[0],  $use_next_warrior[1], 1)

				If _Interrupt_Sleep(1000) Then
					_Function_Finished()
					return
				EndIf
				$color = PixelGetColor($confirm_new_warrior_button[0], $confirm_new_warrior_button[1])
				If $color == $confirm_new_warrior_button[2] or $color == $confirm_new_warrior_button[3] Then
					MouseClick("primary", $confirm_new_warrior_button[0],  $confirm_new_warrior_button[1], 1)

					If _Interrupt_Sleep(1000) Then
						_Function_Finished()
						return
					EndIf
				EndIf
			WEnd

			;attack
			MouseClick("primary", $fight_button[0],  $fight_button[1], 1)
			If _Interrupt_Sleep(500) Then
				_Function_Finished()
				return
			EndIf

			MouseClick("primary", $fight_button[0],  $fight_button[1], 1)
			If _Interrupt_Sleep(2000) Then
				_Function_Finished()
				return
			Endif
		EndIf

		While(PixelGetColor($mercenary_button[0], $mercenary_button[1]) <> $mercenary_button[2])
			MouseClick("primary", $hero_spell[0],  $hero_spell[1], 1)

			If _Interrupt_Sleep(100) Then
				_Function_Finished()
				return
			EndIf

			;si bouton "fin de baie", on sort aussi
			$btncolor = PixelGetColor($bay_get_reward[0], $bay_get_reward[1])
			If ($btncolor == $bay_get_reward[2] or $btncolor == $bay_get_reward[3] or $btncolor == $bay_get_reward[4]) Then
				ConsoleWrite ("Fin de la baie" & @CRLF)
				ExitLoop
			Endif
		WEnd
		ConsoleWrite ("Fin de l'attaque" & @CRLF)



		If _Interrupt_Sleep(2000) Then
			_Function_Finished()
			return
		Endif
		$btncolor = PixelGetColor($bay_get_reward[0], $bay_get_reward[1])
		$warriorbtncolor = PixelGetColor($grayed_new_warrior[0], $grayed_new_warrior[1])
		If ($btncolor == $bay_get_reward[2] or $btncolor == $bay_get_reward[3] or $btncolor == $bay_get_reward[4] or $warriorbtncolor == $grayed_new_warrior[2]) Then
			ConsoleWrite ("Fin de la baie" & @CRLF)
			MouseClick("primary", $bay_get_reward[0],  $bay_get_reward[1], 1)

			If _Interrupt_Sleep(1000) Then
				_Function_Finished()
				return
			EndIf
			MouseClick("primary", $bay_get_reward_confirm[0],  $bay_get_reward_confirm[1], 1)

			If _Interrupt_Sleep(1000) Then
				_Function_Finished()
				return
			EndIf
			MouseClick("primary", $bay_get_reward[0],  $bay_get_reward[1], 1)

			If _Interrupt_Sleep(1000) Then
				_Function_Finished()
				return
			EndIf
			ExitLoop
		EndIf

		If _Interrupt_Sleep(2000) Then
			_Function_Finished()
			return
		EndIf
	Until $count >= 15

	_Function_Finished()
EndFunc


Func launch_bay()
	;If PixelGetColor($bay_quest_button[0], $bay_quest_button[1]) == $bay_quest_button[2] Then
		MouseClick("primary", $bay_quest_button[0],  $bay_quest_button[1], 1)
		If _Interrupt_Sleep(2000) Then
			return True
		EndIf

		MouseClick("primary", $deploy_bests_button[0],  $deploy_bests_button[1], 1)
		If _Interrupt_Sleep(2000) Then
			return True
		EndIf
		MouseClick("primary", $confirm_button[0],  $confirm_button[1], 1)
		If _Interrupt_Sleep(2000) Then
			return True
		EndIf
		MouseClick("primary", $no_spell_confirm_button[0],  $no_spell_confirm_button[1], 1)
		If _Interrupt_Sleep(10000) Then
			return True
		EndIf
		;Return true
	;ElseIf PixelGetColor($mercenary_button[0], $mercenary_button[1]) == $mercenary_button[2] Then ;already launched bay
	;	Return true
	;Else
	;	Return false
	;EndIf
	Return False
EndFunc




Func is_bay_screen()
	Return FindImage("bay_parrot.png")
EndFunc


Func is_bay_stage()
	Return FindImage("bay_chest.png")
EndFunc

#include-once

#include "H2C2.au3"

Global Const $webhook_url = "https://discord.com/api/webhooks/1325520870785024194/5FQ-pkxq3ahTHy6SqCaNNz8rrJDLEibRIxq1QiKrkW755FYxVAvZJAw_zMx9GY1w-LES"

Func DiscordMessage($UserName, $Message)
    Local $Url = $webhook_url
    Local $oHTTP = ObjCreate("winhttp.winhttprequest.5.1")
    Local $Packet = '{"content": "<@' & $UserName & '>: ' & $Message & '"}'

    ; Check user id
    If StringLen($UserName) <> 18 Then
      _Generic_log_ERROR("Discord UserId has an invalid format !!!")
      Return True
    EndIf

    ; Push message
    $oHTTP.open('POST',$Url)
    $oHTTP.setRequestHeader("Content-Type","application/json")
    $oHTTP.send($Packet)
EndFunc

Func DiscordCapcha($UserName)
  DiscordMessage($UserName, "**CAPCHA Alert !!!**")
EndFunc

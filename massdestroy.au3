;#RequireAdmin
#include <AutoItConstants.au3>
#include <MsgBoxConstants.au3>
#include <GUIConstantsEx.au3>
#include <StaticConstants.au3>
#include <WindowsConstants.au3>

#include "hc_actions.au3"

CreateGUI("HC Destroy Equipment")
HotKeySet("{ESC}", "Terminate")
AutoItSetOption("MouseClickDragDelay", 750)

initBlueStacks()

;TODO avant de lancer le script : se positionner dans la fenêtre de suppression en masse

Local $count = 50
Do
#cs
	MouseClick("primary", 336, 384 , 1) ;top left icon of the window
	Sleep(1000)
	;plus ou moins 3 positions pour le bouton briser
	If PixelGetColor(758, 712) == 0xF33E18 Then
		MouseClick("primary", 758, 758 , 1)
	ElseIf PixelGetColor(760, 607) == 0xF33E18 Then
		MouseClick("primary", 760, 607 , 1)
	Else
		MouseClick("primary", 759, 681 , 1)
	EndIf
	Sleep(1000)
	MouseClick("primary", 643, 642 , 1)
	Sleep(2000)
#ce
	;dépose 50 elements dans la fenêtre de suppression en masse à partir de celui en haut à gauche.
	MouseClickDrag("primary", 969, 288, 495, 385);

	$count = $count - 1;
Until $count <=0
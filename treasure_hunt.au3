#include-once

#include "udf\autoit-opencv\cv_enums.au3"
#include "data/hc_coords.au3"
#include "discord.au3"
#include <Math.au3>

Global Const $treasure_box_size[2] = [125, 100] ; width, height
Global Const $treasure_zone[4] = [450, 255, 1100-450, 695-255] ; Full reward map

Global Const $treasure_loupe_enable_button[2] = [ 1320, 450 ] ; Button to enable loupe on map

Global $pattern_box_unrevealed
Global $pattern_box_revealed
Global $pattern_loupe_vertical
Global $pattern_loupe_horizontal

Global $reward_special
Global $reward_special_collected
Global $reward_apples
Global $reward_gold
Global $reward_mana
Global $reward_apples_collected
Global $reward_gold_collected
Global $reward_mana_collected
Global $reward_dj_frag
Global $reward_book
Global $reward_hammer
Global $reward_loupe
Global $reward_diams
Global $reward_dust_blue
Global $reward_dust_green
Global $reward_tickets
Global $reward_event
Global $reward_event_collected
Global $reward_confirm_button

Global $treasure_nb_spe_rewards

Global $treasure_collect_apples
Global $treasure_collect_gold
Global $treasure_collect_mana
Global $treasure_collect_fraghero
Global $treasure_collect_dust
Global $treasure_collect_book
Global $treasure_collect_loupe
Global $treasure_collect_diams
Global $treasure_collect_tickets

Global $treasure_max_loupe_per_level
Global $treasure_nb_gold_collected_total
Global $treasure_nb_gold_collected_level
Global $treasure_stop_after_X_gold
Global $treasure_nb_apple_collected_total
Global $treasure_nb_apple_collected_level
Global $treasure_stop_after_X_apple
Global $treasure_nb_mana_collected_total
Global $treasure_nb_mana_collected_level
Global $treasure_stop_after_X_mana

Global $treasure_nb_box_hit

; Loupe patterns
Global $loupe_arrow_pattern_vertical_position[6][2][3] = [ _
  [[313,67, 1], [0, 0,    0]], _ ;Vertical bar (1 arrows)
  [[190,67, 1], [190,371, 1]], _ ;L shape (2 arrows)
  [[189,87, 1], [189,391, 1]], _ ;L shape vertical mirror (2 arrows)
  [[314,67, 1], [314,371, 1]], _ ;L shape horizontal mirror (2 arrows)
  [[313,87, 1], [313,391, 1]], _ ;L shape horizontal&vertical mirror (2 arrows)
  [[314,87, 1], [314,270, 1]] _ ;Horizontal bar (2 arrows)
]
Global $loupe_arrow_pattern_horizontal_position[6][2][3] = [ _
  [[228,275, 1], [410,275, 1]], _ ;Vertical bar (2 arrows)
  [[105,275, 1], [407,275, 1]], _ ;L shape (2 arrows)
  [[408,173, 1], [105,173, 1]], _ ;L shape vertical mirror (2 arrows)
  [[410,274, 1], [107,274, 1]], _ ;L shape horizontal mirror (2 arrows)
  [[107,171, 1], [410,171, 1]], _ ;L shape horizontal&vertical mirror (2 arrows)
  [[531,173, 1], [107,173, 1]] _  ;Horizontal bar (2 arrows)
]

Global $loupe_pattern_map_search[6][3][3] = [ _
  [[1,0,0], [1,0,0], [1,0,0]], _ ;Vertical bar (left col)
  [[0,1,0], [0,1,1], [0,0,0]], _ ;L shape (bottom left corner)
  [[0,0,0], [0,1,1], [0,1,0]], _ ;L shape vertical mirror (top left corner)
  [[0,1,0], [1,1,0], [0,0,0]], _ ;L shape horizontal mirror (bottom right corner)
  [[0,0,0], [1,1,0], [0,1,0]], _ ;L shape horizontal&vertical mirror (top right corner)
  [[1,1,1], [0,0,0], [0,0,0]]  _ ;Horizontal bar (top row)
]

; Initial position of the loupe pattern according to the map search
; => Use for click to move it
Global $loupe_pattern_init_position[6][2] = [ _
  [4,3], _ ;Vertical bar (top row)
  [2,3], _ ;L shape (bottom left corner)
  [2,2], _ ;L shape vertical mirror (top left corner)
  [3,3], _ ;L shape horizontal mirror (bottom right corner)
  [3,2], _ ;L shape horizontal&vertical mirror (top right corner)
  [3,3]  _ ;Horizontal bar (left col)
]
; Click position offset in the loupe pattern
Global $loupe_pattern_click_offset[6][2] = [ _
  [0,0], _ ;Vertical bar (top row)
  [1,1], _ ;L shape (bottom left corner)
  [1,1], _ ;L shape vertical mirror (top left corner)
  [1,1], _ ;L shape horizontal mirror (bottom right corner)
  [1,1], _ ;L shape horizontal&vertical mirror (top right corner)
  [0,0]  _ ;Horizontal bar (left col)
]


; Boxes Types:
; 0 (Hide) = Unrevealed box
; 1 (Revl) = Revealed box, but uncollected
; 10 (SRwd) = Special reward
; 11 (Gold) = Gold
; 12 (Mana) = Mana
; 13 (Appl) = Apples
; 14 (Hamm) = Hammer
; 15 (Loup) = Loupe
; 16 (Frag) = Fragment DJ
; 17 (Book) = Book
; 18 (Diam) = Diams
; 19 (Dust) = Dust
; 20 (Tckt) = Tickets
; 200 (Coll) = Collected box (undefined)
; 255 (Undf) = Not init
; 256 (Outm) = Out of the map (loupe search in map)

Global $treasure_map[5][4]
Global $treasure_reward_map[5][4]

Const $aRect_Fullmap_Treasure[4] = [$treasure_zone[0], $treasure_zone[1], $treasure_zone[2], $treasure_zone[3]]



Global $threshold = 0.8
Global $Watchdog_Restart = False


Func Treasure_Run()

	If initBlueStacks() Then
    _Function_Finished()
    Return
  EndIf

  $treasure_collect_apples   = (IniRead(@ScriptDir & "/H2C2.ini", "events", "treasureApples", "0") == "1")
  $treasure_collect_gold     = (IniRead(@ScriptDir & "/H2C2.ini", "events", "treasureGold", "0") == "1")
  $treasure_collect_mana     = (IniRead(@ScriptDir & "/H2C2.ini", "events", "treasureMana", "0") == "1")
  $treasure_collect_fraghero = (IniRead(@ScriptDir & "/H2C2.ini", "events", "treasureFragHero", "0") == "1")
  $treasure_collect_dust     = (IniRead(@ScriptDir & "/H2C2.ini", "events", "treasureDust", "0") == "1")
  $treasure_collect_book     = (IniRead(@ScriptDir & "/H2C2.ini", "events", "treasureBook", "0") == "1")
  $treasure_collect_loupe    = (IniRead(@ScriptDir & "/H2C2.ini", "events", "treasureLoupe", "0") == "1")
  $treasure_collect_diams    = (IniRead(@ScriptDir & "/H2C2.ini", "events", "treasureDiams", "0") == "1")
  $treasure_collect_tickets  = (IniRead(@ScriptDir & "/H2C2.ini", "events", "treasureTickets", "0") == "1")
  $treasure_max_loupe_per_level = IniRead(@ScriptDir & "/H2C2.ini", "events", "treasureMaxLoupe", "6")
  $treasure_min_box_per_loupe = IniRead(@ScriptDir & "/H2C2.ini", "events", "treasureMinBoxPerLoupe", "3")
  $treasure_stop_after_X_gold = IniRead(@ScriptDir & "/H2C2.ini", "events", "treasureStopMaxGold", "12")
  $treasure_stop_after_X_apple = IniRead(@ScriptDir & "/H2C2.ini", "events", "treasureStopMaxApple", "12")
  $treasure_stop_after_X_mana = IniRead(@ScriptDir & "/H2C2.ini", "events", "treasureStopMaxMana", "10")
  $treasure_nb_gold_collected_total = 0
  $treasure_nb_apple_collected_total = 0
  $treasure_nb_mana_collected_total = 0
  $treasure_nb_gold_collected_level = 0
  $treasure_nb_apple_collected_level = 0
  $treasure_nb_mana_collected_level = 0
  $treasure_nb_box_hit = 0


  $pattern_box_unrevealed = _OpenCV_imread_and_check(_OpenCV_FindFile("treasure_box_unrevealed.PNG"))
  $pattern_box_revealed = _OpenCV_imread_and_check(_OpenCV_FindFile("treasure_box_revealed.PNG"))
  $pattern_loupe_vertical = _OpenCV_imread_and_check(_OpenCV_FindFile("treasure_loupe_arrow_vertical.png"))
  $pattern_loupe_horizontal = _OpenCV_imread_and_check(_OpenCV_FindFile("treasure_loupe_arrow_horizontal.png"))

  $reward_special = _OpenCV_imread_and_check(_OpenCV_FindFile("treasure_special_reward.PNG"))
  $reward_special_collected = _OpenCV_imread_and_check(_OpenCV_FindFile("treasure_special_reward_collected.PNG"))
  $reward_apples  = _OpenCV_imread_and_check(_OpenCV_FindFile("treasure_apples.PNG"))
  $reward_apples_collected  = _OpenCV_imread_and_check(_OpenCV_FindFile("treasure_apples_collected.PNG"))
  $reward_gold    = _OpenCV_imread_and_check(_OpenCV_FindFile("treasure_gold.PNG"))
  $reward_gold_collected    = _OpenCV_imread_and_check(_OpenCV_FindFile("treasure_gold_collected.PNG"))
  $reward_mana    = _OpenCV_imread_and_check(_OpenCV_FindFile("treasure_mana.PNG"))
  $reward_mana_collected    = _OpenCV_imread_and_check(_OpenCV_FindFile("treasure_mana_collected.PNG"))
  $reward_dj_frag = _OpenCV_imread_and_check(_OpenCV_FindFile("treasure_frag_dj.PNG"))
  $reward_book    = _OpenCV_imread_and_check(_OpenCV_FindFile("treasure_book.PNG"))
  $reward_hammer  = _OpenCV_imread_and_check(_OpenCV_FindFile("treasure_hammer.PNG"))
  $reward_loupe   = _OpenCV_imread_and_check(_OpenCV_FindFile("treasure_loupe.PNG"))
  $reward_diams   = _OpenCV_imread_and_check(_OpenCV_FindFile("treasure_diams.PNG"))
  $reward_dust_blue  = _OpenCV_imread_and_check(_OpenCV_FindFile("treasure_dust_blue.PNG"))
  $reward_dust_green = _OpenCV_imread_and_check(_OpenCV_FindFile("treasure_dust_green.PNG"))
  $reward_tickets = _OpenCV_imread_and_check(_OpenCV_FindFile("treasure_tickets.PNG"))
  $reward_event   = _OpenCV_imread_and_check(_OpenCV_FindFile("treasure_event_reward.PNG"))
  $reward_event_collected   = _OpenCV_imread_and_check(_OpenCV_FindFile("treasure_event_reward_collected.PNG"))

  $reward_confirm_button   = _OpenCV_imread_and_check(_OpenCV_FindFile("treasure_reward_confirm_button.PNG"))

  If FindImage("treasure_no_pass.PNG") Then
    $Treasure_With_Pass = False
  Else
    $Treasure_With_Pass = True
  EndIf
  If $Treasure_With_Pass == False Then
    $treasure_nb_spe_rewards = 1
    _Generic_log_INFO( "Treasure_Run: No pass detected => 1 reward per table" & @CRLF)
  ElseIf FindImage("treasure_silver_pass.PNG") Then
    $treasure_nb_spe_rewards = 2
    _Generic_log_INFO( "Treasure_Run: Silver pass detected => 2 rewards per table" & @CRLF)
  Else
    $treasure_nb_spe_rewards = 3
    _Generic_log_INFO( "Treasure_Run: Gold / Diamond pass detected => 3 rewards per table" & @CRLF)
  EndIf

  Do
    $Watchdog_Restart = False

    Do
      ; Init map content
      For $i = 0 To 4
        For $j = 0 To 3
          $treasure_map[$i][$j] = 255
        Next
      Next
      Treasure_Init_Map()
      ;Treasure_Print_Map()

      ; Check that we have a pass
      If $Treasure_With_Pass Then
        ; Reveal max number of items
        $nb_loupe_used = 0
        $min_to_reveal = 3
        Do
          $Result = Treasure_Loupe_Fit_Search($min_to_reveal)
          If $Result Then
            ; Decrement the expected number of box to reveal
            $min_to_reveal = $min_to_reveal - 1
          Else
            $nb_loupe_used = $nb_loupe_used + 1
          EndIf
          If _Interrupt_Sleep(200) Then
            return True
          EndIf
        Until $Result and $min_to_reveal < $treasure_min_box_per_loupe or $nb_loupe_used >= $treasure_max_loupe_per_level
      EndIf

      ; Collect unrevealed items until event & special reward collected
      $nb_hit_box = 0
      If Treasure_Map_Find_Event_And_Special_Rewards($nb_hit_box) Then
        ExitLoop
      EndIf

      ; Collect desired revealed items
      If Treasure_Map_Collect_Rewards() Then
        ExitLoop
      EndIf

      ; Move to next level
      $color = PixelGetColor(1420, 315)
      If $color == 0xFAB806 Then
        _Generic_log_INFO( "Treasure_Run: Move to next floor !!!" & @CRLF)
        
        ; Increment the total reward counts with level count
        $treasure_nb_gold_collected_total = $treasure_nb_gold_collected_total + $treasure_nb_gold_collected_level
        $treasure_nb_mana_collected_total = $treasure_nb_mana_collected_total + $treasure_nb_mana_collected_level
        $treasure_nb_apple_collected_total = $treasure_nb_apple_collected_total + $treasure_nb_apple_collected_level
        
        ; Update stats
        _Generic_log_INFO( "TreasureHunt: Hit=" & $treasure_nb_box_hit & ", gold=" & $treasure_nb_gold_collected_total & ", apple=" & $treasure_nb_apple_collected_total & ", mana=" & $treasure_nb_mana_collected_total & @CRLF)
        PrintGUIStatus( "TreasureHunt: Hit=" & $treasure_nb_box_hit & ", gold=" & $treasure_nb_gold_collected_total & ", apple=" & $treasure_nb_apple_collected_total & ", mana=" & $treasure_nb_mana_collected_total & @CRLF)

        ; Click button
        MouseClick("primary", 1420, 315, 1, 2)
        If _Interrupt_Sleep(200) Then
          Return True
        EndIf

        ; Click confirm button
        MouseClick("primary", 630, 645, 1, 2)
        If _Interrupt_Sleep(2500) Then
          Return True
        EndIf

        If $Treasure_With_Pass Then
          ;dummy click to loupe popup
          MouseClick("primary", 780, 650, 1)
          If _Interrupt_Sleep(200) Then
            Return true
          Endif
        EndIf
      Else
        _Generic_log_Error( "Treasure_Run: Can't move to next level !!!" & @CRLF)
        Return True
      EndIf
      
    ; TODO update that
    Until( False )

  Until( $Watchdog_Restart == False )

EndFunc


Func Treasure_Init_Map()

  ;opencv screenshot
	Local $aRect[4];
  ; Full map search
  $aRect = $aRect_Fullmap_Treasure

	Local $img = _OpenCV_GetDesktopScreenMat($aRect)

	; Find unrevealed box in map
	Local $aMatches = _OpenCV_FindTemplate($img, $pattern_box_unrevealed, $threshold)
  $nb_unrevealed = UBound($aMatches)
  _Generic_log_TRACE( "Treasure_Init_Map: Found " & $nb_unrevealed & " patterns for UNREVEALED box type." & @CRLF)
  If $nb_unrevealed Then
    For $i = 0 To $nb_unrevealed-1
      $x_map_pos = -1
      $y_map_pos = -1

      If Treasure_Get_Box_Position($aMatches[$i][0], $aMatches[$i][1], $x_map_pos, $y_map_pos) Then
        ; Log color found
        $x_coord = $aRect[0] + $aMatches[$i][0]
        $y_coord = $aRect[1] + $aMatches[$i][1]
        $color = PixelGetColor( $x_coord, $y_coord)
        ;_Generic_log_TRACE( "Treasure_Init_Map: (" & $i & ") Found color=0x" & hex($color, 6) & " at x="& $x_coord & ", y=" & $y_coord & @CRLF)

        ; 0 (Hide) = Unrevealed box
        $box_code = 0

        ; Register found box pattern in map
        $treasure_map[$x_map_pos][$y_map_pos] = $box_code ; Full map
      Else
        ; Error
        _Generic_log_ERROR( "Treasure_Init_Map: (" & $i & ") Treasure_Get_Box_Position error, x_map="& $x_map_pos & ", y_map=" & $y_map_pos & ", at x="& $x_coord & ", y=" & $y_coord & @CRLF)
      EndIf
    Next
  EndIf

	; Find revealed box (but uncollected) in map
	Local $aMatches = _OpenCV_FindTemplate($img, $pattern_box_revealed, $threshold)
  $nb_revealed = UBound($aMatches)
  _Generic_log_TRACE( "Treasure_Init_Map: Found " & $nb_revealed& " patterns for REVEALED box type." & @CRLF)
  If $nb_revealed Then
    For $i = 0 To $nb_revealed-1
      $x_map_pos = -1
      $y_map_pos = -1

      If Treasure_Get_Box_Position($aMatches[$i][0], $aMatches[$i][1], $x_map_pos, $y_map_pos) Then
        ; Log color found
        $x_coord = $aRect[0] + $aMatches[$i][0]
        $y_coord = $aRect[1] + $aMatches[$i][1]
        $color = PixelGetColor( $x_coord, $y_coord)
        ;_Generic_log_TRACE( "Treasure_Init_Map: (" & $i & ") Found color=0x" & hex($color, 6) & " at x="& $x_coord & ", y=" & $y_coord & @CRLF)

        ; 1 (Revl) = Revealed but uncollected
        $box_code = 1

        ; Register found box pattern in map
        $treasure_map[$x_map_pos][$y_map_pos] = $box_code ; Full map
      Else
        ; Error
        _Generic_log_ERROR( "Treasure_Init_Map: (" & $i & ") Treasure_Get_Box_Position error, x_map="& $x_map_pos & ", y_map=" & $y_map_pos & ", at x="& $x_coord & ", y=" & $y_coord & @CRLF)
      EndIf
    Next
  EndIf

  ; All remaining unidentified box being are considered as collected
  _Generic_log_TRACE( "Treasure_Init_Map: Found " & (5*4 - $nb_unrevealed - $nb_revealed) & " patterns for COLLECTED box type." & @CRLF)
  For $i = 0 To 4
    For $j = 0 To 3
      If $treasure_map[$i][$j] == 255 Then
        ; 200 (Coll) = Collected box (undefined)
        $treasure_map[$i][$j] = 200
      EndIf
    Next
  Next

	Return False
EndFunc

Func Treasure_Loupe_Fit_Search($min_to_reveal)

	Global $hwnd
	Local $aRect = WinGetPos($hwnd)

  Local $treasure_map_extended[5+2*2][4+2*2]

  ; Init search pattern extended map (2 row / col on each side)
  For $i = 0 to 4+2*2
    For $j = 0 to 3+2*2
      ; 256 (Outm) = Out of the map (loupe search in map)
      $treasure_map_extended[$i][$j] = 256;
    Next
  Next

  ; Copy treasure map into search map
  For $i = 0 to 4
    For $j = 0 to 3
      $treasure_map_extended[$i+2][$j+2] = $treasure_map[$i][$j]
    Next
  Next

  ; TEMP Debug
  ;Treasure_Print_SearchMap($treasure_map_extended)

  ; Open loupe search mode
  MouseClick("primary", $treasure_loupe_enable_button[0], $treasure_loupe_enable_button[1], 1, 1)
  If _Interrupt_Sleep(200) Then
    return True
  EndIf

  ; Detect loupe pattern type
  $loupe_pattern_type = 0
  $Result = Treasure_Loupe_TypeDetect($loupe_pattern_type)
  If $Result Then
    ; Error detecting the loupe pattern
    _Generic_log_Error( "Treasure_Loupe_Fit_Search: Unable to detect the loupe pattern." & @CRLF)
    return True
  Else
    _Generic_log_Trace( "Treasure_Loupe_Fit_Search: Loupe pattern type = "& $loupe_pattern_type & @CRLF)
  EndIf

  ; TEMP Debug
  ;Treasure_Print_LoupePattern($loupe_pattern_type)

  ; Search for best fit
  $best_score = 0
  $best_pos_x = 0
  $best_pos_y = 0
  $score = 0

  If $loupe_pattern_type == 1 Then
    ; Bottom left to top right
    ;L shape (bottom left corner)
    $loop_xstart = 0
    $loop_ystart = 3+2
    $loop_xend = 4+2
    $loop_yend = 0
    $loop_xstep = 1
    $loop_ystep = -1
  ElseIf $loupe_pattern_type == 3 Then
    ; Bottom right to top left
    ;L shape horizontal mirror (bottom right corner)
    $loop_xstart = 4+2
    $loop_ystart = 3+2
    $loop_xend = 0
    $loop_yend = 0
    $loop_xstep = -1
    $loop_ystep = -1
  ElseIf $loupe_pattern_type == 4 Then
    ; Top right to Bottom left
    ;L shape horizontal&vertical mirror (top right corner)
    $loop_xstart = 4+2
    $loop_ystart = 0
    $loop_xend = 0
    $loop_yend = 3+2
    $loop_xstep = -1
    $loop_ystep = 1
  Else
    ; Default search -> Top left to bottom right // vertically then horizontal
    $loop_xstart = 0
    $loop_ystart = 0
    $loop_xend = 4+2
    $loop_yend = 3+2
    $loop_xstep = 1
    $loop_ystep = 1
  EndIf

  For $i = $loop_xstart to $loop_xend step $loop_xstep
    For $j = $loop_ystart to $loop_yend step $loop_ystep
      $Result = Treasure_Loupe_Fit_Check($i, $j, $treasure_map_extended, $loupe_pattern_type, $score, $min_to_reveal)
      If $Result Then
        ; Error checking fit of the pattern
        _Generic_log_Error( "Treasure_Loupe_Fit_Search: Unable to check fit at position=[" & $i & "," & $j &"]" & @CRLF)
        return True
      ElseIf $best_score < $score Then
        $best_score = $score
        $best_pos_x = $i
        $best_pos_y = $j
        _Generic_log_Trace( "Treasure_Loupe_Fit_Search: New best score (" & $score & ") in position=[" & $i & "," & $j &"]" & @CRLF)
      EndIf
    Next
  Next

  ; Check best score is acceptable to use the loupe
  If $best_score >= 3*$min_to_reveal Then
    ; Move loupe pattern to best fit position
    $delta_x = $best_pos_x - $loupe_pattern_init_position[$loupe_pattern_type][0]
    $delta_y = $best_pos_y - $loupe_pattern_init_position[$loupe_pattern_type][1]
    ;_Generic_log_Trace( "Treasure_Loupe_Fit_Search: Move loupe pattern delta=[" & $delta_x & ","& $delta_y &"]" & @CRLF)

    ; Mouse click loupe pattern to best fit position
    $click_start_x_pos = $loupe_pattern_init_position[$loupe_pattern_type][0] + $loupe_pattern_click_offset[$loupe_pattern_type][0]
    $click_start_y_pos = $loupe_pattern_init_position[$loupe_pattern_type][1] + $loupe_pattern_click_offset[$loupe_pattern_type][1]
    $click_final_x_pos = $click_start_x_pos + $delta_x
    $click_final_y_pos = $click_start_y_pos + $delta_y
    ;_Generic_log_Trace( "Treasure_Loupe_Fit_Search: Move loupe pattern start_pos=[" & $click_start_x_pos & ","& $click_start_y_pos &"], final_pos=["& $click_final_x_pos & ","& $click_final_y_pos &"]" & @CRLF)

    ; Move it
    $click_start_x_coord = Int($treasure_zone[0] + $treasure_box_size[0] * ( $click_start_x_pos - 2 + 0.5))
    $click_start_y_coord = Int($treasure_zone[1] + $treasure_box_size[1] * ( $click_start_y_pos - 2 + 0.5))
    $click_final_x_coord = Int($treasure_zone[0] + $treasure_box_size[0] * ( $click_final_x_pos - 2 + 0.5))
    $click_final_y_coord = 10 + Int($treasure_zone[1] + $treasure_box_size[1] * ( $click_final_y_pos - 2 + 0.5))
    If $delta_x < 0 Then
      ; Final destination on left side of the loupe pattern
      $click_final_x_coord = $click_final_x_coord - Int($treasure_box_size[0] * 0.40)
    Else
      ; Final destination on right side of the loupe pattern
      $click_final_x_coord = $click_final_x_coord + Int($treasure_box_size[0] * 0.40)
    EndIf
    If $delta_y < 0 Then
      ; Final destination on top side of the loupe pattern
      $click_final_y_coord = $click_final_y_coord - Int($treasure_box_size[1] * 0.40)
    Else
      ; Final destination on bottom side of the loupe pattern
      $click_final_y_coord = $click_final_y_coord + Int($treasure_box_size[1] * 0.40)
    EndIf
    MouseClickDrag("primary", $click_start_x_coord, $click_start_y_coord, $click_final_x_coord, $click_final_y_coord, 5)
    ;_Generic_log_Trace( "Treasure_Loupe_Fit_Search: Move loupe pattern mouse start_pos=[" & $click_start_x_coord & ","& $click_start_y_coord &"], final_pos=["& $click_final_x_coord & ","& $click_final_y_coord &"]" & @CRLF)

    ; To escape if bot take shitty decision
    If _Interrupt_Sleep(100) Then
      return True
    EndIf

    ; Validate
    $click_x = 0
    $click_y = 0
    FindImageCoord($hwnd, "treasure_loupe_validate.png", $click_x, $click_y)
    MouseClick("primary", $click_x, $click_y, 1, 1)

    ; Update map with revealed boxed
    For $i = 0 to 2
      For $j = 0 to 2
        If $loupe_pattern_map_search[$loupe_pattern_type][$j][$i] == 1 Then
          $treasure_map[$best_pos_x+$i-2][$best_pos_y+$j-2] = 1 ; 1 (Revl) = Revealed but uncollected
        EndIf
      Next
    Next

    ; TEMP Debug
    ;Treasure_Print_Map()

  Else
    ; Click on red cross and stop using the loupe
    ; Cancel
    $click_x = 0
    $click_y = 0
    FindImageCoord($hwnd, "treasure_loupe_cancel.png", $click_x, $click_y)
    MouseClick("primary", $click_x, $click_y, 1, 1)

    Return True
  EndIf

  Return False

EndFunc

Func Treasure_Loupe_Fit_Check($center_x, $center_y, ByRef $search_map, ByRef $loupe_pattern_id, ByRef $score, $min_to_reveal)
  ; Init score
  $score = 0
  $box_to_reveal = 0
  $box_already_revealed = 0
  $box_out_map = 0
  $box_border_out_map = 0

  ; Count relevent box types under the loupe pattern
  For $i = 0 to 2
    For $j = 0 to 2
      If $loupe_pattern_map_search[$loupe_pattern_id][$j][$i] == 1 Then
        ; Loupe pattern will reveal the box
        Switch $search_map[$center_x+$i][$center_y+$j]
          Case 0 ; 0 (Hide) = Unrevealed box
            $box_to_reveal = $box_to_reveal + 1
          Case 256 ; 256 (Outm) = Out of the map (loupe search in map)
            $box_out_map = $box_out_map + 1
          Case Else ; Other kind of box
            $box_already_revealed = $box_already_revealed + 1
        EndSwitch
      Else
        ; Loupe pattern don't reveal the box
        If $search_map[$center_x+$i][$center_y+$j] == 256 Then
          ; 256 (Outm) = Out of the map (loupe search in map)
          $box_border_out_map = $box_border_out_map + 1
        ElseIf $search_map[$center_x+$i][$center_y+$j] == 1 Then
          ; 1 (Revl) = Revealed box, but uncollected
          $box_border_out_map = $box_border_out_map + 1
        ElseIf $search_map[$center_x+$i][$center_y+$j] == 200 Then
          ; 200 (Coll) = Collected box (undefined)
          $box_border_out_map = $box_border_out_map + 1
        EndIf
      EndIf
    Next
  Next

  ; Compute score
  $score = Int(3 * $box_to_reveal + $box_border_out_map * 0.5)
  If $box_out_map > 0 or $box_to_reveal < $min_to_reveal Then
    $score = 0
  EndIf

  ;_Generic_log_TRACE( "Treasure_Loupe_Fit_Check: Score=" & $score & " for search_map position=["& $center_x &","& $center_y &"], (Hidden="& $box_to_reveal &", Revealed="& $box_already_revealed &", Border="& $box_border_out_map &", Out="& $box_out_map &")." & @CRLF)

  Return False
EndFunc

Func Treasure_Loupe_TypeDetect(ByRef $loupe_id)

  ;opencv screenshot
	Local $aRect[4];
  ; Full map search
  $aRect = $aRect_Fullmap_Treasure

	Local $img = _OpenCV_GetDesktopScreenMat($aRect)

	; Find loupe move arrow position in map to detect loupe pattern type vertical
	Local $aMatches = _OpenCV_FindTemplate($img, $pattern_loupe_vertical, 0.85)
  _Generic_log_TRACE( "Treasure_Loupe_TypeDetect: Found " & UBound($aMatches)& " vertical patterns." & @CRLF)

  $found = False
  $type = 0
  $nb_patterns = UBound($aMatches)
  If $nb_patterns Then
    For $i = 0 To $nb_patterns-1
      _Generic_log_TRACE( "Treasure_Loupe_TypeDetect: vertical arrow position=["&$aMatches[$i][0]&","&$aMatches[$i][1]&"]" & @CRLF)
    Next
    If $nb_patterns == 1 Then
      ; Vertical bar case
      $found = True
      $loupe_id = 0
    ElseIf $nb_patterns == 2 Then
      ; Other cases
      For $idx = 1 To 5
        ; Look if pattern #1 position fit with table position #1
        If ($loupe_arrow_pattern_vertical_position[$idx][0][0]-5 <= $aMatches[0][0] and $loupe_arrow_pattern_vertical_position[$idx][0][0]+5 >= $aMatches[0][0]) and _
           ($loupe_arrow_pattern_vertical_position[$idx][0][1]-5 <= $aMatches[0][1] and $loupe_arrow_pattern_vertical_position[$idx][0][1]+5 >= $aMatches[0][1]) Then
          $test_p1_pos1 = 1
        Else
          $test_p1_pos1 = 0
        EndIf
        ; Look if pattern #1 position fit with table position #2
        If ($loupe_arrow_pattern_vertical_position[$idx][1][0]-5 <= $aMatches[0][0] and $loupe_arrow_pattern_vertical_position[$idx][1][0]+5 >= $aMatches[0][0]) and _
           ($loupe_arrow_pattern_vertical_position[$idx][1][1]-5 <= $aMatches[0][1] and $loupe_arrow_pattern_vertical_position[$idx][1][1]+5 >= $aMatches[0][1]) Then
          $test_p1_pos2 = 1
        Else
          $test_p1_pos2 = 0
        EndIf
        ; Look if pattern #2 position fit with table position #1
        If ($loupe_arrow_pattern_vertical_position[$idx][0][0]-5 <= $aMatches[1][0] and $loupe_arrow_pattern_vertical_position[$idx][0][0]+5 >= $aMatches[1][0]) and _
           ($loupe_arrow_pattern_vertical_position[$idx][0][1]-5 <= $aMatches[1][1] and $loupe_arrow_pattern_vertical_position[$idx][0][1]+5 >= $aMatches[1][1]) Then
          $test_p2_pos1 = 1
        Else
          $test_p2_pos1 = 0
        EndIf
        ; Look if pattern #2 position fit with table position #2
        If ($loupe_arrow_pattern_vertical_position[$idx][1][0]-5 <= $aMatches[1][0] and $loupe_arrow_pattern_vertical_position[$idx][1][0]+5 >= $aMatches[1][0]) and _
           ($loupe_arrow_pattern_vertical_position[$idx][1][1]-5 <= $aMatches[1][1] and $loupe_arrow_pattern_vertical_position[$idx][1][1]+5 >= $aMatches[1][1]) Then
          $test_p2_pos2 = 1
        Else
          $test_p2_pos2 = 0
        EndIf

        ; Check we have detected them !
        If $test_p1_pos1 and $test_p2_pos2 Then
          $found = True
          $loupe_id = $idx
        ElseIf $test_p2_pos1 and $test_p1_pos2 Then
          $found = True
          $loupe_id = $idx
        EndIf
        If $found Then
          ExitLoop
        EndIf
      Next
    Else
      ; Unsupported case
      _Generic_log_ERROR( "Treasure_Loupe_TypeDetect: Unsupported case " & $nb_patterns & " vertical pattern detected !" & @CRLF)
    EndIf

    If $found == False Then
      _Generic_log_ERROR( "Treasure_Loupe_TypeDetect: Shape not detected" & @CRLF)
    Else
      If $loupe_id == 1 Then
        _Generic_log_TRACE( "Treasure_Loupe_TypeDetect: L shape detected" & @CRLF)
      ElseIf $loupe_id == 2 Then
        _Generic_log_TRACE( "Treasure_Loupe_TypeDetect: L shape vertical mirror detected" & @CRLF)
      ElseIf $loupe_id == 3 Then
        _Generic_log_TRACE( "Treasure_Loupe_TypeDetect: L shape horizontal mirror detected" & @CRLF)
      ElseIf $loupe_id == 4 Then
        _Generic_log_TRACE( "Treasure_Loupe_TypeDetect: L shape horizontal & vertical mirror detected" & @CRLF)
      ElseIf $loupe_id == 0 Then
        _Generic_log_TRACE( "Treasure_Loupe_TypeDetect: Vertical bar detected" & @CRLF)
      ElseIf $loupe_id == 5 Then
        _Generic_log_TRACE( "Treasure_Loupe_TypeDetect: Horizontal bar detected" & @CRLF)
      Else
       ; Not possible
      EndIf
    EndIf
  EndIf


  ; Find loupe move arrow position in map to detect loupe pattern type horizontal
	;$aMatches = _OpenCV_FindTemplate($img, $pattern_loupe_horizontal, 0.80)
  ;_Generic_log_TRACE( "Treasure_Loupe_TypeDetect: Found " & UBound($aMatches)& " horizontal patterns." & @CRLF)
  ;If UBound($aMatches) Then
  ;  For $i = 0 To UBound($aMatches)-1
  ;    $x_map_pos = -1
  ;    $y_map_pos = -1
  ;
  ;    If Treasure_Get_Box_Position($aMatches[$i][0], $aMatches[$i][1], $x_map_pos, $y_map_pos) Then
  ;      ; Log color found
  ;      $x_coord = $aRect[0] + $aMatches[$i][0]
  ;      $y_coord = $aRect[1] + $aMatches[$i][1]
  ;      $color = PixelGetColor( $x_coord, $y_coord)
  ;      _Generic_log_TRACE( "Treasure_Loupe_TypeDetect: (" & $i & ") Found color=0x" & hex($color, 6) & " at x="& $x_coord & ", y=" & $y_coord & @CRLF)
  ;
  ;      ; 0 (Hide) = Unrevealed box
  ;      ;$box_code = 0
  ;
  ;      ; Register found box pattern in map
  ;      ;$treasure_map[$x_map_pos][$y_map_pos] = $box_code ; Full map
  ;    Else
  ;      ; Error
  ;      _Generic_log_ERROR( "Treasure_Loupe_TypeDetect: (" & $i & ") Treasure_Get_Box_Position error, x_map="& $x_map_pos & ", y_map=" & $y_map_pos & ", at x="& $x_coord & ", y=" & $y_coord & @CRLF)
  ;    EndIf
  ;  Next
  ;EndIf

  Return not $found
EndFunc

Func Treasure_Map_Update_Reward_Table(ByRef $nb_rwd_event, ByRef $nb_special_rwd)

  ; Init table
  For $i = 0 To 4
    For $j = 0 To 3
      ; 255 (Undf) = Not init
      $treasure_reward_map[$i][$j] = 255
    Next
  Next

  ;opencv screenshot
	Local $aRect[4];
  ; Full map search
  $aRect = $aRect_Fullmap_Treasure

	Local $img = _OpenCV_GetDesktopScreenMat($aRect)

	; Find SPECIAL REWARD
  ; Type #1 picture
	Local $aMatches = _OpenCV_FindTemplate($img, $reward_special, $threshold)
  _Generic_log_TRACE( "Treasure_Map_Update_Reward_Table: Found " & UBound($aMatches) & " patterns for SPECIAL REWARD box type." & @CRLF)
  If UBound($aMatches) Then
    For $i = 0 To UBound($aMatches)-1
      $x_map_pos = -1
      $y_map_pos = -1

      If Treasure_Get_Box_Position($aMatches[$i][0], $aMatches[$i][1], $x_map_pos, $y_map_pos) Then
        ; 10 (SRwd) = Special reward
        $box_code = 10

        ; Register found box pattern in map
        $treasure_reward_map[$x_map_pos][$y_map_pos] = $box_code ; Full map
      Else
        ; Error
        _Generic_log_ERROR( "Treasure_Map_Update_Reward_Table: (" & $i & ") Treasure_Get_Box_Position error, x_map="& $x_map_pos & ", y_map=" & $y_map_pos & @CRLF)
      EndIf
    Next
  EndIf
  $nb_special_rwd = UBound($aMatches)

  ; Type #2 picture
  $aMatches = _OpenCV_FindTemplate($img, $reward_special_collected, $threshold)
  _Generic_log_TRACE( "Treasure_Map_Update_Reward_Table: Found " & UBound($aMatches) & " patterns for SPECIAL REWARD Collected box type." & @CRLF)
  If UBound($aMatches) Then
    For $i = 0 To UBound($aMatches)-1
      $x_map_pos = -1
      $y_map_pos = -1

      If Treasure_Get_Box_Position($aMatches[$i][0], $aMatches[$i][1], $x_map_pos, $y_map_pos) Then
        ; 10 (SRwd) = Special reward
        $box_code = 10

        ; Register found box pattern in map
        $treasure_reward_map[$x_map_pos][$y_map_pos] = $box_code ; Full map
      Else
        ; Error
        _Generic_log_ERROR( "Treasure_Map_Update_Reward_Table: (" & $i & ") Treasure_Get_Box_Position error, x_map="& $x_map_pos & ", y_map=" & $y_map_pos & @CRLF)
      EndIf
    Next
  EndIf
  $nb_special_rwd = $nb_special_rwd + UBound($aMatches)

  ; Find GOLD
  ; Type #1 picture
  $aMatches = _OpenCV_FindTemplate($img, $reward_gold, $threshold)
  _Generic_log_TRACE( "Treasure_Map_Update_Reward_Table: Found " & UBound($aMatches) & " patterns for GOLD box type." & @CRLF)
  If UBound($aMatches) Then
    For $i = 0 To UBound($aMatches)-1
      $x_map_pos = -1
      $y_map_pos = -1

      If Treasure_Get_Box_Position($aMatches[$i][0], $aMatches[$i][1], $x_map_pos, $y_map_pos) Then
        ; 11 (Gold) = Gold
        $box_code = 11

        ; Register found box pattern in map
        $treasure_reward_map[$x_map_pos][$y_map_pos] = $box_code ; Full map
      Else
        ; Error
        _Generic_log_ERROR( "Treasure_Map_Update_Reward_Table: (" & $i & ") Treasure_Get_Box_Position error, x_map="& $x_map_pos & ", y_map=" & $y_map_pos & @CRLF)
      EndIf
    Next
  EndIf
  
  ; Type #2 picture
  $aMatches = _OpenCV_FindTemplate($img, $reward_gold_collected, $threshold)
  _Generic_log_TRACE( "Treasure_Map_Update_Reward_Table: Found " & UBound($aMatches) & " patterns for GOLD (Collected) box type." & @CRLF)
  If UBound($aMatches) Then
    For $i = 0 To UBound($aMatches)-1
      $x_map_pos = -1
      $y_map_pos = -1

      If Treasure_Get_Box_Position($aMatches[$i][0], $aMatches[$i][1], $x_map_pos, $y_map_pos) Then
        ; 11 (Gold) = Gold
        $box_code = 11

        ; Register found box pattern in map
        $treasure_reward_map[$x_map_pos][$y_map_pos] = $box_code ; Full map
      Else
        ; Error
        _Generic_log_ERROR( "Treasure_Map_Update_Reward_Table: (" & $i & ") Treasure_Get_Box_Position error, x_map="& $x_map_pos & ", y_map=" & $y_map_pos & @CRLF)
      EndIf
    Next
  EndIf

  ; Find MANA
  ; Type #1 picture
  $aMatches = _OpenCV_FindTemplate($img, $reward_mana, $threshold)
  _Generic_log_TRACE( "Treasure_Map_Update_Reward_Table: Found " & UBound($aMatches) & " patterns for MANA box type." & @CRLF)
  If UBound($aMatches) Then
    For $i = 0 To UBound($aMatches)-1
      $x_map_pos = -1
      $y_map_pos = -1

      If Treasure_Get_Box_Position($aMatches[$i][0], $aMatches[$i][1], $x_map_pos, $y_map_pos) Then
        ; 12 (Mana) = Mana
        $box_code = 12

        ; Register found box pattern in map
        $treasure_reward_map[$x_map_pos][$y_map_pos] = $box_code ; Full map
      Else
        ; Error
        _Generic_log_ERROR( "Treasure_Map_Update_Reward_Table: (" & $i & ") Treasure_Get_Box_Position error, x_map="& $x_map_pos & ", y_map=" & $y_map_pos & @CRLF)
      EndIf
    Next
  EndIf
  
  ; Type #2 picture
  $aMatches = _OpenCV_FindTemplate($img, $reward_mana_collected, $threshold)
  _Generic_log_TRACE( "Treasure_Map_Update_Reward_Table: Found " & UBound($aMatches) & " patterns for MANA (Collected) box type." & @CRLF)
  If UBound($aMatches) Then
    For $i = 0 To UBound($aMatches)-1
      $x_map_pos = -1
      $y_map_pos = -1

      If Treasure_Get_Box_Position($aMatches[$i][0], $aMatches[$i][1], $x_map_pos, $y_map_pos) Then
        ; 12 (Mana) = Mana
        $box_code = 12

        ; Register found box pattern in map
        $treasure_reward_map[$x_map_pos][$y_map_pos] = $box_code ; Full map
      Else
        ; Error
        _Generic_log_ERROR( "Treasure_Map_Update_Reward_Table: (" & $i & ") Treasure_Get_Box_Position error, x_map="& $x_map_pos & ", y_map=" & $y_map_pos & @CRLF)
      EndIf
    Next
  EndIf

  ; Find APPLES
  ; Type #1 picture
  $aMatches = _OpenCV_FindTemplate($img, $reward_apples, $threshold)
  _Generic_log_TRACE( "Treasure_Map_Update_Reward_Table: Found " & UBound($aMatches) & " patterns for APPLES box type." & @CRLF)
  If UBound($aMatches) Then
    For $i = 0 To UBound($aMatches)-1
      $x_map_pos = -1
      $y_map_pos = -1

      If Treasure_Get_Box_Position($aMatches[$i][0], $aMatches[$i][1], $x_map_pos, $y_map_pos) Then
        ; 13 (Appl) = Apples
        $box_code = 13

        ; Register found box pattern in map
        $treasure_reward_map[$x_map_pos][$y_map_pos] = $box_code ; Full map
      Else
        ; Error
        _Generic_log_ERROR( "Treasure_Map_Update_Reward_Table: (" & $i & ") Treasure_Get_Box_Position error, x_map="& $x_map_pos & ", y_map=" & $y_map_pos & @CRLF)
      EndIf
    Next
  EndIf
  
  ; Type #2 picture
  $aMatches = _OpenCV_FindTemplate($img, $reward_apples_collected, $threshold)
  _Generic_log_TRACE( "Treasure_Map_Update_Reward_Table: Found " & UBound($aMatches) & " patterns for APPLES (Collected) box type." & @CRLF)
  If UBound($aMatches) Then
    For $i = 0 To UBound($aMatches)-1
      $x_map_pos = -1
      $y_map_pos = -1

      If Treasure_Get_Box_Position($aMatches[$i][0], $aMatches[$i][1], $x_map_pos, $y_map_pos) Then
        ; 13 (Appl) = Apples
        $box_code = 13

        ; Register found box pattern in map
        $treasure_reward_map[$x_map_pos][$y_map_pos] = $box_code ; Full map
      Else
        ; Error
        _Generic_log_ERROR( "Treasure_Map_Update_Reward_Table: (" & $i & ") Treasure_Get_Box_Position error, x_map="& $x_map_pos & ", y_map=" & $y_map_pos & @CRLF)
      EndIf
    Next
  EndIf

  ; Find HAMMER
  $aMatches = _OpenCV_FindTemplate($img, $reward_hammer, $threshold)
  _Generic_log_TRACE( "Treasure_Map_Update_Reward_Table: Found " & UBound($aMatches) & " patterns for HAMMER box type." & @CRLF)
  If UBound($aMatches) Then
    For $i = 0 To UBound($aMatches)-1
      $x_map_pos = -1
      $y_map_pos = -1

      If Treasure_Get_Box_Position($aMatches[$i][0], $aMatches[$i][1], $x_map_pos, $y_map_pos) Then
        ; 14 (Hamm) = Hammer
        $box_code = 14

        ; Register found box pattern in map
        $treasure_reward_map[$x_map_pos][$y_map_pos] = $box_code ; Full map
      Else
        ; Error
        _Generic_log_ERROR( "Treasure_Map_Update_Reward_Table: (" & $i & ") Treasure_Get_Box_Position error, x_map="& $x_map_pos & ", y_map=" & $y_map_pos & @CRLF)
      EndIf
    Next
  EndIf

  ; Find LOUPE
  $aMatches = _OpenCV_FindTemplate($img, $reward_loupe, $threshold)
  _Generic_log_TRACE( "Treasure_Map_Update_Reward_Table: Found " & UBound($aMatches) & " patterns for LOUPE box type." & @CRLF)
  If UBound($aMatches) Then
    For $i = 0 To UBound($aMatches)-1
      $x_map_pos = -1
      $y_map_pos = -1

      If Treasure_Get_Box_Position($aMatches[$i][0], $aMatches[$i][1], $x_map_pos, $y_map_pos) Then
        ; 15 (Loup) = Loupe
        $box_code = 15

        ; Register found box pattern in map
        $treasure_reward_map[$x_map_pos][$y_map_pos] = $box_code ; Full map
      Else
        ; Error
        _Generic_log_ERROR( "Treasure_Map_Update_Reward_Table: (" & $i & ") Treasure_Get_Box_Position error, x_map="& $x_map_pos & ", y_map=" & $y_map_pos & @CRLF)
      EndIf
    Next
  EndIf

  ; Find FRAG DJ
  $aMatches = _OpenCV_FindTemplate($img, $reward_dj_frag, $threshold)
  _Generic_log_TRACE( "Treasure_Map_Update_Reward_Table: Found " & UBound($aMatches) & " patterns for FRAG DJ box type." & @CRLF)
  If UBound($aMatches) Then
    For $i = 0 To UBound($aMatches)-1
      $x_map_pos = -1
      $y_map_pos = -1

      If Treasure_Get_Box_Position($aMatches[$i][0], $aMatches[$i][1], $x_map_pos, $y_map_pos) Then
        ; 16 (Frag) = Fragment DJ
        $box_code = 16

        ; Register found box pattern in map
        $treasure_reward_map[$x_map_pos][$y_map_pos] = $box_code ; Full map
      Else
        ; Error
        _Generic_log_ERROR( "Treasure_Map_Update_Reward_Table: (" & $i & ") Treasure_Get_Box_Position error, x_map="& $x_map_pos & ", y_map=" & $y_map_pos & @CRLF)
      EndIf
    Next
  EndIf

  ; Find BOOK
  $aMatches = _OpenCV_FindTemplate($img, $reward_book, $threshold)
  _Generic_log_TRACE( "Treasure_Map_Update_Reward_Table: Found " & UBound($aMatches) & " patterns for BOOK box type." & @CRLF)
  If UBound($aMatches) Then
    For $i = 0 To UBound($aMatches)-1
      $x_map_pos = -1
      $y_map_pos = -1

      If Treasure_Get_Box_Position($aMatches[$i][0], $aMatches[$i][1], $x_map_pos, $y_map_pos) Then
        ; 17 (Book) = Book
        $box_code = 17

        ; Register found box pattern in map
        $treasure_reward_map[$x_map_pos][$y_map_pos] = $box_code ; Full map
      Else
        ; Error
        _Generic_log_ERROR( "Treasure_Map_Update_Reward_Table: (" & $i & ") Treasure_Get_Box_Position error, x_map="& $x_map_pos & ", y_map=" & $y_map_pos & @CRLF)
      EndIf
    Next
  EndIf

  ; Find DIAMS
  $aMatches = _OpenCV_FindTemplate($img, $reward_diams, $threshold)
  _Generic_log_TRACE( "Treasure_Map_Update_Reward_Table: Found " & UBound($aMatches) & " patterns for DIAMS box type." & @CRLF)
  If UBound($aMatches) Then
    For $i = 0 To UBound($aMatches)-1
      $x_map_pos = -1
      $y_map_pos = -1

      If Treasure_Get_Box_Position($aMatches[$i][0], $aMatches[$i][1], $x_map_pos, $y_map_pos) Then
        ; 18 (Diam) = Diam
        $box_code = 18

        ; Register found box pattern in map
        $treasure_reward_map[$x_map_pos][$y_map_pos] = $box_code ; Full map
      Else
        ; Error
        _Generic_log_ERROR( "Treasure_Map_Update_Reward_Table: (" & $i & ") Treasure_Get_Box_Position error, x_map="& $x_map_pos & ", y_map=" & $y_map_pos & @CRLF)
      EndIf
    Next
  EndIf

  ; Find DUST
  ; Type #1 picture
  $aMatches = _OpenCV_FindTemplate($img, $reward_dust_blue, $threshold)
  _Generic_log_TRACE( "Treasure_Map_Update_Reward_Table: Found " & UBound($aMatches) & " patterns for DUST (blue) box type." & @CRLF)
  If UBound($aMatches) Then
    For $i = 0 To UBound($aMatches)-1
      $x_map_pos = -1
      $y_map_pos = -1

      If Treasure_Get_Box_Position($aMatches[$i][0], $aMatches[$i][1], $x_map_pos, $y_map_pos) Then
        ; 19 (Dust) = Dust
        $box_code = 19

        ; Register found box pattern in map
        $treasure_reward_map[$x_map_pos][$y_map_pos] = $box_code ; Full map
      Else
        ; Error
        _Generic_log_ERROR( "Treasure_Map_Update_Reward_Table: (" & $i & ") Treasure_Get_Box_Position error, x_map="& $x_map_pos & ", y_map=" & $y_map_pos & @CRLF)
      EndIf
    Next
  EndIf
  ; Type #2 picture
  $aMatches = _OpenCV_FindTemplate($img, $reward_dust_green, $threshold)
  _Generic_log_TRACE( "Treasure_Map_Update_Reward_Table: Found " & UBound($aMatches) & " patterns for DUST (green) box type." & @CRLF)
  If UBound($aMatches) Then
    For $i = 0 To UBound($aMatches)-1
      $x_map_pos = -1
      $y_map_pos = -1

      If Treasure_Get_Box_Position($aMatches[$i][0], $aMatches[$i][1], $x_map_pos, $y_map_pos) Then
        ; 19 (Dust) = Dust
        $box_code = 19

        ; Register found box pattern in map
        $treasure_reward_map[$x_map_pos][$y_map_pos] = $box_code ; Full map
      Else
        ; Error
        _Generic_log_ERROR( "Treasure_Map_Update_Reward_Table: (" & $i & ") Treasure_Get_Box_Position error, x_map="& $x_map_pos & ", y_map=" & $y_map_pos & @CRLF)
      EndIf
    Next
  EndIf

  ; Find TICKETS
  $aMatches = _OpenCV_FindTemplate($img, $reward_tickets, $threshold)
  _Generic_log_TRACE( "Treasure_Map_Update_Reward_Table: Found " & UBound($aMatches) & " patterns for TICKETS box type." & @CRLF)
  If UBound($aMatches) Then
    For $i = 0 To UBound($aMatches)-1
      $x_map_pos = -1
      $y_map_pos = -1

      If Treasure_Get_Box_Position($aMatches[$i][0], $aMatches[$i][1], $x_map_pos, $y_map_pos) Then
        ; 20 (Tckt) = Tickets
        $box_code = 20

        ; Register found box pattern in map
        $treasure_reward_map[$x_map_pos][$y_map_pos] = $box_code ; Full map
      Else
        ; Error
        _Generic_log_ERROR( "Treasure_Map_Update_Reward_Table: (" & $i & ") Treasure_Get_Box_Position error, x_map="& $x_map_pos & ", y_map=" & $y_map_pos & @CRLF)
      EndIf
    Next
  EndIf

  ; Find EVENT REWARD
  ; Type #1 picture
  $aMatches = _OpenCV_FindTemplate($img, $reward_event, $threshold)
  _Generic_log_TRACE( "Treasure_Map_Update_Reward_Table: Found " & UBound($aMatches) & " patterns for EVENT REWARD box type." & @CRLF)
  If UBound($aMatches) Then
    For $i = 0 To UBound($aMatches)-1
      $x_map_pos = -1
      $y_map_pos = -1

      If Treasure_Get_Box_Position($aMatches[$i][0], $aMatches[$i][1], $x_map_pos, $y_map_pos) Then
        ; 50 (Evnt) = Event Reward
        $box_code = 50

        ; Register found box pattern in map
        $treasure_reward_map[$x_map_pos][$y_map_pos] = $box_code ; Full map
      Else
        ; Error
        _Generic_log_ERROR( "Treasure_Map_Update_Reward_Table: (" & $i & ") Treasure_Get_Box_Position error, x_map="& $x_map_pos & ", y_map=" & $y_map_pos & @CRLF)
      EndIf
    Next
  EndIf
  $nb_rwd_event = UBound($aMatches)

  ; Type #2 picture
  $aMatches = _OpenCV_FindTemplate($img, $reward_event_collected, $threshold)
  _Generic_log_TRACE( "Treasure_Map_Update_Reward_Table: Found " & UBound($aMatches) & " patterns for EVENT REWARD Collected box type." & @CRLF)
  If UBound($aMatches) Then
    For $i = 0 To UBound($aMatches)-1
      $x_map_pos = -1
      $y_map_pos = -1

      If Treasure_Get_Box_Position($aMatches[$i][0], $aMatches[$i][1], $x_map_pos, $y_map_pos) Then
        ; 50 (Evnt) = Event Reward
        $box_code = 50

        ; Register found box pattern in map
        $treasure_reward_map[$x_map_pos][$y_map_pos] = $box_code ; Full map
      Else
        ; Error
        _Generic_log_ERROR( "Treasure_Map_Update_Reward_Table: (" & $i & ") Treasure_Get_Box_Position error, x_map="& $x_map_pos & ", y_map=" & $y_map_pos & @CRLF)
      EndIf
    Next
  EndIf
  $nb_rwd_event = $nb_rwd_event + UBound($aMatches)

	Return False
EndFunc

Func Treasure_Count_Collected_Reward_Table()
  $Exit_Resources = False

  ; Reset current level collected counters
  $treasure_nb_gold_collected_level = 0
  $treasure_nb_apple_collected_level = 0
  $treasure_nb_mana_collected_level = 0

  ; Look in all the table
  For $i = 0 To 4
    For $j = 0 To 3
      ; If Box is collected
      ; 200 (Coll) = Collected box (undefined)
      If $treasure_map[$i][$j] == 200 Then
        ; 11 (Gold) = Gold
        If $treasure_reward_map[$i][$j] == 11 then
          $treasure_nb_gold_collected_level = $treasure_nb_gold_collected_level + 1

        ; 12 (Mana) = Mana
        ElseIf $treasure_reward_map[$i][$j] == 12 Then
          $treasure_nb_mana_collected_level = $treasure_nb_mana_collected_level + 1

        ; 13 (Appl) = Apples
        ElseIf $treasure_reward_map[$i][$j] == 13 Then
          $treasure_nb_apple_collected_level = $treasure_nb_apple_collected_level + 1

        EndIf
      EndIf
    Next
  Next

  _Generic_log_TRACE( "Treasure_Count_Collected_Reward_Table: Collected gold(" & $treasure_nb_gold_collected_level & "), mana(" & $treasure_nb_mana_collected_level & "), apple(" & $treasure_nb_apple_collected_level & ") on map." & @CRLF)

  ; Check levels for Gold
  If $treasure_stop_after_X_gold > 0 and ( $treasure_nb_gold_collected_total + $treasure_nb_gold_collected_level) >= $treasure_stop_after_X_gold Then
    $Exit_Resources = True
    _Generic_log_INFO( "Treasure_Count_Collected_Reward_Table: Full Gold." & @CRLF)
    PrintGUIStatus("TreasureHunt: Full Gold !!!")
    If DiscordNotif_IsEnabled() Then
      DiscordMessage(DiscordGetUserId(), "TreasureHunt: Full Gold !!! ")
    EndIf
  EndIf

  ; Check levels for Mana
  If $treasure_stop_after_X_mana > 0 and ( $treasure_nb_mana_collected_total + $treasure_nb_mana_collected_level) >= $treasure_stop_after_X_gold Then
    $Exit_Resources = True
    _Generic_log_INFO( "Treasure_Count_Collected_Reward_Table: Full Mana." & @CRLF)
    PrintGUIStatus("TreasureHunt: Full Mana !!!")
    If DiscordNotif_IsEnabled() Then
      DiscordMessage(DiscordGetUserId(), "TreasureHunt: Full Mana !!! ")
    EndIf
  EndIf

  ; Check levels for Apple
  If $treasure_stop_after_X_apple > 0 and ( $treasure_nb_apple_collected_total + $treasure_nb_apple_collected_level) >= $treasure_stop_after_X_apple Then
    $Exit_Resources = True
    _Generic_log_INFO( "Treasure_Count_Collected_Reward_Table: Full Apple." & @CRLF)
    PrintGUIStatus("TreasureHunt: Full Apple !!!")
    If DiscordNotif_IsEnabled() Then
      DiscordMessage(DiscordGetUserId(), "TreasureHunt: Full Apple !!! ")
    EndIf
  EndIf

  return $Exit_Resources
EndFunc


Func Treasure_Map_Find_Event_And_Special_Rewards(ByRef $NbBoxHit)

  $nb_rwd_event = 0
  $nb_special_rwd = 0
  $nb_attempt = 5

  $NbBoxHit = 0

  ; Check if we are missing some event & special rewards
  Do
    ; If stuck due to loupe drag / drop issue, refresh the map ...
    If $nb_attempt <= 0 Then
      _Generic_log_TRACE( "Treasure_Map_Find_Event_And_Special_Rewards: Stuck in loop, refresh the map" & @CRLF)
      Treasure_Init_Map()
      $nb_attempt = 5
    EndIf

    ; First update the Reward table
    If Treasure_Map_Update_Reward_Table($nb_rwd_event, $nb_special_rwd) Then
      ; ERROR
      Return True
    EndIf

    ; Count already collected the desired Rewards on the table
    ; (No pass ressource collection auto stop)
    If Treasure_Count_Collected_Reward_Table() Then
      ; Full ressource, then we stop !
      Return True
    EndIf


    If $nb_rwd_event == 0 or $nb_special_rwd < $treasure_nb_spe_rewards Then
      _Generic_log_TRACE( "Treasure_Map_Find_Event_And_Special_Rewards: $nb_rwd_event=" & $nb_rwd_event & ", $nb_special_rwd=" & $nb_special_rwd & @CRLF)
      ; Yes we are missing some ... look for it in hidden box
      $break = 0
      For $i = 0 To 4
        For $j = 0 To 3
          ; 0 (Hide) = Unrevealed box
          If $treasure_map[$i][$j] == 0 Then
            ; Collect it !
            $Result = Treasure_Hit_Box($i, $j)
            If $Result Then
              ; ERROR todo
              Return True
            EndIf

            $NbBoxHit = $NbBoxHit + 1
            $break = 1
            ExitLoop
          EndIf
        Next

        If $break Then
          ExitLoop
        EndIf
      Next
    EndIf

    ; Dummy to exit on ESC
    If _Interrupt_Sleep(1) Then
      Return True
    EndIf
    $nb_attempt = $nb_attempt - 1

  Until $nb_rwd_event >= 1 and $nb_special_rwd >= $treasure_nb_spe_rewards

  If $nb_attempt <= 0 Then
    _Generic_log_ERROR( "Treasure_Map_Find_Event_And_Special_Rewards: Fail after 10 attempts " & @CRLF)
    $Watchdog_Restart = True
    Return True
  EndIf

  ;DEBUG TEMP
  ;Treasure_Print_RewardMap()

  Return False
EndFunc

Func Treasure_Map_Collect_Rewards()

  $Exit_Resources = False

  ; Collect all the remaining rewards !
  For $i = 0 To 4
    For $j = 0 To 3
      ; Check if box has to be collected (different from 200 (Coll) = Collected box (undefined))
      If $treasure_map[$i][$j] <> 200 Then
        $Collect_Box = False
        Switch $treasure_reward_map[$i][$j]
          ; 10 (SRwd) = Special reward
          Case 10
            If 1 Then
              $Collect_Box = True
            EndIf
          ; 11 (Gold) = Gold
          Case 11
            If $treasure_collect_gold Then
              $Collect_Box = True

              $treasure_nb_gold_collected_level = $treasure_nb_gold_collected_level + 1
              If $treasure_stop_after_X_gold > 0 and ($treasure_nb_gold_collected_total + $treasure_nb_gold_collected_level) >= $treasure_stop_after_X_gold Then
                $Exit_Resources = True
                _Generic_log_INFO( "Treasure_Map_Collect_Rewards: Full Gold." & @CRLF)
                PrintGUIStatus("TreasureHunt: Full Gold !!!")
                If DiscordNotif_IsEnabled() Then
                  DiscordMessage(DiscordGetUserId(), "TreasureHunt: Full Gold !!! ")
                EndIf
              EndIf
            EndIf
          ; 12 (Mana) = Mana
          Case 12
            If $treasure_collect_mana Then
              $Collect_Box = True

              $treasure_nb_mana_collected_level = $treasure_nb_mana_collected_level + 1
              If $treasure_stop_after_X_mana > 0 and ($treasure_nb_mana_collected_total + $treasure_nb_mana_collected_level) >= $treasure_stop_after_X_mana Then
                $Exit_Resources = True
                _Generic_log_INFO( "Treasure_Map_Collect_Rewards: Full Mana." & @CRLF)
                PrintGUIStatus("TreasureHunt: Full Mana !!!")
                If DiscordNotif_IsEnabled() Then
                  DiscordMessage(DiscordGetUserId(), "TreasureHunt: Full Mana !!! ")
                EndIf
              EndIf
            EndIf
          ; 13 (Appl) = Apples
          Case 13
            If $treasure_collect_apples Then
              $Collect_Box = True

              $treasure_nb_apple_collected_level = $treasure_nb_apple_collected_level + 1
              If $treasure_stop_after_X_apple > 0 and ($treasure_nb_apple_collected_total + $treasure_nb_apple_collected_level) >= $treasure_stop_after_X_apple Then
                $Exit_Resources = True
                _Generic_log_INFO( "Treasure_Map_Collect_Rewards: Full Apple." & @CRLF)
                PrintGUIStatus("TreasureHunt: Full Apple !!!")
                If DiscordNotif_IsEnabled() Then
                  DiscordMessage(DiscordGetUserId(), "TreasureHunt: Full Apple !!! ")
                EndIf
              EndIf
            EndIf
          ; 14 (Hamm) = Hammer
          Case 14
            If 1 Then
              $Collect_Box = True
            EndIf
          ; 15 (Loup) = Loupe
          Case 15
            If $treasure_collect_loupe Then
              $Collect_Box = True
            EndIf
          ; 16 (Frag) = Fragment DJ
          Case 16
            If $treasure_collect_fraghero Then
              $Collect_Box = True
            EndIf
          ; 17 (Book) = Book
          Case 17
            If $treasure_collect_book Then
              $Collect_Box = True
            EndIf
          ; 18 (Diam) = Diam
          Case 18
            If $treasure_collect_diams Then
              $Collect_Box = True
            EndIf
          ; 19 (Dust) = Dust
          Case 19
            If $treasure_collect_dust Then
              $Collect_Box = True
            EndIf
          ; 20 (Tckt) = Tickets
          Case 20
            If $treasure_collect_tickets Then
              $Collect_Box = True
            EndIf
          ; 50 (Evnt) = Event Reward
          Case 50
            If 1 Then
              $Collect_Box = True
            EndIf
          Case Else
            ; Nothing to do
        EndSwitch

        If $Collect_Box Then
          $Result = Treasure_Hit_Box($i, $j)
          If $Result Then
            _Generic_log_ERROR( "Treasure_Map_Collect_Rewards: Fail hitting the box=[" & $i & "," & $j & "]" & @CRLF)
            Return True
          EndIf
        EndIf
        If $Exit_Resources Then
          Return True
        EndIf
      EndIf
    Next
  Next

  Return False
EndFunc

Func Treasure_Hit_Box($x, $y)
	_Generic_Log_TRACE("Treasure_Hit_Box: Position = [" & $x & "," & $y & "], type=" & Treasure_Asc_Code($treasure_map[$x][$y]) & @CRLF)

	If $treasure_map[$x][$y] == 200 Then
		; ERROR: already collected
		_Generic_Log_ERROR("Treasure_Hit_Box: Error box already collected !" & @CRLF)
		return True
	Else
		; Compute click coordinates
    $x_coord = $treasure_zone[0] + $x * $treasure_box_size[0] + Int($treasure_box_size[0]/2)
    $y_coord = $treasure_zone[1] + $y * $treasure_box_size[1] + Int($treasure_box_size[1]/2)

		; Click on box
    MouseClick("primary", $x_coord, $y_coord , 1, 1)

    ; Reward pop-up expected
    If $treasure_map[$x][$y] == 1 Then
      $button_x = 0
      $button_y = 0
      ; 1 (Revl) = Revealed but uncollected
      Do
        If _Interrupt_Sleep(200) Then
          return True
        EndIf

        Local $aRect = WinGetPos($hwnd)
        Local $img = _OpenCV_GetDesktopScreenMat($aRect)
        $aMatches = _OpenCV_FindTemplate($img, $reward_confirm_button, $threshold)
        If UBound($aMatches) Then
          $button_x = $aMatches[0][0]
          $button_y = $aMatches[0][1]
        EndIf
      Until UBound($aMatches)

      ; Click on button to collect the reward
      MouseClick("primary", $button_x, $button_y, 1, 1)
    EndIf

    ; Wait for reward
    Local $iBegin = TimerInit()
    Local $attempt = 3
    Do
      If _Interrupt_Sleep(200) Then
        return True
      EndIf

      ; Check pop-up menu (no more hammer // reward validation)
      $color = PixelGetColor(749, 800)
      ;_Generic_log_TRACE( "Treasure_Hit_Box: Found color=0x" & hex($color, 6) & @CRLF)

      ; Check timeout -> Hit box missed -> Repeat
      If TimerDiff($iBegin) > 2000 Then
        $attempt = $attempt - 1
        If $attempt == 0 Then
          ; Issue, leave with watchdog flag
          _Generic_log_Error( "Treasure_Hit_Box: Timeout issue, restart through watchdog" & @CRLF)
          $Watchdog_Restart = True
          Return True
        EndIf
        _Generic_log_TRACE( "Treasure_Hit_Box: Timeout repeat hit" & @CRLF)

        ; Click again on box
        MouseClick("primary", $x_coord, $y_coord , 1, 1)

        ; Reset timer
        $iBegin = TimerInit()
      EndIf
    Until( $color == 0x4FC30D or $color == 0x73DB18 or $color == 0x4CBB0C )

    ; Reward validation
    MouseClick("primary", 749, 800, 1, 1)

    Do
      If _Interrupt_Sleep(200) Then
        return True
      EndIf
      $color = PixelGetColor(749, 800)
    Until( $color <> 0x4FC30D and $color <> 0x73DB18 and $color <> 0x4CBB0C )

		; Update cell status
    ; 200 (Coll) = Collected box (undefined)
    $treasure_map[$x][$y] = 200

    ; Increment stat counter :)
    $treasure_nb_box_hit = $treasure_nb_box_hit + 1

		; OK
		return False
	EndIf

EndFunc


Func Treasure_Get_Box_Position($x_pixel_pos, $y_pixel_pos, ByRef $x_map_pos, ByRef $y_map_pos)
  $x_map_pos = Int( $x_pixel_pos / $treasure_box_size[0] )
  $y_map_pos = Int( $y_pixel_pos / $treasure_box_size[1] )
  ;_Generic_log_TRACE( "Treasure_Get_Box_Position: pixel_pos=[" & $x_pixel_pos & "," & $y_pixel_pos & "], map_pos=[" & $x_map_pos & "," & $y_map_pos  & "]" & @CRLF)

  If $x_map_pos < 5 and $y_map_pos < 4 Then
    Return True
  Else
    Return False
  EndIf
EndFunc

Func Treasure_Print_LoupePattern($pattern_id)
  _Generic_log_TRACE("Treasure_Print_LoupePattern:" & @CRLF)
  $row_separator = "----------"& @CRLF

  _Generic_log_TRACE($row_separator)
  For $j = 0 To 2
    $row_str = ""
    For $i = 0 To 2
      If $loupe_pattern_map_search[$pattern_id][$j][$i] Then
        $row_str = $row_str & " X "
      Else
        $row_str = $row_str & "   "
      EndIf
    Next
    $row_str = $row_str & "|" & @CRLF
    _Generic_log_TRACE($row_str)
    _Generic_log_TRACE($row_separator)
  Next
EndFunc

Func Treasure_Print_SearchMap($search_map)
  _Generic_log_TRACE("Treasure_Print_SearchMap:" & @CRLF)
  $row_separator = "-----------------------------------------------"& @CRLF

  _Generic_log_TRACE($row_separator)
  For $j = 0 To 3+2*2
    $row_str = ""
    For $i = 0 To 4+2*2
      $row_str = $row_str & Treasure_Asc_Code($search_map[$i][$j])
    Next
    $row_str = $row_str & "|" & @CRLF
    _Generic_log_TRACE($row_str)
    _Generic_log_TRACE($row_separator)
  Next
EndFunc

Func Treasure_Print_RewardMap()
  _Generic_log_TRACE("Treasure_Print_RewardMap:" & @CRLF)
  $row_separator = "-------------------------------"& @CRLF

  _Generic_log_TRACE($row_separator)
  For $j = 0 To 3
    $row_str = ""
    For $i = 0 To 4
      $row_str = $row_str & Treasure_Asc_Code($treasure_reward_map[$i][$j])
    Next
    $row_str = $row_str & "|" & @CRLF
    _Generic_log_TRACE($row_str)
    _Generic_log_TRACE($row_separator)
  Next
EndFunc

Func Treasure_Print_Map()
  _Generic_log_TRACE("Treasure_Print_Map:" & @CRLF)
  $row_separator = "-------------------------------"& @CRLF

  _Generic_log_TRACE($row_separator)
  For $j = 0 To 3
    $row_str = ""
    For $i = 0 To 4
      $row_str = $row_str & Treasure_Asc_Code($treasure_map[$i][$j])
    Next
    $row_str = $row_str & "|" & @CRLF
    _Generic_log_TRACE($row_str)
    _Generic_log_TRACE($row_separator)
  Next
EndFunc

Func Treasure_Asc_Code($code)
  If $code == 0 Then
    ; 0 (Hide) = Unrevealed box
    Return "|Hide"
  ElseIf $code == 1 Then
    ; 1 (Revl) = Revealed but uncollected
    Return "|Revl"
  ElseIf $code == 10 Then
    ; 10 (SRwd) = Special reward
    Return "|SRwd"
  ElseIf $code == 11 Then
    ; 11 (Gold) = Gold
    Return "|Gold"
  ElseIf $code == 12 Then
    ; 12 (Mana) = Mana
    Return "|Mana"
  ElseIf $code == 13 Then
    ; 13 (Appl) = Apples
    Return "|Appl"
  ElseIf $code == 14 Then
    ; 14 (Hamm) = Hammer
    Return "|Hamm"
  ElseIf $code == 15 Then
    ; 15 (Loup) = Loupe
    Return "|Loup"
  ElseIf $code == 16 Then
    ; 16 (Frag) = Fragment DJ
    Return "|Frag"
  ElseIf $code == 17 Then
    ; 17 (Book) = Book
    Return "|Book"
  ElseIf $code == 18 Then
    ; 18 (Diam) = Diam
    Return "|Diam"
  ElseIf $code == 19 Then
    ; 19 (Dust) = Dust
    Return "|Dust"
  ElseIf $code == 20 Then
    ; 20 (Tckt) = Tickets
    Return "|Tckt"
  ElseIf $code == 50 Then
    ; 50 (Evnt) = Event Reward
    Return "|Evnt"
  ElseIf $code == 200 Then
    ; 200 (Coll) = Collected box (undefined)
    Return "|Coll"
  ElseIf $code == 255 Then
    ; 255 (Undf) = Not init
    Return "|Undf"
  ElseIf $code == 256 Then
    ; 256 (Outm) = Out of the map
    Return "|Outm"
  Else
    Return "|O__O"
  EndIf
EndFunc
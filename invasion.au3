; Manage invasions in hustle castle
#include-once
#include "data/hc_coords.au3"


;invasions coords
Global Const $invasion_attack_button[3] = [1187, 734, 0x73DB18]
Global Const $invasion_no_health_button[2] = [583, 585]
Global Const $invasion_no_spell_button[2] = [583, 585]
Global Const $invasion_no_food_button[3] = [950, 571, 0x73DB18] ; bouton non
Global Const $invasion_next_button[3] = [809, 808, 0x73DB18]
Global Const $invasion_end_button[3] = [75, 804, 0x73DB18]

Func Invasions()

	If initBlueStacks() Then
		_Function_Finished()
		Return
	EndIf

	Local $count = 10 ; (10 invasions)
	Do

		;FAIRE ATTENTION EN CAS DE MODIF : ALGO RECOPIE DANS invasionFromCastle
		If _Interrupt_Sleep(1000) Then
			_Function_Finished()
			return
		Endif;
		MouseClick("primary", $invasion_attack_button[0], $invasion_attack_button[1] , 1) ; � l'attaque
		ConsoleWrite (@TAB & "attaque "& (11-$count) & @CRLF)

		If _Interrupt_Sleep(1500) Then
			_Function_Finished()
			return
		Endif;

		; si �a affiche un diamant, on sort
		$color = PixelGetColor($red_button_no_diamond[0], $red_button_no_diamond[1])
		If $color == $red_button_no_diamond[2] or $color == $red_button_no_diamond[3] or $color == $red_button_no_diamond[4] Then
			If is_diamond_button() Then
				ConsoleWrite (@TAB & "Plus de pommes" & @CRLF)
				_Function_Finished()
				return
			EndIf
		EndIf

		;passe le "vos combattants sont cass�s"
		MouseClick("primary", $invasion_no_health_button[0], $invasion_no_health_button[1] , 1) ; ouioui, ils vont gagner de toute fa�on
		ConsoleWrite (@TAB & "continue si combattants cass�s" & @CRLF)
		If _Interrupt_Sleep(1500) Then
			_Function_Finished()
			return
		Endif;

		;passe le "ya pas de sorts"
		MouseClick("primary", $invasion_no_spell_button[0], $invasion_no_spell_button[1] , 1) ; ouioui, ils vont gagner de toute fa�on
		ConsoleWrite (@TAB & "continue si pas de sort" & @CRLF)

		If _Interrupt_Sleep(500) Then
			_Function_Finished()
			return
		Endif;

		While(PixelGetColor($invasion_next_button[0], $invasion_next_button[1]) <> $invasion_next_button[2]);attend le bouton vert "prochain combat"

			If _Interrupt_Sleep(100) Then
				_Function_Finished()
				return
			Endif;
			;bouton h�ros jusqu'� la fin de l'attaque
			MouseClick("primary", $hero_spell[0],  $hero_spell[1], 1)

			;si les 10 attaques sont termin�es
			If PixelGetColor($invasion_end_button[0], $invasion_end_button[1]) == $invasion_end_button[2] Then
				ExitLoop;
			EndIf


			;ConsoleWrite ("PixelGetColor("&$invasion_next_button[0]&", "&$invasion_next_button[1]&")=" & Hex(PixelGetColor($invasion_next_button[0], $invasion_next_button[1]), 6) & @CRLF)
		WEnd



		;si les 10 attaques sont termin�es
		If PixelGetColor($invasion_end_button[0], $invasion_end_button[1]) == $invasion_end_button[2] Then
			ConsoleWrite (@TAB & "10 attaques termin�es" & @CRLF)
			ExitLoop;
		EndIf

		ConsoleWrite (@TAB & "attaque termin�e" & @CRLF)

		; clique sur "prochain combat"
		MouseClick("primary", $invasion_next_button[0], $invasion_next_button[1] , 1)
		;attend
		If _Interrupt_Sleep(15000) Then
			_Function_Finished()
			return
		Endif;
		ConsoleWrite (@TAB & "prochain" & @CRLF)

		$count = $count - 1;
	Until $count <=0


	; clique sur "ch�teau"
	MouseClick("primary", 675, 804 , 1)

	; todo clique sur 2x sur "obtenir" (si r�compense d'�v�nement

	If _Interrupt_Sleep(15000) Then
		_Function_Finished()
		return
	Endif;
	MouseClick("primary", 959, 811 , 1)

	If _Interrupt_Sleep(1000) Then
		_Function_Finished()
		return
	Endif;
	MouseClick("primary", 959, 811 , 1)

	_Function_Finished()
EndFunc

Func InvasionsFromCastle()
	If initBlueStacks() Then
		_Function_Finished()
		Return
	EndIf

	Local $count = 10 ; (10 invasions)
	Do
		If _Interrupt_Sleep(1000) Then
			_Function_Finished()
			return
		Endif

		;clique sur le bouton carte
		MouseClick("primary", 170, 810, 1)	;carte
		If _Interrupt_Sleep(2000) Then
			_Function_Finished()
			return
		Endif;

		;clique sur le bouton invasion
		MouseClick("primary", 69, 654, 1)
		If _Interrupt_Sleep(1000) Then
			_Function_Finished()
			return
		Endif

		;clique sur le bouton de recherche
		MouseClick("primary", 768, 826, 1)
		If _Interrupt_Sleep(1000) Then
			_Function_Finished()
			return
		Endif

		;recherche l'image d'invasion
		;clique si trouv�
		Local $y = 0, $x = 0, $tolerance = 10
		;Do
			;$search = _ImageSearch('invasion.png', 0, $x, $y, $tolerance)
			;If $search = 0 Then
				;$search = _ImageSearch('invasion2.png', 0, $x, $y, $tolerance)
			;EndIf
			$tolerance = $tolerance + 20
		;Until $search = 1 or $tolerance > 200
		;If $search = 1 Then
		;	MouseClick("primary", $x, $y, 1)
		;Else
			;GUICtrlSetData($label, "image non trouv�e")
			_Function_Finished()
			return
		;EndIf


		;lance l'algo du dessus JE L'AI RECOPIE PARCE QUE JE PENSE QUE CE N'EST QUE TEMPORAIRE. FAIRE ATTENTION EN CAS DE MODIF

		If _Interrupt_Sleep(1000) Then
			_Function_Finished()
			return
		Endif;
		MouseClick("primary", $invasion_attack_button[0], $invasion_attack_button[1] , 1) ; � l'attaque
		ConsoleWrite (@TAB & "attaque "& (11-$count) & @CRLF)

		If _Interrupt_Sleep(1500) Then
			_Function_Finished()
			return
		Endif;
		; si �a affiche un diamant, on sort

		$color = PixelGetColor($red_button_no_diamond[0], $red_button_no_diamond[1])
		If $color == $red_button_no_diamond[2] or $color == $red_button_no_diamond[3] or $color == $red_button_no_diamond[4] Then
			ConsoleWrite (@TAB & "Plus de pommes" & @CRLF)
			_Function_Finished()
			return
		EndIf

		;passe le "vos combattants sont cass�s"
		MouseClick("primary", $invasion_no_health_button[0], $invasion_no_health_button[1] , 1) ; ouioui, ils vont gagner de toute fa�on
		ConsoleWrite (@TAB & "continue si combattants cass�s" & @CRLF)
		If _Interrupt_Sleep(1500) Then
			_Function_Finished()
			return
		Endif;

		;passe le "ya pas de sorts"
		MouseClick("primary", $invasion_no_spell_button[0], $invasion_no_spell_button[1] , 1) ; ouioui, ils vont gagner de toute fa�on
		ConsoleWrite (@TAB & "continue si pas de sort" & @CRLF)

		If _Interrupt_Sleep(500) Then
			_Function_Finished()
			return
		Endif;

		While(PixelGetColor($invasion_next_button[0], $invasion_next_button[1]) <> $invasion_next_button[2]);attend le bouton vert "prochain combat"

			If _Interrupt_Sleep(100) Then
				_Function_Finished()
				return
			Endif;
			;bouton h�ros jusqu'� la fin de l'attaque
			MouseClick("primary", $hero_spell[0],  $hero_spell[1], 1)

			;si les 10 attaques sont termin�es
			If PixelGetColor($invasion_end_button[0], $invasion_end_button[1]) == $invasion_end_button[2] Then
				ExitLoop;
			EndIf


			;ConsoleWrite ("PixelGetColor("&$invasion_next_button[0]&", "&$invasion_next_button[1]&")=" & Hex(PixelGetColor($invasion_next_button[0], $invasion_next_button[1]), 6) & @CRLF)
		WEnd



		;si les 10 attaques sont termin�es
		If PixelGetColor($invasion_end_button[0], $invasion_end_button[1]) == $invasion_end_button[2] Then
			ConsoleWrite (@TAB & "10 attaques termin�es" & @CRLF)
			ExitLoop;
		EndIf

		ConsoleWrite (@TAB & "attaque termin�e" & @CRLF)

		; clique sur "prochain combat"
		MouseClick("primary", $invasion_next_button[0], $invasion_next_button[1] , 1)
		;attend
		If _Interrupt_Sleep(15000) Then
			_Function_Finished()
			return
		Endif;
		ConsoleWrite (@TAB & "prochain" & @CRLF)


		$count = $count - 1;
	Until $count <=0


	; clique sur "ch�teau"
	MouseClick("primary", 675, 804 , 1)

	; todo clique sur 2x sur "obtenir" (si r�compense d'�v�nement

	If _Interrupt_Sleep(15000) Then
		_Function_Finished()
		return
	Endif;
	MouseClick("primary", 959, 811 , 1)

	If _Interrupt_Sleep(1000) Then
		_Function_Finished()
		return
	Endif;
	MouseClick("primary", 959, 811 , 1)

	_Function_Finished()
EndFunc
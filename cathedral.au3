#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_UseX64=y
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****



;la cath�drale
#include-once

#include "data/hc_coords.au3"

;screen types
Global Const $enemyTab[4] = [346, 97, 0xD9C19A, 0xD9C199]
Global Const $campTicks[9] = [690, 330, 1300, 330, 1330, 365, 0xADD7D9, 0xA6CFD0, 0x6CCF2C]
Global Const $choiceTicks[6] = [716, 309, 1126, 311, 0xADD7D9, 0xA6CFD0]
Global Const $sacrifice[6] = [266, 260, 410, 260, 545, 260]
Global Const $emptySquadColor[3] = [0x80D196, 0x6ACAD2, 0x66C2CA]
Global Const $closeWindowButton[3] = [1396, 148, 0xE73010]
Global Const $addFighterButton[3] = [1239, 728, 0x73DB18]
Global Const $menuCheck[3] = [135, 140, 0x395078]

;fight screen
Global Const $firstWarrior[4] = [320, 580, 1380, 580]
Global Const $warriorBgColor = 0x80D196
Global Const $fightButton[4] = [1153, 814, 0x4FC30D, 0x4CBB0C]
Global Const $quickFightButton[4] = [1088, 814, 0xF49205, 0xEA8C05]
Global Const $notFullSquadbutton[3] = [900, 575, 0xF43F18]
Global Const $deployBest[3] = [771, 425, 0x29CFE4]
Global Const $deployBest_Fight4Check[4] = [1050, 285, 0x80D196, 0x7BC990]
Global Const $deployBest_Fight5Check[4] = [1125, 285, 0x80D196, 0x7BC990]

;selection screen
Global Const $confirmButton[3] = [1247, 765, 0x52C310]
Global Const $campConfirmButton[4] = [635, 594, 0x73DB18, 0x5AAC13]
Global Const $sacrificeConfirmButton[4] = [771, 801, 0x73DB18, 0x5AAC13]
Global Const $closeButton[4] = [766, 760, 0x278010, 0x26820D]

;rez
Global Const $rezButton[7] = [740, 570, 0x63C023, 0x5FB822, 0x63BE21, 0x63BF22, 0x5FB720]
Global Const $rezConfirmButton[4] = [680, 630, 0x73DB18, 0x6ED217]
Global Const $nextRezButton[3] = [1034, 838, 0x1ED8E7]
Global Const $cathHealthBarCoords[3] = [160, 1380, 698] ; left, right, height
Global Const $cathHealthBarColors[4] = [0x6AED80, 0x6EF785, 0xD04621, 0xD84922]

;blessings
Global Const $blessings[6] = [385, 350, 763, 350, 1140, 350]
Global Const $confirmBlessing[4] = [769, 780, 0x73DB18, 0x5AAC13]

;end
Global Const $nextStageButton[4] = [1370, 690, 0x73DB18, 0x6ED217]
Global Const $endQuestButton[3] = [1290, 854, 0x4FC30D]
Global Const $endQuestConfirm[3] = [943, 795, 0x73DB18]

;store
Global Const $notEnough[4] = [704, 552, 0xF3B605, 0xFDBD05]

;acceleration
Global $fCathedralFactor = 1.0

;boss location
Global $boss_run = true
Global $mapId = 255
Global $map3boss[3][3][2] = [ [ [ 923, 379], [ 788, 727], [ 751, 111] ], _ ;3 boss circle map
                              [ [ 782, 735], [ 789, 89],  [ 235, 410] ], _ ;3 boss map right
                              [ [ 782, 735], [ 789, 89], [ 1301, 404] ] ]  ;3 boss map left
Global $map2boss[2][2][2] = [ [ [ 986, 373], [ 557, 475] ], _ ;2 boss spirale
                              [ [ 209, 399], [ 1337, 401] ] ] ;2 boss

Global $map_spots_boss_only[10][11][3] = [ _
  _ ;2 boss left:
  [ [379, 422, 0], [222, 411, 1], [376, 583, 0], [558, 627, 0], [1034, 582, 0], [1190, 572, 0], [1190, 411, 0], [1354, 420, 1], [0,0,0], [0,0,0], [0,0,0] ], _
  _ ;2 boss right:
  [ [1190, 411, 0], [1354, 420, 1], [1190, 572, 0], [1034, 582, 0], [558, 627, 0], [376, 583, 0], [379, 422, 0], [222, 411, 1], [0,0,0], [0,0,0], [0,0,0] ], _
  _ ;2 boss spirale left:
  [ [290, 558, 0], [393, 370, 0], [566, 250, 0], [694, 195, 0], [873, 202, 0], [1000, 394, 1], [688, 333, 0], [570, 488, 1], [0,0,0], [0,0,0], [0,0,0] ], _
  _ ;2 boss spirale right:
  [ [1106, 138, 0], [1140, 370, 0], [1060, 582, 0], [934, 522, 0], [1000, 394, 1], [912, 650, 0], [735, 647, 0], [570, 488, 1], [0,0,0], [0,0,0], [0,0,0] ], _
  _ ;3 boss left:
  [ [246, 414, 0], [478, 503, 0], [678, 580, 0], [800, 618, 0], [800, 745, 1], [944, 585, 0], [1110, 502, 0], [1315, 424, 1], [940, 300, 0], [805, 263, 0], [805, 110, 1] ], _
  _ ;3 boss right:
  [ [1313, 402, 0], [1110, 502, 0], [944, 585, 0], [800, 618, 0], [800, 745, 1], [678, 580, 0], [478, 503, 0], [246, 414, 1], [940, 300, 0], [805, 263, 0], [805, 110, 1] ], _
  _ ;3 boss circle left:
  [ [265, 520, 0], [462, 454, 0], [570, 649, 0], [800, 737, 1], [566, 266, 0], [884, 201, 0], [768, 123, 1], [644, 382, 0], [807, 594, 0], [940, 405, 1], [0,0,0] ], _
  _ ;3 boss circle right:
  [ [1215, 202, 0], [1062, 305, 0], [890, 218, 0], [768, 123, 1], [1119, 533, 0], [1007, 651, 0], [800, 737, 1], [566, 266, 0], [644, 382, 0], [807, 594, 0], [940, 405, 1] ], _
  _ ;4 boss left:
  [ [236, 394, 0], [441, 419, 0], [497, 640, 0], [810, 603, 0], [805, 736, 1], [960, 411, 1], [645, 411, 1], [807, 265, 0], [804, 119, 1], [0,0,0], [0,0,0] ], _
  _ ;4 boss right:
  [ [1323, 381, 0], [1142, 398, 0], [1078, 640, 0], [810, 603, 0], [805, 736, 1], [960, 411, 1], [645, 411, 1], [807, 265, 0], [804, 119, 1], [0,0,0], [0,0,0] ] _
]

Global $map_start_spot_position[10][2] = [ _
  [380,  419], _ ;2 boss left =
  [1188, 419], _ ;2 boss right =
  [286,  543], _ ;2 boss spirale left =
  [1107, 141], _ ;2 boss spirale right =
  [248,  426], _ ;3 boss left =
  [1314, 420], _ ;3 boss right =
  [268,  526], _ ;3 boss circle left =
  [1215, 209], _ ;3 boss circle right =
  [239,  409], _ ;4 boss left
  [1323, 396] _  ;4 boss right =
]
Global $map_start_spot_position_old[10][2] = [ _
  [0,   0], _ ;2 boss left
  [1176, 383], _ ;2 boss right
  [274,  507], _ ;2 boss spirale left
  [1095, 105], _ ;2 boss spirale right
  [236,  390], _ ;3 boss left
  [1302, 384], _ ;3 boss right
  [256,  490], _ ;3 boss circle left
  [1203, 173], _ ;3 boss circle right
  [227,  373], _ ;4 boss left
  [1311, 360] _  ;4 boss right
]

;OpenCV image references
Global $fight
Global $camp
Global $dice
Global $boss
Global $robe
Global $fire
Global $guardian
Global $hidden_boss
Global $fighters
Global $menu_camp
Global $menu_choice
Global $menu_sacrifice
Global $menu_fight
Global $menu_clone

Global $threshold = 0.8

Global $stepId

Global $isQuickFight

Func _Cathedral_Interrupt_Sleep($iDelay)
	Global $fInterrupt

	; Call standard interrupt sleep
	Return _Interrupt_Sleep( $fCathedralFactor * $iDelay )
EndFunc

Func Cathedral($count)

  Local $sRuntime = ""

	If initBlueStacks() Then
		_Function_Finished()
		Return
	EndIf

  ; Log enable
  $cath_log_enable = (IniRead(@ScriptDir & "/H2C2.ini", "params", "log_cathe", "0") == "1")

  ; Speed calculation
  $strCathedralSpeed = IniRead(@ScriptDir & "/H2C2.ini", "game", "cathedralSpeed", "Normal")
  If $strCathedralSpeed == "Average" Then
    $iCathedralSpeed = 250
  ElseIf $strCathedralSpeed == "Max" Then
    $iCathedralSpeed = 500
  ElseIf $strCathedralSpeed == "Burn-Baby-Burn" Then
    $iCathedralSpeed = 0
  Else
    $iCathedralSpeed = 100
  EndIf

  $fCathedralFactor = _MIN( $iCathedralSpeed, 500 ) ; Max 5x acceleration
  $fCathedralFactor = _MAX( $fCathedralFactor, 20 ) ; Min x0.2 acceleration
  $fCathedralFactor = $fCathedralFactor / 100
  If $iCathedralSpeed == 0 Then
    $fCathedralFactor = 0.0
  EndIf
  _cath_log_INFO ("Cathedral speed selected: "& $strCathedralSpeed & ", factor=" & $fCathedralFactor & @CRLF)

  ;OpenCV image buffers init:
  $fight = _OpenCV_imread_and_check(_OpenCV_FindFile("cathedral_fight.png"))
  $camp = _OpenCV_imread_and_check(_OpenCV_FindFile("cathedral_camp.png"))
  $dice = _OpenCV_imread_and_check(_OpenCV_FindFile("cathedral_dice.png"))
  $boss = _OpenCV_imread_and_check(_OpenCV_FindFile("cathedral_boss.png"))
  $robe = _OpenCV_imread_and_check(_OpenCV_FindFile("cathedral_robe.png"))
  $fire = _OpenCV_imread_and_check(_OpenCV_FindFile("cathedral_fire.png"))
  $guardian = _OpenCV_imread_and_check(_OpenCV_FindFile("cathedral_guardian.png"))
  $hidden_boss = _OpenCV_imread_and_check(_OpenCV_FindFile("cathe_boss_unrevealed.png"))

  $fighters = _OpenCV_imread_and_check(_OpenCV_FindFile("cathedral_miss_fighter.png"))

  $menu_camp = _OpenCV_imread_and_check(_OpenCV_FindFile("cathedral_camp_menu2.png"))
  $menu_choice = _OpenCV_imread_and_check(_OpenCV_FindFile("cathedral_choice_menu2.png"))
  $menu_fight = _OpenCV_imread_and_check(_OpenCV_FindFile("cathedral_fight_menu2.png"))
  $menu_sacrifice = _OpenCV_imread_and_check(_OpenCV_FindFile("cathedral_sacrifice_menu2.png"))
  $menu_clone = _OpenCV_imread_and_check(_OpenCV_FindFile("cathedral_clone_menu2.png"))

	For $loop = 0 to $count-1
		_cath_log_INFO ("cathedral loop "& $loop & @CRLF)

    Runtime_In_Hour_Min_Sec($sRuntime)
    PrintGUIStatus("Cathedral run done " & $loop & " on " & $count & ", " & $sRuntime)

		;si on est dans le ch�teau
		If is_castle_screen() Then
			If open_map() Then
				_Function_Finished()
        Return
      EndIf
			If open_cathedral() Then
				_Function_Finished()
        Return
      EndIf
		EndIf

		;si on est dans l'�cran cath�drale
		If is_cathedral_screen() Then
			If LaunchCathedral() Then
				_Function_Finished()
				Return
			EndIf
		EndIf

		;si on est d�j� dans la cath�drale
		If is_cathedral_stage() Then              
			If CathedralLoop() Then
				_Function_Finished()
				Return
			EndIf
		EndIf

		;wait for cathedral screen
		Local $iBegin = TimerInit()
		Do
			If is_cathedral_screen() Then
				ExitLoop
			EndIf

			If _Interrupt_Sleep(1000) Then
				_Function_Finished()
				Return
			Endif
		Until TimerDiff($iBegin) > 60000 ; 1 minute devrait suffire
	Next

	_Function_Finished()
EndFunc

Func LaunchCathedral()
	$cathedralPayment = IniRead(@ScriptDir & "/H2C2.ini", "game", "cathedralPayment", "Preselected")
	$cathedralLevel = IniRead(@ScriptDir & "/H2C2.ini", "game", "cathedralLevel", "Preselected")
    
    ; Enable periodic capcha test during cathedral launch (if not already enabled ...)
    $enablePeriodicCapchaTest = True

	If $cathedralPayment == "Apples" Then
		MouseClick("primary", 398, 233, 1, 1)
	ElseIf $cathedralPayment == "Keys" Then
		MouseClick("primary", 688, 233, 1, 1)
	EndIf

	If _Interrupt_Sleep(300) Then
		Return True
	Endif;

	If $cathedralLevel == "Normal" Then
		MouseClick("primary", 336, 412, 1, 1)
	ElseIf $cathedralLevel == "Difficult" Then
		MouseClick("primary", 528, 412, 1, 1)
	ElseIf $cathedralLevel == "Nightmare" Then
		MouseClick("primary", 715, 412, 1, 1)
	EndIf

	If _Interrupt_Sleep(300) Then
		Return True
	Endif

	;launch
	MouseClick("primary", 539, 815, 1, 1)
  do
    $color = PixelGetColor(775, 775)
    ; to manage capcha ...
    If _Interrupt_Sleep(10) then
        Return true
    EndIf
  until $color == 0x73DB18
	If _Interrupt_Sleep(300) Then
		Return True
	Endif

	;confirm
	MouseClick("primary", 775, 775, 1, 1)
	If _Interrupt_Sleep(600) Then
		Return True
	Endif

	;incomplete team
	$color1 = PixelGetColor($no_spell[0], $no_spell[1])
	$color2 = PixelGetColor($no_spell[4], $no_spell[5])
	If ($color1 == $no_spell[2] or $color1 == $no_spell[3]) And ($color2 == $no_spell[6] or $color2 == $no_spell[7]) Then
		MouseClick("primary", $no_spell[0], $no_spell[1], 1, 1) ; ouioui, ils vont gagner de toute fa�on
		If _Interrupt_Sleep(400) Then
			Return True
		Endif
	EndIf

	;no spell
	$color1 = PixelGetColor($no_spell[0], $no_spell[1])
	$color2 = PixelGetColor($no_spell[4], $no_spell[5])
	If ($color1 == $no_spell[2] or $color1 == $no_spell[3]) And ($color2 == $no_spell[6] or $color2 == $no_spell[7]) Then
		MouseClick("primary", $no_spell[0], $no_spell[1], 1, 1) ; ouioui, ils vont gagner de toute fa�on
		If _Interrupt_Sleep(400) Then
			Return True
		Endif
	EndIf

	;no tools. Fly, you tools
	$color1 = PixelGetColor($no_tool[0], $no_tool[1])
	$color2 = PixelGetColor($no_tool[4], $no_tool[5])
	If ($color1 == $no_tool[2] or $color1 == $no_tool[3]) And ($color2 == $no_tool[6] or $color2 == $no_tool[7]) Then
		MouseClick("primary", $no_tool[0], $no_tool[1], 1, 1) ; ouioui, ils vont gagner de toute fa�on
		If _Interrupt_Sleep(400) Then
			Return True
		Endif
	EndIf

	;wait for cathedral first stage
	Local $iBegin = TimerInit()
	Do
		If is_cathedral_stage() Then
			ExitLoop
		EndIf

		If _Interrupt_Sleep(200) Then
			Return True
		Endif
	Until TimerDiff($iBegin) > 60000 ; 1 minute devrait suffire

	Return False
EndFunc


Func CathedralLoop()

  Local $img
  Local $Leave_stage_attempts

	$doDangerousGame = (IniRead(@ScriptDir & "/H2C2.ini", "game", "cathedraldangerousgame", "0") == "1")
  $boss_run_param = (IniRead(@ScriptDir & "/H2C2.ini", "game", "cathedralbossesonly", "0") == "1")

  ; Loop for cathedral floors
	For $stage = 0 to 4
    $Leave_stage_attempts = 0

    ; Wait for being in cathedral level
    Do
      If _Interrupt_Sleep(100) Then
        Return True
      Endif
    Until FindImage("cathedral_part.png")
    
    ; Stop periodic capcha test during cathedral
    _cath_log_INFO ("Disable CAPCHA detection" & @CRLF)
    $enablePeriodicCapchaTest = False

    ; Full / Boss only management
    If $boss_run_param then
      ; Boss only management
      $boss_run = False ; Must be set to false before CathedralDetectMap
      $loop = 3
      Do
        ; Identify the kind of map for boss only mode
        If CathedralDetectMap( $mapId ) Then
          Return True
        EndIf
        If $mapId == 255 Then
          ; Map not identified, retry ...
          If _Interrupt_Sleep(300) Then
            Return True
          Endif
          $loop = $loop - 1
        EndIf
      Until $mapId <> 255 or $loop == 0
      If $mapId == 255 Then
        ; Error on map detection / start point detection => Disable Boss mode for this level
      Else
        ; Map detected, boss mode granted for this floor !
        $boss_run = True
        _cath_log_INFO ("Map id = " & $mapId & @CRLF)
      Endif
    Else
      ; Full cathedrale mode
      $boss_run = False
    EndIf

    $stepId = 0

    ; Loop for map steps
		For $step = 0 to 20

			If _Cathedral_Interrupt_Sleep(100) Then
				Return True
			Endif

			Local $x, $y, $isBoss

      ; Test several time "Find Next" before leaving  stage / quest, some times animation makes it fail once ...
      $next_step_check = 3
      Do
        If FindNextStep($x, $y, $isBoss) Then
          ; Found a waypoint !!!
          Local $timer = TimerInit()
          do
            ; Select waypoint
            MouseClick("primary", $x, $y, 1, 1)
            _cath_log_TRACE("CathedralLoop: way point click" & @CRLF)
            $attempt = 5
            Do
              If _Interrupt_Sleep(100) Then
                Return True
              Endif
              $color = PixelGetColor($menuCheck[0], $menuCheck[1])
              $attempt = $attempt - 1
            until $color == $menuCheck[2] or $attempt == 0

          until $color == $menuCheck[2] or TimerDiff($timer) > 5000 ;Wait for menu or 15s loop timeout

          If TimerDiff($timer) > 5000 and $next_step_check == 1 Then
            ; Error on spot selection => Disable Boss mode for this level
            _cath_log_ERROR("Can't select the spot, switch off boss mode !!!" & @CRLF)
            ;TEMP
            PrintGUIStatus("Can't select the spot, switch off boss mode ! ")
            $boss_run = False

            ; Repeat spot search to move on
            FindNextStep($x, $y, $isBoss)
          EndIf

          Local $found = False

          ; Test several time pixel color before, some times it hangs ...
          $retry = 3
          Do
            $retry = $retry - 1

            ;MouseMove(700, 800, 1)

            ;�cran baston
            If ($found == False) Then
              ;screenshot
              ;Local $aRect[4] = [800, 70, 400, 60]
              ;$img = _OpenCV_GetDesktopScreenMat($aRect)
              ;
              ;;�cran baston
              ;If UBound(_OpenCV_FindTemplate($img, $menu_fight, $threshold)) Then
              ;  _cath_log_INFO ("Fight screen." & @CRLF)
              ;  If CathedralFight($isBoss) Then
              ;    Return True
              ;  Endif
              ;  $found = True
              ;EndIf
              
              ;screenshot
              Local $aRect[4] = [125, 680, 1415-125, 875-680]
              $img = _OpenCV_GetDesktopScreenMat($aRect)

              ;�cran baston
              If UBound(_OpenCV_FindTemplate($img, $menu_fight, $threshold)) Then
                _cath_log_INFO ("Fight screen." & @CRLF)
                If CathedralFight($isBoss) Then
                  Return True
                Endif
                $found = True
              EndIf
            EndIf

            If ($found == False) Then
              ;�cran sacrifice
              If UBound(_OpenCV_FindTemplate($img, $menu_sacrifice, $threshold)) Then
                _cath_log_INFO ("Sacrifice screen." & @CRLF)
                If Sacrifice() Then
                  Return True
                Endif
                $found = True
              EndIf
            EndIf

            ;�cran choix
            If ($found == False) Then
              If UBound(_OpenCV_FindTemplate($img, $menu_choice, $threshold)) Then
                _cath_log_INFO ("Choice screen." & @CRLF)
                If Choice($doDangerousGame) Then
                  Return True
                Endif
                $found = True
              EndIf
            EndIf

            ;�cran camp
            If ($found == False) Then
              If UBound(_OpenCV_FindTemplate($img,$menu_camp, $threshold)) Then
                _cath_log_INFO ("Camp screen." & @CRLF)
                If Camp() Then
                  Return True
                Endif
                $found = True
              EndIf
            EndIf

            ;�cran ajout perso
            If ($found == False) Then
              If UBound(_OpenCV_FindTemplate($img,$menu_clone, $threshold)) Then
                _cath_log_INFO ("Add ally screen." & @CRLF)
                If Ally() Then
                  Return True
                Endif
                $found = True
              EndIf
            EndIf

            ; End loop if menu found
            If $found == True Then
              $retry = 0
              $stepId = $stepId +1
            EndIf

            ; Handle hang with specific move
            If $retry > 0 Then
              If _Interrupt_Sleep(150) Then
                Return True
              Endif
            EndIf

          Until $retry = 0

          If $found == False Then
            ; We may be in another spot menu, try to leave it ...
            MouseClick("primary", $closeWindowButton[0], $closeWindowButton[1], 1, 1); Click on red cross
            _cath_log_TRACE ("CathedralLoop: Nothing found, try closing menu." & @CRLF)
          EndIf

          $next_step_check = 0
        Else
          ; No waypoint found !!!
          If $next_step_check > 0 Then
            ; Check again ...
            $next_step_check = $next_step_check - 1
            If _Interrupt_Sleep(500) Then
              Return True
            Endif
          Endif

          ; No waypoint found after retries !!!
          If $next_step_check == 0 Then

            ; ==> Change LEVEL
            _cath_log_INFO ("Nothing found Next level" & @CRLF)
            $color = PixelGetColor($nextStageButton[0], $nextStageButton[1])
            If $color == $nextStageButton[2] or $color == $nextStageButton[3] Then
              ; Go to next stage
              _cath_log_INFO ("Next Stage" & @CRLF)
              Do
                ; Click on next stage button
                MouseClick("primary", $nextStageButton[0], $nextStageButton[1], 1, 1)
                $color = PixelGetColor($nextStageButton[0], $nextStageButton[1])
              until $color <> $nextStageButton[2] and $color <> $nextStageButton[3]
              If _Interrupt_Sleep(300) Then
                Return True
              Endif

              ; Check no waypoint missed message
              $color = PixelGetColor($red_button_no_diamond[0], $red_button_no_diamond[1])
              If $color == $red_button_no_diamond[2] or $color == $red_button_no_diamond[3] or $color == $red_button_no_diamond[4] Then
                if $boss_run == False Then
                  ; Some waypoints missed ... Click no and go back
                  _cath_log_ERROR ("Net Stage issue, still some waypoints, aborting next stage" & @CRLF)
                EndIf
                ; If boss run then, don't care
                If $Leave_stage_attempts < 3 and $boss_run == False Then
                  $color_bef = PixelGetColor($red_button_no_diamond[0], $red_button_no_diamond[1])
                  Do
                    MouseClick("primary", $red_button_no_diamond[0], $red_button_no_diamond[1], 1, 1)
                    If _Interrupt_Sleep(10) Then
                      Return True
                    Endif
                    $color_after = PixelGetColor($red_button_no_diamond[0], $red_button_no_diamond[1])
                  until $color_bef <> $color_after

                  $Leave_stage_attempts = $Leave_stage_attempts + 1
                Else
                  ; Can't clear floor for unknown reason, switch to next floor anyway ...
                  ; Click Yes and move on
                  $color_bef = PixelGetColor($no_tool[0], $no_tool[1])
                  Do
                    MouseClick("primary", $no_tool[0], $no_tool[1], 1, 1)
                    If _Interrupt_Sleep(10) Then
                      Return True
                    Endif
                    $color_after = PixelGetColor($no_tool[0], $no_tool[1])
                  until $color_bef <> $color_after
                  if $boss_run == False Then
                    PrintGUIStatus("Next Stage issue, still some waypoints, moving to next stage")
                  EndIf
                  ; Moving on new stage now
                  $step = 20
                EndIf
                If _Cathedral_Interrupt_Sleep(1000) Then
                  Return True
                Endif
              Else
                ; Moving on new stage now
                $step = 20

                If _Cathedral_Interrupt_Sleep(1000) Then
                  Return True
                Endif

              EndIf
            ElseIf PixelGetColor($endQuestButton[0], $endQuestButton[1]) == $endQuestButton[2] Then

              ; ==> End quest !!!
              _cath_log_INFO ("End of quest" & @CRLF)
              $color_bef = PixelGetColor($endQuestButton[0], $endQuestButton[1])
              Do
                MouseClick("primary", $endQuestButton[0], $endQuestButton[1], 1, 1)
                If _Interrupt_Sleep(10) Then
                  Return True
                Endif
                $color_after = PixelGetColor($endQuestButton[0], $endQuestButton[1])
              until $color_bef <> $color_after

              If $boss_run == True Then
                ; Wait for confirmation pop-up
                Do
                  If _Interrupt_Sleep(10) Then
                    Return True
                  Endif
                  $color = PixelGetColor($no_tool[0], $no_tool[1])
                  _cath_log_TRACE ("End of quest: wait pop-up" & @CRLF)
                until $color == $no_tool[7]
                ; Click Yes and move on
                $color_bef = PixelGetColor($no_tool[0], $no_tool[1])
                Do
                  MouseClick("primary", $no_tool[0], $no_tool[1], 1, 1)
                  _cath_log_TRACE ("End of quest: wait pop-up to close" & @CRLF)
                  If _Interrupt_Sleep(10) Then
                    Return True
                  Endif
                  $color_after = PixelGetColor($no_tool[0], $no_tool[1])
                until $color_bef <> $color_after
              EndIF

              Do
                ; CLick pour accelerer le recap
                MouseClick("primary", $hero_spell[0],  $hero_spell[1], 1, 1)
                If _Interrupt_Sleep(10) Then
                  Return True
                Endif

                ; Wait for end confirm button
                $color = PixelGetColor($endQuestConfirm[0], $endQuestConfirm[1])
              Until $color == $endQuestConfirm[2]

              ; Click on the button & wait load screen
              Do
                MouseClick("primary", $endQuestConfirm[0], $endQuestConfirm[1], 1, 1)
                If _Interrupt_Sleep(10) Then
                  Return True
                Endif
                $color = PixelGetColor($endQuestConfirm[0], $endQuestConfirm[1])
              until $color <> $endQuestConfirm[2]
              
              ; Enable periodic capcha test before cathe restart
              _cath_log_INFO ("Enable CAPCHA detection" & @CRLF)
              $enablePeriodicCapchaTest = True

              Do
                If _Interrupt_Sleep(100) Then
                  Return True
                Endif
              Until is_cathedral_screen()

              ; Manage extra clicks for rune event
              $isRuneEvent = (IniRead(@ScriptDir & "/H2C2.ini", "params", "runeevent", "0") == "1")
              If $isRuneEvent Then ;wait for cards
                For $i = 0 To 15
                  MouseClick("primary",111, 175, 1, 1)
                  If _Interrupt_Sleep(500) Then
                    _Function_Finished()
                    Return
                  Endif
                Next
              EndIf

              $step = 20
              $stage = 5
            EndIf
          EndIf
        EndIf
      Until $next_step_check = 0
		Next
	Next
	;action ou combat
	;s�lection d'un bonus
	;v�rification du bouton �tage/fin
	Return False
EndFunc

Func CathedralDetectMap(ByRef $mapId)
	;opencv screenshot
	Local $aRect = WinGetPos($hwnd)
	Local $img
  Local $boss_idx
  Local $x, $y, $isBoss
  Local $aMatches_boss

  $mapId = 255

  ;Wait until detecting the start position (level transition wait)
  ;Descriminate start position (left or right side)
  Do
    $found = FindNextStep($x, $y, $isBoss)
    If _Interrupt_Sleep(100) Then
      Return True
    EndIf
  Until( $found )
  _cath_log_INFO("CathedralDetectMap: Start position, x=" & $x & ", y=" & $y & @CRLF)

	;find hidden bosses
  $img = _OpenCV_GetDesktopScreenMat($aRect)
	$aMatches_boss = _OpenCV_FindTemplate($img, $hidden_boss, $threshold)
  ;For $boss_idx = 0 to UBound($aMatches_boss)-1
  ;  _cath_log_INFO("CathedralDetectMap: Boss #" & $boss_idx & ", x=" & $aMatches_boss[$boss_idx][0] & ", y=" & $aMatches_boss[$boss_idx][1] & @CRLF)
  ;Next

	If UBound($aMatches_boss) == 4 Then
    If $map_start_spot_position[8][0] == $x and $map_start_spot_position[8][1] == $y Then
      ; Left side start
      _cath_log_INFO("Found a 4 bosses map, left start" & @CRLF)
      $mapId = 8
    ElseIf $map_start_spot_position[9][0] == $x and $map_start_spot_position[9][1] == $y Then
      ; Right side start
      _cath_log_INFO("Found a 4 bosses map, right start" & @CRLF)
      $mapId = 9
    Else
      _cath_log_INFO("Error 4 bosses map, but start point not found !!!" & @CRLF)
      $mapId = 255
      Return False
    EndIf

  ElseIf UBound($aMatches_boss) == 3 Then
    _cath_log_INFO("Found a 3 bosses map, boss coord: (" & $aMatches_boss[0][0] & "," & $aMatches_boss[0][1] & "),(" & $aMatches_boss[1][0] & "," & $aMatches_boss[1][1] & "),(" & $aMatches_boss[2][0] & "," & $aMatches_boss[2][1] & ")" & @CRLF)
    If $map3boss[0][0][0] == $aMatches_boss[0][0] and $map3boss[0][0][1] == $aMatches_boss[0][1] and $map3boss[0][1][0] == $aMatches_boss[1][0] and $map3boss[0][1][1] == $aMatches_boss[1][1] and $map3boss[0][2][0] == $aMatches_boss[2][0] and $map3boss[0][2][1] == $aMatches_boss[2][1] Then
      ; 3 boss circle template
      If $map_start_spot_position[6][0] == $x and $map_start_spot_position[6][1] == $y Then
        ; Left side start
        _cath_log_INFO("Found a 3 bosses circle map, left start" & @CRLF)
        $mapId = 6
      ElseIf $map_start_spot_position[7][0] == $x and $map_start_spot_position[7][1] == $y Then
        ; Right side start
        _cath_log_INFO("Found a 3 bosses circle map, right start" & @CRLF)
        $mapId = 7
      Else
        _cath_log_INFO("Found a 3 bosses circle map, but start point not found !!!" & @CRLF)
        $mapId = 255
        Return False
      EndIf
    ElseIf $map3boss[2][0][0] == $aMatches_boss[0][0] and $map3boss[2][0][1] == $aMatches_boss[0][1] and $map3boss[2][1][0] == $aMatches_boss[1][0] and $map3boss[2][1][1] == $aMatches_boss[1][1] and $map3boss[2][2][0] == $aMatches_boss[2][0] and $map3boss[2][2][1] == $aMatches_boss[2][1] Then
      If $map_start_spot_position[4][0] == $x and $map_start_spot_position[4][1] == $y Then
        ; 3 boss template
        ; Left side start
        _cath_log_INFO("Found a 3 bosses map, left start" & @CRLF)
        $mapId = 4
      Else
        _cath_log_INFO("Found a 3 bosses map left start, but start point not found !!!" & @CRLF)
        $mapId = 255
        Return False
      EndIf
    ElseIf $map3boss[1][0][0] == $aMatches_boss[0][0] and $map3boss[1][0][1] == $aMatches_boss[0][1] and $map3boss[1][1][0] == $aMatches_boss[1][0] and $map3boss[1][1][1] == $aMatches_boss[1][1] and $map3boss[1][2][0] == $aMatches_boss[2][0] and $map3boss[1][2][1] == $aMatches_boss[2][1] Then
      If $map_start_spot_position[5][0] == $x and $map_start_spot_position[5][1] == $y Then
        ; 3 boss template
        ; Right side start
        _cath_log_INFO("Found a 3 bosses map, right start" & @CRLF)
        $mapId = 5
      Else
        _cath_log_INFO("Found a 3 bosses map left start, but start point not found !!!" & @CRLF)
        $mapId = 255
        Return False
      EndIf
    Else
      _cath_log_INFO("Found a 3 bosses map, not reconized :(" & @CRLF)
      $mapId = 255
      Return False
    EndIf

  ElseIf UBound($aMatches_boss) == 2 Then
    _cath_log_INFO("Found a 2 bosses map, boss coord: ("&$aMatches_boss[0][0]&","&$aMatches_boss[0][1]&"),("&$aMatches_boss[1][0]&","&$aMatches_boss[1][1]&")" & @CRLF)
    If $map2boss[0][0][0] == $aMatches_boss[0][0] and $map2boss[0][0][1] == $aMatches_boss[0][1] and $map2boss[0][1][0] == $aMatches_boss[1][0] and $map2boss[0][1][1] == $aMatches_boss[1][1] Then
      ; 2 boss spirale template
      If $map_start_spot_position[2][0] == $x and $map_start_spot_position[2][1] == $y Then
        ; Left side start
        _cath_log_INFO("Found a 2 bosses spirale map, left start" & @CRLF)
        $mapId = 2
      ElseIf $map_start_spot_position[3][0] == $x and $map_start_spot_position[3][1] == $y Then
        ; Right side start
        _cath_log_INFO("Found a 2 bosses spirale map, right start" & @CRLF)
        $mapId = 3
      Else
        _cath_log_INFO("Found a 2 bosses spirale map, but start point not found !!!" & @CRLF)
        $mapId = 255
        Return False
      EndIf
    ElseIf $map2boss[1][0][0] == $aMatches_boss[0][0] and $map2boss[1][0][1] == $aMatches_boss[0][1] and $map2boss[1][1][0] == $aMatches_boss[1][0] and $map2boss[1][1][1] == $aMatches_boss[1][1] Then
      ; 2 boss template
      If $map_start_spot_position[0][0] == $x and $map_start_spot_position[0][1] == $y Then
        ; Left side start
        _cath_log_INFO("Found a 2 bosses map, left start" & @CRLF)
        $mapId = 0
      ElseIf $map_start_spot_position[1][0] == $x and $map_start_spot_position[1][1] == $y Then
        ; Right side start
        _cath_log_INFO("Found a 2 bosses map, right start" & @CRLF)
        $mapId = 1
      Else
        _cath_log_INFO("Found a 2 bosses map, but start point not found !!!" & @CRLF)
        $mapId = 255
        Return False
      EndIf
    Else
      _cath_log_INFO("Found a 2 bosses map, not reconized :(" & @CRLF)
      $mapId = 255
      Return False
    EndIf

  Else
    _cath_log_ERROR("Unknown map !!!" & @CRLF)
    $mapId = 255
    Return False
	EndIf

	Return False
EndFunc

Func FindNextStep(ByRef $x, ByRef $y, ByRef $isBoss)
  $isBoss = False

  If $boss_run Then
    ; Boss only mode
    If $stepId >= 11 or $mapId == 255 Then
      _cath_log_TRACE("FindNextStep boss error: step=" & $stepId & ", mapId=" & $mapId & @CRLF)
      return False
    Else
      $x = $map_spots_boss_only[$mapId][$stepId][0]
      $y = $map_spots_boss_only[$mapId][$stepId][1]
      $isBoss = $map_spots_boss_only[$mapId][$stepId][2]

      ; End of table delimiter detection
      If $x == 0 and $y == 0 Then
        Return False
      EndIf

      _cath_log_INFO("FindNextStep boss: step=" & $stepId & ", x=" & $x & ", y=" & $y & ", isBoss=" & $isBoss & @CRLF)

      ; Check that we detect the spot or wait (workaround for white moving labels ...)
      $x_bis = $x
      $y_bis = $y
      $isBoss_bis = $isBoss
      $not_found_loop = 0
      Do
        _cath_log_TRACE("FindNextStep boss: loop" & @CRLF)
        $found = FindNextStep_ROI($x_bis, $y_bis, $isBoss_bis, $x-100, $y-100, 200, 200)
        If $found == False Then
          $not_found_loop = $not_found_loop + 1
          _cath_log_ERROR ("FindNextStep boss: Spot not found, #" & $not_found_loop & @CRLF)
          If _Interrupt_Sleep(100) Then
            Return True
          Endif;
        EndIf
        If $not_found_loop == 5 Then
          ; We may be in another spot menu, try to leave it ...
          MouseClick("primary", $closeWindowButton[0], $closeWindowButton[1], 1, 1); Click on red cross
          _cath_log_ERROR ("FindNextStep boss: Spot not found, try closing menu." & @CRLF)
          $not_found_loop = 0
        EndIf
      Until $found

      Return True
    EndIf
  Else
    ; Full map mode
    Local $aRect = WinGetPos($hwnd)
    return FindNextStep_ROI($x, $y, $isBoss, $aRect[0], $aRect[1], $aRect[2], $aRect[3])
  EndIf

	Return False


  ;  ;opencv screenshot
  ;  Local $aRect = WinGetPos($hwnd)
  ;  Local $img = _OpenCV_GetDesktopScreenMat($aRect)
  ;
  ;  ;find match fight
  ;  Local $aMatches = _OpenCV_FindTemplate($img, $fight, $threshold)
  ;  If UBound($aMatches) Then
  ;    _cath_log("Found a fight" & @CRLF)
  ;    $x = $aMatches[0][0] + int( $fight.width / 2 )
  ;    $y = $aMatches[0][1] + int( $fight.height / 2 )
  ;    Return True
  ;  EndIf
  ;
  ;  ;find match camp
  ;  $aMatches = _OpenCV_FindTemplate($img, $camp, $threshold)
  ;
  ;  If UBound($aMatches) Then
  ;    _cath_log("Found a camp" & @CRLF)
  ;    $x = $aMatches[0][0] + int( $camp.width / 2 )
  ;    $y = $aMatches[0][1] + int( $camp.height / 2 )
  ;    Return True
  ;  EndIf
  ;
  ;  ;find match dice
  ;  $aMatches = _OpenCV_FindTemplate($img, $dice, $threshold)
  ;  If UBound($aMatches) Then
  ;    _cath_log("Found a dice" & @CRLF)
  ;    $x = $aMatches[0][0] + int( $dice.width / 2 )
  ;    $y = $aMatches[0][1] + int( $dice.height / 2 )
  ;    Return True
  ;  EndIf
  ;
  ;  ;find match boss
  ;  $aMatches = _OpenCV_FindTemplate($img, $boss, $threshold)
  ;  If UBound($aMatches) Then
  ;    _cath_log("Found a boss" & @CRLF)
  ;    $x = $aMatches[0][0] + int( $boss.width / 2 )
  ;    $y = $aMatches[0][1] + int( $boss.height / 2 )
  ;    $isBoss = True
  ;    Return True
  ;  EndIf
  ;
  ;  ;find match robe
  ;  $aMatches = _OpenCV_FindTemplate($img, $robe, $threshold)
  ;  If UBound($aMatches) Then
  ;    _cath_log("Found a robe" & @CRLF)
  ;    $x = $aMatches[0][0] + int( $robe.width / 2 )
  ;    $y = $aMatches[0][1] + int( $robe.height / 2 )
  ;    Return True
  ;  EndIf
  ;
  ;  ;find match fire
  ;  $aMatches = _OpenCV_FindTemplate($img, $fire, $threshold)
  ;  If UBound($aMatches) Then
  ;    _cath_log("Found a hot level" & @CRLF)
  ;    $x = $aMatches[0][0] + int( $fire.width / 2 )
  ;    $y = $aMatches[0][1] + int( $fire.height / 2 )
  ;    Return True
  ;  EndIf
  ;
  ;  ;find match guardian
  ;  $aMatches = _OpenCV_FindTemplate($img, $guardian, $threshold)
  ;  If UBound($aMatches) Then
  ;    _cath_log("Found a guardian" & @CRLF)
  ;    $x = $aMatches[0][0] + int( $guardian.width / 2 )
  ;    $y = $aMatches[0][1] + int( $guardian.height / 2 )
  ;    $isBoss = True
  ;    Return True
  ;  EndIf
  ;
  ;  _cath_log("Nothing found" & @CRLF)
  ;EndIf
  ;
	;Return False
EndFunc

Func FindNextStep_ROI(ByRef $x, ByRef $y, ByRef $isBoss, $start_x, $start_y, $width, $height)

  ;opencv screenshot
  Local $aRect[4] = [$start_x, $start_y, $width, $height]
  Local $img = _OpenCV_GetDesktopScreenMat($aRect)

  $isBoss = False

  ;find match fight
  Local $aMatches = _OpenCV_FindTemplate($img, $fight, $threshold)
  If UBound($aMatches) Then
    _cath_log_TRACE("Found a fight" & @CRLF)
    $x = $aMatches[0][0] + int( $fight.width / 2 )
    $y = $aMatches[0][1] + int( $fight.height / 2 )
    Return True
  EndIf

  ;find match camp
  $aMatches = _OpenCV_FindTemplate($img, $camp, $threshold)

  If UBound($aMatches) Then
    _cath_log_TRACE("Found a camp" & @CRLF)
    $x = $aMatches[0][0] + int( $camp.width / 2 )
    $y = $aMatches[0][1] + int( $camp.height / 2 )
    Return True
  EndIf

  ;find match dice
  $aMatches = _OpenCV_FindTemplate($img, $dice, $threshold)
  If UBound($aMatches) Then
    _cath_log_TRACE("Found a dice" & @CRLF)
    $x = $aMatches[0][0] + int( $dice.width / 2 )
    $y = $aMatches[0][1] + int( $dice.height / 2 )
    Return True
  EndIf

  ;find match boss
  $aMatches = _OpenCV_FindTemplate($img, $boss, $threshold)
  If UBound($aMatches) Then
    _cath_log_TRACE("Found a boss" & @CRLF)
    $x = $aMatches[0][0] + int( $boss.width / 2 )
    $y = $aMatches[0][1] + int( $boss.height / 2 )
    $isBoss = True
    Return True
  EndIf

  ;find match robe
  $aMatches = _OpenCV_FindTemplate($img, $robe, $threshold)
  If UBound($aMatches) Then
    _cath_log_TRACE("Found a robe" & @CRLF)
    $x = $aMatches[0][0] + int( $robe.width / 2 )
    $y = $aMatches[0][1] + int( $robe.height / 2 )
    Return True
  EndIf

  ;find match fire
  $aMatches = _OpenCV_FindTemplate($img, $fire, $threshold)
  If UBound($aMatches) Then
    _cath_log_TRACE("Found a hot level" & @CRLF)
    $x = $aMatches[0][0] + int( $fire.width / 2 )
    $y = $aMatches[0][1] + int( $fire.height / 2 )
    Return True
  EndIf

  ;find match guardian
  $aMatches = _OpenCV_FindTemplate($img, $guardian, $threshold)
  If UBound($aMatches) Then
    _cath_log_TRACE("Found a guardian" & @CRLF)
    $x = $aMatches[0][0] + int( $guardian.width / 2 )
    $y = $aMatches[0][1] + int( $guardian.height / 2 )
    $isBoss = True
    Return True
  EndIf

  _cath_log_TRACE("Nothing found" & @CRLF)

	Return False
EndFunc



Func CathedralFight($isBoss)

	LaunchFight()

  If _Interrupt_Sleep(500) Then
		Return True
	Endif;

	; si le bouton de confirmation team incomplete s'affiche
  $color1 = PixelGetColor($notFullSquadbutton[0], $notFullSquadbutton[1])
	If ($color1 == $notFullSquadbutton[2] ) Then
    MouseClick("primary", $notFullSquadbutton[0], $notFullSquadbutton[1], 1, 1) ; cancel

		If RezFighters() Then
      _cath_log_ERROR (@TAB & @TAB & "CathedralFight: RezFighters exit " & @CRLF)
			Return True
		EndIf
		If LaunchFight() Then
      _cath_log_ERROR (@TAB & @TAB & "CathedralFight: LaunchFight exit " & @CRLF)
			Return True
		EndIf
	EndIf

	; on attend la fin de l attaque
  ; Dummy clic on bosses only -> Event reward
	If fight($isBoss, false) Then
    _cath_log_ERROR (@TAB & @TAB & "CathedralFight: fight function exit " & @CRLF)
		Return True
	EndIf
	If _Cathedral_Interrupt_Sleep(500) Then
		Return True
	Endif

	_cath_log_INFO (@TAB & @TAB & "Fin de l'attaque" & @CRLF)
	If end_fight($isBoss) Then
    _cath_log_ERROR (@TAB & @TAB & "CathedralFight: end_fight function exit " & @CRLF)
		Return True
	EndIf
	Local $foundBlessing
	If WaitBlessing($foundBlessing) Then
		Return True
	EndIf
	If _Interrupt_Sleep(200) Then
		Return True
	Endif

	If $foundBlessing Then
		If SelectBlessing() Then
      _cath_log_ERROR (@TAB & @TAB & "CathedralFight: SelectBlessing exit " & @CRLF)
			Return True
		EndIf
	EndIf

	If _Cathedral_Interrupt_Sleep(2000) Then
		Return True
	Endif;
EndFunc

Func FillCathedralSquad()

  _cath_log_TRACE (@TAB & @TAB & "FillCathedralSquad " & @CRLF)

  Do
    $color1 = PixelGetColor($deployBest_Fight4Check[0], $deployBest_Fight4Check[1])
    $color2 = PixelGetColor($deployBest_Fight5Check[0], $deployBest_Fight5Check[1])
    $test = ( $color1 == $deployBest_Fight4Check[2] or $color1 == $deployBest_Fight4Check[3] ) or ( $color2 == $deployBest_Fight5Check[2] or $color2 == $deployBest_Fight5Check[3] )
    $test_fail = ( $color1 == 0xFFFFFF ) and ( $color2 == 0xFFFFFF )
    _cath_log_TRACE (@TAB & @TAB & "FillCathedralSquad : color Fight4=0x" & hex($color1, 6) & " ,color Fight5=0x" & hex($color2, 6) & @CRLF )
    If $test or $test_fail Then
      ; Missing fighters
      MouseClick("primary", $deployBest[0], $deployBest[1], 1, 1)
      If _Interrupt_Sleep(200) Then
        Return True
      Endif
    EndIf
  Until not $test

	Return False

EndFunc

Func LaunchFight()
  _cath_log_TRACE(@TAB & @TAB & "LaunchFight" & @CRLF)

  ; Wait for green fight button
  _cath_log_TRACE (@TAB & @TAB & "LaunchFight: wait fight button ! " & @CRLF)
  $color = PixelGetColor($fightButton[0], $fightButton[1])
  While $color <> $fightButton[2] and $color <> $fightButton[3]
    $color = PixelGetColor($fightButton[0], $fightButton[1])
    If _Interrupt_Sleep(25) Then
      Return True
    Endif
    _cath_log_TRACE ("LaunchFight: loop1" & @CRLF)
  WEnd

  do
    If FillCathedralSquad() Then
      Return True
    Endif

    If _Interrupt_Sleep(200) Then
      Return True
    Endif

    _cath_log_TRACE(@TAB & @TAB & "LaunchFight loop" & @CRLF)
    If $isQuickFight Then
      MouseClick("primary", $quickFightButton[0], $quickFightButton[1], 1, 1)
    Else
      MouseClick("primary", $fightButton[0], $fightButton[1], 1, 1)
    Endif

    If _Interrupt_Sleep(100) Then
      Return True
    Endif

    ;no tools. Fly, you tools
    $color1 = PixelGetColor($no_tool[0], $no_tool[1])
    $color2 = PixelGetColor($no_tool[4], $no_tool[5])
    If ($color1 == $no_tool[2] or $color1 == $no_tool[3]) And ($color2 == $no_tool[6] or $color2 == $no_tool[7]) Then
      MouseClick("primary", $no_tool[0], $no_tool[1] , 1) ; ouioui, ils vont gagner de toute fa�on
      If _Interrupt_Sleep(300) Then
        return True
      Endif
    EndIf

    ;incomplete team
    $color1 = PixelGetColor($incomplete_team[0], $incomplete_team[1])
    $color2 = PixelGetColor($incomplete_team[4], $incomplete_team[5])
    If ($color1 == $incomplete_team[2] or $color1 == $incomplete_team[3]) And ($color2 == $incomplete_team[6] or $color2 == $incomplete_team[7]) Then
      MouseClick("primary", $incomplete_team[0], $incomplete_team[1] , 1) ; ouioui, ils vont gagner de toute fa�on
      If _Interrupt_Sleep(300) Then
        return True
      Endif
    EndIf

    $loop = false
    If $isQuickFight Then
      $color = PixelGetColor($quickFightButton[0], $quickFightButton[1])
      If $color <> $quickFightButton[2] and $color <> $quickFightButton[3] Then
        $loop = true
      EndIf
    Else
      $color = PixelGetColor($fightButton[0], $fightButton[1])
      If $color <> $fightButton[2] and $color <> $fightButton[3] then
        $loop = true
      EndIf
    Endif

  until $loop

  _cath_log_TRACE(@TAB & @TAB & "LaunchFight loop end" & @CRLF)

	Return False
EndFunc

Func RezFighters()

  _cath_log_TRACE(@TAB & @TAB & "RezFighters" & @CRLF)

	Local $nbFightersToRez = IniRead(@ScriptDir & "/H2C2.ini", "game", "cathedralRez", "0")
	If $nbFightersToRez == 0 Then
		Return True ; stop
	EndIf

	;trouver le 1er
	Local $x, $y
	If FindFirstWarrior($x, $y) Then
		Return True
	Endif

	;pour nombre de combattants param�tr� - 1
	For $i = 0 to $nbFightersToRez-1
		;si bouton rez vert
		$color1 = PixelGetColor($rezButton[0], $rezButton[1])
		If $color1 == $rezButton[2] or $color1 == $rezButton[3] or $color1 == $rezButton[4] or $color1 == $rezButton[5] or $color1 == $rezButton[6] Then
			MouseClick("primary", $rezButton[0], $rezButton[1], 1)
			If _Interrupt_Sleep(300) Then
				Return True
			Endif
			$color2 = PixelGetColor($rezConfirmButton[0], $rezConfirmButton[1])
			If $color2 == $rezConfirmButton[2] or $color2 == $rezConfirmButton[3] Then
				MouseClick("primary", $rezConfirmButton[0], $rezConfirmButton[1], 1)
				If _Interrupt_Sleep(300) Then
					Return True
				Endif
			Else
				_cath_log_ERROR("RezFighters: Pas de bouton confirmation rez...")
			EndIf
		Else
			_cath_log_ERROR("RezFighters: Pas de bouton rez...")
		EndIf

		; clic suivant
		MouseClick("primary", $nextRezButton[0], $nextRezButton[1], 1)
		If _Cathedral_Interrupt_Sleep(2000) Then
			Return True
		Endif
	Next

	;sors de l'�cran
	MouseClick("primary", 165, 524, 1)
	If _Cathedral_Interrupt_Sleep(1000) Then
		Return True
	Endif

	Return False

EndFunc

;todo find warrior with parameter start, stop step height, offset
Func FindFirstWarrior(ByRef $xx, ByRef $yy)
	For $i = $cathHealthBarCoords[0] to $cathHealthBarCoords[1] Step 10
		$color = PixelGetColor($i, $cathHealthBarCoords[2])
		;MouseMove($i, $warriorsHealthBars[1], 1)
		If $color == $cathHealthBarColors[0] or $color == $cathHealthBarColors[1] or $color == $cathHealthBarColors[2] or $color == $cathHealthBarColors[3] Then

			$xx = $i+30
			$yy = $warriorsHealthBars[1]-100
			;ouvre l'�cran perso
			MouseClick("primary", $xx, $yy, 1, 1)
			_wait_warrior_screen() ;cf dungeon
			ExitLoop
		EndIf
	Next
	;n'a pas trouv� de combattant. On sonne? On gueule?
	;on les ressuscite si l'option est coch�e (TODO option :D )
	Return False

EndFunc

Func WaitBlessing(ByRef $found)

	;$found = False
  ;Local $threshold = 0.95
	;Local $aRect = WinGetPos($hwnd)
	Local $iBegin = TimerInit()
	_cath_log_TRACE(@TAB & @TAB & "Wait Blessing..."& @CRLF)

  ;While FindImage("cathedral_blessing_menu.png") == False and TimerDiff($iBegin) < 15000
  While PixelGetColor(756, 77) <> 0x00EAFF and TimerDiff($iBegin) < 15000
    If _Interrupt_Sleep(100) Then
      Return True
    Endif;
    _cath_log_TRACE ("WaitBlessing: loop1" & @CRLF)
  WEnd
  If TimerDiff($iBegin) >= 15000 Then
    _cath_log_ERROR (@TAB & @TAB & "CathedralFight: WaitBlessing timeout" & @CRLF)
    PrintGUIStatus("CathedralFight: WaitBlessing timeout")
    $found = False
    Return True
  else
    $found = True
  EndIf

	Return False
EndFunc

Func SelectBlessing()
  do
    $choice = Random(0, 2, 1)
    Do
      MouseClick("primary", $blessings[$choice*2], $blessings[$choice*2+1], 1, 1)

      If _Interrupt_Sleep(50) Then
        Return True
      Endif;
      _cath_log_TRACE ("SelectBlessing: loop" & @CRLF)

    Until FindImage("cathedral_blessing_selected.png")

    If _Cathedral_Interrupt_Sleep(1000) Then
      Return True
    Endif;

    $color = PixelGetColor($confirmBlessing[0], $confirmBlessing[1])
    _cath_log_TRACE ("SelectBlessing: loop2, color=0x" & hex($color, 6) & @CRLF)
    If _Interrupt_Sleep(50) Then
      Return True
    Endif
  until $color == $confirmBlessing[2]
  Do
    MouseClick("primary", $confirmBlessing[0], $confirmBlessing[1], 1, 1)
    If _Interrupt_Sleep(50) Then
      Return True
    Endif
    $colorchoice1 = PixelGetColor($confirmBlessing[0], $confirmBlessing[1])
    _cath_log_TRACE ("SelectBlessing: loop3, color=0x" & hex($colorchoice1, 6) & @CRLF)
  Until $colorchoice1 <> $confirmBlessing[2] and $colorchoice1 <> $confirmBlessing[3]

	Return False
EndFunc



Func Choice($doDangerousGame)
  _cath_log_TRACE(@TAB & @TAB & "Choice" & @CRLF)

  If $doDangerousGame Then

    ; Click on choice
    Do
      MouseClick("primary", $choiceTicks[0], $choiceTicks[1], 1, 1) ;try it
      If _Interrupt_Sleep(100) Then
        Return True
      Endif

      $colorchoice1 = PixelGetColor($choiceTicks[0], $choiceTicks[1])
      _cath_log_TRACE ("Choice: loop1a, color =0x" & hex($colorchoice1, 6) & @CRLF)
    Until $colorchoice1 <> $choiceTicks[4] and $colorchoice1 <> $choiceTicks[5]

	Else

    ; Click on choice
    Do
      MouseClick("primary", $choiceTicks[2], $choiceTicks[3] , 1, 1) ;skip
      If _Interrupt_Sleep(100) Then
        Return True
      Endif

      $colorchoice2 = PixelGetColor($choiceTicks[2], $choiceTicks[3])
      _cath_log_TRACE ("Choice: loop1b, color =0x" & hex($colorchoice2, 6) & @CRLF)
    Until $colorchoice2 <> $choiceTicks[4] and $colorchoice2 <> $choiceTicks[5]

	EndIf

  ; Click on choice, until button disappear
  Do
    MouseClick("primary", $confirmButton[0], $confirmButton[1], 1, 1)
    If _Interrupt_Sleep(100) Then
      Return True
    Endif
    $colorchoice1 = PixelGetColor($confirmButton[0], $confirmButton[1])
    _cath_log_TRACE ("Choice: loop2, color =0x" & hex($colorchoice1, 6) & @CRLF)
  Until $colorchoice1 <> $confirmButton[2]

  If $doDangerousGame Then
    ; coin toss, wait for close button
    Do
      If _Interrupt_Sleep(300) Then
        Return True
      Endif
      $colorchoice1 = PixelGetColor($closeButton[0], $closeButton[1])
      _cath_log_TRACE ("Choice: loop3, color =0x" & hex($colorchoice1, 6) & @CRLF)
    Until $colorchoice1 == $closeButton[2] or $colorchoice1 == $closeButton[3]

    ; Click on button & wait button to disappear
    Do
      MouseClick("primary", $closeButton[0], $closeButton[1], 1, 1)
      If _Interrupt_Sleep(100) Then
        Return True
      Endif
      $colorchoice1 = PixelGetColor($closeButton[0], $closeButton[1])
      _cath_log_TRACE ("Choice: loop4, color =0x" & hex($colorchoice1, 6) & @CRLF)
    Until $colorchoice1 <> $closeButton[2] and $colorchoice1 <> $closeButton[3]

  EndIf

	Return False
EndFunc


Func Ally()
  _cath_log_TRACE(@TAB & @TAB & "Ally" & @CRLF)

  ; Wait for button
  Do
    $colorchoice1 = PixelGetColor($addFighterButton[0], $addFighterButton[1])
    _cath_log_TRACE ("Ally: loop1, color =0x" & hex($colorchoice1, 6) & @CRLF)
    If _Interrupt_Sleep(100) Then
      Return True
    Endif
  Until $colorchoice1 == $addFighterButton[2]

  ; Click on button & wait next menu
  Do
    MouseClick("primary", $addFighterButton[0], $addFighterButton[1] , 1, 1) ;get reward
    $colorchoice1 = PixelGetColor($closeButton[0], $closeButton[1])
    _cath_log_TRACE ("Ally: loop2, color =0x" & hex($colorchoice1, 6) & @CRLF)
    If _Interrupt_Sleep(100) Then
      Return True
    Endif
  Until $colorchoice1 == $closeButton[2] or $colorchoice1 == $closeButton[3]

  ; Click on button & wait button to disappear
  Do
    MouseClick("primary", $closeButton[0], $closeButton[1], 1, 1)
    If _Interrupt_Sleep(100) Then
      Return True
    Endif
    $colorchoice1 = PixelGetColor($closeButton[0], $closeButton[1])
    _cath_log_TRACE ("Ally: loop3, color =0x" & hex($colorchoice1, 6) & @CRLF)
  Until $colorchoice1 <> $closeButton[2] and $colorchoice1 <> $closeButton[3]

	Return False
EndFunc

Func Camp()
  _cath_log_TRACE(@TAB & @TAB & "Camp" & @CRLF)
  ; wait for menu readiness
  Do
    $colorchoice1 = PixelGetColor($campTicks[2], $campTicks[3])
    If _Interrupt_Sleep(100) Then
      Return True
    Endif
    _cath_log_TRACE ("Camp: loop1, color =0x" & hex($colorchoice1, 6) & @CRLF)
  Until $colorchoice1 == $campTicks[6] or $colorchoice1 == $campTicks[7]

  Do
    ; Click on selection
    Do
      MouseClick("primary", $campTicks[2], $campTicks[3], 1, 1) ;get reward
      If _Interrupt_Sleep(100) Then
        Return True
      Endif
      MouseClick("primary", 156, 150, 1, 1) ;dummy for deadlock fix
      If _Interrupt_Sleep(100) Then
        Return True
      Endif

      $colorchoice1 = PixelGetColor($campTicks[2], $campTicks[3])
      _cath_log_TRACE ("Camp: loop2, color =0x" & hex($colorchoice1, 6) & @CRLF)
    Until $colorchoice1 <> $campTicks[8]

    If _Interrupt_Sleep(100) Then
      Return True
    Endif
    _cath_log_TRACE ("Camp: loop3" & @CRLF)

    ; Click on confirm button
    MouseClick("primary", $confirmButton[0], $confirmButton[1] , 1, 1)

    ; Wait confirmation pop-up button
    $colorchoice1 = PixelGetColor($campConfirmButton[0], $campConfirmButton[1])
  Until $colorchoice1 == $campConfirmButton[2]

  Do
    ; Click on confirmation pop-up
    MouseClick("primary", $campConfirmButton[0], $campConfirmButton[1] , 1, 1)
    If _Interrupt_Sleep(100) Then
      Return True
    Endif

    ; Wait pop-up menu to disappear
    $colorchoice1 = PixelGetColor($closeButton[0], $closeButton[1])
    _cath_log_TRACE ("Camp: loop4, color =0x" & hex($colorchoice1, 6) & @CRLF)
  Until $colorchoice1 == $closeButton[2] or $colorchoice1 == $closeButton[3]

  ; Click on button & wait button to disappear
  Do
    MouseClick("primary", $closeButton[0], $closeButton[1], 1, 1)
    If _Interrupt_Sleep(100) Then
      Return True
    Endif
    $colorchoice1 = PixelGetColor($closeButton[0], $closeButton[1])
    _cath_log_TRACE ("Camp: loop5, color =0x" & hex($colorchoice1, 6) & @CRLF)
  Until $colorchoice1 <> $closeButton[2] and $colorchoice1 <> $closeButton[3]

	Return False
EndFunc

Func Sacrifice()
  _cath_log_TRACE(@TAB & @TAB & "Sacrifice" & @CRLF)

	;camp tick, sacrifice pass tick : same.
  ; wait for menu readiness
  Do
    $colorchoice1 = PixelGetColor($campTicks[4], $campTicks[5])
    _cath_log_TRACE ("Sacrifice: loop1" & @CRLF)
    If _Interrupt_Sleep(100) Then
      Return True
    Endif
  Until $colorchoice1 == $campTicks[6] or $colorchoice1 == $campTicks[7]

  Do
    Do
      ; Click on selection
      Do
        MouseClick("primary", $campTicks[4], $campTicks[5], 1, 1) ;skip
        If _Interrupt_Sleep(100) Then
          Return True
        Endif
        MouseClick("primary", 156, 150, 1, 1) ;dummy for deadlock fix
        If _Interrupt_Sleep(100) Then
          Return True
        Endif

        $colorchoice1 = PixelGetColor($campTicks[4], $campTicks[5])
        _cath_log_TRACE ("Sacrifice: loop2, color =0x" & hex($colorchoice1, 6) & @CRLF)
      Until $colorchoice1 <> $campTicks[8]

      ; Click on confirm button
      MouseClick("primary", $confirmButton[0], $confirmButton[1] , 1, 1)
      _cath_log_TRACE ("Sacrifice: loop3" & @CRLF)
      If _Interrupt_Sleep(100) Then
        Return True
      Endif

      ; Wait confirmation pop-up button
      $colorchoice1 = PixelGetColor($sacrificeConfirmButton[0], $sacrificeConfirmButton[1])
    Until $colorchoice1 == $sacrificeConfirmButton[2]

    ; Click on confirmation pop-up
    MouseClick("primary", $sacrificeConfirmButton[0], $sacrificeConfirmButton[1] , 1, 1)
    If _Interrupt_Sleep(100) Then
      Return True
    Endif
    _cath_log_TRACE ("Sacrifice: loop4" & @CRLF)

    ; Wait pop-up menu to disappear
    $colorchoice1 = PixelGetColor($closeButton[0], $closeButton[1])
  Until $colorchoice1 <> $closeButton[2] and $colorchoice1 <> $closeButton[3]

	Return False
EndFunc



Func CathedralStore()

	_cath_log_INFO("CathedralStore" & @CRLF)

	If initBlueStacks() Then
		_Function_Finished()
		Return
	EndIf


	While 1
		Local $x, $y
		If FindImageCoord($hwnd, "cathedral_chest_leg.png", $x, $y) Then

			_cath_log_TRACE("Find ("&$x&","&$y&")" & @CRLF)

			MouseClick("primary", $x, $y, 1)
			If _Interrupt_Sleep(1000) Then
				_Function_Finished()
				Return
			Endif
			;todo v�rifie qu'on a bien le bouton vert
			;clique dessus
			MouseClick("primary", 674, 630, 1)
			If _Interrupt_Sleep(2000) Then
				_Function_Finished()
				Return
			Endif
			;not enough particle
			$color = PixelGetColor($notEnough[0], $notEnough[1])
			If $color == $notEnough[2] or $color == $notEnough[3] Then
				_cath_log_ERROR ("Not enough particle" & @CRLF)
				MouseClick("primary", 79, 812, 1)
				_Function_Finished()
				Return
			EndIf
			;clique pour acc�l�rer
			MouseClick("primary", 674, 630, 1)
			If _Interrupt_Sleep(1000) Then
				_Function_Finished()
				Return
			Endif
			;clique pour fermer
			MouseClick("primary", 79, 812, 1)
			;boucle
		EndIf

		If _Interrupt_Sleep(1000) Then
			_Function_Finished()
			Return
		Endif
	Wend


	_Function_Finished()
	Return
EndFunc

Func Cathedral_BuyManuscriptsChest($count)

	If initBlueStacks() Then
		_Function_Finished()
		Return
  EndIf

  ;si on est sur la carte
  ;If is_map_screen() And Not is_cathedral_stage() Then
  ;	back_to_castle()
  ;EndIf

  ;si on est dans le ch�teau
  If is_castle_screen() Then
    open_map()
    open_cathedral()
  EndIf

  ;si on est dans l'�cran cath�drale
  If is_cathedral_screen() == false Then
    PrintGUIStatus("Fail entering cathedral menu")
    _cath_log_ERROR ("Fail entering cathedral menu" & @CRLF)
    return true
  EndIf

  ;go dans le magasin
  MouseClick("primary", 1200, 800, 1)
  If _Interrupt_Sleep(500) Then
    Return true
  Endif

  ;scroll to big chest
  MouseClickDrag("primary", 680, 850, 680, 225)
  If _Interrupt_Sleep(500) Then
    Return true
  Endif

  ;check big chest is here
  If FindImage("cathedral_manuscripts_big_chest.png") == false Then
    ; erreur
    PrintGUIStatus("Fail detecting chest in cathedral shop")
    _cath_log_ERROR ("Fail detecting chest in cathedral shop" & @CRLF)
    return true
  EndIf

  ; loop to by chests
	For $cnt = 0 to $count-1

    ;click big chest
    MouseClick("primary", 240, 580, 1)
    If _Interrupt_Sleep(200) Then
      Return true
    Endif

    ;not enough particle
    $color = PixelGetColor($notEnough[0], $notEnough[1])
    If $color == $notEnough[2] or $color == $notEnough[3] Then
      PrintGUIStatus("Fail detecting chest in cathedral shop")
      _cath_log_ERROR ("Not enough particle" & @CRLF)
      MouseClick("primary", 79, 812, 1)
      Return true
    EndIf

    ;confirm
    MouseClick("primary", 780, 650, 1)
    If _Interrupt_Sleep(200) Then
      Return true
    Endif

    ;dummy click to speed up openning
    MouseClick("primary", 780, 650, 1)
    If _Interrupt_Sleep(200) Then
      Return true
    Endif

    ;dummy click to speed up openning
    MouseClick("primary", 160, 850, 1)
    If _Interrupt_Sleep(200) Then
      Return true
    Endif

		_cath_log_INFO ("cathedral chest bought "& $cnt & @CRLF)
    PrintGUIStatus("Cathedral big chest bought "& $cnt+1 & " on " & $count)
	Next

  return false

EndFunc

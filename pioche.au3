
#include-once

#include "data/hc_coords.au3"
#include <Math.au3>

Global Const $pioche_box_size[2] = [120, 115] ; width, height
Global Const $pioche_zone[4] = [413, 252, 1126-413, 820-252] ; Full map
Global Const $pioche_zone_last_row[4] = [413, 710, 1126-413, 710-250] ; Last row only

Global Const $pioche_color_box_1hit_acc[1] = [ 0xCE955C ] ; Accessible
Global Const $pioche_color_box_1hit_ina[1] = [ 0x996034 ] ; Inaccessible
Global Const $pioche_color_box_2hit_acc[2] = [ 0xB0ACA7, 0xB0ADA8 ] ; Accessible
Global Const $pioche_color_box_2hit_ina[2] = [ 0x76706A ] ; Inaccessible


Global $pattern_box_collected
Global $pattern_box_reward_collected
Global $pattern_reward_bw
Global $pattern_reward_color
Global $pattern_2hit_1left

Global $pioche_last_hit_x = 0


; Boxes Types:
; 0 (CBox) = collected box
; 1 (D1-a)= 1 hit box, no reward, accessible
; 2 (D1-i)= 1 hit box, no reward, inaccessible
; 3 (D2-a)= 2 hit box, no reward, accessible
; 4 (D2-i)= 2 hit box, no reward, inaccessible
; 5 (D2-1)= 2 hit box, no reward, 1 hit left
; 6 (R1-a)= 1 hit box, reward, accessible
; 7 (R1-i)= 1 hit box, reward, inaccessible
; 8 (R2-2)= 2 hit box, reward, accessible
; 9 (R2-i)= 2 hit box, reward, inaccessible
; 10 (R2-1)= 2 hit box, reward, 1 hit left
; 20 (CRwd)= Collected reward
; 255 (Undf) = Not init

Global $pioche_map[6][5]

Const $aRect_Fullmap[4] = [$pioche_zone[0], $pioche_zone[1], $pioche_zone[2], $pioche_zone[3]]
Const $aRect_Lastrow[4] = [$pioche_zone_last_row[0], $pioche_zone_last_row[1], $pioche_zone_last_row[2], $pioche_zone_last_row[3]]



Global $threshold = 0.8
Global $Watchdog_Restart = False



Func Pioche_Run()

	If initBlueStacks() Then
    _Function_Finished()
    Return
  EndIf

  PrintGUIStatus("Pioching pioching ...")

  $pattern_box_collected = _OpenCV_imread_and_check(_OpenCV_FindFile("pioche_box_collected.PNG"))
  $pattern_box_reward_collected = _OpenCV_imread_and_check(_OpenCV_FindFile("pioche_box_reward_collected.PNG"))
  $pattern_reward_bw = _OpenCV_imread_and_check(_OpenCV_FindFile("pioche_reward.PNG"))
  $pattern_reward_color = _OpenCV_imread_and_check(_OpenCV_FindFile("pioche_reward_color.PNG"))
  $pattern_2hit_1left = _OpenCV_imread_and_check(_OpenCV_FindFile("pioche_box2hit_1left.PNG"))

  Do
    $Watchdog_Restart = False
    
    ; Check if reward pop-up is still opened (previous failure)
    If PixelGetColor(623, 86) == 0xF0FF00 Then     
      ; Reward validation
      MouseClick("primary", 749, 785, 1, 1)
      If _Interrupt_Sleep(1000) Then
        Return True
      Endif
    EndIf

    ; Init map content
    For $i = 0 To 5
      For $j = 0 To 4
        $pioche_map[$i][$j] = 255
      Next
    Next
    Pioche_Find_Box_in_Map( True, $pioche_map )

    ; Loop until no more pioche
    Do
      $Result = Pioche_Pick_Reward_On_Map()
    Until( $Result or $fInterrupt )
    If $Result Then
      Pioche_Print_Map()
	  
	  ; Check there is no more pioche
	  $Result = FindImage("pioche_missing.png")
	  If $Result Then
      _Generic_Log_ERROR("Pioche_Run: No more pioche !" & @CRLF)
      _Function_Finished()
      return TRUE
	  EndIf
      
      ; Check there is no more pioche #2
      $pioche_map_fail = true
      For $i = 0 To 5
        For $j = 0 To 4
          If $pioche_map[$i][$j] <> 255 then
            $pioche_map_fail = false
          EndIf
        Next
      Next
      If $pioche_map_fail Then
        _Generic_Log_ERROR("Pioche_Run: No more pioche #2 !" & @CRLF)
        _Function_Finished()
        return TRUE
      EndIf
      
    EndIf
  Until( $Watchdog_Restart == False )

	_Function_Finished()
EndFunc

Func Pioche_Pick_Reward_On_Map()
	$ref_box_x_tmp = 0
	$ref_box_y_tmp = 0
	$reward_found = false

	$rwd_x = 0
	$rwd_y = 0
	$distance = 255
	$ref_box_x = 0
	$ref_box_y = 0
  $leave = False

	; Look for closest reward
	For $j = 0 to 4
		For $i = 0 to 5
      If $pioche_map[$i][$j] >= 6 and $pioche_map[$i][$j] <= 10 Then
        ; Reward found, look for closest path to reach it
        $reward_found = True
        $distance_new = 0
        Pioche_Look_Closest_Collected_Box($i, $j, $ref_box_x_tmp, $ref_box_y_tmp, $distance_new)

        ; *** Reward selection priority ***
        ; 1st -> Top row rewards (to avoid to loose it)
        If $j == 0 Then
          $rwd_x = $i
          $rwd_y = $j
          $distance = $distance_new
          $ref_box_x = $ref_box_x_tmp
          $ref_box_y = $ref_box_y_tmp
          _Generic_Log_TRACE("Pioche_Pick_Reward_On_Map: Top row reward found in ["& $rwd_x &","& $rwd_y &"], with way point ["& $ref_box_x &"," & $ref_box_y & "] and dist=" & $distance & @CRLF)
          $leave = True
          ExitLoop
        ; 2nd -> Minimal distance is shorter for this reward (to save pioches)
        ElseIf $distance_new < $distance Then
          $rwd_x = $i
          $rwd_y = $j
          $distance = $distance_new
          $ref_box_x = $ref_box_x_tmp
          $ref_box_y = $ref_box_y_tmp
          _Generic_Log_TRACE("Pioche_Pick_Reward_On_Map: Closest reward found in ["& $rwd_x &","& $rwd_y &"], with way point ["& $ref_box_x &"," & $ref_box_y & "] and dist=" & $distance & @CRLF)
        EndIf
			EndIf
		Next
    If $leave Then
      ExitLoop
    EndIf
	Next

	; Reach the reward
	If $reward_found Then
		; Reach the reward
		_Generic_Log_INFO("Pioche_Pick_Reward_On_Map: Reward found in ["& $rwd_x &","& $rwd_y &"]" & @CRLF)
		If Pioche_Reach_Box($rwd_x, $rwd_y, $ref_box_x, $ref_box_y) Then
			_Generic_Log_ERROR("Pioche_Pick_Reward_On_Map: Pioche_Reach_Box error while going to reward." & @CRLF)
			return True
		EndIf
	Else
		; if no reward on map, then look for shortest path to go to last row
    $distance_new = 0
		Pioche_Look_Closest_Collected_Box($pioche_last_hit_x, 4, $ref_box_x, $ref_box_y, $distance_new)

    ; Found path not on last row - 1, look for other option on last row - 1
    If $ref_box_y <> 3 Then
      For $i = 0 to 5
        If $pioche_map[$i][3] == 0 or $pioche_map[$i][3] == 20 Then
          $ref_box_x = $i
          $ref_box_y = 3
        EndIf
      Next
    EndIf

		; Go down straight from this position
		$target_x = $ref_box_x
		$target_y = 4
		_Generic_Log_INFO("Pioche_Pick_Reward_On_Map: No reward found, go down from [" & $ref_box_x & "," & $ref_box_y & "] to [" & $target_x & "," & $target_y & "]" & @CRLF)
		If Pioche_Reach_Box($target_x, $target_y, $ref_box_x, $ref_box_y) Then
			_Generic_Log_ERROR("Pioche_Pick_Reward_On_Map: Pioche_Reach_Box error while going down without reward." & @CRLF)
			return True
		EndIf
	EndIf

	Return False
EndFunc

Func Pioche_Reach_Box(ByRef $target_x, ByRef $target_y, ByRef $pos_x, ByRef $pos_y)

	_Generic_Log_TRACE("Pioche_Reach_Box: Target=[" & $target_x & "," & $target_y & "], Position=[" & $pos_x & "," & $pos_y & "]" & @CRLF)

	; Reach the position
	Do
		; Look closest box to hit
		If Pioche_Next_Box_To_Hit($target_x, $target_y, $pos_x, $pos_y) Then
			_Generic_Log_ERROR("Pioche_Reach_Box: Pioche_Next_Box_To_Hit error" & @CRLF)
			return True
		EndIf

    ; Get if 2 hits are needed
    Switch $pioche_map[$pos_x][$pos_y]
      Case 3 To 4
        $two_hit_box = True
      Case 8 To 9
        $two_hit_box = True
      Case Else
        $two_hit_box = False
    EndSwitch

		; Hit it
		If Pioche_Hit_Box($pos_x, $pos_y) Then
			_Generic_Log_ERROR("Pioche_Reach_Box: Pioche_Hit_Box error" & @CRLF)
			return True
		EndIf

		; Check if second it needed
		If $two_hit_box Then
      ; Wait animation end
      If _Interrupt_Sleep(300) Then
        Return True
      Endif

			; Hit it again
      _Generic_Log_TRACE("Pioche_Reach_Box: 2nd Pioche_Hit_Box" & @CRLF)
			If Pioche_Hit_Box($pos_x, $pos_y) Then
				_Generic_Log_ERROR("Pioche_Reach_Box: 2nd Pioche_Hit_Box error" & @CRLF)
				return True
			EndIf
		EndIf

		; Check if we hit the last row
		If $pos_y == 4 Then
			; Need to refresh the map (map scrolled down)
			Pioche_Shift_Map_Up()
      If _Interrupt_Sleep(200) Then
        Return True
      Endif

			; Update last row of the map
			Pioche_Find_Box_in_Map( False, $pioche_map )

			; Update position in the loop
			$pos_y = _Max($pos_y - 1, 0)
			$target_y = _Max($target_y - 1, 0)
		EndIf

		; Check again
		if $pioche_map[$pos_x][$pos_y] <> 0 and $pioche_map[$pos_x][$pos_y] <> 20 Then
			; ERROR !
			_Generic_Log_ERROR("Pioche_Reach_Box: Box not collected after 2 hits ???" & @CRLF)
		EndIf

	; Reward is reached
	Until( $target_x == $pos_x and $target_y == $pos_y )

	Return False

EndFunc

Func Pioche_Look_Closest_Collected_Box($rwd_x, $rwd_y, ByRef $start_x, ByRef $start_y, ByRef $distance)
	; Init the search
	$distance = 255
	$start_x = 0
	$start_y = 0

	; Look for minimal distance
	For $j = 0 to 4
		For $i = 0 to 5
			; Search for collected box or collected reward
			If $pioche_map[$i][$j] == 0 or $pioche_map[$i][$j] == 20 Then
				; Compute distance from reward
				$new_distance = Abs($rwd_x - $i) + Abs($rwd_y - $j)
				If $new_distance < $distance Then
					; Register new minimum
					$distance = $new_distance
					$start_x = $i
					$start_y = $j
				EndIf
			EndIf
		Next
	Next

	_Generic_Log_TRACE("Pioche_Look_Closest_Collected_Box: Reward=[" & $rwd_x & "," & $rwd_y & "], Closest=[" & $start_x & "," & $start_y & "], Dist=" & $distance & @CRLF)
EndFunc

Func Pioche_Next_Box_To_Hit($rwd_x, $rwd_y, ByRef $pos_x, ByRef $pos_y)

  ; Order here defines the priority on movement => first vertical, then lateral

	If ($rwd_y - $pos_y) > 0 Then
		; Move down on Y-axis direction
		$pos_y = $pos_y + 1
		_Generic_Log_INFO("Pioche_Next_Box_To_Hit: Move down to [" & $pos_x & "," & $pos_y & "]" & @CRLF)
	ElseIf ($rwd_y - $pos_y) < 0 Then
		; Move up on Y-axis direction
		$pos_y = $pos_y - 1
		_Generic_Log_INFO("Pioche_Next_Box_To_Hit: Move up to [" & $pos_x & "," & $pos_y & "]" & @CRLF)
	ElseIf ($rwd_x - $pos_x) > 0 Then
		; Move right on X-axis direction
		$pos_x = $pos_x + 1
		_Generic_Log_INFO("Pioche_Next_Box_To_Hit: Move right to [" & $pos_x & "," & $pos_y & "]" & @CRLF)
	ElseIf ($rwd_x - $pos_x) < 0 Then
		; Move left on X-axis direction
		$pos_x = $pos_x - 1
		_Generic_Log_INFO("Pioche_Next_Box_To_Hit: Move left to [" & $pos_x & "," & $pos_y & "]" & @CRLF)
	Else
		; Issue !!!
		_Generic_Log_ERROR("Pioche_Next_Box_To_Hit: Error" & @CRLF)
		Return True
	EndIf

	Return False
EndFunc

Func Pioche_Hit_Box($x, $y)

	_Generic_Log_TRACE("Pioche_Hit_Box: Position = [" & $x & "," & $y & "], type=" & Pioche_Asc_Code($pioche_map[$x][$y]) & @CRLF)

	If $pioche_map[$x][$y] == 0 or $pioche_map[$x][$y] == 20 Then
		; ERROR: already broken
		_Generic_Log_ERROR("Pioche_Hit_Box: Error box already collected !" & @CRLF)
		return True
	Else
		; Compute click coordinates
    $x_coord = $pioche_zone[0] + $x * $pioche_box_size[0] + Int($pioche_box_size[0]/2)
    $y_coord = $pioche_zone[1] + $y * $pioche_box_size[1] + Int($pioche_box_size[1]/2)

    ; Get if 2 hits are needed
    Switch $pioche_map[$x][$y]
      Case 6 To 7
        $reward_popup = True
      Case 10 To 10
        $reward_popup = True
      Case Else
        $reward_popup = False
    EndSwitch

		; Click on box
    MouseClick("primary", $x_coord, $y_coord , 1, 1)

		; Wait refresh
    If $reward_popup Then
      ; Reward pop-up expected
      Local $iBegin = TimerInit()
      Local $attempt = 3
      Do
        If _Interrupt_Sleep(300) Then
          return True
        EndIf
        ; Check pop-up menu (no more pioche // reward validation)
        $color = PixelGetColor(623, 86)
        _Generic_log_TRACE( "Pioche_Hit_Box: Found color=0x" & hex($color, 6) & @CRLF)
        
        ; Check timeout -> Hit box missed -> Repeat
        If TimerDiff($iBegin) > 2000 Then
          $attempt = $attempt - 1
          If $attempt == 0 Then
            ; Issue, leave with watchdog flag
            _Generic_log_Error( "Pioche_Hit_Box: Timeout issue, restart through watchdog" & @CRLF)
            $Watchdog_Restart = True
            Return True
          EndIf
          _Generic_log_TRACE( "Pioche_Hit_Box: Timeout repeat hit" & @CRLF)
          ; Click again on box
          MouseClick("primary", $x_coord, $y_coord , 1, 1)
          ; Reset timer
          $iBegin = TimerInit()
        EndIf
      Until( $color == 0xF0FF00 )
      
      ; Reward validation
      MouseClick("primary", 749, 785, 1, 1)
      
      Do
        If _Interrupt_Sleep(200) Then
          return True
        EndIf
        $color = PixelGetColor(623, 86)
      Until( $color <> 0xF0FF00 )
      
    ElseIf $y == 4 Then
      If $pioche_map[$x][$y] == 3 or $pioche_map[$x][$y] == 4 or $pioche_map[$x][$y] == 8 or $pioche_map[$x][$y] == 9 Then
        ; No reward pop-up expected on double hit box (1st hit) -> no scrolling
        If _Interrupt_Sleep(200) Then
          Return True
        Endif
      Else
        ; No reward pop-up expected on single hit box
        ; But, scrolling expected so wait longer
        If _Interrupt_Sleep(1000) Then
          Return True
        Endif
      EndIf
    Else
      ; No reward pop-up expected, nor scrolling
      If _Interrupt_Sleep(200) Then
        Return True
      Endif
    Endif

    ;If $color == 0x73DB18 Then
      ; No more pioche !
      ; TODO
    ;EndIf

		; Update cell status
		Switch $pioche_map[$x][$y]
			Case 1 to 2 ; 1 hit box
				$pioche_map[$x][$y] = 0 ; move to Collected box
			Case 3 to 4 ; 2 hit box
				$pioche_map[$x][$y] = 5 ; move to 1 left hit (2-hit box)
			Case 5 ; 2 hit box with 1 hit left
				$pioche_map[$x][$y] = 0 ; move to Collected box
			Case 6 to 7 ; 1 hit box reward
				$pioche_map[$x][$y] = 20 ; move to Collected Reward box
			Case 8 to 9 ; 1 hit box reward
				$pioche_map[$x][$y] = 10 ; move to 1 left hit reward (2-hit reward box)
			Case 10 ; 2 hit box with 1 hit left
				$pioche_map[$x][$y] = 20 ; move to Collected Reward box
			Case Else
				; Error
				_Generic_Log_ERROR("Pioche_Hit_Box: Error unexpected box value to update : " & $pioche_map[$x][$y] & " !" & @CRLF)
				return True
		EndSwitch

    ; Save last X-axis position
    $pioche_last_hit_x = $x

		; OK
		return False
	EndIf

EndFunc

Func Pioche_Shift_Map_Up()
	_Generic_Log_INFO("Pioche_Shift_Map_Up" & @CRLF)
	For $j = 0 to 4-1
		For $i = 0 to 5
			; Copy N+1 line to N
			$pioche_map[$i][$j] = $pioche_map[$i][$j+1]
		Next
	Next
  For $i = 0 to 5
    ; Set undefined the last row
    $pioche_map[$i][4] = 255
  Next
EndFunc

Func Pioche_Find_Box_in_Map($full_not_last_row, ByRef $pioche_map)

  ;opencv screenshot
	Local $aRect[4];
  If $full_not_last_row Then
    ; Full map search
    $aRect = $aRect_Fullmap
  Else
    ; Last row search
    $aRect = $aRect_Lastrow
  EndIf

	Local $img = _OpenCV_GetDesktopScreenMat($aRect)


	; Find collected box (no reward) in map
	Local $aMatches = _OpenCV_FindTemplate($img, $pattern_box_collected, $threshold)
  _Generic_log_TRACE( "Pioche_Find_Box_in_Map: (" & $full_not_last_row &") Found " & UBound($aMatches)& " patterns for COLLECTED box type." & @CRLF)
  If UBound($aMatches) Then
    For $i = 0 To UBound($aMatches)-1
      $x_map_pos = -1
      $y_map_pos = -1

      If Pioche_Get_Box_Position($aMatches[$i][0], $aMatches[$i][1], $x_map_pos, $y_map_pos) Then
        ; Log color found
        $x_coord = $aRect[0] + $aMatches[$i][0]
        $y_coord = $aRect[1] + $aMatches[$i][1]
        $color = PixelGetColor( $x_coord, $y_coord)
        _Generic_log_TRACE( "Pioche_Find_Box_in_Map: (" & $i & ") Found color=0x" & hex($color, 6) & " at x="& $x_coord & ", y=" & $y_coord & @CRLF)

        ; 0 (CBox) = collected box
        $box_code = 0

        ; Register found box pattern in map
        If $full_not_last_row Then
          $pioche_map[$x_map_pos][$y_map_pos] = $box_code ; Full map
        Else
          $pioche_map[$x_map_pos][4] = $box_code ; Last row
        EndIf
      Else
        ; Error
        _Generic_log_ERROR( "Pioche_Find_Box_in_Map: (" & $i & ") Pioche_Get_Box_Position error, x_map="& $x_map_pos & ", y_map=" & $y_map_pos & ", at x="& $x_coord & ", y=" & $y_coord & @CRLF)
      EndIf
    Next
  EndIf

	; Find collected box (reward) in map
	Local $aMatches = _OpenCV_FindTemplate($img, $pattern_box_reward_collected, $threshold)
  _Generic_log_TRACE( "Pioche_Find_Box_in_Map: (" & $full_not_last_row &") Found " & UBound($aMatches)& " patterns for COLLECTED REWARD box type." & @CRLF)
  If UBound($aMatches) Then
    For $i = 0 To UBound($aMatches)-1
      $x_map_pos = -1
      $y_map_pos = -1

      If Pioche_Get_Box_Position($aMatches[$i][0], $aMatches[$i][1], $x_map_pos, $y_map_pos) Then
        ; Log color found
        $x_coord = $aRect[0] + $aMatches[$i][0]
        $y_coord = $aRect[1] + $aMatches[$i][1]
        $color = PixelGetColor( $x_coord, $y_coord)
        _Generic_log_TRACE( "Pioche_Find_Box_in_Map: (" & $i & ") Found color=0x" & hex($color, 6) & " at x="& $x_coord & ", y=" & $y_coord & @CRLF)

        ; 20 (CRwd)= Collected reward
        $box_code = 20

        ; Register found box pattern in map
        If $full_not_last_row Then
          $pioche_map[$x_map_pos][$y_map_pos] = $box_code ; Full map
        Else
          $pioche_map[$x_map_pos][4] = $box_code ; Last row
        EndIf
      Else
        ; Error
        _Generic_log_ERROR( "Pioche_Find_Box_in_Map: (" & $i & ") Pioche_Get_Box_Position error, x_map="& $x_map_pos & ", y_map=" & $y_map_pos & ", at x="& $x_coord & ", y=" & $y_coord & @CRLF)
      EndIf
    Next
  EndIf

	; Find rewards B/W in map
	Local $aMatches = _OpenCV_FindTemplate($img, $pattern_reward_bw, $threshold)
  _Generic_log_TRACE( "Pioche_Find_Box_in_Map: (" & $full_not_last_row &") Found " & UBound($aMatches)& " patterns for REWARD box type." & @CRLF)
  If UBound($aMatches) Then
    For $i = 0 To UBound($aMatches)-1
      $x_map_pos = -1
      $y_map_pos = -1

      If Pioche_Get_Box_Position($aMatches[$i][0], $aMatches[$i][1], $x_map_pos, $y_map_pos) Then
        ; Log color found
        $x_coord = $aRect[0] + $aMatches[$i][0]
        $y_coord = $aRect[1] + $aMatches[$i][1]
        $color = PixelGetColor( $x_coord, $y_coord)
        _Generic_log_TRACE( "Pioche_Find_Box_in_Map: (" & $i & ") Found color=0x" & hex($color, 6) & " at x="& $x_coord & ", y=" & $y_coord & @CRLF)

        If $color == $pioche_color_box_1hit_ina[0] Then
          ; 7 (R1-i)= 1 hit box, reward, inaccessible
          $box_code = 7
        ElseIf $color == $pioche_color_box_2hit_ina[0] Then
          ; 9 (R2-i)= 2 hit box, reward, inaccessible
          $box_code = 9
        Else
          $box_code = 1000
        EndIf

        ; Register found box pattern in map
        If $full_not_last_row Then
          $pioche_map[$x_map_pos][$y_map_pos] = $box_code ; Full map
        Else
          $pioche_map[$x_map_pos][4] = $box_code ; Last row
        EndIf
      Else
        ; Error
        _Generic_log_ERROR( "Pioche_Find_Box_in_Map: (" & $i & ") Pioche_Get_Box_Position error, x_map="& $x_map_pos & ", y_map=" & $y_map_pos & ", at x="& $x_coord & ", y=" & $y_coord & @CRLF)
      EndIf
    Next
  EndIf

  ; Find rewards color in map
	Local $aMatches = _OpenCV_FindTemplate($img, $pattern_reward_color, $threshold)
  _Generic_log_TRACE( "Pioche_Find_Box_in_Map: (" & $full_not_last_row &") Found " & UBound($aMatches)& " patterns for REWARD COLOR box type." & @CRLF)
  If UBound($aMatches) Then
    For $i = 0 To UBound($aMatches)-1
      $x_map_pos = -1
      $y_map_pos = -1

      If Pioche_Get_Box_Position($aMatches[$i][0], $aMatches[$i][1], $x_map_pos, $y_map_pos) Then
        ; Log color found
        $x_coord = $aRect[0] + $aMatches[$i][0]
        $y_coord = $aRect[1] + $aMatches[$i][1]
        $color = PixelGetColor( $x_coord, $y_coord)
        _Generic_log_TRACE( "Pioche_Find_Box_in_Map: (" & $i & ") Found color=0x" & hex($color, 6) & " at x="& $x_coord & ", y=" & $y_coord & @CRLF)

        If $color == $pioche_color_box_1hit_acc[0] Then
          ; 6 (R1-a)= 1 hit box, reward, accessible
          $box_code = 6
        ElseIf $color == $pioche_color_box_2hit_acc[0] or $color == $pioche_color_box_2hit_acc[1] Then
          ; 8 (R2-2)= 2 hit box, reward, accessible
          $box_code = 8
        Else
          $box_code = 1000
        EndIf

        ; Register found box pattern in map
        If $full_not_last_row Then
          $pioche_map[$x_map_pos][$y_map_pos] = $box_code ; Full map
        Else
          $pioche_map[$x_map_pos][4] = $box_code ; Last row
        EndIf
      Else
        ; Error
        _Generic_log_ERROR( "Pioche_Find_Box_in_Map: (" & $i & ") Pioche_Get_Box_Position error, x_map="& $x_map_pos & ", y_map=" & $y_map_pos & ", at x="& $x_coord & ", y=" & $y_coord & @CRLF)
      EndIf
    Next
  EndIf

  ; Find 2hits box with 1 hit left in map
	Local $aMatches = _OpenCV_FindTemplate($img, $pattern_2hit_1left, $threshold)
  _Generic_log_TRACE( "Pioche_Find_Box_in_Map: (" & $full_not_last_row &") Found " & UBound($aMatches)& " patterns for ONE HIT LEFT box type." & @CRLF)
  If UBound($aMatches) Then
    For $i = 0 To UBound($aMatches)-1
      $x_map_pos = -1
      $y_map_pos = -1

      If Pioche_Get_Box_Position($aMatches[$i][0], $aMatches[$i][1], $x_map_pos, $y_map_pos) Then
        ; Log color found
        $x_coord = $aRect[0] + $aMatches[$i][0]
        $y_coord = $aRect[1] + $aMatches[$i][1]
        $color = PixelGetColor( $x_coord, $y_coord)
        _Generic_log_TRACE( "Pioche_Find_Box_in_Map: (" & $i & ") Found color=0x" & hex($color, 6) & " at x="& $x_coord & ", y=" & $y_coord & @CRLF)

        If $pioche_map[$x_map_pos][$y_map_pos] <> 255 Then
          ; 10 (R2-1)= 2 hit box, reward, 1 hit left
          $box_code = 10
        Else
          ; 5 (D2-1)= 2 hit box, no reward, 1 hit left
          $box_code = 5
        EndIf

        ; Register found box pattern in map
        If $full_not_last_row Then
          $pioche_map[$x_map_pos][$y_map_pos] = $box_code ; Full map
        Else
          $pioche_map[$x_map_pos][4] = $box_code ; Last row
        EndIf
      Else
        ; Error
        _Generic_log_ERROR( "Pioche_Find_Box_in_Map: (" & $i & ") Pioche_Get_Box_Position error, x_map="& $x_map_pos & ", y_map=" & $y_map_pos & ", at x="& $x_coord & ", y=" & $y_coord & @CRLF)
      EndIf
    Next
  EndIf

  ; Find remains box types ...
  For $i = 0 To 5
    For $j = 0 To 4
      If $pioche_map[$i][$j] == 255 Then ; Undefined
        $x_coord = $aRect[0] + $i * $pioche_box_size[0] + Int($pioche_box_size[0]/2)
        If $full_not_last_row Then
          $y_coord = $aRect[1] + $j * $pioche_box_size[1] + Int($pioche_box_size[1]/2)
        Else
          $y_coord = $aRect[1] + Int($pioche_box_size[1]/2)
        EndIf
        $color = PixelGetColor( $x_coord, $y_coord)

        If $color == $pioche_color_box_1hit_acc[0] Then
          ; 1 (D1-a)= 1 hit box, no reward, accessible
          $box_code = 1
        ElseIf $color == $pioche_color_box_1hit_ina[0] Then
          ; 2 (D1-i)= 1 hit box, no reward, inaccessible
          $box_code = 2
        ElseIf $color == $pioche_color_box_2hit_acc[0] or $color == $pioche_color_box_2hit_acc[1] Then
          ; 3 (D2-a)= 2 hit box, no reward, accessible
          $box_code = 3
        ElseIf $color == $pioche_color_box_2hit_ina[0] Then
          ; 4 (D2-i)= 2 hit box, no reward, inaccessible
          $box_code = 4
        Else
          _Generic_log_ERROR( "Pioche_Find_Box_in_Map: Unreconized box at [" & $i & ","& $j & "] Found color=0x" & hex($color, 6) & " at x="& $x_coord & ", y=" & $y_coord & @CRLF)
          $box_code = 1000
          
          ; Failure, restart
          $Watchdog_Restart = True
          Return True
        EndIf
        _Generic_log_TRACE( "Pioche_Find_Box_in_Map: [" & $i & ","& $j & "], found cell (" & Pioche_Asc_Code($box_code) & ") with color=0x" & hex($color, 6) & " at x="& $x_coord & ", y=" & $y_coord & @CRLF)

        ; Register found box pattern in map
        If $full_not_last_row Then
          $pioche_map[$i][$j] = $box_code ; Full map
        ElseIf $j == 4 Then
          $pioche_map[$i][$j] = $box_code ; Last row
        EndIf
      EndIf
    Next
  Next

	Return False
EndFunc

Func Pioche_Get_Box_Position($x_pixel_pos, $y_pixel_pos, ByRef $x_map_pos, ByRef $y_map_pos)
  $x_map_pos = Int( $x_pixel_pos / $pioche_box_size[0] )
  $y_map_pos = Int( $y_pixel_pos / $pioche_box_size[1] )
  _Generic_log_TRACE( "Pioche_Get_Box_Position: pixel_pos=[" & $x_pixel_pos & "," & $y_pixel_pos & "], map_pos=[" & $x_map_pos & "," & $y_map_pos  & "]" & @CRLF)

  If $x_map_pos < 6 and $y_map_pos < 5 Then
    Return True
  Else
    Return False
  EndIf
EndFunc

Func Pioche_Print_Map()
  _Generic_log_TRACE("Pioche_Print_Map:" & @CRLF)
  $row_separator = "-------------------------------"& @CRLF

  _Generic_log_TRACE($row_separator)
  For $j = 0 To 4
    $row_str = ""
    For $i = 0 To 5
      $row_str = $row_str & Pioche_Asc_Code($pioche_map[$i][$j])
    Next
    $row_str = $row_str & "|" & @CRLF
    _Generic_log_TRACE($row_str)
    _Generic_log_TRACE($row_separator)
  Next
EndFunc

Func Pioche_Asc_Code($code)
  If $code == 0 Then
    ; 0 (CBox) = collected box
    Return "|CBox"
  ElseIf $code == 1 Then
    ; 1 (D1-a)= 1 hit box, no reward, accessible
    Return "|D1-a"
  ElseIf $code == 2 Then
    ; 2 (D1-i)= 1 hit box, no reward, inaccessible
    Return "|D1-i"
  ElseIf $code == 3 Then
    ; 3 (D2-a)= 2 hit box, no reward, accessible
    Return "|D2-a"
  ElseIf $code == 4 Then
    ; 4 (D2-i)= 2 hit box, no reward, inaccessible
    Return "|D2-i"
  ElseIf $code == 5 Then
    ; 5 (D2-1)= 2 hit box, no reward, 1 hit left
    Return "|D2-1"
  ElseIf $code == 6 Then
    ; 6 (R1-a)= 1 hit box, reward, accessible
    Return "|R1-a"
  ElseIf $code == 7 Then
    ; 7 (R1-i)= 1 hit box, reward, inaccessible
    Return "|R1-i"
  ElseIf $code == 8 Then
    ; 8 (R2-2)= 2 hit box, reward, accessible
    Return "|R2-2"
  ElseIf $code == 9 Then
    ; 9 (R2-i)= 2 hit box, reward, inaccessible
    Return "|R2-i"
  ElseIf $code == 10 Then
    ; 10 (R2-1)= 2 hit box, reward, 1 hit left
    Return "|R2-1"
  ElseIf $code == 20 Then
    ; 20 (CRwd)= Collected reward
    Return "|CRwd"
  ElseIf $code == 255 Then
    ; 255 (Undf) = Not init
    Return "|Undf"
  Else
    Return "|O__O"
  EndIf
EndFunc
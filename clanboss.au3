;#RequireAdmin
#include <AutoItConstants.au3>
#include <MsgBoxConstants.au3>
#include <GUIConstantsEx.au3>
#include <StaticConstants.au3>
#include <WindowsConstants.au3>

Global Const $boss_fight_button[5] = [1227, 844, 0x52C310, 0x51C30F, 0x4FC30D] ;0x52C310, 0x51C30F
Global Const $confirm_fighters[2] = [637, 598] ;common with others
Global Const $boss_end_attack[3] = [691, 852, 0x4FC30D]

;TODO avant de lancer le script : se mettre dans la porte de l'abîme

Func ClanBoss($boss)

	If initBlueStacks() Then
		_Function_Finished()
		Return
	EndIf

	Local $count = 20
	Do
		;clic sur le boss
		Local $x=-1, $y=-1
		If $boss == "Smasher" Then
			FindImageCoord($hwnd, "boss_smasher.png", $x, $y)
		ElseIf $boss == "Pumba" Then
			FindImageCoord($hwnd, "boss_pumba.png", $x, $y)
		ElseIf $boss == "Papa Troll" Then
			FindImageCoord($hwnd, "boss_troll.png", $x, $y)
		ElseIf $boss == "Sporozilla" Then
			FindImageCoord($hwnd, "boss_sporo.png", $x, $y)
		Else
			ConsoleWrite ("Choisir un boss" & @CRLF)
			ExitLoop
		EndIf

		If $x==-1 or $y==-1 Then
			ConsoleWrite ("Choisir un autre boss" & @CRLF)
			ExitLoop
		EndIf

		MouseClick("primary", $x, $y , 1)	;todo only if $x, $y <> -1 ?

		If _Interrupt_Sleep(1000) Then
			_Function_Finished()
			return
		Endif

		$color = PixelGetColor($boss_fight_button[0], $boss_fight_button[1])
		While(Not($color == $boss_fight_button[2] or $color == $boss_fight_button[3] or $color == $boss_fight_button[4]));attend le bouton vert
			If _Interrupt_Sleep(100) Then
				_Function_Finished()
				return
			Endif;
			$color = PixelGetColor($boss_fight_button[0], $boss_fight_button[1])
		WEnd
		MouseClick("primary",$boss_fight_button[0], $boss_fight_button[1], 1)

		If _Interrupt_Sleep(1500) Then
			_Function_Finished()
			return
		Endif;
		MouseClick("primary", $fight_button[0], $fight_button[1], 1) ; à l'attaque 2
		;ConsoleWrite ("bouton attaque" & @CRLF)

		If _Interrupt_Sleep(1500) Then
			_Function_Finished()
			return
		Endif;
		; si ça affiche un diamant, on sort
		$color = PixelGetColor($red_button_no_diamond[0], $red_button_no_diamond[1])
		If $color == $red_button_no_diamond[2] or $color == $red_button_no_diamond[3] or $color == $red_button_no_diamond[4] Then
			If is_diamond_button() Then
				ConsoleWrite (@TAB & "Plus de pommes" & @CRLF)
				_Function_Finished()
				return
			EndIf
		EndIf

		;passe le "vos combattants sont cassés
		MouseClick("primary", $confirm_fighters[0], $confirm_fighters[1], 1) ; ouioui, ils vont gagner de toute façon

		;bouton héros jusqu'à la fin de l'attaque
		While(PixelGetColor($boss_end_attack[0], $boss_end_attack[1]) <> $boss_end_attack[2])
			If _Interrupt_Sleep(100) Then
				_Function_Finished()
				return
			Endif;
			MouseClick("primary", $hero_spell[0],  $hero_spell[1], 1)
		WEnd

		ConsoleWrite ("Fin de l'attaque" & @CRLF)
		If end_fight(False) Then
			_Function_Finished()
			return
		Endif;


		;attend
		If _Interrupt_Sleep(20000) Then
			_Function_Finished()
			return
		Endif;

		$count = $count - 1;
	Until $count <=0

	_Function_Finished()
EndFunc
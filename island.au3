; Manage invasions in hustle castle
#include-once
#include "data/hc_coords.au3"

Global $system;

;invasions coords
Global Const $island_attack_button[3] = [1187, 734, 0x73DB18]
Global Const $island_no_health_button[2] = [583, 585]
Global Const $island_no_spell_button[2] = [583, 585]
Global Const $island_no_food_button[3] = [950, 571, 0x73DB18] ; bouton non
Global Const $island_next_button[3] = [809, 808, 0x73DB18]
Global Const $island_end_button[3] = [75, 804, 0x73DB18]

Func TheIsland()

	If initBlueStacks() Then
		_Function_Finished()
		Return
	EndIf

	Local $count = 16 ; (16 �tapes)
	Do

		If _Interrupt_Sleep(1000) Then
			_Function_Finished()
			return
		Endif;
		MouseClick("primary", $island_attack_button[0], $island_attack_button[1] , 1) ; � l'attaque
		ConsoleWrite (@TAB & "attaque "& (11-$count) & @CRLF)

		If _Interrupt_Sleep(1500) Then
			_Function_Finished()
			return
		Endif;

		; si �a affiche un diamant, on sort
		$color = PixelGetColor($red_button_no_diamond[0], $red_button_no_diamond[1])
		If $color == $red_button_no_diamond[2] or $color == $red_button_no_diamond[3] Then
			If is_diamond_button() Then
				ConsoleWrite (@TAB & "Plus de pommes" & @CRLF)
				_Function_Finished()
				return
			EndIf
		EndIf

		;passe le "vos combattants sont cass�s"
		MouseClick("primary", $island_no_health_button[0], $island_no_health_button[1] , 1) ; ouioui, ils vont gagner de toute fa�on
		ConsoleWrite (@TAB & "continue si combattants cass�s" & @CRLF)
		If _Interrupt_Sleep(1500) Then
			_Function_Finished()
			return
		Endif;

		;passe le "ya pas de sorts"
		MouseClick("primary", $island_no_spell_button[0], $island_no_spell_button[1] , 1) ; ouioui, ils vont gagner de toute fa�on
		ConsoleWrite (@TAB & "continue si pas de sort" & @CRLF)

		If _Interrupt_Sleep(500) Then
			_Function_Finished()
			return
		Endif;

		While(PixelGetColor($island_next_button[0], $island_next_button[1]) <> $island_next_button[2]);attend le bouton vert "prochain combat"

			If _Interrupt_Sleep(100) Then
				_Function_Finished()
				return
			Endif;
			;bouton h�ros jusqu'� la fin de l'attaque
			MouseClick("primary", $hero_spell[0],  $hero_spell[1], 1)

			;si les 10 attaques sont termin�es
			If PixelGetColor($island_end_button[0], $island_end_button[1]) == $island_end_button[2] Then
				ExitLoop;
			EndIf


			;ConsoleWrite ("PixelGetColor("&$island_next_button[0]&", "&$island_next_button[1]&")=" & Hex(PixelGetColor($island_next_button[0], $island_next_button[1]), 6) & @CRLF)
		WEnd



		;si les 10 attaques sont termin�es
		If PixelGetColor($island_end_button[0], $island_end_button[1]) == $island_end_button[2] Then
			ConsoleWrite (@TAB & "16 �tapes termin�es" & @CRLF)
			ExitLoop;
		EndIf

		ConsoleWrite (@TAB & "attaque termin�e" & @CRLF)

		; clique sur "prochain combat"
		MouseClick("primary", $island_next_button[0], $island_next_button[1] , 1)
		;attend
		If _Interrupt_Sleep(12000) Then
			_Function_Finished()
			return
		Endif;
		ConsoleWrite (@TAB & "prochain" & @CRLF)

		$count = $count - 1;
	Until $count <=0


	_Function_Finished()
EndFunc
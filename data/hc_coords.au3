#include-once


Global Const $look_for_diamond[5] = [580, 640, 599, 0x10FFFF, 0x21B5FA] ; mesuré : 0x10FFFF
;Global Const $ask_for_diamond_2[3] = [613, 597, 0x21B5FA] ; mesuré : 0x10FFFF
Global Const $red_button_no_diamond[5] = [842, 618, 0xDC0000, 0xDE0000, 0xDB0000] ; mesuré : 0x10FFFF

;cancel exit
Global Const $cancel_exit_button[3] = [891, 574, 0xF33E18]

;hero
Global Const $hero_spell[2] = [180, 292]

;attack
Global Const $fight_button[2] = [1161, 791]	;common with others

;arena end button
Global Const $end_fight_button[7] = [688, 845, 0x52C310, 0x51C30F, 0x4FC30D, 0x4CBB0C, 0x4FBB0F] ;(avant 0x4FC20D,  0x4FC30D,  0x52C310, 0x51C30F. Changement avec la 1.41?? ça tourne??)
Global Const $end_fight_next_button[3] = [809, 808, 0x73DB18]

;incomplete_team
Global Const $incomplete_team[8] = [586, 570, 0x6ED217, 0x73DB18, 839, 570, 0xEA3D17, 0xF43F18]
Global Const $no_tool[8] = [581, 566, 0xF3B605, 0xFDBD05, 915, 566, 0x6ED217, 0x73DB18]
Global Const $no_spell[8] = [586, 570, 0x6ED217, 0x73DB18, 839, 570, 0xEA3D17, 0xF43F18]

Global Const $close_button[2] = [1470, 80]

; Pop-up window
Global Const $close_popup[2] = [775, 600]
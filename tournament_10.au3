;#RequireAdmin
#include <AutoItConstants.au3>
#include <MsgBoxConstants.au3>
#include <GUIConstantsEx.au3>
#include <StaticConstants.au3>
#include <WindowsConstants.au3>
#include <WindowsConstants.au3>
#include "ocr.au3"

;bouton fin combat

Global Const $arena_outside_window[2] = [1375, 290]
Global Const $arena_close_window[2] = [1230, 150]
Global Const $arena_attack_button[6] = [1091, 740, 0x73DB18, 0x6ED217, 0x7C7C7C, 0x777777] ;vert ou gris le 9e adversaire a un bouton diff�rent...
Global Const $arena_quickfight_button[4] = [835, 780, 0xF49205, 0xEA8C05]
Global Const $arena_end_button[3] = [1288, 186, 0xF2AB0B] ;yellow = present
Global const $tournament_opponents_coords[20] = [865, 234, 712, 410, 1012, 410,  566, 578, 862, 578, 1160, 578, 417, 744, 718, 744, 1020, 744, 1314, 744]
Global Const $tournament_outline_self[20] = [908, 208,  760, 360,  1058, 360,  611, 540,  908, 540,  1206, 540,  461, 720,  761, 720,  1058, 720,  1356, 720]
Global const $tournament_you_got_reward[2] = [772, 484]
Global const $tournament_mail_reward[2] = [1036, 758]
Global const $tournament_close_mail[2] = [1474, 88]
Global Const $castle_under_attack_button[2] = [759, 601]
Global Const $first_opponent_color[4] = [0xE4AB00, 0xe3AB00, 0xE6AB00, 0xE7AB00]
Global Const $second_opponent_color[3] = [0x84A9B6, 0x7FA3AF, 0x86A9B9]
Global Const $third_opponent_color[3] = [0xCD6939, 0xC56537, 0xCF693A]
Global Const $other_opponent_color[3] = [0x79A6CE, 0x749FC6, 0x7AA6D0]
Global Const $fought_opponent_color[3] = [0xA8A7A8, 0xA2A1A2, 0xAAA7AA]
Global Const $outline_self_color[2] = [0xFFF45A, 0xF5EA56]

Global $isQuickFight

;apples, apples everywhere
;428, 271, 0xC0200B (pomme sur le dos du gobelin)
;534, 331, 0x91B61E (t�te du gobelin)
;814, 592, 0xEA8C05 (bouton pour voir la pub)

Func Tournament($strategy, $payment, $getReward)
	If initBlueStacks() Then
		_Function_Finished()
		Return
	EndIf


	Local $firstOpening = True
	For $loop = 0 to 9

		;si on est sur la carte. Todo, faire mieux
		;If is_map_screen() Then
			;back_to_castle()
		;EndIf

		;si on est dans le ch�teau
		If is_castle_screen() Then
			ConsoleWrite ("Tournament: castle screen "& @CRLF)
			If open_map_and_arena($firstOpening) Then
				_Function_Finished()
				Return
			EndIf
			$firstOpening = False
		EndIf

		;si on est dans l'�cran de lancement d'ar�ne
		If is_tournament_screen() Then
			ConsoleWrite ("Tournament: is_tournament_screen  "& @CRLF)
			If launch_arena($payment) Then
				_Function_Finished()
				Return
			EndIf

			;on doit attendre jusqu'� ce qu'on ait l'�cran d'ar�ne
			;wait for arena screen
			Local $iBegin = TimerInit()
			Local $clickcounter=0
			Do
				If _Interrupt_Sleep(1000) Then
					_Function_Finished()
					Return
				Endif
				$clickcounter = $clickcounter +1

				; toutes les 10 secondes, clique pour ne pas se faire d�connecter si l'attente est longue
				If $clickcounter >= 2 Then
					$clickcounter = 0
					MouseClick("primary", 1436, 388, 1)
					ConsoleWrite ("Tournament: clic  "& @CRLF)
				EndIf

				If is_tournament_running() Then
					ExitLoop
				EndIf
			Until TimerDiff($iBegin) > 3600000 ; 60 minutes devraient suffire

		EndIf

		;si on est d�j� dans l'ar�ne
		If is_tournament_running() Then
			If tournament_loop($strategy, $getReward) Then
				_Function_Finished()
				Return
			EndIf
		EndIf


		;wait for arena screen
		Local $iBegin = TimerInit()
		Do
			If _Interrupt_Sleep(1000) Then
				_Function_Finished()
				Return
			Endif

			If is_tournament_screen() Then
				ExitLoop
			EndIf
			ConsoleWrite ("Tournament: boucle fin tournoi "& @CRLF)
		Until TimerDiff($iBegin) > 60000 ; 1 minute devrait suffire
	Next


	_Function_Finished()
EndFunc

Func Tournament10($strategy, $payment, $getReward, $count)
	If initBlueStacks() Then
		_Function_Finished()
		Return
	EndIf
	Local $firstOpening = True
	Local $t=1
	Do
  
    ; If already in Arena menu
    If is_tournament_screen() Then
      ; Already in arena menu
      ConsoleWrite ("Tournament10: Already in arena menu"& @CRLF)
    Else
      ; Not in arena menu !
      ConsoleWrite ("Tournament10: Not in arena menu !"& @CRLF)
      ConsoleWrite ("Tournament10: back_to_castle"& @CRLF)
      If back_to_castle() Then
        _Function_Finished()
        Return
      EndIf

      ConsoleWrite ("Tournament10: open_map_and_arena"& @CRLF)
      If open_map_and_arena($firstOpening) Then
        _Function_Finished()
        Return
      EndIf
    EndIf
    
    $firstOpening = False
    ConsoleWrite ("Tournament10: launch_arena"& @CRLF)
		If launch_arena($payment) Then
			_Function_Finished()
			Return
		EndIf

    $t = $t + 1

		Local $clickcounter=0
		While(is_tournament_screen())
			If _Interrupt_Sleep(1000) Then
				_Function_Finished()
				Return
			Endif
			$clickcounter = $clickcounter +1

			; toutes les 10 secondes, clique pour ne pas se faire d�connecter si l'attente est longue
			If $clickcounter >= 10 Then
				$clickcounter = 0
				MouseClick("primary", 1436, 388, 1)
				;ConsoleWrite ("clic  "& @CRLF)
			EndIf
		WEnd
		;MsgBox($MB_SYSTEMMODAL, "", "Le tournoi commence")
		;tant que le tournoi n'est pas termin� (il faut pr�voir un timer)
    ConsoleWrite ("Tournament10: tournament_loop"& @CRLF)
    
    Local $sRuntime = ""
    Runtime_In_Hour_Min_Sec($sRuntime)
    PrintGUIStatus("Cathedral run done " & $t & " on " & $count & ", " & $sRuntime)
    
		If tournament_loop($strategy, $getReward) Then
			_Function_Finished()
			Return
		EndIf
	Until $t>$count

	_Function_Finished()
EndFunc

Func ContinueTournament10($strategy, $payment, $getReward, $count)
	If initBlueStacks() Then
		_Function_Finished()
		Return
	EndIf

	If tournament_loop($strategy, $getReward) Then
		_Function_Finished()
		Return
	EndIf

	Tournament10($strategy, $payment, $getReward, $count-1)

EndFunc


Func tournament_loop($strategy, $getReward)

	Local $etape = 0
	Local $tournament_timer = TimerInit()
	;pour les etapes de 1 a 5
	Local $errorCount = 0
	Local $finished = False
	Do
		Local $step_timer = TimerInit()
		ConsoleWrite (TimerDiff($tournament_timer) & " : Etape "&$etape& @CRLF)
	;	pour chaque adversaire de 10 � 1
		Local $nbTests = 0
		Local $arrayForStrategy2[10] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
		Local $localStrategy = $strategy
		If $strategy == "RBBAA" Then
			Switch $etape
				Case 0
					$localStrategy = "Random"
				Case 1
					$localStrategy = "Bottom2Top"
				Case 2
					$localStrategy = "Bottom2Top"
				Case 3
					$localStrategy = "AboveMe"
				Case 4
					$localStrategy = "AboveMe"
			EndSwitch
		ElseIf $strategy == "RBBBA" Then
			Switch $etape
				Case 0
					$localStrategy = "Random"
				Case 1
					$localStrategy = "Bottom2Top"
				Case 2
					$localStrategy = "Bottom2Top"
				Case 3
					$localStrategy = "Bottom2Top"
				Case 4
					$localStrategy = "AboveMe"
			EndSwitch
		ElseIf $strategy == "RBAAA" Then
			Switch $etape
				Case 0
					$localStrategy = "Random"
				Case 1
					$localStrategy = "Bottom2Top"
				Case 2
					$localStrategy = "AboveMe"
				Case 3
					$localStrategy = "AboveMe"
				Case 4
					$localStrategy = "AboveMe"
			EndSwitch
		ElseIf $strategy == "BBBBA" Then
			Switch $etape
				Case 0
					$localStrategy = "Bottom2Top"
				Case 1
					$localStrategy = "Bottom2Top"
				Case 2
					$localStrategy = "Bottom2Top"
				Case 3
					$localStrategy = "Bottom2Top"
				Case 4
					$localStrategy = "AboveMe"
			EndSwitch
		EndIf
		Local $found = False
		Local $foundMyself = False
		Do
			$finished = (PixelGetColor($arena_end_button[0], $arena_end_button[1]) == $arena_end_button[2]);
			ConsoleWrite (@TAB & "Test fin tournoi : "&$finished& @CRLF)
			if $finished Then
				ExitLoop
			EndIf

			ConsoleWrite (@TAB & " test adversaire "&$nbTests& @CRLF)
			;choisis un adversaire � tester en fonction de la strat�gie
			If $localStrategy == "Bottom2Top" Then
				$opponent = 9-$nbTests
			ElseIf $localStrategy == "Top2Bottom" Then
				$opponent = $nbTests
			ElseIf $localStrategy == "Random" Then
				Do
					$opponent = Random(0, 9, 1)
				Until $arrayForStrategy2[$opponent] == 0
			ElseIf $localStrategy == "AboveMe" Then
				$opponent = 9-$nbTests
				;find myself
				;$counter = 9
				;Do
				;	$color = PixelGetColor($tournament_outline_self[2*($counter)], $tournament_outline_self[2*($counter)+1])
				;	If $color == $outline_self_color[0] Or $color = $outline_self_color[1] Then
				;		$foundMyself = True
				;	EndIf
				;	$counter = $counter-1
				;Until $counter<0 or $foundMyself == True

				;If $counter <=0 Then
				;	$localStrategy = "Bottom2Top"
				;	$opponent = 9-$nbTests
				;Else
				;	$opponent = $counter-1
				;EndIf
			EndIf


			;si l adversaire  est bleu/bronze/argent/or (dispo) alors
			Local $opponent_state = opponent_available($opponent)

			If $opponent_state == 2 Then
				ConsoleWrite (@TAB & "Ca n'est pas un adversaire. On tente de refermer la fenetre" & @CRLF)
				MouseClick("primary", $arena_outside_window[0], $arena_outside_window[1], 1)
				If _Interrupt_Sleep(500) Then
					Return True
				Endif;
				MouseClick("primary", $arena_outside_window[0], $arena_outside_window[1], 1)
				If _Interrupt_Sleep(1000) Then
					Return True
				Endif;
				$errorCount = $errorCount + 1

				If is_castle_under_attack() Then
					MouseClick("primary", $castle_under_attack_button[0], $castle_under_attack_button[1], 1)
					If _Interrupt_Sleep(3000) Then
						Return True
					Endif;
					$errorCount = 10
				EndIf

				;si on coince dans une boucle
				If $errorCount == 10 Then
					ConsoleWrite (@TAB & "On est dans une boucle. On reset" & @CRLF)
					If back_to_castle() Then
						Return True
					EndIf

					If open_map_and_arena() Then
						Return True
					EndIf

					$errorCount = 0
				EndIf

			ElseIf $opponent_state == 0 Then
				ConsoleWrite (@TAB & "Adversaire " & $opponent & " deja combattu" & @CRLF)
				If _Interrupt_Sleep(500) Then
					Return True
				Endif;
				$nbTests = $nbTests + 1;
				$arrayForStrategy2[$opponent] = 1

			ElseIf $opponent_state == 1 Then
				ConsoleWrite (@TAB & "Adversaire " & $opponent & " dispo." & @CRLF)
				opponent_select($opponent)
				If _Interrupt_Sleep(2000) Then
					Return True
				Endif;


				$color = PixelGetColor($arena_attack_button[0], $arena_attack_button[1])
				ConsoleWrite(@TAB & @TAB & "Couleur du bouton d'attaque : 0x" & hex($color, 6) & @CRLF)

	            ; si le bouton est gris
				If ($color == $arena_attack_button[4] or $color == $arena_attack_button[5]) Then
					;clic sur la croix de fermeture si l'action nest pas lancee
					MouseClick("primary", $arena_close_window[0], $arena_close_window[1], 1)
					If _Interrupt_Sleep(500) Then
						Return True
					Endif;
					ConsoleWrite (@TAB & @TAB & "Bouton gris. Etape non finie. On attend 10s, on reset" & @CRLF)
					If _Interrupt_Sleep(10000) Then
						Return True
					Endif;
					$nbTests = 0
					For $i = 0 to 9
						$arrayForStrategy2[$i] = 0
					Next

				; si le bouton est vert
				ElseIf $color == $arena_attack_button[2] or $color == $arena_attack_button[3] Then
					If $localStrategy == "AboveMe" and $foundMyself == False Then
						$nbTests = $nbTests + 1;
					Else
						ConsoleWrite (@TAB & @TAB & "Bouton vert. On attaque" & @CRLF)
						$found = True
					EndIf
				; si y'a pas de bouton
				Else
					If $localStrategy == "AboveMe" and $foundMyself == False Then
						If $nbTests == 0 Then
							ConsoleWrite (@TAB & @TAB & "c'est iom, mais je suis premier. Strat�gie bottom" & @CRLF)
							$localStrategy = "Bottom2Top"
						Else
							ConsoleWrite (@TAB & @TAB & "c'est iom, on attaque le suivant" & @CRLF)
							$foundMyself = True
						EndIf
					Else
						ConsoleWrite (@TAB & @TAB & "c'est iom!" & @CRLF)
						If _Interrupt_Sleep(1000) Then
							Return True
						Endif;
					Endif
					$nbTests = $nbTests + 1;
				EndIf

			EndIf

			If $nbTests > 9 and $localStrategy == "AboveMe" and not($found) Then
				ConsoleWrite (@TAB & "echec de la strat�gie AboveMe. On refait un tour en Bottom2Top" & @CRLF)
				$localStrategy = "Bottom2Top"
				$nbTests = 0
			EndIf


		Until $nbTests > 9 or $finished or $found or TimerDiff($step_timer) > 180000  or TimerDiff($tournament_timer) > 900000

		ConsoleWrite (@TAB & "adversaire choisi" & @CRLF)

		If $nbTests > 9 Then
			ConsoleWrite (@TAB & "ERREUR : PASSAGE DE 10 ADVERSAIRES SANS EN TROUVER UN." & @CRLF)
		EndIf

		If $finished or TimerDiff($step_timer) > 180000 or TimerDiff($tournament_timer) > 900000 Then
			ExitLoop
		EndIf
        
        ; On detecte le nom de l'adversaire pour plus tard :)
        $Buffer = ""
        If _OCR_Screen(370, 484, 800-370, 518-484, $Buffer ) Then
            ConsoleWrite (@TAB & @TAB & "Nom Adversaire ERROR" & @CRLF)
        Else
            ConsoleWrite (@TAB & @TAB & "Nom Adversaire = " & $Buffer & @CRLF)
        EndIf

		; on attaque
		If $isQuickFight Then
			MouseClick("primary", $arena_quickfight_button[0], $arena_quickfight_button[1], 1)
		Else
			MouseClick("primary", $arena_attack_button[0], $arena_attack_button[1], 1)
		Endif

		If _Interrupt_Sleep(1000) Then
			Return True
		Endif

		;no tools. Fly, you tools
		$color1 = PixelGetColor($no_tool[0], $no_tool[1])
		$color2 = PixelGetColor($no_tool[4], $no_tool[5])
		If ($color1 == $no_tool[2] or $color1 == $no_tool[3]) And ($color2 == $no_tool[6] or $color2 == $no_tool[7]) Then
			MouseClick("primary", $no_tool[0], $no_tool[1] , 1) ; ouioui, ils vont gagner de toute fa�on
			If _Interrupt_Sleep(1000) Then
				_Function_Finished()
				return
			Endif
		EndIf

		; on attend la fin de l attaque
		If fight($isQuickFight, false) Then
			Return True
		EndIf

		ConsoleWrite (@TAB & @TAB & "Fin de l'attaque" & @CRLF)
		If end_fight(False) Then
			Return True
		EndIf



		; VERSION 1.40.0 le bouton end fight renvoie au ch�teau
		;teste si on a le bouton carte. si oui, sort

		If _Interrupt_Sleep(10000) Then
			Return True
		Endif

		;If is_castle_screen() Then
			;open_arena(False)
		;EndIf

		;on passe a l �tape suivante
		$etape = $etape + 1;
		;If _Interrupt_Sleep(2000) Then	;VERSION 1.40.0 remettre � 10000 si correction
		;	Return True
		;Endif;


	Until $etape>=5 or $finished or TimerDiff($tournament_timer) > 900000


	If  $etape>=5 Then

		ConsoleWrite ("FIN : plus d'�tapes" & @CRLF)

		; VERSION 1.40.0 le bouton end fight renvoie au ch�teau
		If _Interrupt_Sleep(8000) Then
			Return True
		Endif
		If _castleOrMap() Then
      ConsoleWrite ("Not in arena menu" & @CRLF)
			open_map_and_arena(False)
		EndIf



		;on attend le bouton
		Local $timer = TimerInit()	;des fois il n'y a pas de bouton de fin avec une r�compense
		While PixelGetColor($arena_end_button[0], $arena_end_button[1]) <> $arena_end_button[2] and TimerDiff($timer) < 120000 and Not is_tournament_screen()
			If _Interrupt_Sleep(3000) Then
				Return True
			Endif;
			ConsoleWrite (@TAB & "Attend la fin" & @CRLF)
			;au cas ou il y ait un marteau, par exemple
			MouseClick("primary", $arena_outside_window[0], $arena_outside_window[1], 1)
      _Interrupt_Sleep(1000)
		WEnd
		ConsoleWrite ("FIN : cadeau sur le bouton" & @CRLF)
	ElseIf $finished Then
		ConsoleWrite ("FIN : cadeau sur le bouton (2)" & @CRLF)
	ElseIf TimerDiff($tournament_timer) > 900000 Then
		ConsoleWrite ("FIN : timer" & @CRLF)
    EndIf

  ; Close current arena
	MouseClick("primary", $arena_end_button[0], $arena_end_button[1], 1)
	If _Interrupt_Sleep(1500) Then
		Return True
	Endif


	If $getReward == "1" Then
		ConsoleWrite ("R�compense" & @CRLF)
		MouseClick("primary", $tournament_you_got_reward[0], $tournament_you_got_reward[1], 1)
		If _Interrupt_Sleep(2000) Then
			Return True
		Endif
		;MouseClickDrag("primary", 1270, 720, 1270, 480)
		If _Interrupt_Sleep(500) Then
			Return True
		Endif
		MouseClick("primary", $tournament_mail_reward[0], $tournament_mail_reward[1], 1)
		If _Interrupt_Sleep(4000) Then
			Return True
		Endif
		MouseClick("primary", $tournament_close_mail[0], $tournament_close_mail[1], 1)


		;ici, on est de retour au ch�teau, il peut y avoir une pub pour les pommes
	EndIf

	If _Interrupt_Sleep(4000) Then
		Return True
	Endif


	Return False
EndFunc


Func opponent_available($number)
	Local $x, $y

	$x = $tournament_opponents_coords[2*($number)]
	$y = $tournament_opponents_coords[2*($number)+1]

	$color = PixelGetColor($x, $y)
	ConsoleWrite(@TAB & "Couleur en ("&$x&", "&$y&") = 0x" & hex($color, 6) & @CRLF)

	If $color == $first_opponent_color[0] or $color == $first_opponent_color[1] or $color == $first_opponent_color[2] or $color == $first_opponent_color[3] Then
		ConsoleWrite (@TAB & "couleur du premier" & @CRLF)
		Return 1
	ElseIf $color == $second_opponent_color[0] or $color == $second_opponent_color[1] or $color == $second_opponent_color[2] Then
		ConsoleWrite (@TAB & "couleur du deuxieme" & @CRLF)
		Return 1
	ElseIf $color == $third_opponent_color[0] or $color == $third_opponent_color[1] or $color == $third_opponent_color[2] Then
		ConsoleWrite (@TAB & "couleur du troisieme" & @CRLF)
		Return 1
	ElseIf $color == $other_opponent_color[0] or $color == $other_opponent_color[1] or $color == $other_opponent_color[2] Then
		ConsoleWrite (@TAB & "couleur des suivants" & @CRLF)
		Return 1
	ElseIf $color == $fought_opponent_color[0] or $color == $fought_opponent_color[1] or $color == $fought_opponent_color[2] Then
		ConsoleWrite (@TAB & "couleur d�j� combattu" & @CRLF)
		Return 0
	Else
		ConsoleWrite(@TAB & "Couleur en ("&$x&", "&$y&") = 0x" & hex($color, 6) & @CRLF)
		Return 2
	EndIf
	;couleur autour du joueur 0xFFF359
EndFunc

Func opponent_select($number)
	Local $x, $y

	$x = $tournament_opponents_coords[2*($number)]
	$y = $tournament_opponents_coords[2*($number)+1]

	MouseClick("primary", $x, $y, 1)
EndFunc

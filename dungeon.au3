;
#include-once

#include "data/hc_coords.au3"



Global Const $spot_window[8] = [940, 447, 0x70D231, 0xC3FCCB, 0xCEFFFF, 0x203811, 0x6CCA2F, 0x1F3610]	;vert (bouton), pastel (sacrifice), vert boss, vert boss #2, vert #3
Global Const $versus[3] = [757, 183, 0xFBBC04]

Global Const $end_quest_button[4] = [1299, 851, 0x4FC30D, 0x4CBB0C]
Global Const $end_quest_confirm[2] = [632, 594]
Global Const $invite_partner[4] = [1329, 775, 0x67C526, 0x70D231]
Global Const $restart_quest[4] = [1256, 778, 0x6CCA2F, 0xF16147] ; vert, rouge (partner left)
Global Const $accept[2] = [690, 814]
Global Const $accept_restart[2] = [620, 814]
Global Const $confirm[4] = [772, 814, 0x08A210, 0x089C0F]
Global Const $confirmButtonAfterSacrificeChoice[2] = [693, 756]
Global Const $confirmAfterSacrifice[3] = [944, 171, 0xF16147]
Global Const $confirmAfterChoice[3] = [1012, 330, 0x1BF7FF] ; TO BE TESTED

Global Const $firstWarriorDungeon[4] = [320, 580, 1380, 580]
Global Const $warriorsHealthBars[4] = [160, 720, 1380, 724] ;662 hauteur pour la cath�drale
Global Const $warriorsHealthBarsMortal = 715 ;662 hauteur pour la cath�drale
Global Const $warriorsHealthBarsSacrifice = 678 ;662 hauteur pour la cath�drale
Global Const $warriorBgColorDungeon = 0x80D196
Global Const $healthBarColors[4] = [0x6AED80, 0x6EF785, 0xD04621, 0xD84922]

Global Const $fightScreenTabs[4] = [827, 78, 0xD0B994, 0xD9C19A]
Global Const $quickFightButtonDongeon[4] = [930, 814, 0xF49205, 0xEA8C05]
Global Const $deployBest_Leader[2] = [600, 437]
Global Const $deployBest_Follower[2] = [1200, 437]

Global Const $leaderReadyButton[4] = [500, 449, 0x6CCA2F, 0xF33520] ; coords, green, red
Global Const $leaderSacrificeReadyButton[4] = [450, 449, 0x6CCA2F, 0xF33520] ; coords, green, red
Global Const $followerSacrificeReadyButton[4] = [1050, 449, 0x6CCA2F, 0xF33520] ; coords, green, red
Global Const $followerReadyButton[5] = [1050, 449, 0x6CCA2F, 0xF33520, 0xFF3421] ; coords, green, red (hover), red
Global Const $influenceLeftReadyButton[4] = [400, 453, 0x6CCA2F, 0x70D231] ; coords, green, hover
Global Const $influenceRightReadyButton[4] = [1041, 453, 0x6CCA2F, 0x70D231] ; coords, green, hover
Global Const $influenceLeftChoiceTest[6] = [242, 300, 0x910C18, 0x3F890B, 0x3F870B, 0x3F860B] ; coords, green, red
Global Const $leaderFightButton[6] = [1157, 779, 0x37C771, 0x3ACB71, 0x6ED217, 0x73DB18] ; coords, grayed hover, grayed, ready hover, ready
Global Const $leaderSacrificeButton[6] = [1154, 736, 0x37C771, 0x3ACB71, 0x6ED217, 0x73DB18] ; coords, grayed hover, grayed, ready hover, ready

Global Const $mortalReadyButton[2] = [724, 438]

Global Const $malus_close_button[3] = [679, 791, 0x4FC30D]
Global Const $rare_close_button[3] = [680, 848, 0x4FC30D]
Global Const $coin_toss_close_button[4] = [769, 765, 0x6ED217, 0x73DB18]

Global Const $go_there_button[3] = [1165, 783, 0x73DB18]
Global Const $select_squad_rect[4] = [145, 500, 1245, 240]
Global Const $rez_button[5] = [752, 569, 0x63BE21, 0x63C023, 0x5FB720]

Global Const $partner_early_left[3] = [914, 750, 0xF16147]

Global $isQuickFight

Func DungeonLoop($count)
  Local $sRuntime = ""

	If initBlueStacks() Then
		_Function_Finished()
		Return
	EndIf

  ; Assume dunjeon is already launched, toss passed

  For $loop = 0 to $count-1
    Runtime_In_Hour_Min_Sec($sRuntime)
    _dungeon_log_INFO("Dungeon run done " & $loop & " on " & $count & ", " & $sRuntime & @CRLF)
    PrintGUIStatus("Dungeon run done " & $loop & " on " & $count & ", " & $sRuntime)

    ; Check who am I
    $whoami = DungeonWhoAmI()

    ; Start dungeon script
    If $whoami == "leader" Then
      If DungeonLeader() Then
        ExitLoop
      EndIf
    ElseIf $whoami == "follower" Then
      If DungeonFollower() Then
        ExitLoop
      EndIf
    EndIf

    ; Dungeon is over
    If $loop < $count-1 Then
      ; Enable periodic capcha test during dungeon restart
      $enablePeriodicCapchaTest = True
    
      ; Restart
      If RestartDungeon() Then
        ExitLoop
      EndIf
      
      ; Disable periodic capcha test after restarting the dungeon
      $enablePeriodicCapchaTest = False
    EndIf

  Next

	_Function_Finished()
EndFunc

;first dungeon must be launched manually to find a partner. So the bot starts in dungeon screen
;if no coin toss, open a level. If tab is blue and button is green, you're the leader. If tab is beige or button is disabled green, you're the follower

Func Dungeon($whoami)
	If $whoami == "leader" Then
		DungeonLeader()
	ElseIf $whoami == "follower" Then
		DungeonFollower()
	Else
		;attend l'�cran de d�marrage du donjon
		If WaitDungeonToss($whoami, 60000) Then
			_Function_Finished()
			Return
		EndIf

		;clique sur valider
		If $whoami == "leader" Then
			DungeonLeader()
		ElseIf $whoami == "follower" Then
			DungeonFollower()
		EndIf
	EndIf

EndFunc

Func DungeonWhoAmI()

  Local $offset_x = 1045
  Local $end = 800
  Local $yellow_color = 0xFFF600
  Do
    $color = PixelGetColor($offset_x, 810)
    $offset_x = $offset_x - 5
  Until( $offset_x <= $end or $color == $yellow_color )

  If $color == $yellow_color Then
		_dungeon_log_INFO("DungeonWhoAmI - Found a yellow in Follower box" & @CRLF)
		return "follower"
  Else
		_dungeon_log_INFO("DungeonWhoAmI - Found a yellow in Leader box" & @CRLF)
		return "leader"
  EndIf

EndFunc

Func DungeonLeader()
  Local $partner_left = false

	If initBlueStacks() Then
		_Function_Finished()
		Return True
	EndIf

	For $step = 0 to 50
		Local $x, $y, $type
		If Not FindNextStage($x, $y, $type, $partner_left) Then
			If FindImage("dungeon_ready_button.png") Then ;on est dans un �cran de combat, c'est probablement qu'on a perdu
				_dungeon_log_INFO("DungeonLeader - Fight lost" & @CRLF)
				$type = "fightlost"
			ElseIf FindImage("dungeon_rare_reward.png") Then ;r�compense rare. On sort et on re-tente de trouver le niveau suivant
				_dungeon_log_INFO("DungeonLeader - rare reward" & @CRLF)
				MouseClick("primary", $rare_close_button[0], $rare_close_button[1], 1)
				If _Interrupt_Sleep(2000) Then
					_Function_Finished()
					return True
				Endif
				FindNextStage($x, $y, $type, $partner_left)
			Else
				ExitLoop
			EndIf
		EndIf

		;si on est pas rest� dans un �cran combat, on ouvre le niveau suivant
		If $type <> "fightlost" Then
		; Open dungeon waypoint
			MouseClick("primary", $x, $y, 1, 1)
			If _Interrupt_Sleep(500) Then
				_Function_Finished()
				Return True
			Endif

		; Select dungeon waypoint
			MouseClick("primary", $go_there_button[0], $go_there_button[1], 1, 1)
			If _Interrupt_Sleep(500) Then
				_Function_Finished()
				Return True
			Endif

			;not the good tab
			$color = PixelGetColor($fightScreenTabs[0], $fightScreenTabs[1])
			If $color == $fightScreenTabs[2] or $color == $fightScreenTabs[3] Then
				MouseClick("primary", $fightScreenTabs[0], $fightScreenTabs[1], 1, 1)
				If _Interrupt_Sleep(500) Then
					_Function_Finished()
					Return True
				Endif
			Endif
		Else
			$type = "fight"
		EndIf


		;�cran baston
		If $type == "fight" or $type == "boss" or $type == "shadow" Then ; enemy
			If DungeonLeaderFight() Then
				_Function_Finished()
				Return True
			Endif
		Endif

		;�cran combat mortel
		If $type == "mortal" Then ; enemy
			If DungeonLeaderMortal() Then
				_Function_Finished()
				Return True
			Endif
		Endif

		;�cran sacrifice
		If $type == "sacrifice" Then ; enemy
			If DungeonLeaderSacrifice() Then
				_Function_Finished()
				Return True
			Endif
		Endif

		;�cran influence invisible
		If $type == "invisible" Then ; enemy
			If DungeonLeaderInfluence() Then
				_Function_Finished()
				Return True
			Endif
		Endif
	Next

  If $partner_left Then
    return true
  Else
    return false
  EndIf
EndFunc

;if it's not a full boss dungeon (or a 5 boss or a 4 boss), restart dungeon and loop

Func DungeonBoss()

	If initBlueStacks() Then
		_Function_Finished()
		Return True
	EndIf

	Local $timeout = 6000 ;first timeout : 6 seconds as we're supposed to already be in the dungeon. After loop, we wait more
	For $step = 0 to 20 ;TODO parameter
		;if coin toss
		Local $whoami
		If WaitDungeonToss($whoami, $timeout) Then ;TODO recognize loading screen.
			_Function_Finished()
			Return True
		EndIf
		MouseClick("primary", $coin_toss_close_button[0], $coin_toss_close_button[1], 1, 1)
		If _Interrupt_Sleep(1000) Then
			Return True
		Endif
		$timeout=60000 ;one minute as for now

		;dungeon started TODO test?

		;else if boss dungeon
		If is_dungeon_boss() Then
			_dungeon_log_INFO("DungeonBoss - Found! GOGOGO!!" & @CRLF)
			DungeonBossLoop($whoami)
		;restart TODO not else. restart after boss loop anyway
		Else
			_dungeon_log_INFO("DungeonBoss - Restart dungeon" & @CRLF)
			If _Interrupt_Sleep(2000) Then ;let the poor dev interrupt . TODO remove
				Return True
			Endif
			RestartDungeon()
		EndIf
	Next

	Return False
EndFunc

Func is_dungeon_boss()
	Return FindImage("dungeon_seven_boss.png")
EndFunc

Func DungeonBossLoop($whoami)
	If $whoami == "leader" Then
		DungeonLeader()
	ElseIf $whoami == "follower" Then
		DungeonFollower()
	EndIf
EndFunc

Func CheckSpecialReward()
  Local Const $special_reward = _OpenCV_imread_and_check(_OpenCV_FindFile("dungeon_special_reward.png"))

  ;opencv screenshot
  Local Const $threshold = 0.9
	Local $hwnd
	Local $aRect = WinGetPos($hwnd)
	Local $img = _OpenCV_GetDesktopScreenMat($aRect)

  ;find match fight
	Local $aMatches = _OpenCV_FindTemplate($img, $special_reward, $threshold)
	If UBound($aMatches) Then
		_dungeon_log_INFO("CheckSpecialReward - Found a special reward" & @CRLF)
		MouseClick("primary", 766, 802, 1, 1)
		If _Interrupt_Sleep(5000) Then
			_Function_Finished()
			Return
		Endif
  EndIf
EndFunc


Func FindNextStage(ByRef $x, ByRef $y, ByRef $type, ByRef $partner_left)
	Local Const $fight = _OpenCV_imread_and_check(_OpenCV_FindFile("dungeon_fight.png"))
	Local Const $boss = _OpenCV_imread_and_check(_OpenCV_FindFile("dungeon_boss.png"))
	Local Const $boss2 = _OpenCV_imread_and_check(_OpenCV_FindFile("dungeon_boss 2.png"))
	Local Const $boss3 = _OpenCV_imread_and_check(_OpenCV_FindFile("dungeon_boss 3.png"))
	Local Const $mortal = _OpenCV_imread_and_check(_OpenCV_FindFile("dungeon_mortal.png"))
	Local Const $mortal3 = _OpenCV_imread_and_check(_OpenCV_FindFile("dungeon_mortal3.png"))
	Local Const $sacrifice = _OpenCV_imread_and_check(_OpenCV_FindFile("dungeon_sacrifice.png"))
	Local Const $invisible = _OpenCV_imread_and_check(_OpenCV_FindFile("dungeon_invisible.png"))
	Local Const $shadow = _OpenCV_imread_and_check(_OpenCV_FindFile("dungeon_shadow.png"))
	Local Const $shadow2 = _OpenCV_imread_and_check(_OpenCV_FindFile("dungeon_shadow 2.png"))
	Local Const $special_reward = _OpenCV_imread_and_check(_OpenCV_FindFile("dungeon_special_reward.png"))
	Local Const $threshold = 0.9

	;opencv screenshot
	Global $hwnd
	Local $aRect = WinGetPos($hwnd)
	;Local $aRect[4] = [5, 12, 1539, 910] ;todo prendre les mesures de la fen�tre
	Local $img = _OpenCV_GetDesktopScreenMat($aRect)


  $partner_left = false

  ;find match fight
	Local $aMatches = _OpenCV_FindTemplate($img, $special_reward, $threshold)
	If UBound($aMatches) Then
		_dungeon_log_INFO("FindNextStage - Found a special reward" & @CRLF)
		MouseClick("primary", 766, 802, 1, 1)
		If _Interrupt_Sleep(2000) Then
			_Function_Finished()
			Return False
		Endif

		$img = _OpenCV_GetDesktopScreenMat($aRect)
	EndIf

  ; Check if partner left
  $color = PixelGetColor($partner_early_left[0], $partner_early_left[1])
  If $color == $partner_early_left[1] Then
    _dungeon_log_INFO("FindNextStage - Partner has left ..."& @CRLF)
    $partner_left = true
  EndIf

	;find match fight
	Local $aMatches = _OpenCV_FindTemplate($img, $fight, $threshold)
	If UBound($aMatches) Then
		_dungeon_log_TRACE("FindNextStage - Found a fight" & @CRLF)
		$x = $aMatches[0][0] + Int( $fight.width/4 )
		$y = $aMatches[0][1] + Int( $fight.height/4 )
		$type = "fight"
		Return True
	EndIf

	;find match boss
	$aMatches = _OpenCV_FindTemplate($img, $boss, $threshold)
	If UBound($aMatches) Then
		_dungeon_log_TRACE("FindNextStage - Found a boss" & @CRLF)
		$x = $aMatches[0][0] + Int( $boss.width/4 )
		$y = $aMatches[0][1] + Int( $boss.height/4 )
		$type = "boss"
		Return True
	EndIf

	;find match boss
	$aMatches = _OpenCV_FindTemplate($img, $boss2, $threshold)
	If UBound($aMatches) Then
		_dungeon_log_TRACE("FindNextStage - Found a boss (2)" & @CRLF)
		$x = $aMatches[0][0] + Int( $boss2.width/4 )
		$y = $aMatches[0][1] + Int( $boss2.height/4 )
		$type = "boss"
		Return True
	EndIf

	;find match boss
	$aMatches = _OpenCV_FindTemplate($img, $boss3, $threshold)
	If UBound($aMatches) Then
		_dungeon_log_TRACE("FindNextStage - Found a boss (3)" & @CRLF)
		$x = $aMatches[0][0] + Int( $boss3.width/4 )
		$y = $aMatches[0][1] + Int( $boss3.height/4 )
		$type = "boss"
		Return True
	EndIf

	;find match combat
	$aMatches = _OpenCV_FindTemplate($img, $mortal, $threshold)
	If UBound($aMatches) Then
		_dungeon_log_TRACE("FindNextStage - Found a mortal combat" & @CRLF)
		$x = $aMatches[0][0] + Int( $mortal.width/4 )
		$y = $aMatches[0][1] + Int( $mortal.height/4 )
		$type = "mortal"
		Return True
	EndIf

	;find match combat
	$aMatches = _OpenCV_FindTemplate($img, $mortal3, $threshold)
	If UBound($aMatches) Then
		_dungeon_log_TRACE("FindNextStage - Found a mortal combat (3)" & @CRLF)
		$x = $aMatches[0][0] + Int( $mortal3.width/4 )
		$y = $aMatches[0][1] + Int( $mortal3.height/4 )
		$type = "mortal"
		Return True
	EndIf

	;find match sacrifice
	$aMatches = _OpenCV_FindTemplate($img, $sacrifice, $threshold)
	If UBound($aMatches) Then
		_dungeon_log_TRACE("FindNextStage - Found a sacrifice" & @CRLF)
		$x = $aMatches[0][0] + Int( $sacrifice.width/4 )
		$y = $aMatches[0][1] + Int( $sacrifice.height/4 )
		$type = "sacrifice"
		Return True
	EndIf

	;find match invisible
	$aMatches = _OpenCV_FindTemplate($img, $invisible, $threshold)
	If UBound($aMatches) Then
		_dungeon_log_TRACE("FindNextStage - Found an invisible" & @CRLF)
		$x = $aMatches[0][0] + Int( $invisible.width/4 )
		$y = $aMatches[0][1] + Int( $invisible.height/4 )
		$type = "invisible"
		Return True
	EndIf

	;find match shadow fight
	$aMatches = _OpenCV_FindTemplate($img, $shadow, $threshold)
	If UBound($aMatches) Then
		_dungeon_log_TRACE("FindNextStage - Found a shadow fight" & @CRLF)
		$x = $aMatches[0][0] + Int( $shadow.width/4 )
		$y = $aMatches[0][1] + Int( $shadow.height/4 )
		$type = "shadow"
		Return True
	EndIf

	;find match shadow fight
	$aMatches = _OpenCV_FindTemplate($img, $shadow2, $threshold)
	If UBound($aMatches) Then
		_dungeon_log_TRACE("FindNextStage - Found a shadow fight (2)" & @CRLF)
		$x = $aMatches[0][0] + Int( $shadow2.width/4 )
		$y = $aMatches[0][1] + Int( $shadow2.height/4 )
		$type = "shadow"
		Return True
	EndIf

	_dungeon_log_ERROR("FindNextStage - Nothing found" & @CRLF)
	Return False
EndFunc

Func DungeonLeaderFight()
	_dungeon_log_INFO("DungeonLeaderFight - Fill squad" & @CRLF)
	If FillSquadTest("fight", true) Then
		Return True
	Endif

	_dungeon_log_TRACE("DungeonLeaderFight - wait" & @CRLF)
	If _Interrupt_Sleep(500) Then
		Return True
	Endif

	;incomplete team
	$color1 = PixelGetColor($incomplete_team[0], $incomplete_team[1])
	$color2 = PixelGetColor($incomplete_team[4], $incomplete_team[5])
	If ($color1 == $incomplete_team[2] or $color1 == $incomplete_team[3]) And ($color2 == $incomplete_team[6] or $color2 == $incomplete_team[7]) Then
		MouseClick("primary", $incomplete_team[0], $incomplete_team[1], 1, 1) ; ouioui, ils vont gagner de toute façon
		If _Interrupt_Sleep(500) Then
			_Function_Finished()
			return
		Endif
	EndIf

	;confirme
	Local $x=-1, $y=-1
	FindImageCoord($hwnd, "dungeon_ready_button.png", $x, $y)

	If $x==-1 or $y==-1 Then
		_dungeon_log_ERROR("DungeonLeaderFight - Erreur bouton ready non trouve" & @CRLF)
	Else
		MouseClick("primary", $x+20, $y+20, 1, 1)
		_dungeon_log_TRACE("DungeonLeaderFight - Fight - clic pret"& @CRLF)
	EndIf

	If _Interrupt_Sleep(500) Then
		Return True
	Endif

	;attend que le bouton se colorie (aka l'autre joueur a confirm�)
	_dungeon_log_INFO("DungeonLeaderFight - wait other player" & @CRLF)
	If WaitFollower() Then
		Return True
	EndIf

	If $isQuickFight Then
		MouseClick("primary", $quickFightButtonDongeon[0], $quickFightButtonDongeon[1] , 1, 1)
	Else
		MouseClick("primary", $leaderFightButton[0], $leaderFightButton[1] , 1, 1)
	Endif

	If _Interrupt_Sleep(500) Then
		Return True
	Endif

	; on attend la fin de l attaque
	If fight(true, false) Then
		Return True
	EndIf

	_dungeon_log_INFO(@TAB & @TAB & "DungeonLeaderFight - Fin de l'attaque" & @CRLF)
	If end_fight(False) Then
		Return True
	EndIf

	If $isQuickFight == false Then
		If _Interrupt_Sleep(4000) Then
		Return True
		Endif
	EndIf

	;if event, click somewhere to pass reward
	MouseClick("primary", 1195, 845 , 1, 1)

	Return false
EndFunc

Func DungeonLeaderMortal()
	_dungeon_log_INFO("DungeonLeaderMortal - Fill squad" & @CRLF)
	If FillSquadTest("mortal", true) Then
		Return True
	Endif

	_dungeon_log_INFO("DungeonLeaderMortal - wait" & @CRLF)
	If _Interrupt_Sleep(500) Then
		Return True
	Endif

	;confirme
	MouseClick("primary", $mortalReadyButton[0], $mortalReadyButton[1], 1, 1)

	_dungeon_log_INFO("DungeonLeaderMortal - wait other player" & @CRLF)
	;attend que le bouton se colorie (aka l'autre joueur a confirm�)
	If WaitFollower() Then
		Return True
	EndIf

	If $isQuickFight Then
		MouseClick("primary", $quickFightButtonDongeon[0], $quickFightButtonDongeon[1] , 1, 1)
	Else
		MouseClick("primary", $leaderFightButton[0], $leaderFightButton[1] , 1, 1)
	Endif

	If _Interrupt_Sleep(500) Then
		Return True
	Endif

	; on attend la fin de l attaque
	If fight(true, false) Then
		Return True
	EndIf

	_dungeon_log_TRACE(@TAB & @TAB & "DungeonLeaderMortal - Fin de l'attaque" & @CRLF)
	If end_fight(False) Then
		Return True
	EndIf

	If $isQuickFight == false Then
		If _Interrupt_Sleep(10000) Then
		  Return True
		Endif
	EndIf

	Return False
EndFunc

Func DungeonLeaderSacrifice()
	_dungeon_log_INFO("DungeonLeaderSacrifice - Fill squad" & @CRLF)
	If FillSquadTest("sacrifice", true) Then
		Return True
	Endif;


	_dungeon_log_TRACE("wait" & @CRLF)
	If _Interrupt_Sleep(500) Then
		Return True
	Endif;

	;confirme
	MouseClick("primary", $leaderSacrificeReadyButton[0], $leaderSacrificeReadyButton[1], 1, 1)

	_dungeon_log_INFO("DungeonLeaderSacrifice - wait other player" & @CRLF)
	;attend que le bouton se colorie (aka l'autre joueur a confirm�)
	If WaitReadyFollower() Then
		Return True
	EndIf

	MouseClick("primary", $leaderSacrificeButton[0], $leaderSacrificeButton[1], 1, 1)

	If _Interrupt_Sleep(1500) Then
		Return True
	Endif;

	MouseClick("primary", $accept[0], $accept[1], 1, 1)

	If _Interrupt_Sleep(1000) Then
		_Function_Finished()
		return True
	Endif

	Return false
EndFunc

Func DungeonLeaderInfluence()
	If Choose() Then
		Return True
	EndIf

	_dungeon_log_INFO("DungeonLeaderInfluence - wait other player" & @CRLF)
	;attend que le bouton se colorie (aka l'autre joueur a confirm�)
	If WaitReadyFollower() Then
		Return True
	EndIf

	MouseClick("primary", $leaderSacrificeButton[0], $leaderSacrificeButton[1], 1, 1)

  ;attends d'avoir un bouton vert
  Local $iBegin = TimerInit()
  Local $color
  Do
    $color = PixelGetColor($confirmAfterChoice[0], $confirmAfterChoice[1])
    If _Interrupt_Sleep(100) Then
      Return True
    Endif
  Until TimerDiff($iBegin) > 10000 or $color == $confirmAfterChoice[2]
  MouseClick("primary", $confirmButtonAfterSacrificeChoice[0], $confirmButtonAfterSacrificeChoice[1], 1, 1)

	Return False
EndFunc

Func WaitFollower()
	Local $iBegin = TimerInit()
  Local $firstLoop = true
	Do
		If _Interrupt_Sleep(100) Then
			Return True
		Endif;

		$color = PixelGetColor($leaderFightButton[0], $leaderFightButton[1])
    If $firstLoop Then
      _dungeon_log_TRACE(@TAB & "WaitFollower - Attente. Couleur du bouton d'attaque : 0x" & hex($color, 6) & @CRLF)
      $firstLoop = false
    EndIf
		If $color == $leaderFightButton[4] or $color == $leaderFightButton[5] Then
			Return False
		EndIf

	Until TimerDiff($iBegin) > 120000 ; 2min or it's dead

	_dungeon_log_ERROR(@TAB & @TAB & "WaitFollower - Fin du chrono" & @CRLF)
	;alert : second is inactive, end of script
	Return True

EndFunc

Func WaitReadyFollower()
	Local $iBegin = TimerInit()
  Local $firstLoop = true
	Do
		If _Interrupt_Sleep(300) Then
			Return True
		Endif;

		$color = PixelGetColor($leaderSacrificeButton[0], $leaderSacrificeButton[1])
		If $firstLoop Then
      _dungeon_log_TRACE(@TAB & "WaitReadyFollower - Attente. Couleur du bouton d'attaque : 0x" & hex($color, 6) & @CRLF)
      $firstLoop = false
    EndIf
		If $color == $leaderSacrificeButton[4] or $color == $leaderSacrificeButton[5] Then
			Return False
		EndIf

	Until TimerDiff($iBegin) > 60000 ; 1min or it's dead

	_dungeon_log_ERROR(@TAB & @TAB & "WaitReadyFollower - Fin du chrono" & @CRLF)
	;alert : second is inactive, end of script
	Return True

EndFunc

Func DungeonFollower()
	If initBlueStacks() Then
		_Function_Finished()
		Return True
	EndIf

	Local Const $dungeon_detect_fight = _OpenCV_imread_and_check(_OpenCV_FindFile("dungeon_fight_menu_detection.png"))
	Local Const $dungeon_detect_mortal = _OpenCV_imread_and_check(_OpenCV_FindFile("dungeon_mortal_window_detection.png"))
	Local Const $dungeon_detect_sacrifice = _OpenCV_imread_and_check(_OpenCV_FindFile("dungeon_sacrifice_detection.png"))
	Local Const $dungeon_detect_choice = _OpenCV_imread_and_check(_OpenCV_FindFile("dungeon_choice_detection.png"))

	Local $x
	Local $y
	Local $x_prev = 0
	Local $y_prev = 0
	Local $waypoint_detected
	Local $same_spot_count = 0

	$dungeon_finished = False
	Do
		; Dummy click for event rewards ...
		MouseClick("primary", 1516, 179, 1, 1)

		;_dungeon_log_TRACE("DungeonFollower - boucle"& @CRLF)

		; TODO : check if still necessary
		$color = PixelGetColor($malus_close_button[0], $malus_close_button[1])
		If $color == $malus_close_button[2] Then
			_dungeon_log_INFO("DungeonFollower - Malus button detected & closed"& @CRLF)
			MouseClick("primary", $malus_close_button[0], $malus_close_button[1], 1, 1)
			If _Interrupt_Sleep(5000) Then
				_Function_Finished()
				return True
			Endif
		EndIf

		; TODO : check if still functional
		$rare_reward = FindImageCoord2($hwnd, "dungeon_special_reward.png", $x, $y, 0.95)
		If $rare_reward Then
			_dungeon_log_INFO("DungeonFollower - Rare reward detected & closed"& @CRLF)
			MouseClick("primary", $rare_close_button[0], $rare_close_button[1], 1, 1)
			If _Interrupt_Sleep(5000) Then
				_Function_Finished()
				return True
			Endif
		EndIf

		; Check if partner left
		$color = PixelGetColor($partner_early_left[0], $partner_early_left[1])
		If $color == $partner_early_left[1] Then
			_dungeon_log_INFO("DungeonFollower - Partner has left ..."& @CRLF)
			return true
		EndIf

		;recherche l'image "d�m�nager ici"
		;clique si trouv�
		$waypoint_detected = FindImageCoord2($hwnd, "dungeon_select.png", $x, $y, 0.95)

		If Not $waypoint_detected Then	; test another
			$waypoint_detected = FindImageCoord2($hwnd, "dungeon_select2.png", $x, $y, 0.95)
		EndIf

		If Not $waypoint_detected Then	; test another
			$waypoint_detected = FindImageCoord2($hwnd, "dungeon_select_right.png", $x, $y, 0.95)
			If $waypoint_detected Then	; found right coord. Needs to move $x to the left for next operations
				$x = $x - 210
			EndIf
		EndIf

		; Check if this is not the same waypoint as already processed
		Local $same_spot
		If $x == $x_prev and $y == $y_prev Then
			$same_spot = True
			$same_spot_count = $same_spot_count + 1
			;_dungeon_log_TRACE("DungeonFollower - same spot "& $same_spot_count & @CRLF)

			If $same_spot_count == 50 Then ; au bout de 50x le m�me spot, y'a p't'�tre un souci de blocage. On r�initialise
				$same_spot_count = 0
				$x_prev = 0
				$y_prev = 0
				$same_spot = False
				;_dungeon_log_TRACE("DungeonFollower - same spot reset"& @CRLF)
			EndIf
		Else
			$same_spot = False
			$same_spot_count = 0
		EndIf

		If $waypoint_detected and Not $same_spot Then

			; Record waypoint to filter it next time
			$x_prev = $x
			$y_prev = $y

			;clique sur le spot correspondant
			MouseClick("primary", $x+100, $y+80, 1, 1)
			_dungeon_log_TRACE("DungeonFollower - clic spot ("&$x+80&","&$y+80&")"& @CRLF)
			If _Interrupt_Sleep(500) Then
				_Function_Finished()
				return True
			Endif

			;not the good tab
			$color = PixelGetColor($fightScreenTabs[0], $fightScreenTabs[1])
			If $color == $fightScreenTabs[2] or $color == $fightScreenTabs[3] Then
				MouseClick("primary", $fightScreenTabs[0], $fightScreenTabs[1], 1, 1)
				If _Interrupt_Sleep(500) Then
					_Function_Finished()
					Return True
				Endif
			Endif

			; Look for menu type
			Local $aRect = WinGetPos($hwnd)
			Local $img = _OpenCV_GetDesktopScreenMat($aRect)

			Local $searching_type = true

			;si c'est un combat mortel, met des bonshommes
			If $searching_type Then
				Local $aMatches_mortal = _OpenCV_FindTemplate($img, $dungeon_detect_mortal, 0.80)
				If UBound($aMatches_mortal) Then
					$searching_type = false
					_dungeon_log_INFO("DungeonFollower - Mortal combat detected"& @CRLF)
					fillsquadTest("mortal", false)
					If _Interrupt_Sleep(500) Then
						_Function_Finished()
						return True
					Endif

					; Ready
					MouseClick("primary", $mortalReadyButton[0], $mortalReadyButton[1], 1, 1)
				EndIf
			EndIf

			;si c'est un spot combat (tester bouton vert, valide)
			If $searching_type Then
				Local $aMatches_fight = _OpenCV_FindTemplate($img, $dungeon_detect_fight, 0.80)
				If UBound($aMatches_fight) Then
					$searching_type = false
					_dungeon_log_INFO("DungeonFollower - Fight found"& @CRLF)

					fillsquadTest("fight", false)

					Local $x=-1, $y=-1
					FindImageCoord($hwnd, "dungeon_ready_button.png", $x, $y)

					If $x==-1 or $y==-1 Then
						_dungeon_log_ERROR ("DungeonFollower - Erreur bouton ready non trouve" & @CRLF)
					Else
						MouseClick("primary", $x+20, $y+20, 1, 1)
						_dungeon_log_TRACE("DungeonFollower - Fight - clic pret"& @CRLF)
					EndIf

					If _Interrupt_Sleep(500) Then
						_Function_Finished()
						return True
					Endif

					;	incomplete team
					$color1 = PixelGetColor($incomplete_team[0], $incomplete_team[1])
					$color2 = PixelGetColor($incomplete_team[4], $incomplete_team[5])
					If ($color1 == $incomplete_team[2] or $color1 == $incomplete_team[3]) And ($color2 == $incomplete_team[6] or $color2 == $incomplete_team[7]) Then
						_dungeon_log_INFO("DungeonFollower - Fight - Incomplete team"& @CRLF)
						MouseClick("primary", $incomplete_team[0], $incomplete_team[1] , 1, 1) ; ouioui, ils vont gagner de toute façon
						If _Interrupt_Sleep(500) Then
							_Function_Finished()
							return
						Endif
					EndIf
				EndIf
			EndIf

			;si c'est influence
			If $searching_type Then
				Local $aMatches_choice = _OpenCV_FindTemplate($img, $dungeon_detect_choice, 0.95)
				If UBound($aMatches_choice) Then
					$searching_type = false
					_dungeon_log_INFO("DungeonFollower - Influence detected"& @CRLF)
					Choose()
					If _Interrupt_Sleep(500) Then
						_Function_Finished()
						return True
					Endif

					;attends d'avoir un bouton vert
					Local $iBegin = TimerInit()
					Local $color
					Do
						$color = PixelGetColor($confirmAfterChoice[0], $confirmAfterChoice[1])
						If _Interrupt_Sleep(100) Then
							_Function_Finished()
							return True
						Endif
					Until TimerDiff($iBegin) > 10000 or $color == $confirmAfterChoice[2]

          Do
            MouseClick("primary", $confirmButtonAfterSacrificeChoice[0], $confirmButtonAfterSacrificeChoice[1], 1, 1)
            If _Interrupt_Sleep(100) Then
              _Function_Finished()
              return True
            Endif
						$color = PixelGetColor($confirmAfterChoice[0], $confirmAfterChoice[1])
					Until $color <> $confirmAfterChoice[2]
				EndIf
			EndIf

			;si c'est un sacrifice, met un bonshomme
			If $searching_type Then
				Local $aMatches_sacrifice = _OpenCV_FindTemplate($img, $dungeon_detect_sacrifice, 0.95)
				If UBound($aMatches_sacrifice) Then
					$searching_type = false
					_dungeon_log_INFO("DungeonFollower - Sacrifice detected"& @CRLF)

					fillsquadTest("sacrifice", false)
					If _Interrupt_Sleep(500) Then
						_Function_Finished()
						return True
					Endif

					MouseClick("primary", $followerSacrificeReadyButton[0], $followerSacrificeReadyButton[1], 1, 1)
					_dungeon_log_TRACE("DungeonFollower - Sacrifice - clic pret"& @CRLF)

					;attends d'avoir un bouton vert
					Local $iBegin = TimerInit()
					Local $color
					Do
						$color = PixelGetColor($confirmAfterSacrifice[0], $confirmAfterSacrifice[1])
						If _Interrupt_Sleep(100) Then
							_Function_Finished()
							return True
						Endif
					Until TimerDiff($iBegin) > 10000 or $color == $confirmAfterSacrifice[2]
					MouseClick("primary", $confirmButtonAfterSacrificeChoice[0], $confirmButtonAfterSacrificeChoice[1], 1, 1)
					If _Interrupt_Sleep(500) Then
						_Function_Finished()
						return True
					Endif
				EndIf
			EndIf

			If $searching_type Then
				_dungeon_log_ERROR("DungeonFollower - Don't know what it is, color=" & $color &@CRLF)

				; Reset previous waypoint to allow reentering current waypoint
				$x_prev = 0
				$y_prev = 0
			EndIf

			;sors
			If _Interrupt_Sleep(500) Then
				_Function_Finished()
				return True
			Endif
			_dungeon_log_TRACE("DungeonFollower - sors"& @CRLF)
			MouseClick("primary", 1407, 136, 1, 1)

		Else
			CheckSpecialReward()
		EndIf

		If _Interrupt_Sleep(500) Then
			_Function_Finished()
			return True
		Endif

		$color = PixelGetColor($end_quest_button[0], $end_quest_button[1])
		If $color == $end_quest_button[2] or $color == $end_quest_button[3] Then
			$dungeon_finished = True
			_dungeon_log_INFO("DungeonFollower - Dungeon finished"& @CRLF)
		EndIf

	Until $dungeon_finished

	Return False

EndFunc

Func FillSquadTest($type, $roleLeader)
	Local $detection
	Local Const $threshold = 0.90

	For $i = 0 To 5
		;opencv screenshot
		Local $aRect = WinGetPos($hwnd)
		Local $img = _OpenCV_GetDesktopScreenMat($aRect)
		Local $aMatches

    ;Check missing fighter ... (fight only) (Deploy best button)
    If $type == "fight" Then
      $detection = _OpenCV_imread_and_check(_OpenCV_FindFile("dungeon_fight_detection.png"))
      ;find match fight
      $aMatches = _OpenCV_FindTemplate($img, $detection, $threshold)
      If UBound($aMatches) Then
        ; Missing fighters
        ; Apply quick deploy
        If $roleLeader == true Then
          MouseClick("primary", $deployBest_Leader[0], $deployBest_Leader[1], 1, 1)
        Else
          MouseClick("primary", $deployBest_Follower[0], $deployBest_Follower[1], 1, 1)
        EndIf
        If _Interrupt_Sleep(300) Then
          Return True
        Endif
      EndIf
	  Return False      ;no need to check again if fighters are alive after quick deploy
    ElseIf $type == "mortal" Then
      ; Mortal
      $detection = _OpenCV_imread_and_check(_OpenCV_FindFile("dungeon_fight_detection.png"))
    Else
      ; Sacrifice
      $detection = _OpenCV_imread_and_check(_OpenCV_FindFile("dungeon_sacrifice_detection.png"))
    EndIf

    ; Check again after quick deploy
		$img = _OpenCV_GetDesktopScreenMat($aRect)
    $aMatches = _OpenCV_FindTemplate($img, $detection, $threshold)
		If UBound($aMatches) Then
			Local $xto = $aMatches[0][0]+$detection.width/2+30
			Local $yto = $aMatches[0][1]+$detection.height/2-30
			_dungeon_log_TRACE("FillSquadTest - trouve un match en " &$xto& ","&$yto&@CRLF)

			Local $xfrom, $yfrom
			If $type == "fight" Then
				;If Not FindFirstWarriorTest($xfrom, $yfrom) Then
					Return False	;finished
				;EndIf
			EndIf
			If $type == "mortal" Then
        If $roleLeader == false Then
          ; Follower => Put weak players
          If Not FindLastWarriorMortal($xfrom, $yfrom) Then
            Return False	;finished
          EndIf
        Else
          ; Leader => Put best players
          If Not FindFirstWarriorTest($xfrom, $yfrom) Then
            Return False	;finished
          EndIf
        EndIf
			EndIf
			If $type == "sacrifice" Then
				If Not FindLastWarriorSacrifice($xfrom, $yfrom) Then
					Return False	;finished
				EndIf
			EndIf

      ; Drag fighter
			MouseClickDrag("primary", $xfrom, $yfrom, $xto, $yto, 3)

			If _Interrupt_Sleep(500) Then
				Return True
			Endif;
		Else
      If $type <> "fight" Then
        _dungeon_log_ERROR("FillSquadTest - no match found."&@CRLF)
      EndIF
    EndIf
    Next
	Return False
EndFunc

;todo find warrior with parameter start, stop step height, offset
Func FindFirstWarriorTest(ByRef $xx, ByRef $yy)
  _dungeon_log_TRACE("FindFirstWarriorTest start" &@CRLF)
	For $i = $warriorsHealthBars[0] to $warriorsHealthBars[2] Step 10
		$color = PixelGetColor($i, $warriorsHealthBars[1])
		;MouseMove($i, $warriorsHealthBars[1], 1)
		If $color == $healthBarColors[0] or $color == $healthBarColors[1] or $color == $healthBarColors[2] or $color == $healthBarColors[3] Then

			$xx = $i+30
			$yy = $warriorsHealthBars[1]-100
			;ouvre l'�cran perso
			MouseClick("primary", $xx, $yy, 1, 1)
			_wait_warrior_screen()
			$color = PixelGetColor($rez_button[0], $rez_button[1])
			;sors de l'�cran
			MouseClick("primary", 165, 524, 1, 1)
			If _Interrupt_Sleep(500) Then
				_Function_Finished()
				return
			Endif;
			If $color == $rez_button[2] or $color == $rez_button[3] or $color == $rez_button[4] Then
				;the warrior is dead
				$i = $i+50
        _dungeon_log_TRACE("FindFirstWarriorTest => Dead warrior."&@CRLF)
				ContinueLoop
			Else
				;sinon
        _dungeon_log_TRACE("FindFirstWarriorTest => Warrior found."&@CRLF)
				Return True
			EndIf
		EndIf
	Next
	;n'a pas trouv� de combattant. On sonne? On gueule?
	;on les ressuscite si l'option est coch�e (TODO option :D )
  _dungeon_log_ERROR("FindFirstWarriorTest => Pas trouv�."&@CRLF)
	Return False

EndFunc

Func FindLastWarriorMortal(ByRef $xx, ByRef $yy)
  _dungeon_log_TRACE("FindLastWarriorMortal start" &@CRLF)
	For $i = $warriorsHealthBars[2] to $warriorsHealthBars[0] Step -10
		$color = PixelGetColor($i, $warriorsHealthBarsMortal)
		;MouseMove($i, $warriorsHealthBarsMortal, 1)
		If $color == $healthBarColors[0] or $color == $healthBarColors[1] or $color == $healthBarColors[2] or $color == $healthBarColors[3] Then
			$xx = $i-30
			$yy = $warriorsHealthBarsMortal-100
			;ouvre l'�cran perso
			MouseClick("primary", $xx, $yy, 1, 1)
			_wait_warrior_screen()
			$color = PixelGetColor($rez_button[0], $rez_button[1])
			;sors de l'�cran
			MouseClick("primary", 165, 524, 1, 1)
			If _Interrupt_Sleep(500) Then
				_Function_Finished()
				return
			Endif;
			If $color == $rez_button[2] or $color == $rez_button[3] or $color == $rez_button[4] Then
				;the warrior is dead
				$i = $i-50
        _dungeon_log_TRACE("FindLastWarriorMortal => Dead warrior."&@CRLF)
				ContinueLoop
			Else
				;sinon
        _dungeon_log_TRACE("FindLastWarriorMortal => Warrior found."&@CRLF)
				Return True
			EndIf
		EndIf
	Next
  _dungeon_log_ERROR("FindLastWarriorMortal => Pas trouv�."&@CRLF)
	;n'a pas trouv� de combattant. On sonne? On gueule?
	Return False

EndFunc


Func FindLastWarriorSacrifice(ByRef $xx, ByRef $yy)
  _dungeon_log_TRACE("FindLastWarriorSacrifice start" &@CRLF)
	For $i = $warriorsHealthBars[2] to $warriorsHealthBars[0] Step -10

		$color = PixelGetColor($i, $warriorsHealthBarsSacrifice)
		;MouseMove($i, $warriorsHealthBarsMortal, 1)
		If $color == $healthBarColors[0] or $color == $healthBarColors[1] or $color == $healthBarColors[2] or $color == $healthBarColors[3] Then
			$xx = $i-30
			$yy = $warriorsHealthBarsSacrifice-100
			;ouvre l'�cran perso
			MouseClick("primary", $xx, $yy, 1, 1)
			_wait_warrior_screen()
			$color = PixelGetColor($rez_button[0], $rez_button[1])
			;sors de l'�cran
			MouseClick("primary", 165, 524, 1, 1)
			If _Interrupt_Sleep(500) Then
				_Function_Finished()
				return
			Endif;
			If $color == $rez_button[2] or $color == $rez_button[3] or $color == $rez_button[4] Then
				;the warrior is dead
				$i = $i-50
        _dungeon_log_TRACE("FindLastWarriorSacrifice => Dead warrior."&@CRLF)
				ContinueLoop
			Else
				;sinon
        _dungeon_log_TRACE("FindLastWarriorSacrifice => Warrior found."&@CRLF)
				Return True
			EndIf
		EndIf
	Next
	;n'a pas trouv� de combattant. On sonne? On gueule?
  _dungeon_log_ERROR("FindLastWarriorSacrifice => Pas trouv�."&@CRLF)
	Return False

EndFunc

Func Choose()
	$color = PixelGetColor($influenceLeftReadyButton[0], $influenceLeftReadyButton[1])
	If $color == $influenceLeftReadyButton[2] or $color == $influenceLeftReadyButton[3] Then
		;left choice
		_dungeon_log_INFO("Choose - left choice"& @CRLF)

    Do
      ; Select central choice
      MouseClick("primary", 454, 338, 1, 1)
      If _Interrupt_Sleep(200) Then
        Return True
      Endif;

      ;confirme on menu
      MouseClick("primary", 760, 621, 1, 1)
      If _Interrupt_Sleep(500) Then
        Return True
      Endif;

      ;Confirm selec : 454 / 276 / 0x49F9FF => Blue halo around choice
      $color = PixelGetColor(454, 276)
      _dungeon_log_TRACE("Choose - left sel and confirm, color=0x" & hex($color, 6) & @CRLF)
    Until( $color == 0x49F9FF )

    Do
      MouseClick("primary", $influenceLeftReadyButton[0], $influenceLeftReadyButton[1], 1, 1)
      If _Interrupt_Sleep(500) Then
        Return True
      Endif;

      ;Confirm selec ready button red
      $color = PixelGetColor($influenceLeftReadyButton[0], $influenceLeftReadyButton[1])
      _dungeon_log_TRACE("Choose - left ready button red, color=0x" & hex($color, 6) & @CRLF)
    Until( $color == 0xF33520 or $color == 0xF23520 )
	EndIf


	$color = PixelGetColor($influenceRightReadyButton[0], $influenceRightReadyButton[1])
	If $color == $influenceRightReadyButton[2] or $color == $influenceRightReadyButton[3] Then
		;right choice
		_dungeon_log_INFO("Choose - right choice"& @CRLF)

    Do
      ; Select central choice
      MouseClick("primary", 1077, 343, 1, 1)
      If _Interrupt_Sleep(200) Then
        Return True
      Endif;

      ;confirme on menu
      MouseClick("primary", 760, 621, 1, 1)
      If _Interrupt_Sleep(500) Then
        Return True
      Endif;

      ;Confirm selec : 454 / 276 / 0x49F9FF => Blue halo around choice
      $color = PixelGetColor(1078, 276)
      _dungeon_log_TRACE("Choose - right sel and confirm, color=0x" & hex($color, 6) & @CRLF)

    Until( $color == 0x49F9FF )

    Do
      MouseClick("primary", $influenceRightReadyButton[0], $influenceRightReadyButton[1], 1, 1)
      If _Interrupt_Sleep(500) Then
        Return True
      Endif;

      ;Confirm selec ready button red
      $color = PixelGetColor($influenceRightReadyButton[0], $influenceRightReadyButton[1])
      _dungeon_log_TRACE("Choose - right ready button red, color=0x" & hex($color, 6) & @CRLF)
    Until( $color == 0xF33520 or $color == 0xF23520 )

	EndIf
EndFunc



Func RestartDungeon()
  Local $iBegin
  $random_restart = (IniRead(@ScriptDir & "/H2C2.ini", "game", "dungeonrandomrestart", "0") == "1")
  

	MouseClick("primary", $end_quest_button[0], $end_quest_button[1], 1, 1)
  _dungeon_log_TRACE("RestartDungeon - End quest click"& @CRLF)
	If _Interrupt_Sleep(2000) Then
		_Function_Finished()
		Return True
	Endif

	MouseClick("primary", $end_quest_confirm[0], $end_quest_confirm[1], 1, 1)
  _dungeon_log_TRACE("RestartDungeon - End quest confirm click"& @CRLF)
	If _Interrupt_Sleep(6000) Then
		_Function_Finished()
		Return True
	Endif

	;speed up dungeon recap
	MouseClick("primary", $end_quest_confirm[0], $end_quest_confirm[1], 1, 1)
	If _Interrupt_Sleep(5000) Then
		_Function_Finished()
		Return True
	Endif

	Local Const $invite_to_send = _OpenCV_imread_and_check(_OpenCV_FindFile("dungeon_restart_invite_to_send.png"))
	Local Const $invite_sent = _OpenCV_imread_and_check(_OpenCV_FindFile("dungeon_restart_invite_sent.png"))
	Local Const $invite_both_ready = _OpenCV_imread_and_check(_OpenCV_FindFile("dungeon_restart_both_ready.png"))
	Local Const $threshold = 0.8
	Local $aRect = WinGetPos($hwnd)

  ; Trye re-engage dungeon with partner for 5 min
  $has_left = false
  $ready_to_start = false
  $first_msg = false
  $iBegin = TimerInit()
  $timeout = 5 * 60 * 1000
	Do
		If _Interrupt_Sleep(1000) Then
			Return True
		Endif;

		$img = _OpenCV_GetDesktopScreenMat($aRect)
		$aMatches_invite_to_send = _OpenCV_FindTemplate($img, $invite_to_send, 0.8)
		$aMatches_invite_sent = _OpenCV_FindTemplate($img, $invite_sent, 0.8)
		$aMatches_invite_both_ready = _OpenCV_FindTemplate($img, $invite_both_ready, 0.8)

    ; Check if we need to invite partner
		If UBound($aMatches_invite_to_send) Then
      ; Click on button to invite
      MouseClick("primary", $invite_partner[0], $invite_partner[1], 1, 1)
      _dungeon_log_INFO("RestartDungeon - Invite partner click and wait"& @CRLF)

    ; Check if we have sent invite and wait partner invite
    ElseIf UBound($aMatches_invite_sent) Then
			; Do nothing, wait for him
      _dungeon_log_TRACE("RestartDungeon - Wait for partner invite validation"& @CRLF)

    ; Check if partner invite is received or both are ready
    ElseIf UBound($aMatches_invite_both_ready) Then
			; We can leave the loop to engage the dungeon restart
      _dungeon_log_TRACE("RestartDungeon - Partner invite recevied or Partner ready"& @CRLF)
      $ready_to_start = true
    Else
      ; Check if he left
      $color = PixelGetColor($restart_quest[0], $restart_quest[1])
      If $color == $restart_quest[3] Then
        ; Partner may have leave the DJ
        _dungeon_log_INFO("RestartDungeon - Partner left DJ !!!"& @CRLF)
        If $random_restart == false Then
          ; No auto-restart with random, we stop here !
          Return True
        Else
          ; Auto-restart with random, we relaunch DJ with random !
          $has_left = true
          $ready_to_start = true
          _dungeon_log_INFO("RestartDungeon - Let's restart with random"& @CRLF)
        EndIf
      EndIf

      _dungeon_log_TRACE("RestartDungeon - Partner invite loop - Nothing detected"& @CRLF)
		EndIf
	Until($ready_to_start or TimerDiff($iBegin) > $timeout)

  If TimerDiff($iBegin) > $timeout Then
    ; Timeout
    _dungeon_log_ERROR("RestartDungeon - Partner validation timeout"& @CRLF)
    _Function_Finished()
    Return True
  EndIf

  ;Wait a bit to not miss the menu
	If _Interrupt_Sleep(500) Then
		_Function_Finished()
		Return True
	Endif

  If $has_left == false Then
    ; Restart quest search
    _dungeon_log_INFO("RestartDungeon - Partner answered restart quest"& @CRLF)
    MouseClick("primary", $restart_quest[0], $restart_quest[1], 1, 1)
  Else
    ; Quit dungeon recap menu
    _dungeon_log_INFO("RestartDungeon - Quit recap menu"& @CRLF)
    MouseClick("primary", $close_button[0], $close_button[1], 1, 1)
  EndIf

	If _Interrupt_Sleep(1000) Then
		_Function_Finished()
		Return True
	Endif

	$isRuneEvent = (IniRead(@ScriptDir & "/H2C2.ini", "params", "runeevent", "0") == "1")
	If $isRuneEvent Then
		_dungeon_log_TRACE("RestartDungeon - Rune event click & wait "& @CRLF)
		If _Interrupt_Sleep(3000) Then
			Return True
		Endif
		MouseClick("primary", 900, 845, 1, 1) ;somewhere
		If _Interrupt_Sleep(2000) Then
			Return True
		Endif
	Endif

  ; Captcha test before restart !
  If test_capcha() Then
    _dungeon_log_INFO("RestartDungeon - Captcha detected !!"& @CRLF)
    Return True
  EndIf
  
  If $has_left == true Then
    ; Re-open dungeon menu
    _dungeon_log_INFO("RestartDungeon - Re-Open dungeon menu"& @CRLF)
    
    ;si on est sur la carte
		If is_map_screen() And Not is_dungeon_screen() Then
      _dungeon_log_TRACE("RestartDungeon - Go back to castle"& @CRLF)
			back_to_castle()
		EndIf
    
    ;si on est dans le ch�teau
		If is_castle_screen() Then
      _dungeon_log_TRACE("RestartDungeon - In castle screen, open_map"& @CRLF)
			If open_map() Then
        _dungeon_log_ERROR("RestartDungeon - Can't open map"& @CRLF)
				_Function_Finished()
        Return
      EndIf
      _dungeon_log_TRACE("RestartDungeon - In map screen, open_dungeon"& @CRLF)
      drag_map_left()
			If open_dungeon() Then
        _dungeon_log_ERROR("RestartDungeon - Can't join the dungeon menu"& @CRLF)
				_Function_Finished()
        Return
      EndIf
		EndIf

		;si on est dans l'�cran donjon
		If Not is_dungeon_screen() Then
      _dungeon_log_ERROR("RestartDungeon - Not in dungeon menu :("& @CRLF)
      _Function_Finished()
      Return
    Else
      _dungeon_log_TRACE("RestartDungeon - In dungeon menu :)"& @CRLF)
		EndIf
  EndIf

  ; Restart dungeon
  $dungeonPayment = IniRead(@ScriptDir & "/H2C2.ini", "game", "dungeonPayment", "Preselected")
  If $dungeonPayment == "Apples" Then
    _dungeon_log_INFO("RestartDungeon - Go with apples !!"& @CRLF)
		MouseClick("primary", 415, 213, 1, 1)
	ElseIf $dungeonPayment == "Torches" Then
    _dungeon_log_INFO("RestartDungeon - Go with torches !!"& @CRLF)
		MouseClick("primary", 672, 208, 1, 1)
	EndIf

	If _Interrupt_Sleep(2000) Then
		_Function_Finished()
		Return True
	Endif
  ; Launch DJ
	MouseClick("primary", $accept_restart[0], $accept_restart[1], 1, 1)

	If _Interrupt_Sleep(3000) Then
		_Function_Finished()
		Return True
	Endif
  ; Validate team for DJ
	MouseClick("primary", $confirm[0], $confirm[1], 1)

  ; Partner has left, and we want to restart with random => Select random DJ
  If $random_restart == true and $has_left == true Then
    If _Interrupt_Sleep(2000) Then
      _Function_Finished()
      Return True
    Endif
  
    ; Click on Random button
    MouseClick("primary", 840, 450, 1, 1)    
  EndIf

  Local $whoami
  WaitDungeonToss($whoami, 3*60*1000)
  If _Interrupt_Sleep(1500) Then
		_Function_Finished()
		Return True
	Endif
  MouseClick("primary", $coin_toss_close_button[0], $coin_toss_close_button[1], 1, 1)

	Return False
EndFunc

Func _wait_warrior_screen()
 	Local Const $wscreen = _OpenCV_imread_and_check(_OpenCV_FindFile("dungeon_warriorscreen.png"))
	Local Const $threshold = 0.8
	Local $aRect = WinGetPos($hwnd)
	Local $iBegin = TimerInit()
	Do
		If _Interrupt_Sleep(300) Then
			Return True
		Endif;

		;Local $aRect[4] = [5, 12, 1539, 910] ;todo prendre les mesures de la fen�tre
		Local $img = _OpenCV_GetDesktopScreenMat($aRect)
		;find match fight
		Local $aMatches = _OpenCV_FindTemplate($img, $wscreen, $threshold)

		If UBound($aMatches) Then
			Return False

		EndIf
	Until TimerDiff($iBegin) > 10000

  _dungeon_log_ERROR("_wait_warrior_screen => Timeout."&@CRLF)
	Return False
EndFunc


Func WaitDungeonToss(ByRef $whoami, $timeout)
	Local Const $leader = _OpenCV_imread_and_check(_OpenCV_FindFile("dungeon_leader.png"))
	Local Const $follower = _OpenCV_imread_and_check(_OpenCV_FindFile("dungeon_follower.png"))
	Local Const $threshold = 0.8

	Local $aRect = WinGetPos($hwnd)

	Local $iBegin = TimerInit()
	Do
		If _Interrupt_Sleep(300) Then
			Return True
		Endif

		;Local $aRect[4] = [5, 12, 1539, 910] ;todo prendre les mesures de la fen�tre
		Local $img = _OpenCV_GetDesktopScreenMat($aRect)

		;find match
		Local $aMatches = _OpenCV_FindTemplate($img, $leader, $threshold)
		If UBound($aMatches) Then
			$whoami = "leader"
			_dungeon_log_INFO("WaitDungeonToss - I'm the "&$whoami & @CRLF)
			Return False
		Endif
		$aMatches = _OpenCV_FindTemplate($img, $follower, $threshold)
		If UBound($aMatches) Then
			$whoami = "follower"
			_dungeon_log_INFO("WaitDungeonToss - I'm the "&$whoami & @CRLF)
			Return False

		EndIf
	Until TimerDiff($iBegin) > $timeout ;une minute. On verra plus tard si il faut plus
	$whoami = "not found"
	_dungeon_log_ERROR("WaitDungeonToss - Error: "&$whoami & @CRLF)
	Return False
EndFunc





Func FillDungeonSquad($roleLeader)
	Local Const $empty_fighter = _OpenCV_imread_and_check(_OpenCV_FindFile("empty_fighter.png"))
	Local Const $threshold = 0.9

	For $i = 0 To 5
		;opencv screenshot
		Local $aRect = WinGetPos($hwnd)
		Local $img = _OpenCV_GetDesktopScreenMat($aRect)
		;find match fight
		Local $aMatches = _OpenCV_FindTemplate($img, $empty_fighter, $threshold)
    If UBound($aMatches) Then
      ; Missing fighters
      If $roleLeader == true Then
        MouseClick("primary", $deployBest_Leader[0], $deployBest_Leader[1] , 1, 1)
      Else
        MouseClick("primary", $deployBest_Follower[0], $deployBest_Follower[1] , 1, 1)
      EndIf
      If _Interrupt_Sleep(300) Then
        Return True
      Endif
    Else
      ; No missing fighters -> Quit
      Return False ;finished
    EndIf

    ; Check again after quick deploy
    $aMatches = _OpenCV_FindTemplate($img, $empty_fighter, $threshold)
		If UBound($aMatches) Then
			Local $xto = $aMatches[0][0]+$empty_fighter.width/2
			Local $yto = $aMatches[0][1]+$empty_fighter.height/2
			_dungeon_log_TRACE("FillDungeonSquad - trouve un vide en " &$xto& ","&$yto&@CRLF)

			Local $xfrom, $yfrom
			If Not FindDungeonWarrior($xfrom, $yfrom) Then
				Return False	;finished
			EndIf

			MouseClickDrag("primary", $xfrom, $yfrom, $xto, $yto)

			If _Interrupt_Sleep(1000) Then
				Return True
			Endif;
		EndIf
    Next
	Return False
EndFunc

Func FindDungeonWarrior(ByRef $xx, ByRef $yy)

;~ 	Local Const $tank = _OpenCV_imread_and_check(_OpenCV_FindFile("fighter_tank.png"))
;~ 	Local Const $mage = _OpenCV_imread_and_check(_OpenCV_FindFile("fighter_mage.png"))
;~ 	Local Const $archer = _OpenCV_imread_and_check(_OpenCV_FindFile("fighter_archer.png"))
;~ 	Local Const $threshold = 0.8


;~ 	;opencv screenshot
;~ 	Local $aRect = WinGetPos($hwnd)
;~ 	$aRect[0] = $aRect[0]+$select_squad_rect[0]
;~ 	$aRect[1] = $aRect[1]+$select_squad_rect[1]
;~ 	$aRect[2] = $select_squad_rect[2]
;~ 	$aRect[3] = $select_squad_rect[3]
;~ 	Local $img = _OpenCV_GetDesktopScreenMat($aRect);()

;~ 	;find match tank
;~ 	Local $aMatches = _OpenCV_FindTemplate($img, $tank, $threshold)
;~ 	If UBound($aMatches) Then
;~ 		$xx = $aMatches[0][0]+$aRect[0]
;~ 		$yy = $aMatches[0][0]+$aRect[1] - 20
;~ 		_dungeon_log_TRACE("trouve un tank en " &$xx& ","&$yy&@CRLF)
;~ 		Return True
;~ 	EndIf

;~ 	;find match mage
;~ 	$aMatches = _OpenCV_FindTemplate($img, $mage, $threshold)
;~ 	If UBound($aMatches) Then
;~ 		$xx = $aMatches[0][0]+$aRect[0]
;~ 		$yy = $aMatches[0][0]+$aRect[1] - 20
;~ 		_dungeon_log_TRACE("trouve un mage en " &$xx& ","&$yy&@CRLF)
;~ 		Return True
;~ 	EndIf

;~ 	;find match arher
;~ 	$aMatches = _OpenCV_FindTemplate($img, $archer, $threshold)
;~ 	If UBound($aMatches) Then
;~ 		$xx = $aMatches[0][0]+$aRect[0]
;~ 		$yy = $aMatches[0][0]+$aRect[1] - 20
;~ 		_dungeon_log_TRACE("trouve un archer en " &$xx& ","&$yy&@CRLF)
;~ 		Return True
;~ 	EndIf

	For $searchx = 256 to 1390 Step 10
		$colorstart = PixelGetColor($searchx, 725)
		If $colorstart <> 0xC6FCCE Then
			_dungeon_log_TRACE("trouve un d�but en " &$searchx&@CRLF)
			For $searchendx = $searchx to 1390 Step 10
				$colorend = PixelGetColor($searchendx, 725)
				If $colorend == 0xC6FCCE Then
					_dungeon_log_TRACE("trouve une fin en " &$searchendx&@CRLF)
					$xx = $searchx + ($searchendx-$searchx)/2
					_dungeon_log_TRACE("milieu " &$xx&@CRLF)
					$yy = 725 - 40

;~ 					MouseClick("primarey", $x, $y, 1)
;~ 					If _Interrupt_Sleep(1500) Then
;~ 						_Function_Finished()
;~ 						return
;~ 					Endif;



					Return False
				EndIf
			Next
		EndIf
	Next

	;n'a pas trouv� de combattant. On sonne? On gueule?
	Return False

EndFunc


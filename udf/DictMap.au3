;_MapInit
;_MapAddKeyValuePair
;_MapReassignKey
;_MapAppendToKey
;_MapRemoveKey
;_MapGetValue
;_MapToString
;_MapToArray
;_MapTo2dArray
;_MapFrom2DArray
;_MapToIniSection
;_MapFromIniSection



Func _MapInit()
  Local $map = ObjCreate("Scripting.Dictionary")

    If Not IsObj($map) Then
        Exit MsgBox(0, "Error", "Object not created")
    EndIf

  return $map
EndFunc ;_MapInit



Func _MapAddKeyValuePair($map , $key , $value)
    If $map.Exists($key) Then
        msgbox(0, 'Error' , '"' & $key & '"' & ' already exists with a value of ' & '"' & $map.Item($key) & '"')
    Else
        $map.Add($key, $value)
    EndIf
EndFunc ;_MapAddKeyValuePair



Func _MapReassignKey($map , $key , $value)
    If $map.Exists($key) Then
        $map.remove($key)
        $map.Add($key, $value)
    Else
        $map.Add($key, $value)
    EndIf
EndFunc ;_MapReassignKey



Func _MapAppendToKey($map , $key , $value)
    If $map.Exists($key) Then
        $sCurrent = $map.Item($key)
        $map.remove($key)
        $map.Add($key, $sCurrent & ";" & $value)
    Else
        $map.Add($key, $value)
    EndIf
EndFunc ;_MapAppendToKey



Func _MapRemoveKey($map , $key)
        $map.remove($key)
EndFunc ;_MapRemoveKey



Func _MapGetValue($map , $key)
    return $map.Item($key)
EndFunc ;_MapGetValue



Func _MapToString($map)
    local $sOut = ""

for $key in $map.Keys
     $sOut &= $key & " = " & $map.Item($key) & @LF
Next

return stringreplace($sOut , @LF , "" , -1)
EndFunc ;_MapToString



Func _MapToArray($map)
local $aArray[$map.count]

    $i = 0
    for $key in $map.Keys
            $aArray[$i] = $key & " = " & $map.Item($key)
            $i += 1
    Next

return $aArray
EndFunc ;_MapToArray



Func _MapTo2dArray($map)
local $aArray[$map.count][2]

    $i = 0
    for $key in $map.Keys
        $aArray[$i][0] = $key
        $aArray[$i][1] = $map.Item($key)
        $i += 1
    Next

return $aArray
EndFunc ;_MapTo2dArray
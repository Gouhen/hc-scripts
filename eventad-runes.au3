;#RequireAdmin
#include <AutoItConstants.au3>
#include <MsgBoxConstants.au3>
#include <GUIConstantsEx.au3>
#include <StaticConstants.au3>
#include <WindowsConstants.au3>

#include "hc_actions.au3"

HotKeySet("{ESC}", "ExitApplication")


;TODO avant de lancer le script :
; 1) ouvrir hustle castle
; 2) ouvrir les paramètres sur applications => hustle => stockage

;MouseClick("primary", 243, 23 , 1) ; accueil de bluestacks
;Sleep(2000)
;;MouseClick("primary",883, 239 , 1) ; application systeme (6e icône, sinon ça marche pas)
;MouseClick("primary",518, 206 , 1) ; application systeme (5e icône, sinon ça marche pas)
;;MouseClick("primary",399, 209 , 1) ; application systeme (4e icône, sinon ça marche pas)
;Sleep(2000)
;MouseClick("primary",710, 480 , 1) ; paramètres android
;Sleep(2000)
;MouseClick("primary",333, 300 , 1) ; stockage
;Sleep(4000)
;MouseClick("primary",217, 413 , 1) ; applications
;Sleep(4000)
;MouseClick("primary",188, 398 , 1) ; hustle castle (actuellement en 2e position)
;MouseClick("primary",70, 508 , 1) ; hustle castle (actuellement en 3e position)
;Sleep(2000)


Func RuneEventAds($autoclose)

	initBlueStacks()

	Local $count = 10
	Do
		MouseClick("primary", 416, 419 , 1) ; effacer les données
		If _Interrupt_Sleep(2000) Then
			_Function_Finished()
			Return
		EndIf

		MouseClick("primary", 1171, 560 , 1) ; confirmer
		If _Interrupt_Sleep(3000) Then
			_Function_Finished()
			Return
		EndIf

		MouseClick("primary", 426, 20 , 1) ; 2e onglet de bluestacks (hustle castle)
		If _Interrupt_Sleep(15000) Then ;15 secondes
			_Function_Finished()
			Return
		EndIf

		;While(PixelGetColor(771, 608) <> 0x666563)
		;	If _Interrupt_Sleep(100) Then
		;	_Function_Finished()
		;		return
		;	Endif;
		;WEnd
		MouseClick("primary", 771, 608 , 1) ;accept
		ConsoleWrite ("accept" & @CRLF)
		If _Interrupt_Sleep(20000) Then
			_Function_Finished()
			Return
		EndIf

		MouseClick("primary", 100, 100 , 1) ;clic sur l'ecran, des fois une fenêtre apparait sans raison
		ConsoleWrite ("ecran" & @CRLF)
		If _Interrupt_Sleep(20000) Then
			_Function_Finished()
			Return
		EndIf

		;bouton parametres
		;While(PixelGetColor(1443, 641) <> 0x666563)
		;	If _Interrupt_Sleep(100) Then
		;	_Function_Finished()
		;		return
		;	Endif;
		;WEnd

		;trèèèès long. fait exprès
		If _Interrupt_Sleep(40000) Then
			_Function_Finished()
			Return
		EndIf
		MouseClick("primary", 1470, 659 , 1)
		ConsoleWrite ("bouton parametres" & @CRLF)
		If _Interrupt_Sleep(3000) Then
			_Function_Finished()
			Return
		EndIf

		MouseClick("primary", 755, 476 , 1) ;gestion du compte
		If _Interrupt_Sleep(2000) Then
			_Function_Finished()
			Return
		EndIf

		MouseClick("primary", 775, 423 , 1) ;google
		If _Interrupt_Sleep(1500) Then
			_Function_Finished()
			Return
		EndIf

		MouseClick("primary", 775, 423 , 1) ;google 2x parce qu'il y a des fois des ratées
		If _Interrupt_Sleep(4000) Then
			_Function_Finished()
			Return
		EndIf

		MouseClick("primary", 542, 576 , 1) ;compte G
		If _Interrupt_Sleep(4000) Then
			_Function_Finished()
			Return
		EndIf

		MouseClick("primary", 1002, 595 , 1) ;confirme G
		If _Interrupt_Sleep(2000) Then
			_Function_Finished()
			Return
		EndIf

		MouseClick("primary", 652, 663 , 1) ;confirm agaiiiin!
		If _Interrupt_Sleep(20000) Then
			_Function_Finished()
			Return
		EndIf


		;todo voir si c'est suffisant
		If back_to_castle() Then
			_Function_Finished()
			Return
		EndIf
		If _Interrupt_Sleep(4000) Then
			_Function_Finished()
			Return
		EndIf

		If back_to_castle() Then
			_Function_Finished()
			Return
		EndIf
		If _Interrupt_Sleep(2000) Then
			_Function_Finished()
			Return
		EndIf


		MouseClickDrag("primary", 184, 382, 1353, 382) ;gauche
		If _Interrupt_Sleep(2000) Then
			_Function_Finished()
			Return
		EndIf

		MouseClickDrag("primary", 184, 382, 1353, 382)  ;gauche
		If _Interrupt_Sleep(2000) Then
			_Function_Finished()
			Return
		EndIf

		MouseClickDrag("primary", 306, 760, 306, 244)  ;bas
		If _Interrupt_Sleep(4000) Then
			_Function_Finished()
			Return
		EndIf
		 ;si jamais 'faut cliquer vite vite ;)
		;MouseClick("primary", 700, 866 , 1) ; tapis volant (à changer en fct des events et de la hauteur de la caserne)
		MouseClick("primary", 1193, 744 , 1) ; par terre
		If _Interrupt_Sleep(3000) Then
			_Function_Finished()
			Return
		EndIf

		MouseClick("primary", 1310, 253 , 1) ; magasin d'event
		If _Interrupt_Sleep(2000) Then
			_Function_Finished()
			Return
		EndIf

		MouseClick("primary", 621, 694 , 1) ; pub

		If wait_and_exit_add($autoclose) Then
			_Function_Finished()
			Return
		EndIf

		 ;attend les cartes
		If _Interrupt_Sleep(25000) Then
			_Function_Finished()
			Return
		EndIf

		MouseClick("primary", 622, 23 , 1) ; 3e onglet de bluestacks. Doit être ouvert préalablement sur applications => hustle => stockage
		If _Interrupt_Sleep(4000) Then
			_Function_Finished()
			Return
		EndIf



	Until $count <0
	_Function_Finished()
EndFunc
#AutoIt3Wrapper_icon=data/robot_drive.ico

#include <AutoItConstants.au3>
#include <GUIConstantsEx.au3>
#include <StaticConstants.au3>
#include <WindowsConstants.au3>
#include <GUIComboBox.au3>
#include <TabConstants.au3>

#include "ImageSearch.au3"

#include "ad.au3"

#include "eventad-runes.au3"


#include "hc_actions.au3"

Global $GUI1
Global $Lb1
Global $Btn1
Global $label
Global $btnAds
Global $btnMapAds
Global $btnResourceAds
Global $btnAllAds
Global $btnFreeRes
Global $chbAutocloseAds

Global $pass = 0;

Opt("GUIOnEventMode", 1)

init()
initBlueStacks()

CreateGUI("Pub2Mes2")

; interruption de fonctions
HotKeySet("{ESC}", "_Interrupt")
Global $fInterrupt = 0

AutoItSetOption("MouseClickDragDelay", 750)

While 1
	Sleep(100)
WEnd


Func CreateGUI($title)
	; ########## Début de la création de la GUI 1 ##########
	$winheight = 400
	$GUI1 = GUICreate($title, 280, $winheight, 1620, 10, default)

	GUISetOnEvent($GUI_EVENT_CLOSE, "ExitApplication");

	$top = 50
	$btnAds = GUICtrlCreateButton("Daily Ads", 10, $top, 80, 25)
	GUICtrlSetOnEvent($btnAds, "DoDailyAds")

	$chbAutocloseAds = GUICtrlCreateCheckbox("Autoclose Ads", 100, $top)
	GUICtrlSetOnEvent($chbAutocloseAds, "checkboxAutocloseAds")
	$isAutocloseAdd = (IniRead(@ScriptDir & "/H2C2.ini", "game", "autocloseads", "0") == "1")
	If $isAutocloseAdd Then
		GUICtrlSetState($chbAutocloseAds, $GUI_CHECKED)
	Else
		GUICtrlSetState($chbAutocloseAds, $GUI_UNCHECKED)
	EndIf

	$top = $top + 40
	$btnMapAds = GUICtrlCreateButton("Map Ads", 10, $top, 80, 25)
	GUICtrlSetOnEvent($btnMapAds, "DoMapAds")

	$btnFreeRes = GUICtrlCreateButton("Free Res.", 100, $top, 80, 25)
	GUICtrlSetOnEvent($btnFreeRes, "DoFreeResources")

	$top = $top + 40
	$btnResourceAds = GUICtrlCreateButton("Resource Ads", 10, $top, 80, 25)
	GUICtrlSetOnEvent($btnResourceAds, "DoResourceAds")

	$top = $top + 40
	$btnAllAds = GUICtrlCreateButton("ALL Ads", 10, $top, 80, 25)
	GUICtrlSetOnEvent($btnAllAds, "DoAllAds")


	$label = GUICtrlCreateLabel("", 0, $winheight-20, 400, 20, $SS_SUNKEN)


	GUISetState(@SW_SHOW, $GUI1) ; On affiche la GUI1
EndFunc


Func SwitchGuiState($activation)
	GUICtrlSetState($btnAds, $activation)
	GUICtrlSetState($btnMapAds, $activation)
	GUICtrlSetState($btnResourceAds, $activation)
	GUICtrlSetState($chbAutocloseAds, $activation)
	GUICtrlSetState($btnFreeRes, $activation)
	GUICtrlSetState($btnAllAds, $activation)
EndFunc




Func BeforeLaunch($message)
	Global $fInterrupt
	$fInterrupt = 0

	GUICtrlSetData($label, $message)
	SwitchGuiState($GUI_DISABLE)
EndFunc

Func DoDailyAds()
	BeforeLaunch("run Daily Ads script...")
	DailyAds(IsChecked($chbAutocloseAds))
EndFunc

Func DoMapAds()
	BeforeLaunch("run Map Ads script...")
	MapAds(IsChecked($chbAutocloseAds))
EndFunc

Func DoResourceAds()
	BeforeLaunch("run Resource Ads script...")
	ResourceAds(IsChecked($chbAutocloseAds))
EndFunc

Func DoFreeResources()
	BeforeLaunch("run Free Resources script...")
	FreeResources()
EndFunc

Func DoAllAds()
	DoDailyAds()
	DoResourceAds()
	DoResourceAds() ;2x
	DoMapAds()
	DoFreeResources()
EndFunc

Func IsChecked($nCtrl)
    If BitAND(GUICtrlRead($nCtrl), $GUI_CHECKED) = $GUI_CHECKED Then Return "1"
    Return "0"
EndFunc

Func checkboxAutocloseAds()
	IniWrite(@ScriptDir & "/H2C2.ini", "game", "autocloseads", IsChecked($chbAutocloseAds))
EndFunc


Func ExitApplication()
    Exit
EndFunc   ;==>Terminate

Func Pass()
    $pass = 1
EndFunc   ;==>Terminate

Func Wait($timeout)
	GUICtrlSetState($Btn1, $GUI_ENABLE);
	For $i = 1 to $timeout
		$min = Floor(($timeout-$i) / 60);
		$sec = mod(($timeout-$i), 60);
		GUICtrlSetData ($Lb1, "Attente : " & $min & "'" & $sec &"''");

		If $pass = 1 Then
			ExitLoop
		EndIf
		Sleep(1000)
	Next
	GUICtrlSetData ($Lb1, "");
	$pass = 0;
	GUICtrlSetState($Btn1, $GUI_DISABLE);
	WinActivate("BlueStacks")
EndFunc

Func _Interrupt()
	Global $fInterrupt

	ConsoleWrite("interrupt")

    ; set the flag
    $fInterrupt = 1
EndFunc

Func _Interrupt_Sleep($iDelay)
	Global $fInterrupt

	Local $iBegin = TimerInit()
	Do
		Sleep(100)
		; Look for the interrupt
		If $fInterrupt or test_capcha() Then
			; And return True if set
			Return True
		EndIf
	Until TimerDiff($iBegin) > $iDelay

	; Return False if timed out and no interrupt was set
	Return False
EndFunc

Func _MouseClickApprox($button, $x, $y, $clicks)
	$x = $x+Random(-10, 10)
	$y = $y+Random(-10, 10)
	MouseClick($button, $x, $y, $clicks)
EndFunc

Func _GetEndFightButtonColor()
	initBlueStacks()

	$color = PixelGetColor($end_fight_button[0], $end_fight_button[1])
	MouseMove($end_fight_button[0], $end_fight_button[1])

	IniWrite("/H2C2.ini", "EndFightButton", "Color", $color)

EndFunc



Func _Function_finished()
	Global $fInterrupt

	ConsoleWrite("_Function_finished : interrupt = " & $fInterrupt & @CRLF)
	; reactivate all buttons and disable stop button
	$fInterrupt = 0
	ConsoleWrite("_Function_finished : interrupt = " & $fInterrupt & @CRLF)
	SwitchGuiState($GUI_ENABLE)
	If $fInterrupt = 0 Then
		GUICtrlSetData($label, "Finished.")
	Else
		GUICtrlSetData($label, "Finished (incorrect state).")
	EndIf
EndFunc

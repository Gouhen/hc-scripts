; Manage houselight in hustle castle
#include-once
#include "data/hc_coords.au3"


;number "one" coords
Global Const $lighthouse_attack_energy_number[11] = [917, 353,   917, 476,   917, 593,   917, 716,   917, 833,    0xFFFFFF]
Global Const $lighthouse_attack_buttons[11] = [830, 324,   830, 444,   830, 564,   830, 684,   830, 803,   0x70D231] ;mesur� 0x6CCA2F
Global Const $lighthouse_confirm_attack[2] = [1100, 803]
Global Const $lighthouse_confirm_quickFight[2] = [840, 820]
Global Const $lighthouse_no_spell_button[2] = [583, 585]

Global $isQuickFight

Func Lighthouse()

	If initBlueStacks() Then
		_Function_Finished()
		Return
	EndIf




	Local $count = 0 ; (5 opponents)
	Do

		;FAIRE ATTENTION EN CAS DE MODIF : ALGO RECOPIE DANS invasionFromCastle
		If _Interrupt_Sleep(1000) Then
			_Function_Finished()
			return
		Endif

		;si il y a un bouton
		$color = PixelGetColor($lighthouse_attack_buttons[$count*2], $lighthouse_attack_buttons[($count*2)+1])
		MouseMove($lighthouse_attack_buttons[$count*2], $lighthouse_attack_buttons[($count*2)+1])
		If $color == $lighthouse_attack_buttons[10] Then

			;si le nombre est pas blanc
			$color = PixelGetColor($lighthouse_attack_energy_number[$count*2], $lighthouse_attack_energy_number[($count*2)+1])
			MouseMove($lighthouse_attack_buttons[$count*2], $lighthouse_attack_buttons[($count*2)+1])
			If not($color == $lighthouse_attack_energy_number[10]) Then
				ConsoleWrite (@TAB & "Plus d'�nergie" & @CRLF)
				_Function_Finished()
				return
			EndIf


			MouseClick("primary", $lighthouse_attack_energy_number[$count*2], $lighthouse_attack_energy_number[($count*2)+1] , 1)

			If _Interrupt_Sleep(4000) Then ;todo raccourcir le d�lais si les lenteurs disparaissent. Ou attendre le bouton vert
				_Function_Finished()
				return
			Endif

			If $isQuickFight Then
				MouseClick("primary", $lighthouse_confirm_quickFight[0], $lighthouse_confirm_quickFight[1] , 1)
			Else
				MouseClick("primary", $lighthouse_confirm_attack[0], $lighthouse_confirm_attack[1] , 1)
			Endif


			If _Interrupt_Sleep(1000) Then
				_Function_Finished()
				return
			Endif

			;no tools. Fly, you tools
			$color1 = PixelGetColor($no_tool[0], $no_tool[1])
			$color2 = PixelGetColor($no_tool[4], $no_tool[5])
			If ($color1 == $no_tool[2] or $color1 == $no_tool[3]) And ($color2 == $no_tool[6] or $color2 == $no_tool[7]) Then
				MouseClick("primary", $no_tool[0], $no_tool[1] , 1) ; ouioui, ils vont gagner de toute fa�on
				If _Interrupt_Sleep(1000) Then
					_Function_Finished()
					return
				Endif
			EndIf

			;incomplete team
			$color1 = PixelGetColor($incomplete_team[0], $incomplete_team[1])
			$color2 = PixelGetColor($incomplete_team[4], $incomplete_team[5])
			If ($color1 == $incomplete_team[2] or $color1 == $incomplete_team[3]) And ($color2 == $incomplete_team[6] or $color2 == $incomplete_team[7]) Then
				MouseClick("primary", $incomplete_team[0], $incomplete_team[1] , 1) ; ouioui, ils vont gagner de toute fa�on
				If _Interrupt_Sleep(1000) Then
					_Function_Finished()
					return
				Endif
			EndIf

			;passe le "ya pas de sorts"
			MouseClick("primary", $lighthouse_no_spell_button[0], $lighthouse_no_spell_button[1] , 1) ; ouioui, ils vont gagner de toute fa�on
			ConsoleWrite (@TAB & "continue si pas de sort" & @CRLF)



			If _Interrupt_Sleep(500) Then
				_Function_Finished()
				return
			Endif;

			; on attend la fin de l attaque
			If fight(true, false) Then
				_Function_Finished()
				return
			EndIf

			ConsoleWrite (@TAB & @TAB & "Fin de l'attaque" & @CRLF)
			If end_fight(False) Then
				_Function_Finished()
				return
			EndIf

			;todo v�rifier qu'on est dans l'�cran phare
			If _wait_lighthouse_screen() Then
				_Function_Finished()
				return
			Endif;

		EndIf

		$count = $count + 1;


	Until $count == 5


	_Function_Finished()
EndFunc


Func _wait_lighthouse_screen()

	Local Const $cloud = _OpenCV_imread_and_check(_OpenCV_FindFile("lighthouse_cloud.png"))
	Local Const $threshold = 0.8
	Local $aRect = WinGetPos($hwnd)
	Local $iBegin = TimerInit()
	Do
		If _Interrupt_Sleep(1000) Then
			Return True
		Endif

		MouseClick("primary", 1205, 485, 1)

		Local $img = _OpenCV_GetDesktopScreenMat($aRect)
		;find match fight
		Local $aMatches = _OpenCV_FindTemplate($img, $cloud, $threshold)

		If UBound($aMatches) Then
			ConsoleWrite("Found lighthouse screen" & @CRLF)

			Return False
		EndIf

	Until TimerDiff($iBegin) > 60000 ; 1 minute devrait suffire

	Return False
EndFunc
#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_UseX64=y
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****



#include-once
#include "data/hc_coords.au3"

Global Const $titanc_attack_energy_number[11] = [1150, 340,   1150, 440,   1150, 540,   1150, 640,   1150, 750,    0xFFFFFF]
Global Const $titanc_attack_buttons[14] = [1085, 340,   1085, 440,   1085, 545,   1085, 645,   1085, 750, 0x63C023, 0x63BF22, 0x63BE21, 0x5FB720] ;mesur� 0x6CCA2F
;Global Const $titanc_confirm_attack[2] = [1170, 810]



Func TitanTower($deployBests)

	If initBlueStacks() Then
		_Function_Finished()
		Return
	EndIf

	$level = 1

	For $loop = 0 to 9
		ConsoleWrite ("titan loop "& $loop & @CRLF)

		;si on est sur la carte
		;If is_map_screen() And Not is_titan_stage() Then
		;	back_to_castle()
		;EndIf

		;si on est dans le ch�teau
		If is_castle_screen() Then
			open_map()
			open_tower()
		EndIf

		;si on est dans l'�cran de choix d'activit� titan
		If is_titan_activity_screen() Then
			open_tower_activity()
		EndIf

		;si on est dans l'�cran tours titan
		If is_titan_tower_screen() Then
			If open_tower_level($level) Then
				_Function_Finished()
				Return
			EndIf
		EndIf

		If is_titan_tower_stage() Then
			If TitanTowerLoop($deployBests) Then
				_Function_Finished()
				Return
			EndIf

			$level = $level + 1
			ConsoleWrite ("Level up (" &$level& ")"&@CRLF)
		EndIf
	Next

	_Function_Finished()
EndFunc

Func open_tower_activity()
	MouseClick("primary", 505, 425, 1)

	If _Interrupt_Sleep(6000) Then
		Return True
	Endif
EndFunc


Func open_tower_level($level)
	ConsoleWrite ("Open Level " &$level& @CRLF)
	If $level == 1 Then
		MouseClick("primary", 431, 380, 1)
	ElseIf $level == 2 Then
		MouseClick("primary", 767, 380, 1)
	ElseIf $level == 3 Then
		MouseClick("primary", 1009, 380, 1)
	EndIf

	If _Interrupt_Sleep(1500) Then
		Return True
	Endif

	Return False
EndFunc

Func TitanTowerLoop($deployBests)

	;tant que bouton d'attaque vert

	$color = PixelGetColor(715, 806 )
	While $color == 0x6ED217 Or $color == 0x73DB18
		;attaque
		MouseClick("primary",715, 806, 1)
		If _Interrupt_Sleep(1000) Then
			Return True
		Endif

		TitanFight($deployBests)

		;si on est dans l'�cran tours titan (pour g�rer un bug si on sort du niveau 30.)
		If is_titan_tower_screen() Then
			Return False
		Endif

	WEnd

	;bouton retour
	MouseClick("primary", 106, 803, 1)
	ConsoleWrite (@TAB & "retour" & @CRLF)
	If _Interrupt_Sleep(500) Then
		Return True
	Endif

	Return False
EndFunc

Func TitanFight($deployBests)
	If $deployBests Then
		;deployer les meilleurs 593, 427
		MouseClick("primary",593, 427, 1)
		If _Interrupt_Sleep(500) Then
			Return True
		Endif
	Endif

	;au combat  0x6ED217 0x73DB18
	MouseClick("primary",1222, 761, 1) ;todo verifier la couleur?
	If _Interrupt_Sleep(1000) Then
		Return True
	Endif

	; on attend la fin de l attaque
	If fight(true, false) Then
		Return True
	EndIf

	;ConsoleWrite (@TAB & @TAB & "Fin de l'attaque" & @CRLF)
	If end_fight(true) Then
		Return True
	EndIf

	If _Interrupt_Sleep(1500) Then
		Return True
	Endif
EndFunc


Func TitanColiseum($deployBests)

	If initBlueStacks() Then
		_Function_Finished()
		Return
	EndIf


	ConsoleWrite ("titan coliseum"& @CRLF)

	;si on est sur la carte
	;If is_map_screen() And Not is_titan_stage() Then
	;	back_to_castle()
	;EndIf

	;si on est dans le ch�teau
	If is_castle_screen() Then
		open_map()
		open_tower()
	EndIf

	;si on est dans l'�cran de choix d'activit� titan
	If is_titan_activity_screen() Then
		open_coliseum_activity()
	EndIf

	;si on est dans l'�cran colisee titan
	If is_titan_coliseum_stage() Then
		If TitanColiseumLoop($deployBests) Then
			_Function_Finished()
			Return
		EndIf
	EndIf

	_Function_Finished()
EndFunc

Func TitanColiseumLoop($deployBests)

	If initBlueStacks() Then
		_Function_Finished()
		Return
	EndIf

	Local $count = 0 ; (5 opponents)
	Do

		If _Interrupt_Sleep(1000) Then
			_Function_Finished()
			return
		Endif;

		;si il y a un bouton
		$color = PixelGetColor($titanc_attack_buttons[$count*2], $titanc_attack_buttons[($count*2)+1])
		MouseMove($titanc_attack_buttons[$count*2], $titanc_attack_buttons[($count*2)+1])
		If $color == $titanc_attack_buttons[10] or $color == $titanc_attack_buttons[11] or $color == $titanc_attack_buttons[12] or $color == $titanc_attack_buttons[13] Then

			;si le nombre est pas blanc
			$color = PixelGetColor($titanc_attack_energy_number[$count*2], $titanc_attack_energy_number[($count*2)+1])
			MouseMove($titanc_attack_buttons[$count*2], $titanc_attack_buttons[($count*2)+1])
			If not($color == $titanc_attack_energy_number[10]) Then
				ConsoleWrite (@TAB & "Plus d'�nergie" & @CRLF)
				_Function_Finished()
				return
			EndIf


			MouseClick("primary", $titanc_attack_energy_number[$count*2], $titanc_attack_energy_number[($count*2)+1] , 1)

			If _Interrupt_Sleep(1000) Then ;todo raccourcir le d�lais si les lenteurs disparaissent. Ou attendre le bouton vert
				_Function_Finished()
				return
			Endif;

			TitanFight($deployBests)

			;todo v�rifier qu'on est dans l'�cran phare
			If _wait_coliseum_screen() Then
				_Function_Finished()
				return
			Endif;

		EndIf

		$count = $count + 1;


	Until $count == 5


	_Function_Finished()
EndFunc

Func _wait_coliseum_screen()

	Local Const $cloud = _OpenCV_imread_and_check(_OpenCV_FindFile("titan_coliseum_top.png"))
	Local Const $threshold = 0.8
	Local $aRect = WinGetPos($hwnd)
	Local $iBegin = TimerInit()
	Do
		If _Interrupt_Sleep(1000) Then
			Return True
		Endif

		MouseClick("primary", 1292, 713, 1)

		Local $img = _OpenCV_GetDesktopScreenMat($aRect)
		;find match fight
		Local $aMatches = _OpenCV_FindTemplate($img, $cloud, $threshold)

		If UBound($aMatches) Then
			ConsoleWrite("screen Found" & @CRLF)
			Return False
		EndIf

	Until TimerDiff($iBegin) > 60000 ; 1 minute devrait suffire

	Return False
EndFunc



Func open_coliseum_activity()
	MouseClick("primary", 1138, 425, 1)

	If _Interrupt_Sleep(6000) Then
		Return True
	Endif
EndFunc

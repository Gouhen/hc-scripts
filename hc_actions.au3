#include-once

#include <AutoItConstants.au3>
#include <MsgBoxConstants.au3>

#include "helpers.au3"
#include "data/hc_coords.au3"
#include "discord.au3"

#include "udf\autoit-opencv\opencv_udf_utils.au3"

Global $hwnd
Global $discordEnableCapcha


Global $go_to_castle_button[9] = [110, 737, 0x2379B2, 87, 812, 0x7D6B63, 112, 835, 0x635D4A]
Global $go_to_map_button[9] = [114, 736, 0x9FEBEF, 73, 768, 0xF2206E, 88, 813, 0xFAB573]
Global Const $test_pixel_castle_under_attack[9] = [772, 417, 0x7BC4D3, 750, 456, 0xFFFFFF, 840, 496, 0x7BC4D3]

; under attack update : 766, 603
; wait 8-10 seconds
; ok button 920, 744

Func init()
	$go_to_map_button[2] = IniRead("/H2C2.ini", "GoToMapButtonColors", "color1", 0x9FEBEF)
	$go_to_map_button[5] = IniRead("/H2C2.ini", "GoToMapButtonColors", "color2", 0xF2206E)
	$go_to_map_button[8] = IniRead("/H2C2.ini", "GoToMapButtonColors", "color3", 0xFAB573)
EndFunc

Func initBlueStacks()
	Opt("GUIOnEventMode", 1) ; Activation du mode événementiel
	Opt("WinTitleMatchMode", 2)
	Opt("MouseCoordMode", 0) ; coordonnées souris relatives
	Opt("PixelCoordMode", 0)

   WinWait("Hustle Castle", "", 10)
   $hwnd = WinActivate("Hustle Castle") ;hustle castle en mode fenêtre
   If _Interrupt_Sleep(2000) Then
      Return True
   Endif;


	if $hwnd == 0 Then
		ConsoleWrite ("Pas trouvé la fenêtre" & @CRLF)
		Return False
	EndIf
	;WinSetTitle($hWnd, "HC")
	If WinMove($hWnd, "", 5, 12, 1539, 910)	Then;strange coordinates to match bluestacks
		ConsoleWrite ("moved" & @CRLF)
	Else
		ConsoleWrite ("error" & @CRLF)
	EndIf

	Return False
EndFunc

Func is_castle_screen()
  $result = FindImage("castle_ct_button.png")
	Return $result
EndFunc

Func is_map_screen()
	; If map screen, then the go to castle button is visible
  $result = PixelGetColor($go_to_castle_button[0], $go_to_castle_button[1]) == $go_to_castle_button[2] _
	   And ( PixelGetColor($go_to_castle_button[3], $go_to_castle_button[4]) == $go_to_castle_button[5] or PixelGetColor($go_to_castle_button[3], $go_to_castle_button[4]) == 0x7B6963 ) _
	   And PixelGetColor($go_to_castle_button[6], $go_to_castle_button[7]) == $go_to_castle_button[8]
	return $result
EndFunc

Func is_tournament_screen()
	Return FindImage("tournament_donald.png")
EndFunc


Func is_tournament_running()
	Return FindImage("tournament_skycastle.png")
EndFunc

Func is_dungeon_screen()
	Return FindImage("dungeon_menu_detect.png")
EndFunc

Func is_cathedral_screen()
	Return FindImage("cathedral_master.png")
EndFunc


Func is_cathedral_stage()
	Return FindImage("cathedral_part.png")
EndFunc

Func is_titan_tower_screen()
	Return FindImage("titan_tower_easy.png")
EndFunc

Func is_titan_activity_screen()
	Return FindImage("titan_tower_activity.png")
EndFunc

Func is_titan_tower_stage()
	Return FindImage("titan_tower_top.png")
EndFunc

Func is_titan_coliseum_stage()
	Return FindImage("titan_coliseum_top.png")
EndFunc

Func FindImage($image_name)
	Local $unusedx, $unusedy
	Return FindImageCoord($hwnd, $image_name, $unusedx, $unusedy)
EndFunc

Func FindImageCoord2($window, $image_name, ByRef $x, ByRef $y, $threshold)
	Local Const $master = _OpenCV_imread_and_check(_OpenCV_FindFile($image_name))
	;opencv screenshot
	Local $aRect = WinGetPos($window)
	Local $img = _OpenCV_GetDesktopScreenMat($aRect)
	;find match fight
	Local $aMatches = _OpenCV_FindTemplate($img, $master, $threshold)

	If UBound($aMatches) Then
		$x = $aMatches[0][0]
		$y = $aMatches[0][1]

		Return True
	EndIf

	Return False
EndFunc

Func FindImageCoord($window, $image_name, ByRef $x, ByRef $y)
	Local Const $threshold = 0.8
	Return FindImageCoord2($window, $image_name, $x, $y, $threshold)
EndFunc


Func is_castle_under_attack()
	return PixelGetColor($test_pixel_castle_under_attack[0], $test_pixel_castle_under_attack[1]) == $test_pixel_castle_under_attack[2] _
	   And PixelGetColor($test_pixel_castle_under_attack[3], $test_pixel_castle_under_attack[4]) == $test_pixel_castle_under_attack[5] _
	   And PixelGetColor($test_pixel_castle_under_attack[6], $test_pixel_castle_under_attack[7]) == $test_pixel_castle_under_attack[8]
EndFunc

Func is_diamond_button()
	For $x = $look_for_diamond[0] To $look_for_diamond[1] Step 4
		$color = PixelGetColor($x, $look_for_diamond[2])
		MouseMove($x, $look_for_diamond[2])
		If $color == $look_for_diamond[3] Or $color = $look_for_diamond[4] Then
			Return True
		EndIf
	Next
	Return False
EndFunc

; True = Castle menu obtenu
; False = Non
Func _castleOrMap()
	;teste si on a le bouton carte. si oui, sort
	If is_castle_screen() Then
		Return True
	EndIf


	;teste si on a le bouton carte. si oui, clique sur le bouton retour
	If is_map_screen() Then
		MouseClick("primary", $go_to_castle_button[3], $go_to_castle_button[4], 1)
		;attends
		If _Interrupt_Sleep(1000) Then
			Return False
		Endif;

		If is_castle_screen() Then
			Return True
		EndIf
	EndIf

	Return False
EndFunc

; FALSE = OK in castle, TRUE error
Func back_to_castle()

	If _castleOrMap() Then
		Return False
	EndIf


	;si non clique en haut à droite (activité, fenêtre d'info du château)
	MouseClick("primary", $close_button[0], $close_button[1], 1)
	;attends
	If _Interrupt_Sleep(2000) Then
		Return True
	Endif;

	If _castleOrMap() Then
		Return False
	EndIf

	;si non clique en haut à droite
	MouseClick("primary", $close_button[0], $close_button[1], 1)
	;attends
	If _Interrupt_Sleep(2000) Then
		Return True
	Endif;

	;teste si on a le bouton carte. si oui, sort

	If _castleOrMap() Then
		Return False
	EndIf

	ConsoleWrite("Could not return to castle")

	Return False
EndFunc

Func open_map($drag=True)
	MouseClick("primary", 170, 810, 1)	;carte
	If _Interrupt_Sleep(2000) Then
		Return True
	Endif;

	If $drag Then
		drag_map_right()
	EndIf

	Return False
EndFunc

Func drag_map_right()
	MouseClickDrag("primary", 35, 120, 900, 120)

	If _Interrupt_Sleep(200) Then
		Return True
	Endif;
	MouseClickDrag("primary", 35, 120, 900, 120)

	If _Interrupt_Sleep(200) Then
		Return True
	Endif;
EndFunc

Func drag_map_left()
	MouseClickDrag("primary", 900, 120, 35, 120)

	If _Interrupt_Sleep(200) Then
		Return True
	Endif;
EndFunc

Func open_arena()
	MouseClick("primary", 620, 760, 1)	;arène
	If _Interrupt_Sleep(2000) Then
		Return True
	Endif;
	Return False
EndFunc

Func open_bay()
	MouseClick("primary", 1050, 749, 1)	;bay
	If _Interrupt_Sleep(1500) Then
		Return True
	Endif
	Return False
EndFunc

Func open_cathedral()
	MouseClick("primary", 1371, 777, 1)	;cathedral (ex dungeon)
	If _Interrupt_Sleep(1500) Then
		Return True
	Endif
	Return False
EndFunc

Func open_portal()
	MouseClick("primary", 1377, 494, 1)	;portail
	If _Interrupt_Sleep(1500) Then
		Return True
	Endif
	Return False
EndFunc

Func open_tower()
	MouseClick("primary", 141, 235, 1)	;tower
	If _Interrupt_Sleep(1500) Then
		Return True
	Endif
	Return False
EndFunc



Func open_dungeon()
	MouseClick("primary", 809, 789, 1)	;dungeon attention, il faut bouger la map d'abord
	If _Interrupt_Sleep(1500) Then
		Return True
	Endif
	Return False
EndFunc

Func open_lighthouse()
	MouseClick("primary", 579, 714, 1)	;phare attention, il faut bouger la map d'abord
	If _Interrupt_Sleep(1500) Then
		Return True
	Endif
	Return False
EndFunc

Func open_journey()
	MouseClick("primary", 635, 146, 1)	;excursion attention, il faut bouger la map d'abord
	If _Interrupt_Sleep(1500) Then
		Return True
	Endif
	Return False
EndFunc

Func close_area()
	; clique en haut à droite
	MouseClick("primary", $close_button[0], $close_button[1], 1)
	If _Interrupt_Sleep(2000) Then
		Return True
	Endif
	Return False
EndFunc


Func open_map_and_arena($drag=True)
	If open_map($drag) Then
		Return True
	EndIf
	If open_arena() Then
		Return True
	EndIf
	Return False

EndFunc

Func open_map_and_portal($drag=True)
	If open_map($drag) Then
		Return True
	EndIf

	If open_portal() Then
		Return True
	EndIf

	Return False
EndFunc

Func open_invasion()
	If open_map() Then
		Return True
	EndIf

	MouseClick("primary", 1407, 494, 1)

	If _Interrupt_Sleep(2000) Then
		Return True
	Endif;

	Return False
EndFunc

Func launch_arena($payment)
	If $payment == "Apples" Then
		MouseClick("primary", 357, 265, 1)
	ElseIf $payment == "Tickets" Then
		MouseClick("primary", 637, 265, 1)
	EndIf

	If _Interrupt_Sleep(500) Then
		Return True
	Endif;

	MouseClick("primary", 500, 775, 1)
	If _Interrupt_Sleep(2000) Then
		Return True
	Endif;

	MouseClick("primary", 757, 808, 1)
	If _Interrupt_Sleep(2000) Then
		Return True
	Endif;

	;no tools. Fly, you tools
	$color1 = PixelGetColor($no_tool[0], $no_tool[1])
	$color2 = PixelGetColor($no_tool[4], $no_tool[5])
	If ($color1 == $no_tool[2] or $color1 == $no_tool[3]) And ($color2 == $no_tool[6] or $color2 == $no_tool[7]) Then
		MouseClick("primary", $no_tool[0], $no_tool[1] , 1) ; ouioui, ils vont gagner de toute façon
		If _Interrupt_Sleep(1000) Then
			return True
		Endif
	EndIf

	Return False
EndFunc

Func select_opponent($x, $y)
	MouseClick("primary", $x, $y, 1)
	If _Interrupt_Sleep(2000) Then
		Return True
	Endif
	;confirm
	MouseClick("primary", 1100, 730, 1)
	If _Interrupt_Sleep(1000) Then
		Return True
	Endif
	;clic sur la croix de fermeture si l'action nest pas lancee
	MouseClick("primary", 1208, 151, 1)
	If _Interrupt_Sleep(500) Then
		Return True
	Endif

	Return False
EndFunc

Func end_fight($heros)
  Local $color
  
  ; Dummy click for reward
  If $heros Then
    MouseClick("primary", $hero_spell[0],  $hero_spell[1], 1, 1)
    ;ControlClick($hwnd, "", "", "primary", 1, $hero_spell[0],  $hero_spell[1]);clic where the mouse is... dammit
  EndIf
  
  ; Wait for green fight button
  do
    $color = PixelGetColor(749, 785)
    ;ConsoleWrite (@TAB & @TAB & "end_fight green button loop ! " & @CRLF)
  until $color == 0x73DB18 or $color == 0x6ED217
  
  Do
    ;ConsoleWrite (@TAB & @TAB & "end_fight loop ..." & @CRLF)
    MouseClick("primary", 749, 785, 1, 1)
    If _Interrupt_Sleep(200) Then
      Return True
    Endif
    $color = PixelGetColor(749, 785)
  Until( $color <> 0x73DB18 and $color <> 0x6ED217 )
  ;ConsoleWrite (@TAB & @TAB & "end_fight loop end ! " & @CRLF)

	Return False
EndFunc

Func fight($heros, $nextButton)
	Local $color
	do
    ;ConsoleWrite (@TAB & @TAB & "fight loop ... " & @CRLF)
		If _Interrupt_Sleep(100) Then
			return True
		Endif

		;bouton héros jusqu'à la fin de l'attaque
		If $heros Then
			MouseClick("primary", $hero_spell[0],  $hero_spell[1], 1, 1)
			;ControlClick($hwnd, "", "", "primary", 1, $hero_spell[0],  $hero_spell[1]);clic where the mouse is... dammit
		EndIf

		;si les 10 attaques sont terminées
		If $nextButton and PixelGetColor($end_fight_next_button[0], $end_fight_next_button[1]) == $end_fight_next_button[2] Then	;TODO generic next button
			ExitLoop
		EndIf

		;ConsoleWrite ("PixelGetColor("&$island_next_button[0]&", "&$island_next_button[1]&")=" & Hex(PixelGetColor($island_next_button[0], $island_next_button[1]), 6) & @CRLF)
		$color = PixelGetColor($end_fight_button[0], $end_fight_button[1])
	until( $color == $end_fight_button[2] or $color == $end_fight_button[3] or $color == $end_fight_button[4] or $color == $end_fight_button[5] or $color == $end_fight_button[6] );attend le bouton vert "fin du combat"
  
  ;ConsoleWrite (@TAB & @TAB & "fight loop end ! " & @CRLF)

	Return False
EndFunc

; Detect if we have been attacked or disconnected
Func test_disco_or_attacked($jobId)
  $detected = False
  
  ; detect the pop-up frame
  If ( PixelGetColor(1092, 243) == 0xFFFFFF ) and ( PixelGetColor(1100,232) == 0x395078 ) Then
    ; Attacked case ? (look for light blue & white of castle in pop-up)
    If ( PixelGetColor(805,506) == 0x7AC4D4 ) and ( PixelGetColor(798,499) == 0xFFFFFF ) Then
      _Generic_Log_INFO("Under attack pop-up detected !" & @CRLF)
      $detected = True
      
    ; Disconnected case
    ElseIf ( PixelGetColor(1092, 243) == 0xFFFFFF ) and ( PixelGetColor(1100,232) == 0x395078 ) Then
      _Generic_Log_INFO("Disconnected pop-up detected !" & @CRLF)
      $detected = True
      
      ; Wait 2min before reconnecting
      ; TODO
    Else
      _Generic_Log_INFO("Pop-up detected but not reconnized !" & @CRLF)
    EndIf
  EndIf
  
  ; Common part
  If $detected  Then
    ; Close pop-up
    MouseClick("primary", $close_popup[0], $close_popup[1], 1, 2)
    
    ; Wait until reload to the castle
    Do
      _Interrupt_Sleep(500)
    Until( is_castle_screen() )
    
    ; Close on "reconnection pop-up"
    MouseClick("primary", $close_popup[0], $close_popup[1], 1, 2)
  
    ; Relaunch activity
    Switch $jobId
      Case 0 to 0 ; Cathedral
        If open_map() Then
          Return True
        EndIf
        If open_cathedral() Then
          Return True
        EndIf
      Case 1 to 1 ; Dungeon
        If open_map() Then
          Return True
        EndIf
        drag_map_left()
        If open_dungeon() Then
          Return True
        EndIf
      Case Else
        _Generic_Log_Error("Disconnected pop-up: unknown job to restart ! job=" & $jobId & @CRLF)
    EndSwitch
  EndIf
  
	Return False
EndFunc

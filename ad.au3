; Passe les pubs dans hustle castle
#include-once

#include "data/hc_coords.au3"

Global Const $quests_button[2] = [67, 549]
Global Const $daily_quests_button[2] = [198, 307]
Global Const $first_daily_quest_button[2] = [495, 421]
Global Const $test_pixel_get_reward[4] = [1137, 717, 0x73DB18, 0xFDBD05] ;green : get reward. orange : watch ad ; mesuré : vert=0x6ED217   orange=0xF3B605
Global Const $watch_add_button[2] = [1035, 725]
Global Const $add_with_sound_button[3] = [884, 540, 0x3E82F7] ;bleu : want sound?


Global Const $square_add[5] = [1043, 389, 469, 395, 0x373A3F]
Global Const $add_open_button[3] = [806, 686, 0x3E82F7] ;bleu
Global Const $add_close_button[3] = [578, 689, 0xFFFFFF] ;blanc

Global Const $tiny_add[5] = [952, 438, 585, 423, 0x1E1E1E]
Global Const $tiny_close_button[2] = [901, 265]

;todo
Global Const $ad_background_color[3] = [0x373A3F, 0x1E1E1E, 0x3D3D3D]


;ads in castle (TODO not in PC version for now
Global Const $test_pixel_confirm_close_vid[3] = [1126, 592, 0x3E82F7] ; blue = confirm close vid
Global Const $confirm_close_button[2] = [719, 620]


;market (pc : todo)
Global Const $market_button[2] = [1428, 804]
Global Const $treasure_button[2] = [952, 850]
Global Const $test_pixel_treasure_chest[4] = [176, 674, 0x73DB18, 0xFDBD05] ;green : get reward. orange : watch ad
Global Const $treasure_chest_confirm[2] = [757, 763]
Global Const $portal_closed_panel[3] = [517, 477,0x00BDCA]


Global Const $arena_shop[2] = [1215, 224]
Global Const $bay_shop[2] = [1227, 250]
Global Const $dungeon_shop[2] = [1244, 818]
Global Const $portal_shop[2] = [1267, 265]
Global Const $lighthouse_shop[2] = [1252, 321]
Global Const $cathedral_shop[2] = [1241, 815]
Global Const $btn_free_resources[2] = [792, 209]

Global Const $arena_freeres[2] = [792, 209]
Global Const $bay_freeres[2] = [829, 251]
Global Const $dungeon_freeres[2] = [859, 245]
Global Const $portal_freeres[2] = [913, 167]
Global Const $lighthouse_freeres[2] = [949, 829]
Global Const $cathedral_freeres[2] = [863, 246]

Global Const $mithril_resource_coords[4] = [1073, 122, 1055, 512] ;mesuré : 0xF5BF0F voir add_button_color
Global Const $gold_resource_coords[4] = [1264, 67, 993, 454]
Global Const $apple_resource_coords[4] = [1078, 63, 1058, 456]
Global Const $mana_resource_coords[4] = [884, 62, 860, 458]
Global Const $wood_resource_coords[4] = [680, 63, 663, 451]
Global Const $iron_resource_coords[4] = [489, 67, 468, 453]


;ad buttons
Global Const $test_pixel_arena_ad_button[2] = [1242, 338]
Global Const $test_pixel_bay_ad_button[2] = [1254, 367]
Global Const $test_pixel_dungeon_ad_button[2] = [1305, 603]
Global Const $test_pixel_portal_ad_button[2] = [1292, 440]
Global Const $test_pixel_lighthouse_ad_button[2] = [1307, 409]
Global Const $test_pixel_journey_ad_button[2] = [1199, 754]
Global Const $test_pixel_cathedral_ad_button[2] = [1349, 606]

Global Const $add_button_colors[3] = [0xFFC710, 0xFDBD05, 0xF5BF0F] ;orange : watch ad



Func DailyAds($autoclose)

	If initBlueStacks() Then
		_Function_Finished()
		Return
	EndIf

	; 2) va au chateau
	; 3) ouvre les quêtes
	If back_to_castle() Then
		_Function_Finished()
		Return
	EndIf
	If open_daily_ads() Then
		_Function_Finished()
		Return
	EndIf

	Local $count = 0
	Do
		ConsoleWrite("pubs journalières " & $count & @CRLF)
		ConsoleWrite ("get color ad button("&$test_pixel_get_reward[0]&", "&$test_pixel_get_reward[1]&") = " & hex(PixelGetColor($test_pixel_get_reward[0], $test_pixel_get_reward[1]), 6) & @CRLF)
		If PixelGetColor($test_pixel_get_reward[0], $test_pixel_get_reward[1]) == $test_pixel_get_reward[2] Then ; vert get reward
			MouseClick("primary", $watch_add_button[0], $watch_add_button[1], 1)
			ConsoleWrite("bouton vert => récompense"& @CRLF)
			If _Interrupt_Sleep(2000) Then
				_Function_Finished()
				return
			Endif
			;en cas de double récompense (genre rune), clique à côté du bouton
			MouseClick("primary", 830, 731, 1)
			If _Interrupt_Sleep(2000) Then
				_Function_Finished()
				return
			Endif;
		ElseIf PixelGetColor($test_pixel_get_reward[0], $test_pixel_get_reward[1]) == $test_pixel_get_reward[3] Then ;orange watch add
			MouseClick("primary", $watch_add_button[0], $watch_add_button[1], 1)
			ConsoleWrite("bouton orange => pub"& @CRLF)
			If wait_and_exit_add($autoclose) Then
				_Function_Finished()
				Return
			EndIf
			$count = $count + 1
			;des fois, il faut cliquer plusieurs fois pour sortir d'une pub => on retourne au chateau, c'est plus simple
			If _Interrupt_Sleep(3000) Then
				_Function_Finished()
				return
			Endif;
			; close ad window
			MouseClick("primary", 1304, 86, 1)
			If _Interrupt_Sleep(2000) Then
				_Function_Finished()
				return
			Endif;
			If open_daily_ads() Then
				_Function_Finished()
				Return
			EndIf
		ElseIf PixelGetColor($test_pixel_get_reward[0], $test_pixel_get_reward[1]) == 0xFFFFFF Then
			ConsoleWrite("plus de bouton? On a fini les pubs"& @CRLF)
			$count = 6
			If _Interrupt_Sleep(3000) Then
				_Function_Finished()
				return
			Endif;
		Else
			ConsoleWrite("Coincé? On sort des pubs")
			If wait_and_exit_add($autoclose) Then
				_Function_Finished()
				Return
			EndIf

			; close ad window
			MouseClick("primary", 1304, 86, 1)
			If _Interrupt_Sleep(2000) Then
				_Function_Finished()
				return
			Endif;
			If open_daily_ads() Then
				_Function_Finished()
				Return
			EndIf
		Endif
	Until $count == 6


	ConsoleWrite("fin pub journalieres"& @CRLF)
	;sors de l'ecran des pubs

	; close ad window
	MouseClick("primary", 1304, 86, 1)
	If _Interrupt_Sleep(2000) Then
		_Function_Finished()
		return
	Endif;

	;pub coffre
	MouseClick("primary", $market_button[0], $market_button[1], 1)
	If _Interrupt_Sleep(2000) Then
		_Function_Finished()
		return
	Endif;

	MouseClick("primary", $treasure_button[0], $treasure_button[1], 1)
	If _Interrupt_Sleep(1000) Then
		_Function_Finished()
		return
	Endif;

	If PixelGetColor($test_pixel_treasure_chest[0], $test_pixel_treasure_chest[1]) == $test_pixel_treasure_chest[2] Then ; vert get reward
		MouseClick("primary", $test_pixel_treasure_chest[0], $test_pixel_treasure_chest[1], 1)
		ConsoleWrite("bouton vert => récompense"& @CRLF)
	ElseIf PixelGetColor($test_pixel_treasure_chest[0], $test_pixel_treasure_chest[1]) == $test_pixel_treasure_chest[3] Then ;orange watch add
		MouseClick("primary", $test_pixel_treasure_chest[0], $test_pixel_treasure_chest[1], 1)
		ConsoleWrite("bouton orange => verif"& @CRLF)
		If _Interrupt_Sleep(2000) Then
			_Function_Finished()
			return
		Endif;
		If PixelGetColor($treasure_chest_confirm[0], $treasure_chest_confirm[1]) == $test_pixel_treasure_chest[3] Then
			MouseClick("primary", $treasure_chest_confirm[0], $treasure_chest_confirm[1], 1)
			ConsoleWrite("confirm orange => pub"& @CRLF)
			If wait_and_exit_add($autoclose) Then
				_Function_Finished()
				Return
			EndIf
		EndIf
	Endif

	ConsoleWrite("FIN"& @CRLF)

	_Function_Finished()
EndFunc

Func MapAds($autoclose)

	If initBlueStacks() Then
		_Function_Finished()
		Return
	EndIf

	If back_to_castle() Then
		_Function_Finished()
		Return
	EndIf
	If map_ads($autoclose) Then
		_Function_Finished()
		Return
	EndIf
	_Function_Finished()
EndFunc


Func map_ads($autoclose)
	ConsoleWrite("pubs carte"& @CRLF)

	;ouvre la carte
	;scroll
	If open_map(True) Then ;open map and drag
		Return True
	EndIf

	;4 pubs portail, 1 pub excursion, 1 pub phare, 2 pubs des autres.
	;donc on fait 1 portail, boucle arène/baie/donjon/portail 2x, puis excursion, phare et dernier portail. pfouuu


;portal 1
	If open_portal() Then
		Return True
	EndIf


	$color = PixelGetColor($test_pixel_portal_ad_button[0], $test_pixel_portal_ad_button[1])
	If $color == $add_button_colors[0] or $color==$add_button_colors[1] or $color==$add_button_colors[2] Then ;orange
		MouseClick("primary", $test_pixel_portal_ad_button[0], $test_pixel_portal_ad_button[1], 1)
		ConsoleWrite("bouton orange => pub portail" & @CRLF)
		If wait_and_exit_add($autoclose) Then
			Return True
		EndIf
	Endif
	If _Interrupt_Sleep(3000) Then
		Return True
	Endif;

	If close_area() Then ;close
		Return True
	EndIf

	Local $count = 0
	Local $timer = TimerInit()
	Do
	;arena
		If open_arena() Then ;open arena
			Return True
		EndIf

		$color = PixelGetColor($test_pixel_arena_ad_button[0], $test_pixel_arena_ad_button[1])
		ConsoleWrite("Couleur: " & Hex($color, 6) & @CRLF)
		If $color == $add_button_colors[0] or $color==$add_button_colors[1] or $color==$add_button_colors[2] Then ;orange
			MouseClick("primary", $test_pixel_arena_ad_button[0], $test_pixel_arena_ad_button[1], 1)
			ConsoleWrite("bouton orange => pub arène" & $count & @CRLF)
			If wait_and_exit_add($autoclose) Then
				Return True
			EndIf
		Endif
		If _Interrupt_Sleep(3000) Then
			Return True
		Endif;

		If close_area() Then ;close
			Return True
		EndIf

	;bay  $test_pixel_bay_ad_button
		If open_bay() Then
			Return True
		EndIf

		$color = PixelGetColor($test_pixel_bay_ad_button[0], $test_pixel_bay_ad_button[1])
		If $color == $add_button_colors[0] or $color==$add_button_colors[1] or $color==$add_button_colors[2] Then ;orange
			MouseClick("primary", $test_pixel_bay_ad_button[0], $test_pixel_bay_ad_button[1], 1)
			ConsoleWrite("bouton orange => pub baie" & $count & @CRLF)
			If wait_and_exit_add($autoclose) Then
				Return True
			EndIf
		Endif
		If _Interrupt_Sleep(3000) Then
			Return True
		Endif;

		If close_area() Then ;close
			Return True
		EndIf

	;cathedral
		If open_cathedral() Then
			Return True
		EndIf


		$color = PixelGetColor($test_pixel_cathedral_ad_button[0], $test_pixel_cathedral_ad_button[1])
		If $color == $add_button_colors[0] or $color==$add_button_colors[1] or $color==$add_button_colors[2] Then ;orange
			MouseClick("primary", $test_pixel_cathedral_ad_button[0], $test_pixel_cathedral_ad_button[1], 1)
			ConsoleWrite("bouton orange => pub cathedrale" & $count & @CRLF)
			If wait_and_exit_add($autoclose) Then
				Return True
			EndIf
		Endif
		If _Interrupt_Sleep(3000) Then
			Return True
		Endif;

		If close_area() Then ;close
			Return True
		EndIf

	;portal 2 and 3
		If open_portal() Then
			Return True
		EndIf


		$color = PixelGetColor($test_pixel_portal_ad_button[0], $test_pixel_portal_ad_button[1])
		If $color == $add_button_colors[0] or $color==$add_button_colors[1] or $color==$add_button_colors[2] Then ;orange
			MouseClick("primary", $test_pixel_portal_ad_button[0], $test_pixel_portal_ad_button[1], 1)
			ConsoleWrite("bouton orange => pub portail" & $count & @CRLF)
			If wait_and_exit_add($autoclose) Then
				Return True
			EndIf
		Endif
		If _Interrupt_Sleep(3000) Then
			Return True
		Endif;

		If close_area() Then ;close
			Return True
		EndIf


		;move map to the lighthouse
		MouseClickDrag("primary", 900, 120, 35, 120)

		;lighthouse
		If open_lighthouse() Then
			Return True
		EndIf


		$color = PixelGetColor($test_pixel_lighthouse_ad_button[0], $test_pixel_lighthouse_ad_button[1])
		If $color == $add_button_colors[0] or $color==$add_button_colors[1] or $color==$add_button_colors[2] Then ;orange
			MouseClick("primary", $test_pixel_lighthouse_ad_button[0], $test_pixel_lighthouse_ad_button[1], 1)
			ConsoleWrite("bouton orange => pub phare" & @CRLF)
			If wait_and_exit_add($autoclose) Then
				Return True
			EndIf
		Endif
		If _Interrupt_Sleep(3000) Then
			Return True
		Endif;

		If close_area() Then ;close
			Return True
		EndIf

	; journey
		If open_journey() Then
			Return True
		EndIf


		$color = PixelGetColor($test_pixel_journey_ad_button[0], $test_pixel_journey_ad_button[1])
		If $color == $add_button_colors[0] or $color==$add_button_colors[1] or $color==$add_button_colors[2] Then ;orange
			MouseClick("primary", $test_pixel_journey_ad_button[0], $test_pixel_journey_ad_button[1], 1)
			ConsoleWrite("bouton orange => pub excursion" & @CRLF)
			If wait_and_exit_add($autoclose) Then
				Return True
			EndIf
		Endif
		If _Interrupt_Sleep(3000) Then
			Return True
		Endif;

		If close_area() Then ;close
			Return True
		EndIf

	; dungeon
		If open_dungeon() Then
			Return True
		EndIf


		$color = PixelGetColor($test_pixel_dungeon_ad_button[0], $test_pixel_dungeon_ad_button[1])
		If $color == $add_button_colors[0] or $color==$add_button_colors[1] or $color==$add_button_colors[2] Then ;orange
			MouseClick("primary", $test_pixel_dungeon_ad_button[0], $test_pixel_dungeon_ad_button[1], 1)
			ConsoleWrite("bouton orange => pub donjon" & $count & @CRLF)
			If wait_and_exit_add($autoclose) Then
				Return True
			EndIf
		Endif
		If _Interrupt_Sleep(3000) Then
			Return True
		Endif;

		If close_area() Then ;close
			Return True
		EndIf

		;move map back
		MouseClickDrag("primary", 35, 120, 900, 120)


		$count = $count + 1
	Until $count == 3 or TimerDiff($timer) > 600000  ; 10 minutes should be enough


;portal 4
	If open_portal() Then
		Return True
	EndIf


	$color = PixelGetColor($test_pixel_portal_ad_button[0], $test_pixel_portal_ad_button[1])
	If $color == $add_button_colors[0] or $color==$add_button_colors[1] or $color==$add_button_colors[2] Then ;orange
		MouseClick("primary", $test_pixel_portal_ad_button[0], $test_pixel_portal_ad_button[1], 1)
		ConsoleWrite("bouton orange => pub portail" & @CRLF)
		If wait_and_exit_add($autoclose) Then
			Return True
		EndIf
	Endif
	If _Interrupt_Sleep(3000) Then
		Return True
	Endif;

	If close_area() Then ;close
		Return True
	EndIf

	If back_to_castle() Then
		Return True
	EndIf

	Return false
EndFunc


Func ResourceAds($autoclose)

	If initBlueStacks() Then
		_Function_Finished()
		Return
	EndIf

	If back_to_castle() Then
		_Function_Finished()
		Return
	EndIf
	If resource_ads($autoclose) Then
		_Function_Finished()
		Return
	EndIf
	_Function_Finished()
EndFunc


Func resource_ads($autoclose)
	ConsoleWrite("pubs ressources"& @CRLF)

	If open_resource_ad($mithril_resource_coords, $autoclose) Then
		Return True
	Endif

	If open_resource_ad($gold_resource_coords, $autoclose) Then
		Return True
	Endif

	If open_resource_ad($apple_resource_coords, $autoclose) Then
		Return True
	Endif

	If open_resource_ad($mana_resource_coords, $autoclose) Then
		Return True
	Endif

	If open_resource_ad($wood_resource_coords, $autoclose) Then
		Return True
	Endif

	If open_resource_ad($iron_resource_coords, $autoclose) Then
		Return True
	Endif

	Return false
EndFunc

Func open_resource_ad($coords, $autoclose)
	;open
	MouseClick("primary", $coords[0], $coords[1], 1)
	If _Interrupt_Sleep(1000) Then
		Return True
	Endif
	$color = PixelGetColor($coords[2], $coords[3])
	If $color == $add_button_colors[0] or $color==$add_button_colors[1] or $color==$add_button_colors[2] Then ;orange
		MouseClick("primary", $coords[2], $coords[3], 1)
		;ConsoleWrite("orange => pub"& @CRLF)
		If wait_and_exit_add($autoclose) Then
			Return True
		EndIf
	EndIf
	;close
	MouseClick("primary", $coords[0], $coords[1], 1)
	Return False
EndFunc

;pubs journalières
Func open_daily_ads()
	MouseClick("primary", $quests_button[0], $quests_button[1], 1)
	If _Interrupt_Sleep(2000) Then
		Return True
	Endif

	MouseClick("primary", $daily_quests_button[0], $daily_quests_button[1], 1)
	Sleep(1000)

	MouseClick("primary", $first_daily_quest_button[0], $first_daily_quest_button[1], 1)
	Sleep(1000)
EndFunc




Func wait_and_exit_add($autoclose)
	;TODO tant que le centre est gris (chargement) on attend.

	Local $duration = 0
	$adDuration = IniRead(@ScriptDir & "/H2C2.ini", "ads", "duration", "40s")
	If $adDuration == "40s" Then
		$duration = 40000
	ElseIf $adDuration == "20s" Then
		$duration = 20000
	ElseIf $adDuration == "1min" Then
		$duration = 60000
	ElseIf $adDuration == "1min30" Then
		$duration = 90000
	Else
		$duration = 32000
	EndIf
	Do
		$color1 = PixelGetColor(869, 862)
		ConsoleWrite("bg color 0x"&hex($color1, 6) & @CRLF)
		If _Interrupt_Sleep(1000) Then
			Return True
		Endif;
	Until $color1 = $ad_background_color[0] or $color1 = $ad_background_color[1] or $color1 = $ad_background_color[2]

	;If _Interrupt_Sleep(10000) Then
	;	Return True
	;Endif;
	; type de pub sans timer
	;$color1 = PixelGetColor($square_add[0], $square_add[1])
	;MouseMove($square_add[0], $square_add[1])
	;ConsoleWrite("bg color 0x"&hex($color1, 6) & @CRLF)
	;$color2 = PixelGetColor($square_add[2], $square_add[3])
	;MouseMove($square_add[2], $square_add[3])
	;$color3 = PixelGetColor($add_open_button[0], $add_open_button[1])
	;MouseMove($add_open_button[0], $add_open_button[1])
	;ConsoleWrite("btn color 0x"&hex($color3, 6) & @CRLF)

	;If $color1 == $square_add[4] And $color2 == $square_add[4] And $color3 = $add_open_button[2] Then
	;	MouseClick("primary", $add_close_button[0], $add_close_button[1], 1)
	;	Return False
	;Endif

	;mini pub
	;$color1 = PixelGetColor($tiny_add[0], $tiny_add[1])
	;MouseMove($tiny_add[0], $tiny_add[1])
	;$color2 = PixelGetColor($square_add[2], $square_add[3])
	;MouseMove($tiny_add[2], $tiny_add[3])

	;If $color1 == $tiny_add[2] And $color2 == $tiny_add[2] Then
	;	MouseClick("primary", $tiny_close_button[0], $tiny_close_button[1], 1)
	;	Return False
	;Endif

	;$color1 = PixelGetColor($add_with_sound_button[0], $add_with_sound_button[1])
	;MouseMove($add_with_sound_button[0], $add_with_sound_button[1])


	;If $color1 == $add_with_sound_button[2] Then ;blue. Want sound? No but no choice...
	;	MouseClick("primary", $add_with_sound_button[0], $add_with_sound_button[1], 1)
	;Endif

	;todo gérer les pubs plein écran

	; sinon pub pc standard
	If _Interrupt_Sleep($duration) Then
		Return True
	Endif;
	If $autoclose == "0" Then
		MouseClick("primary", 1495, 68, 1) ;pubs plein écran
		MouseClick("primary", 1203, 229, 1) ; pubs standard
		If _Interrupt_Sleep(2000) Then
			Return True
		Endif;
		ConsoleWrite ("demande confirmation ne fonctionne pas sur PC, pour l'instant" & @CRLF)
		If PixelGetColor($test_pixel_confirm_close_vid[0], $test_pixel_confirm_close_vid[1]) == $test_pixel_confirm_close_vid[2] Then
			ConsoleWrite("Demande confirmation pour faire peur")
			MouseClick("primary", $confirm_close_button[0], $confirm_close_button[1], 1)
			If _Interrupt_Sleep(2000) Then
				Return True
			Endif;
		EndIf
	EndIf

	Return False
EndFunc


Func FreeResources()
		If initBlueStacks() Then
		_Function_Finished()
		Return
	EndIf

	; va au chateau
	If back_to_castle() Then
		_Function_Finished()
		Return
	EndIf

	If open_map(True) Then ;open map and drag
		_Function_Finished()
		Return
	EndIf

	If open_arena() Then ;open arena
		_Function_Finished()
		Return
	EndIf

	MouseClick("primary", $arena_shop[0], $arena_shop[1], 1)
	If _Interrupt_Sleep(1500) Then
		_Function_Finished()
		return
	Endif

	MouseClick("primary", $arena_freeres[0], $arena_freeres[1], 1)
	If _Interrupt_Sleep(1500) Then
		_Function_Finished()
		return
	Endif

	If close_area() Then ;close arena
		_Function_Finished()
		Return
	EndIf



	If open_bay() Then ;open bay
		_Function_Finished()
		Return
	EndIf


	MouseClick("primary", $bay_shop[0], $bay_shop[1], 1)
	If _Interrupt_Sleep(1500) Then
		_Function_Finished()
		return
	Endif

	MouseClick("primary", $bay_freeres[0], $bay_freeres[1], 1)
	If _Interrupt_Sleep(1500) Then
		_Function_Finished()
		return
	Endif

	If close_area() Then ;close bay
		_Function_Finished()
		Return
	EndIf


	;todo cathedral
	If open_cathedral() Then ;open cathedral
		_Function_Finished()
		Return
	EndIf


	MouseClick("primary", $cathedral_shop[0], $cathedral_shop[1], 1)
	If _Interrupt_Sleep(1500) Then
		_Function_Finished()
		return
	Endif

	MouseClick("primary", $cathedral_freeres[0], $cathedral_freeres[1], 1)
	If _Interrupt_Sleep(1500) Then
		_Function_Finished()
		return
	Endif

	If close_area() Then ;close bay
		_Function_Finished()
		Return
	EndIf


	;only if portal is open

	If open_portal() Then ;open portal
		_Function_Finished()
		Return
	EndIf

	$color = PixelGetColor($portal_closed_panel[0], $portal_closed_panel[1])
	If $color <> $portal_closed_panel[2] Then

		MouseClick("primary", $portal_shop[0], $portal_shop[1], 1)
		If _Interrupt_Sleep(1500) Then
			_Function_Finished()
			return
		Endif

		MouseClick("primary", $portal_freeres[0], $portal_freeres[1], 1)
		If _Interrupt_Sleep(1500) Then
			_Function_Finished()
			return
		Endif
	Endif

	If close_area() Then ;close portal
		_Function_Finished()
		Return
	EndIf


	;move map to the lighthouse
	MouseClickDrag("primary", 900, 120, 35, 120)

	If open_lighthouse() Then ;open portal
		_Function_Finished()
		Return
	EndIf

	MouseClick("primary", $lighthouse_shop[0], $lighthouse_shop[1], 1)
	If _Interrupt_Sleep(1500) Then
		_Function_Finished()
		return
	Endif

	MouseClick("primary", $lighthouse_freeres[0], $lighthouse_freeres[1], 1)
	If _Interrupt_Sleep(1500) Then
		_Function_Finished()
		return
	Endif

	If close_area() Then ;close shop
		_Function_Finished()
		Return
	EndIf

	If close_area() Then ;close lighthouse
		_Function_Finished()
		Return
	EndIf



	If open_dungeon() Then ;open dungeon
		_Function_Finished()
		Return
	EndIf

	MouseClick("primary", $dungeon_shop[0], $dungeon_shop[1], 1)
	If _Interrupt_Sleep(1500) Then
		_Function_Finished()
		return
	Endif

	MouseClick("primary", $dungeon_freeres[0], $dungeon_freeres[1], 1)
	If _Interrupt_Sleep(1500) Then
		_Function_Finished()
		return
	Endif

	If close_area() Then ;close dungeon
		_Function_Finished()
		Return
	EndIf

	;move map back
	MouseClickDrag("primary", 35, 120, 900, 120)

	_Function_Finished()

EndFunc
; attack all the castles on the map
#include-once
#include "data/hc_coords.au3"


Func PvP()

	If initBlueStacks() Then
		_Function_Finished()
		Return
	EndIf

	Local $count = 10 ; (10 attacks)
	Do

		If _Interrupt_Sleep(1000) Then
			_Function_Finished()
			return
		Endif

		;clique sur le bouton carte
		MouseClick("primary", 170, 810, 1)	;carte
		If _Interrupt_Sleep(2000) Then
			_Function_Finished()
			return
		Endif;

		;recherche l'image ennemi dispo
		;clique si trouv�
		Local $y = 0, $x = 0, $tolerance = 10
		;Do
		;	$search = _ImageSearch('map_pvp_icon.png', 0, $x, $y, $tolerance)
		;	$tolerance = $tolerance + 20
		;Until $search = 1 or $tolerance > 200
		;If $search = 1 Then
		;	MouseClick("primary", $x, $y, 1)
		;Else
			;GUICtrlSetData($label, "image non trouv�e")
			_Function_Finished()
			return
		;EndIf

		If _Interrupt_Sleep(1500) Then
			_Function_Finished()
			return
		Endif;

		;recherche l'image chateau ennemi
		;Do
		;	$search = _ImageSearch('enemy_castle.png', 0, $x, $y, $tolerance)
		;	$tolerance = $tolerance + 20
		;Until $search = 1 or $tolerance > 200
		;If $search = 1 Then
		;	MouseClick("primary", $x, $y, 1)
		;Else
			;GUICtrlSetData($label, "image non trouv�e")
			_Function_Finished()
			return
		;EndIf



		If _Interrupt_Sleep(1500) Then
			_Function_Finished()
			return
		Endif;

		;recherche l'image attaque instantan�e
		;Do
		;	$search = _ImageSearch('instant_attack.png', 0, $x, $y, $tolerance)
		;	$tolerance = $tolerance + 20
		;Until $search = 1 or $tolerance > 200
		;If $search = 1 Then
		;	MouseClick("primary", $x, $y, 1)
		;Else
			;GUICtrlSetData($label, "image non trouv�e")
			_Function_Finished()
			return
		;EndIf

		; si �a affiche un diamant, on sort
		If PixelGetColor($red_button_no_diamond[0], $red_button_no_diamond[1]) == $red_button_no_diamond[2] Then
			If is_diamond_button() Then
				ConsoleWrite (@TAB & "Plus de pommes" & @CRLF)
				_Function_Finished()
				return
			EndIf
		EndIf

		;passe le "vos combattants sont cass�s"
		MouseClick("primary", $invasion_no_health_button[0], $invasion_no_health_button[1] , 1) ; ouioui, ils vont gagner de toute fa�on
		ConsoleWrite (@TAB & "continue si combattants cass�s" & @CRLF)
		If _Interrupt_Sleep(1500) Then
			_Function_Finished()
			return
		Endif;

		;passe le "ya pas de sorts"
		MouseClick("primary", $invasion_no_spell_button[0], $invasion_no_spell_button[1] , 1) ; ouioui, ils vont gagner de toute fa�on
		ConsoleWrite (@TAB & "continue si pas de sort" & @CRLF)

		If _Interrupt_Sleep(500) Then
			_Function_Finished()
			return
		Endif;

		If fight(True, False) Then
			_Function_Finished
			return
		EndIf

		ConsoleWrite (@TAB & @TAB & "Fin de l'attaque" & @CRLF)
		If end_fight(False) Then
			_Function_Finished
			return
		EndIf

		If _Interrupt_Sleep(6000) Then
			_Function_Finished()
			return
		Endif

		$count = $count - 1;
	Until $count <=0
EndFunc
#include-once
#include "data/hc_coords.au3"

;TODO avant de lancer le script : positionner le niveau à jouer en bas de la fenêtre

Global Const $portal_level[2] = [776, 712]
Global Const $portal_attack[2] = [1016, 791]
Global Const $portal_quickFight[2] = [806, 796]
Global Const $portal_confirm_fighters[2] = [637, 598] ; common with others
Global Const $portal_end_attack[5] = [561, 845, 0xF69207, 0xF79208, 0xF49205] ; range mesuré : 0x4CBB0C ; 0xEC8C06 ; 0xEA8C05

Global $isQuickFight

Func Portal()

	If initBlueStacks() Then
		_Function_Finished()
		Return
	EndIf

	Local $count = 50
	Do
		If _portal_fight($portal_level[0], $portal_level[1]) Then
			_Function_Finished()
			return
		Endif;


		$count = $count - 1;
	Until $count <=0

	_Function_Finished()
EndFunc

Func PortalChestLevels()
	Global $hwnd

	If initBlueStacks() Then
		_Function_Finished()
		Return
	EndIf

	Local Const $chest = _OpenCV_imread_and_check(_OpenCV_FindFile("portal_chest.png"))
	Local Const $tower = _OpenCV_imread_and_check(_OpenCV_FindFile("portal_tower.png"))
	Local Const $threshold = 0.8
	;cherche un coffre ou une tour. Si trouve, attaque.  scrolle vers le bas.

	For $i = 0 to 40  ; scroll de 2 niveaux environ. 80-> 1
		;opencv screenshot
		Local $aRect = WinGetPos($hwnd)
		;Local $aRect[4] = [5, 12, 1539, 910] ;todo prendre les mesures de la fenêtre
		Local $img = _OpenCV_GetDesktopScreenMat($aRect)
		;find match fight
		Local $cMatches = _OpenCV_FindTemplate($img, $chest, $threshold)
		Local $tMatches = _OpenCV_FindTemplate($img, $tower, $threshold)

		If UBound($cMatches) Then
			ConsoleWrite("Found a chest" & @CRLF)
			$x = $cMatches[0][0]
			$y = $cMatches[0][1] - 20	; taille de l'icône voir si ça colle

			If _portal_fight($x, $y) Then
				_Function_Finished()
				return
			Endif
		ElseIf UBound($tMatches) Then
			ConsoleWrite("Found a tower" & @CRLF)
			$x = $tMatches[0][0]
			$y = $tMatches[0][1] - 20	; taille de l'icône voir si ça colle

			If _portal_fight($x, $y) Then
				_Function_Finished()
				return
			Endif
		EndIf

		MouseClickDrag("primary", 150, 700, 150, 100)

		If _Interrupt_Sleep(1000) Then
			_Function_Finished()
			Return
		Endif
	Next
	_Function_Finished()
EndFunc

Func _portal_fight($x, $y)
	;clic sur le niveau de portail
	If _Interrupt_Sleep(2000) Then
		Return True
	Endif
	MouseClick("primary", $x, $y , 1) ; niveau de portail en bas de la scroll
	;ConsoleWrite ("lance le niveau" & @CRLF)
	If _Interrupt_Sleep(2000) Then
		Return True
	Endif

  ; à l'attaque
	If $isQuickFight Then
		MouseClick("primary", $portal_quickFight[0], $portal_quickFight[1] , 1)
	Else
		MouseClick("primary", $portal_attack[0], $portal_attack[1] , 1)
	Endif
	;ConsoleWrite ("bouton attaque" & @CRLF)
	If _Interrupt_Sleep(1000) Then
		Return True
	Endif
	; si ça affiche un diamant, on sort
	$color = PixelGetColor($red_button_no_diamond[0], $red_button_no_diamond[1])
	If $color == $red_button_no_diamond[2] or $color == $red_button_no_diamond[3] or $color == $red_button_no_diamond[4] Then
		If is_diamond_button() Then
			ConsoleWrite (@TAB & "Plus de pommes" & @CRLF)
			Return True
		EndIf
	EndIf
	;passe le "vos combattants sont cassés
	MouseClick("primary", $portal_confirm_fighters[0], $portal_confirm_fighters[1], 1) ; ouioui, ils vont gagner de toute façon

	;ConsoleWrite ("bouton attaque" & @CRLF)
	If _Interrupt_Sleep(1000) Then
		Return True
	Endif

	; si ça affiche un diamant, on sort
	$color = PixelGetColor($red_button_no_diamond[0], $red_button_no_diamond[1])
	If $color == $red_button_no_diamond[2] or $color == $red_button_no_diamond[3] or $color == $red_button_no_diamond[4] Then
		If is_diamond_button() Then
			ConsoleWrite (@TAB & "Plus de pommes" & @CRLF)
			Return True
		EndIf
	EndIf

	;attend le bouton orange
	;on attend la fin de l attaque
	$color1 = PixelGetColor($portal_end_attack[0], $portal_end_attack[1])
	$color2 = PixelGetColor($end_fight_button[0], $end_fight_button[1])
	While(not ($color1 == $portal_end_attack[2] or $color1 == $portal_end_attack[3] or $color1 == $portal_end_attack[4]) and not($color2 == $end_fight_button[2] or $color2 == $end_fight_button[3] or $color2 == $end_fight_button[4] or $color2 == $end_fight_button[5] or $color2 == $end_fight_button[6]))
		MouseClick("primary", $hero_spell[0],  $hero_spell[1], 1)
		If _Interrupt_Sleep(1000) Then
			Return True
		Endif;
		$color1 = PixelGetColor($portal_end_attack[0], $portal_end_attack[1])
		$color2 = PixelGetColor($end_fight_button[0], $end_fight_button[1])
	WEnd

	ConsoleWrite ("Fin du niveau" & @CRLF)
	MouseClick("primary", $portal_end_attack[0], $portal_end_attack[1], 1)
	If _Interrupt_Sleep(500) Then
		Return True
	Endif;
	;attend
	If _wait_portal_screen() Then
		Return True
	Endif

	;si on est en event runes, des runes peuvent s'afficher.
	;timer de 2 secondes, puis à nouveau wait (qui contient un timer d'1 seconde)
	$isRuneEvent = (IniRead(@ScriptDir & "/H2C2.ini", "params", "runeevent", "0") == "1")
	If $isRuneEvent Then
		If _Interrupt_Sleep(2000) Then
			Return True
		Endif
		If _wait_portal_screen() Then
			Return True
		Endif
	EndIf

	Return False
EndFunc

Func _wait_portal_screen()

	Local Const $gobelin = _OpenCV_imread_and_check(_OpenCV_FindFile("portal_gobelin.png"))
	Local Const $threshold = 0.8
	Local $aRect = WinGetPos($hwnd)
	Local $iBegin = TimerInit()
	Do
		If _Interrupt_Sleep(1000) Then
			Return True
		Endif


		MouseClick("primary", 1289, 490, 1)

		;Local $aRect[4] = [5, 12, 1539, 910] ;todo prendre les mesures de la fenêtre
		Local $img = _OpenCV_GetDesktopScreenMat($aRect)
		;find match fight
		Local $aMatches = _OpenCV_FindTemplate($img, $gobelin, $threshold)

		If UBound($aMatches) Then
			ConsoleWrite("Found a gobelin => portal" & @CRLF)
			$x = $aMatches[0][0]
			$y = $aMatches[0][1] - 70	; taille de l'icône voir si ça colle

			Return False

		EndIf
	Until TimerDiff($iBegin) > 60000 ; 1 minute devrait suffire

	Return False
EndFunc
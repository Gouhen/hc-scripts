#include-once

#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Icon=data/robot_drive.ico
#AutoIt3Wrapper_UseX64=Y
#AutoIt3Wrapper_Outfile_x64=build\H2C2.exe
#AutoIt3Wrapper_Res_Description=A bot for HC
#AutoIt3Wrapper_Res_Fileversion=0.1.4.8
#AutoIt3Wrapper_Res_ProductName=H2C2
#AutoIt3Wrapper_Run_After=C:\Progra~1\7-Zip\7z.exe a "%scriptdir%\build\%scriptfile%-%fileversion%.zip" "%outx64%"
;#AutoIt3Wrapper_Run_After=curl -s -u Gouhen https://api.bitbucket.org/2.0/repositories/Gouhen/hc-scripts/downloads -F files=@%scriptdir%\build\%scriptfile%-%fileversion%.zip
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****

#include <AutoItConstants.au3>
#include <GUIConstants.au3>
#include <GUIConstantsEx.au3>
#include <StaticConstants.au3>
#include <WindowsConstants.au3>
#include <GUIComboBox.au3>
#include <TabConstants.au3>

#include "ad.au3"
#include "invasion.au3"
#include "portal.au3"
#include "lighthouse.au3"
#include "clanboss.au3"
#include "tournament_10.au3"
#include "bay.au3"
#include "pvp.au3"
#include "dungeon.au3"
#include "cathedral.au3"
#include "titan.au3"
#include "pioche.au3"
#include "treasure_hunt.au3"

#include "eventad-runes.au3"
#include "island.au3"

#include "discord.au3"

#include "udf\autoit-opencv\opencv_udf_utils.au3"
#include "ocr.au3"


#include "hc_actions.au3"

; Enable logs
Global $logging = True ; File loging "file.log"
Global $log_level = 0 ; 0=None, 1=Error, 2=Info, 3=Trace
Global $cath_log_enable = False
Global $dungeon_log_enable = False
Global $log_level_CB

Global $GUI1
Global $Lb1
Global $Btn1
Global $btnBay
Global $btnContinueBay
Global $btnArena
Global $btnAds
Global $btnMapAds
Global $btnResourceAds
Global $btnAllAds
Global $cmbAdsDuration
Global $btnFreeRes
Global $chbAutocloseAds
Global $btnPortal
Global $btnPortalChests
Global $btnLighthouse
Global $btnTitanTower
Global $btnTitanColiseum
Global $chbTitanBest
Global $btnCathedral
Global $btnCathedralChestLoop
Global $cmbCathedralLevel
Global $cmbCathedralPay
Global $chbCathedralDangerousGame
Global $chbCathedralBossesOnly
Global $cmbCathedralNbToRez
Global $edtCathedralCount
Global $cmbCathedralSpeed
Global $edtChestBuyNumber
Global $btnTournament10
Global $btnContinueTournament10
Global $cmbTournament10
Global $cmbTournamentPay
Global $chbTournamentGetReward
Global $edtTournamentCount
Global $btnInvasions
Global $btnRuneAdds
Global $btnIsland
Global $btnClanBoss
Global $cmbClanBoss
Global $label
Global $edtXCoord
Global $edtYCoord
Global $btnGetColor
Global $edtGetColor
Global $btnTest
Global $btnDungeonM
Global $btnDungeonS
Global $btnDungeonBoss
Global $btnDungeonLoop
Global $edtDungeonCount
Global $chbRuneEvent
Global $chbQuickFight
Global $cmbDungeonPay
Global $chbDungeonRandomRestart
Global $btnEventExcavationAuto
Global $chbDiscordEnableCapcha
Global $chbDiscordEnableNotif
Global $edtDiscordUserId
Global $btnDiscordUserIdTest

Global $chbLogCathe
Global $chbLogDungeon
Global $cmbLogLevel

Global $btnTresureHuntRun
Global $edtTresureMaxLoupePerLevel
Global $cmbTresureMinBoxPerLoupe
Global $edtTresureStopMaxGold
Global $edtTresureStopMaxApple
Global $edtTresureStopMaxMana
Global $chbTresureRwdApple
Global $chbTresureRwdGold
Global $chbTresureRwdMana
Global $chbTresureRwdFragHero
Global $chbTresureRwdDust
Global $chbTresureRwdBook
Global $chbTresureRwdLoupe
Global $chbTresureRwdDiams
Global $chbTresureRwdTickets

Global $hScriptRuntime

Global $pass = 0;

Global $enablePeriodicCapchaTest = True
Global $isQuickFight
Global $discordUserId
Global $discordEnableCapcha
Global $discordEnableNotif
Global $discordScriptNotif

Global $capcha_img

; Delete log file, if any
FileDelete(@ScriptDir & '\file.log')

Opt("GUIOnEventMode", 1)

If Not FileExists(@ScriptDir & "22296.mp3") Then
	FileInstall("data\22296.mp3", @ScriptDir & "\22296.mp3")
	FileInstall("data\levelup2.mp3", @ScriptDir & "\levelup2.mp3")
	FileInstall("captcha.png", @ScriptDir & "\captcha.png")
	FileInstall("invasion.png", @ScriptDir & "\invasion.png")
	FileInstall("invasion2.png", @ScriptDir & "\invasion2.png")
	FileInstall("enemy_castle.png", @ScriptDir & "\enemy_castle.png")
	FileInstall("map_pvp_icon.png", @ScriptDir & "\map_pvp_icon.png")
	FileInstall("instant_attack.png", @ScriptDir & "\instant_attack.png")
	FileInstall("castle_map_button.png", @ScriptDir & "\castle_map_button.png")
	FileInstall("castle_ct_button.png", @ScriptDir & "\castle_ct_button.png")
	FileInstall("map_castle_button.png", @ScriptDir & "\map_castle_button.png")
	FileInstall("launcher_close_game_icon.png", @ScriptDir & "\launcher_close_game_icon.png")

	FileInstall("empty_fighter.png", @ScriptDir & "\empty_fighter.png")

	FileInstall("tournament_donald.png", @ScriptDir & "\tournament_donald.png")
	FileInstall("tournament_skycastle.png", @ScriptDir & "\tournament_skycastle.png")

	FileInstall("bay_parrot.png", @ScriptDir & "\bay_parrot.png")
	FileInstall("bay_chest.png", @ScriptDir & "\bay_chest.png")

	FileInstall("cathedral_master.png", @ScriptDir & "\cathedral_master.png")
	FileInstall("cathedral_part.png", @ScriptDir & "\cathedral_part.png")
	FileInstall("cathedral_boss.png", @ScriptDir & "\cathedral_boss.png")
	FileInstall("cathedral_camp.png", @ScriptDir & "\cathedral_camp.png")
	FileInstall("cathedral_dice.png", @ScriptDir & "\cathedral_dice.png")
	FileInstall("cathedral_fight.png", @ScriptDir & "\cathedral_fight.png")
	FileInstall("cathedral_robe.png", @ScriptDir & "\cathedral_robe.png")
	FileInstall("cathedral_fire.png", @ScriptDir & "\cathedral_fire.png")
	FileInstall("cathedral_guardian.png", @ScriptDir & "\cathedral_guardian.png")
	FileInstall("cathedral_chest_leg.png", @ScriptDir & "\cathedral_chest_leg.png")
	FileInstall("cathedral_miss_fighter.png", @ScriptDir & "\cathedral_miss_fighter.png")
	FileInstall("cathe_boss_unrevealed.png", @ScriptDir & "\cathe_boss_unrevealed.png")

	FileInstall("cathedral_camp_menu.png", @ScriptDir & "\cathedral_camp_menu.png")
	FileInstall("cathedral_camp_menu2.png", @ScriptDir & "\cathedral_camp_menu2.png")
	FileInstall("cathedral_choice_menu.png", @ScriptDir & "\cathedral_choice_menu.png")
	FileInstall("cathedral_choice_menu2.png", @ScriptDir & "\cathedral_choice_menu2.png")
	FileInstall("cathedral_sacrifice_menu.png", @ScriptDir & "\cathedral_sacrifice_menu.png")
	FileInstall("cathedral_sacrifice_menu2.png", @ScriptDir & "\cathedral_sacrifice_menu2.png")
	FileInstall("cathedral_clone_menu.png", @ScriptDir & "\cathedral_clone_menu.png")
	FileInstall("cathedral_clone_menu2.png", @ScriptDir & "\cathedral_clone_menu2.png")
	FileInstall("cathedral_fight_menu.png", @ScriptDir & "\cathedral_fight_menu.png")
	FileInstall("cathedral_fight_menu2.png", @ScriptDir & "\cathedral_fight_menu2.png")

	FileInstall("cathedral_blessing_selected.png", @ScriptDir & "\cathedral_blessing_selected.png")
	FileInstall("cathedral_blessing_menu.png", @ScriptDir & "\cathedral_blessing_menu.png")

	FileInstall("cathedral_manuscripts_big_chest.png", @ScriptDir & "\cathedral_manuscripts_big_chest.png")

	FileInstall("portal_chest.png", @ScriptDir & "\portal_chest.png")
	FileInstall("portal_tower.png", @ScriptDir & "\portal_tower.png")
	FileInstall("portal_gobelin.png", @ScriptDir & "\portal_gobelin.png")

	FileInstall("lighthouse_cloud.png", @ScriptDir & "\lighthouse_cloud.png")

	FileInstall("titan_tower_top.png", @ScriptDir & "\titan_tower_top.png")
	FileInstall("titan_tower_easy.png", @ScriptDir & "\titan_tower_easy.png")
	FileInstall("titan_tower_activity.png", @ScriptDir & "\titan_tower_activity.png")
	FileInstall("titan_coliseum_top.png", @ScriptDir & "\titan_coliseum_top.png")

	FileInstall("boss_smasher.png", @ScriptDir & "\boss_smasher.png")
	FileInstall("boss_pumba.png", @ScriptDir & "\boss_pumba.png")
	FileInstall("boss_troll.png", @ScriptDir & "\boss_troll.png")
	FileInstall("boss_sporo.png", @ScriptDir & "\boss_sporo.png")



	FileInstall("dungeon_fight.png", @ScriptDir & "\dungeon_fight.png")
	FileInstall("dungeon_boss.png", @ScriptDir & "\dungeon_boss.png")
	FileInstall("dungeon_boss 2.png", @ScriptDir & "\dungeon_boss 2.png")
	FileInstall("dungeon_boss 3.png", @ScriptDir & "\dungeon_boss 3.png")
	FileInstall("dungeon_mortal.png", @ScriptDir & "\dungeon_mortal.png")
	FileInstall("dungeon_mortal3.png", @ScriptDir & "\dungeon_mortal3.png")
	FileInstall("dungeon_sacrifice.png", @ScriptDir & "\dungeon_sacrifice.png")
	FileInstall("dungeon_invisible.png", @ScriptDir & "\dungeon_invisible.png")
	FileInstall("dungeon_shadow.png", @ScriptDir & "\dungeon_shadow.png")
	FileInstall("dungeon_shadow 2.png", @ScriptDir & "\dungeon_shadow 2.png")
	FileInstall("dungeon_special_reward.png", @ScriptDir & "\dungeon_special_reward.png")
	FileInstall("dungeon_select.png", @ScriptDir & "\dungeon_select.png")
	FileInstall("dungeon_select2.png", @ScriptDir & "\dungeon_select2.png")
	FileInstall("dungeon_select_right.png", @ScriptDir & "\dungeon_select_right.png")
	FileInstall("dungeon_warriorscreen.png", @ScriptDir & "\dungeon_warriorscreen.png")
	FileInstall("dungeon_leader.png", @ScriptDir & "\dungeon_leader.png")
	FileInstall("dungeon_follower.png", @ScriptDir & "\dungeon_follower.png")
	FileInstall("dungeon_seven_boss.png", @ScriptDir & "\dungeon_seven_boss.png")
	FileInstall("dungeon_sacrifice_detection.png", @ScriptDir & "\dungeon_sacrifice_detection.png")
	FileInstall("dungeon_fight_detection.png", @ScriptDir & "\dungeon_fight_detection.png")
	FileInstall("dungeon_choice_detection.png", @ScriptDir & "\dungeon_choice_detection.png")
	FileInstall("dungeon_mortal_window_detection.png", @ScriptDir & "\dungeon_mortal_window_detection.png")
	FileInstall("dungeon_fight_menu_detection.png", @ScriptDir & "\dungeon_fight_menu_detection.png")
	FileInstall("dungeon_ready_button.png", @ScriptDir & "\dungeon_ready_button.png")
	FileInstall("dungeon_rare_reward.png", @ScriptDir & "\dungeon_rare_reward.png")
	FileInstall("dungeon_menu_detect.png", @ScriptDir & "\dungeon_menu_detect.png")

	FileInstall("dungeon_restart_invite_to_send.png", @ScriptDir & "\dungeon_restart_invite_to_send.png")
	FileInstall("dungeon_restart_invite_sent.png", @ScriptDir & "\dungeon_restart_invite_sent.png")
	FileInstall("dungeon_restart_both_ready.png", @ScriptDir & "\dungeon_restart_both_ready.png")

	FileInstall("pioche_box_collected.PNG", @ScriptDir & "\pioche_box_collected.PNG")
	FileInstall("pioche_box_reward_collected.PNG", @ScriptDir & "\pioche_box_reward_collected.PNG")
	FileInstall("pioche_reward.PNG", @ScriptDir & "\pioche_reward.PNG")
	FileInstall("pioche_reward_color.PNG", @ScriptDir & "\pioche_reward_color.PNG")
	FileInstall("pioche_box2hit_1left.PNG", @ScriptDir & "\pioche_box2hit_1left.PNG")
	FileInstall("pioche_missing.PNG", @ScriptDir & "\pioche_missing.PNG")

	FileInstall("treasure_box_unrevealed.PNG", @ScriptDir & "\treasure_box_unrevealed.PNG")
	FileInstall("treasure_box_revealed.PNG", @ScriptDir & "\treasure_box_revealed.PNG")
	FileInstall("treasure_loupe_arrow_vertical.PNG", @ScriptDir & "\treasure_loupe_arrow_vertical.PNG")
	FileInstall("treasure_loupe_arrow_horizontal.PNG", @ScriptDir & "\treasure_loupe_arrow_horizontal.PNG")
	FileInstall("treasure_loupe_validate.PNG", @ScriptDir & "\treasure_loupe_validate.PNG")
	FileInstall("treasure_loupe_cancel.PNG", @ScriptDir & "\treasure_loupe_cancel.PNG")

	FileInstall("treasure_special_reward.PNG", @ScriptDir & "\treasure_special_reward.PNG")
	FileInstall("treasure_special_reward_collected.PNG", @ScriptDir & "\treasure_special_reward_collected.PNG")
	FileInstall("treasure_apples.PNG", @ScriptDir & "\treasure_apples.PNG")
	FileInstall("treasure_apples_collected.PNG", @ScriptDir & "\treasure_apples_collected.PNG")
	FileInstall("treasure_gold.PNG", @ScriptDir & "\treasure_gold.PNG")
	FileInstall("treasure_gold_collected.PNG", @ScriptDir & "\treasure_gold_collected.PNG")
	FileInstall("treasure_mana.PNG", @ScriptDir & "\treasure_mana.PNG")
	FileInstall("treasure_mana_collected.PNG", @ScriptDir & "\treasure_mana_collected.PNG")
	FileInstall("treasure_frag_dj.PNG", @ScriptDir & "\treasure_frag_dj.PNG")
	FileInstall("treasure_book.PNG", @ScriptDir & "\treasure_book.PNG")
	FileInstall("treasure_hammer.PNG", @ScriptDir & "\treasure_hammer.PNG")
	FileInstall("treasure_loupe.PNG", @ScriptDir & "\treasure_loupe.PNG")
	FileInstall("treasure_diams.PNG", @ScriptDir & "\treasure_diams.PNG")
	FileInstall("treasure_dust_blue.PNG", @ScriptDir & "\treasure_dust_blue.PNG")
	FileInstall("treasure_dust_green.PNG", @ScriptDir & "\treasure_dust_green.PNG")
	FileInstall("treasure_tickets.PNG", @ScriptDir & "\treasure_tickets.PNG")
	FileInstall("treasure_event_reward.PNG", @ScriptDir & "\treasure_event_reward.PNG")
	FileInstall("treasure_event_reward_collected.PNG", @ScriptDir & "\treasure_event_reward_collected.PNG")
	FileInstall("treasure_reward_confirm_button.PNG", @ScriptDir & "\treasure_reward_confirm_button.PNG")
	FileInstall("treasure_no_pass.PNG", @ScriptDir & "\treasure_no_pass.PNG")
	FileInstall("treasure_silver_pass.PNG", @ScriptDir & "\treasure_silver_pass.PNG")

	FileInstall("udf\autoit-opencv\autoit_addon460.dll", @ScriptDir & "\autoit_addon460.dll")
	FileInstall("udf\autoit-opencv\autoit_opencv_com460.dll", @ScriptDir & "\autoit_opencv_com460.dll")
	FileInstall("udf\autoit-opencv\opencv\opencv_world460.dll", @ScriptDir & "\opencv_world460.dll")
	FileInstall("udf\autoit-opencv\opencv\opencv_videoio_msmf460_64.dll", @ScriptDir & "\opencv_videoio_msmf460_64.dll")
	FileInstall("udf\autoit-opencv\opencv\opencv_videoio_ffmpeg460_64.dll", @ScriptDir & "\opencv_videoio_ffmpeg460_64.dll")
EndIf

init()


If FileExists("udf\autoit-opencv\opencv\opencv_world460.dll") Then
    _OpenCV_Open_And_Register("udf\autoit-opencv\opencv\opencv_world460.dll", "udf\autoit-opencv\autoit_opencv_com460.dll")
Else
    _OpenCV_Open_And_Register("opencv_world460.dll", "autoit_opencv_com460.dll")
EndIf


OnAutoItExitRegister("_OnAutoItExit")

;OpenCV image buffers init:
$capcha_img = _OpenCV_imread_and_check(_OpenCV_FindFile("captcha.png"))

initBlueStacks()

CreateGUI("H2C2")

; interruption de fonctions
HotKeySet("{ESC}", "_Interrupt")
Global $fInterrupt = 1

AutoItSetOption("MouseClickDragDelay", 750)

While 1
	Sleep(100)
WEnd

Func CreateRaidersGui()

	$top = 50
	$btnTournament10 = GUICtrlCreateButton("Tournament", 10, $top, 80, 25)
	GUICtrlSetOnEvent($btnTournament10, "DoTournament10")

	$btnContinueTournament10 = GUICtrlCreateButton("Continue Tour.", 100, $top, 80, 25)
	GUICtrlSetOnEvent($btnContinueTournament10, "DoContinueTournament10")

	$top = $top + 60
	;GUICtrlCreateLabel("Strategy", 50, $top+6)
	;$edtStrategy = GUICtrlCreateInput("", 100, $top, 30, 25)

	;$top = $top + 40
	GUICtrlCreateLabel("Strategy", 50, $top+6)
	$cmbTournament10 = GUICtrlCreateCombo("Bottom2Top", 100, $top+2, 80, 25)
	GUICtrlSetData($cmbTournament10, "Top2Bottom|Random|AboveMe|RBBBA|RBBAA|RBAAA|BBBBA")
	GUICtrlSetOnEvent($cmbTournament10, "comboTournament")
	$tournamentStrategy = IniRead(@ScriptDir & "/H2C2.ini", "game", "tournamentStrategy", "Bottom2Top")
	_GUICtrlComboBox_SelectString($cmbTournament10, $tournamentStrategy)

	$top = $top + 30
	GUICtrlCreateLabel("Payment", 50, $top+6)
	$cmbTournamentPay = GUICtrlCreateCombo("Preselected", 100, $top+2, 80, 25)
	GUICtrlSetData($cmbTournamentPay, "Apples|Tickets")
	GUICtrlSetOnEvent($cmbTournamentPay, "comboTournamentPay")
	$tournamentPayment = IniRead(@ScriptDir & "/H2C2.ini", "game", "tournamentPayment", "Preselected")
	_GUICtrlComboBox_SelectString($cmbTournamentPay, $tournamentPayment)

	$top = $top + 30
	$tournamentCount = IniRead(@ScriptDir & "/H2C2.ini", "game", "tournamentCount", "10")
	$label = GUICtrlCreateLabel("Runs", 50, $top+6)
	$edtTournamentCount = GUICtrlCreateInput($tournamentCount, 100, $top, 30, 25, $ES_NUMBER)


	$top = $top + 60
	$chbTournamentGetReward = GUICtrlCreateCheckbox("Get reward", 100, $top)
	GUICtrlSetOnEvent($chbTournamentGetReward, "checkboxTournamentGetReward")
	$isGetReward = (IniRead(@ScriptDir & "/H2C2.ini", "game", "tournamentGetReward", "0") == "1")
	If $isGetReward Then
		GUICtrlSetState($chbTournamentGetReward, $GUI_CHECKED)
	Else
		GUICtrlSetState($chbTournamentGetReward, $GUI_UNCHECKED)
	EndIf



	$top = $top + 80
	$btnLighthouse = GUICtrlCreateButton("lighthouse", 10, $top, 80, 25)
	;GUICtrlSetTip($btnLighthouse, "lighthouse")
	GUICtrlSetOnEvent($btnLighthouse, "Dolighthouse")



	$top = $top + 80
	$btnBay = GUICtrlCreateButton("Bay", 10, $top, 80, 25)
	GUICtrlSetOnEvent($btnBay, "DoBay")

	$btnContinueBay = GUICtrlCreateButton("Continue Bay", 100, $top, 80, 25)
	GUICtrlSetOnEvent($btnContinueBay, "DoContinueBay")


EndFunc

Func CreateHuntersGui()

	$top = 50

	$btnCathedral = GUICtrlCreateButton("Cathedral", 10, $top, 80, 25)
	GUICtrlSetOnEvent($btnCathedral, "DoCathedral")

	$chbCathedralDangerousGame  = GUICtrlCreateCheckbox("Dangerous Game", 100, $top)
	GUICtrlSetOnEvent($chbCathedralDangerousGame, "checkboxCathedralDangerousGame")
	$isDangerousGame = (IniRead(@ScriptDir & "/H2C2.ini", "game", "cathedraldangerousgame", "0") == "1")
	If $isDangerousGame Then
		GUICtrlSetState($chbCathedralDangerousGame, $GUI_CHECKED)
	Else
		GUICtrlSetState($chbCathedralDangerousGame, $GUI_UNCHECKED)
	EndIf

	$chbCathedralBossesOnly  = GUICtrlCreateCheckbox("Bosses Only", 220, $top)
	GUICtrlSetOnEvent($chbCathedralBossesOnly, "checkboxCathedralBossesOnly")
	$isBossesOnly = (IniRead(@ScriptDir & "/H2C2.ini", "game", "cathedralbossesonly", "0") == "1")
	If $isBossesOnly Then
		GUICtrlSetState($chbCathedralBossesOnly, $GUI_CHECKED)
	Else
		GUICtrlSetState($chbCathedralBossesOnly, $GUI_UNCHECKED)
	EndIf

	$top = $top + 30
	GUICtrlCreateLabel("Payment", 50, $top+6)
	$cmbCathedralPay = GUICtrlCreateCombo("Preselected", 100, $top+2, 80, 25)
	GUICtrlSetData($cmbCathedralPay, "Apples|Keys")
	GUICtrlSetOnEvent($cmbCathedralPay, "comboCathedralPay")
	$cathedralPayment = IniRead(@ScriptDir & "/H2C2.ini", "game", "cathedralPayment", "Preselected")
	_GUICtrlComboBox_SelectString($cmbCathedralPay, $cathedralPayment)

	$top = $top + 30
	GUICtrlCreateLabel("Level", 50, $top+6)
	$cmbCathedralLevel = GUICtrlCreateCombo("Preselected", 100, $top+2, 80, 25)
	GUICtrlSetData($cmbCathedralLevel, "Normal|Difficult|Nightmare")
	GUICtrlSetOnEvent($cmbCathedralLevel, "comboCathedralLevel")
	$cathedralLevel = IniRead(@ScriptDir & "/H2C2.ini", "game", "cathedralLevel", "Preselected")
	_GUICtrlComboBox_SelectString($cmbCathedralLevel, $cathedralLevel)

	$top = $top + 30
	GUICtrlCreateLabel("If dead, ressurect", 50, $top+6)
	GUICtrlCreateLabel("fighters", 190, $top+6)
	$cmbCathedralNbToRez = GUICtrlCreateCombo("0", 150, $top+2, 30, 25)
	GUICtrlSetData($cmbCathedralNbToRez, "1|2|3|4|5|6|7|8")
	GUICtrlSetOnEvent($cmbCathedralNbToRez, "comboCathedralNbToRez")
	$nbToRez = IniRead(@ScriptDir & "/H2C2.ini", "game", "cathedralRez", "0")
	_GUICtrlComboBox_SelectString($cmbCathedralNbToRez, $nbToRez)

	$top = $top + 30
	$cathedralCount = IniRead(@ScriptDir & "/H2C2.ini", "game", "cathedralCount", "10")
	$label = GUICtrlCreateLabel("Runs", 50, $top+6)
	$edtCathedralCount = GUICtrlCreateInput($cathedralCount, 100, $top, 30, 25, $ES_NUMBER)

	$top = $top + 30
	$label = GUICtrlCreateLabel("Cathedral Speed Factor", 50, $top+6)
	$cmbCathedralSpeed = GUICtrlCreateCombo("Normal", 170, $top+3, 100, 25)
  GUICtrlSetData($cmbCathedralSpeed, "Average|Max|Burn-Baby-Burn")
	GUICtrlSetOnEvent($cmbCathedralSpeed, "comboCathedralSpeed")
	$cathedralSpeed = IniRead(@ScriptDir & "/H2C2.ini", "game", "cathedralSpeed", "Normal")
	_GUICtrlComboBox_SelectString($cmbCathedralSpeed, $cathedralSpeed)



	$top = $top + 30
	$chestBuyNumber = IniRead(@ScriptDir & "/H2C2.ini", "game", "chestBuyNumber", "1")
	$label = GUICtrlCreateLabel("Cathedral Big Chests to buy", 50, $top+6)
	$edtChestBuyNumber = GUICtrlCreateInput($chestBuyNumber, 200, $top, 30, 25, $ES_NUMBER)
    $btnCathedralChestLoop = GUICtrlCreateButton("Store loop", 240, $top, 60, 25)
	GUICtrlSetOnEvent($btnCathedralChestLoop, "DoCathedralBigChestLoop")



	$top = $top + 80
	$btnPortal = GUICtrlCreateButton("Portal", 10, $top, 80, 25)
	GUICtrlSetTip($btnPortal, "Move the Portal level to loop at the bottom of the screen")
	GUICtrlSetOnEvent($btnPortal, "DoPortal")
	$btnPortalChests = GUICtrlCreateButton("Portal Chests", 100, $top, 80, 25)
	GUICtrlSetOnEvent($btnPortalChests, "DoPortalChests")



	$top = $top + 80
	$btnDungeonLoop = GUICtrlCreateButton("Dungeon Loop", 10, $top, 100, 25)
	GUICtrlSetOnEvent($btnDungeonLoop, "doDungeonLoop")

	$dungeonCount = IniRead(@ScriptDir & "/H2C2.ini", "game", "dungeonCount", "10")
	$label = GUICtrlCreateLabel("Dungeon Runs", 150, $top+6)
	$edtDungeonCount = GUICtrlCreateInput($dungeonCount, 240, $top, 40, 25, $ES_NUMBER)
	$top = $top + 25
	GUICtrlCreateLabel("Payment", 50, $top+6)
	$cmbDungeonPay = GUICtrlCreateCombo("Preselected", 100, $top+2, 80, 25)
	GUICtrlSetData($cmbDungeonPay, "Apples|Torches")
	GUICtrlSetOnEvent($cmbDungeonPay, "comboDungeonPay")
	$dungeonPayment = IniRead(@ScriptDir & "/H2C2.ini", "game", "dungeonPayment", "Preselected")
	_GUICtrlComboBox_SelectString($cmbDungeonPay, $dungeonPayment)
	$chbDungeonRandomRestart  = GUICtrlCreateCheckbox("Restart w/ random", 200, $top)
	GUICtrlSetOnEvent($chbDungeonRandomRestart, "checkboxDungeonRandomRestart")
	$isDungeonRandomRestart = (IniRead(@ScriptDir & "/H2C2.ini", "game", "dungeonrandomrestart", "0") == "1")
	If $isDungeonRandomRestart Then
		GUICtrlSetState($chbDungeonRandomRestart, $GUI_CHECKED)
	Else
		GUICtrlSetState($chbDungeonRandomRestart, $GUI_UNCHECKED)
	EndIf


EndFunc

Func CreateWorkersGui()

	$top = 50
	$btnClanBoss = GUICtrlCreateButton("Clan Boss", 10, $top, 80, 25)
	GUICtrlSetTip($btnClanBoss, "Open the gate of the abyss first")
	GUICtrlSetOnEvent($btnClanBoss, "DoClanBoss")

	$cmbClanBoss = GUICtrlCreateCombo("Smasher", 100, $top+2, 80, 25)
	GUICtrlSetData($cmbClanBoss, "Pumba|Papa Troll|Sporozilla")
	GUICtrlSetOnEvent($cmbClanBoss, "comboClanBoss")
	$clanBoss = IniRead(@ScriptDir & "/H2C2.ini", "game", "clanBoss", "Smasher")
	_GUICtrlComboBox_SelectString($cmbClanBoss, $clanBoss)


	$top = $top + 80
	$btnTitanTower = GUICtrlCreateButton("Titan Tower", 10, $top, 80, 25)
	GUICtrlSetOnEvent($btnTitanTower, "DoTitanTower")
	$btnTitanColiseum = GUICtrlCreateButton("Titan Coliseum", 100, $top, 80, 25)
	GUICtrlSetOnEvent($btnTitanColiseum, "DoTitanColiseum")
	$chbTitanBest  = GUICtrlCreateCheckbox("Deploy the bests", 210, $top)
	GUICtrlSetOnEvent($chbTitanBest, "checkboxTitanBest")
	$isTitanBest = (IniRead(@ScriptDir & "/H2C2.ini", "game", "titanBest", "1") == "1")
	If $isTitanBest Then
		GUICtrlSetState($chbTitanBest, $GUI_CHECKED)
	Else
		GUICtrlSetState($chbTitanBest, $GUI_UNCHECKED)
	EndIf

EndFunc



Func CreateAdsGui()
	$top = 50
	$btnAds = GUICtrlCreateButton("Daily Ads", 10, $top, 80, 25)
	GUICtrlSetOnEvent($btnAds, "DoDailyAds")

	$chbAutocloseAds = GUICtrlCreateCheckbox("Autoclose Ads", 100, $top)
	GUICtrlSetOnEvent($chbAutocloseAds, "checkboxAutocloseAds")
	$isAutocloseAdd = (IniRead(@ScriptDir & "/H2C2.ini", "game", "autocloseads", "0") == "1")
	If $isAutocloseAdd Then
		GUICtrlSetState($chbAutocloseAds, $GUI_CHECKED)
	Else
		GUICtrlSetState($chbAutocloseAds, $GUI_UNCHECKED)
	EndIf

	$top = $top + 40
	$btnMapAds = GUICtrlCreateButton("Map Ads", 10, $top, 80, 25)
	GUICtrlSetOnEvent($btnMapAds, "DoMapAds")

	$btnFreeRes = GUICtrlCreateButton("Free Res.", 100, $top, 80, 25)
	GUICtrlSetOnEvent($btnFreeRes, "DoFreeResources")

	$top = $top + 40
	$btnResourceAds = GUICtrlCreateButton("Resource Ads", 10, $top, 80, 25)
	GUICtrlSetOnEvent($btnResourceAds, "DoResourceAds")

	$top = $top + 40
	$btnAllAds = GUICtrlCreateButton("ALL Ads", 10, $top, 80, 25)
	GUICtrlSetOnEvent($btnAllAds, "DoAllAds")
	$cmbAdsDuration = GUICtrlCreateCombo("40s", 100, $top+2, 80, 25)
	GUICtrlSetData($cmbAdsDuration, "20s|1min|1min30")
	GUICtrlSetOnEvent($cmbAdsDuration, "comboAdsDuration")
	$adDuration = IniRead(@ScriptDir & "/H2C2.ini", "ads", "duration", "40s")
	_GUICtrlComboBox_SelectString($cmbAdsDuration, $adDuration)
EndFunc


Func CreateExperimentalGui($winwidth, $winheight)
	$top = 50

	$edtXCoord = GUICtrlCreateInput("", 10, $top, 30, 25)
	$edtYCoord = GUICtrlCreateInput("", 50, $top, 30, 25)
	$btnGetColor = GUICtrlCreateButton("Pick", 90, $top, 40, 25)
	$edtGetColor = GUICtrlCreateInput("", 140, $top, 80, 25)
	GUICtrlSetOnEvent($btnGetColor, "DoGetColor")


	$top = $top + 40
	$btnTest = GUICtrlCreateButton("Test", 10, $top, 80, 25)
	GUICtrlSetOnEvent($btnTest, "DoTest")

	$top = $top + 40
	$btnGetEndFightColor = GUICtrlCreateButton("Get Color", 10, $top, 80, 25)
	GUICtrlSetOnEvent($btnGetEndFightColor, "_GetEndFightButtonColor")
	$btnGetMapBtnColor = GUICtrlCreateButton("Get Map Btn Color", 100, $top, 100, 25)
	GUICtrlSetOnEvent($btnGetMapBtnColor, "_GetGoToMapButtonColor")


	$top = $top + 40
	$btnDungeonBoss = GUICtrlCreateButton("Boss Dungeon", 10, $top, 100, 25)
	GUICtrlSetOnEvent($btnDungeonBoss, "doDungeonBoss")


	$top = $top + 60
	GUICtrlCreateLabel("Old", 10, $top+6)

	$top = $top + 30
	$btnInvasions = GUICtrlCreateButton("Invasions", 10, $top, 80, 25)
	GUICtrlSetTip($btnInvasions, "Open the first invasion first")
	GUICtrlSetOnEvent($btnInvasions, "DoInvasions")

	$top = $top + 30
	$btnRuneAdds = GUICtrlCreateButton("Rune Ads", 10, $top, 80, 25)
	GUICtrlSetOnEvent($btnRuneAdds, "DoRuneEventAds")

	$btnIsland = GUICtrlCreateButton("Island", 120, $top, 80, 25)
	GUICtrlSetOnEvent($btnIsland, "DoTheIsland")

	$top = $top + 30
	$btnDungeonM = GUICtrlCreateButton("Dungeon Leader", 10, $top, 100, 25)
	GUICtrlSetOnEvent($btnDungeonM, "doDungeonLeader")

	$btnDungeonS = GUICtrlCreateButton("Dungeon Follower", 120, $top, 100, 25)
	GUICtrlSetOnEvent($btnDungeonS, "doDungeonFollower")

	$top = $top + 30
	$btnTournament10 = GUICtrlCreateButton("Tour Test", 10, $top, 80, 25)
	GUICtrlSetOnEvent($btnTournament10, "DoTournament")

;~ 	$top = $top + 40
;~ 	$btnSwitchAccount = GUICtrlCreateButton("Switch", 10, $top, 80, 25)
;~ 	GUICtrlSetOnEvent($btnSwitchAccount, "DoSwitchAccount")

	$top = $top + 50

	GUICtrlCreateLabel("Log Level", 50, $top+6)
	$cmbLogLevel = GUICtrlCreateCombo("None", 100, $top+2, 80, 25)
	GUICtrlSetData($cmbLogLevel, "ERROR|INFO|TRACE")
	GUICtrlSetOnEvent($cmbLogLevel, "comboLogLevel")
	$log_level_CB = IniRead(@ScriptDir & "/H2C2.ini", "game", "logLevel", "NONE")
	_GUICtrlComboBox_SelectString($cmbLogLevel, $log_level_CB)

  $top = $top + 30

	$chbLogCathe  = GUICtrlCreateCheckbox("Log Cathedral", 10, $top)
	GUICtrlSetOnEvent($chbLogCathe, "checkboxLogCathe")
	$cath_log_enable = (IniRead(@ScriptDir & "/H2C2.ini", "params", "log_cathe", "0") == "1")
  If $cath_log_enable Then
		GUICtrlSetState($chbLogCathe, $GUI_CHECKED)
	Else
		GUICtrlSetState($chbLogCathe, $GUI_UNCHECKED)
	EndIf

	$chbLogDungeon  = GUICtrlCreateCheckbox("Log Dungeon", 100, $top)
	GUICtrlSetOnEvent($chbLogDungeon, "checkboxLogDungeon")
	$dungeon_log_enable = (IniRead(@ScriptDir & "/H2C2.ini", "params", "log_dungeon", "0") == "1")
  If $dungeon_log_enable Then
		GUICtrlSetState($chbLogDungeon, $GUI_CHECKED)
	Else
		GUICtrlSetState($chbLogDungeon, $GUI_UNCHECKED)
	EndIf

	Local $sFileVersion = FileGetVersion(@AutoItExe)
	GUICtrlCreateLabel($sFileVersion, $winwidth-42, $winheight-40)
EndFunc

Func CreateEventGui()
	$top = 30
  GUICtrlCreateLabel("Treasure Hunt:", 10, $top)
  $top = $top + 18

  $btnTresureHuntRun = GUICtrlCreateButton("Treasure Hunt RUN !", 150, $top, 120, 25)
	GUICtrlSetOnEvent($btnTresureHuntRun, "DoTreasureHunt")

  $chbTresureRwdApple  = GUICtrlCreateCheckbox("Collect Apples", 50, $top)
	GUICtrlSetOnEvent($chbTresureRwdApple, "checkboxTreasureCollectApples")
	$treasure_collect_apples = (IniRead(@ScriptDir & "/H2C2.ini", "events", "treasureApples", "0") == "1")
  If $treasure_collect_apples Then
		GUICtrlSetState($chbTresureRwdApple, $GUI_CHECKED)
	Else
		GUICtrlSetState($chbTresureRwdApple, $GUI_UNCHECKED)
	EndIf
  $top = $top + 18

  $chbTresureRwdGold  = GUICtrlCreateCheckbox("Collect Gold", 50, $top)
	GUICtrlSetOnEvent($chbTresureRwdGold, "checkboxTreasureCollectGold")
	$treasure_collect_gold = (IniRead(@ScriptDir & "/H2C2.ini", "events", "treasureGold", "0") == "1")
  If $treasure_collect_gold Then
		GUICtrlSetState($chbTresureRwdGold, $GUI_CHECKED)
	Else
		GUICtrlSetState($chbTresureRwdGold, $GUI_UNCHECKED)
	EndIf
  $top = $top + 18

  $chbTresureRwdMana  = GUICtrlCreateCheckbox("Collect Mana", 50, $top)
	GUICtrlSetOnEvent($chbTresureRwdMana, "checkboxTreasureCollectMana")
	$treasure_collect_mana = (IniRead(@ScriptDir & "/H2C2.ini", "events", "treasureMana", "0") == "1")
  If $treasure_collect_mana Then
		GUICtrlSetState($chbTresureRwdMana, $GUI_CHECKED)
	Else
		GUICtrlSetState($chbTresureRwdMana, $GUI_UNCHECKED)
	EndIf
  $top = $top + 18

  $chbTresureRwdFragHero  = GUICtrlCreateCheckbox("Collect Frag Hero", 50, $top)
	GUICtrlSetOnEvent($chbTresureRwdFragHero, "checkboxTreasureCollectFragHero")
	$treasure_collect_fraghero = (IniRead(@ScriptDir & "/H2C2.ini", "events", "treasureFragHero", "0") == "1")
  If $treasure_collect_fraghero Then
		GUICtrlSetState($chbTresureRwdFragHero, $GUI_CHECKED)
	Else
		GUICtrlSetState($chbTresureRwdFragHero, $GUI_UNCHECKED)
	EndIf
  $top = $top + 18

  $chbTresureRwdDust  = GUICtrlCreateCheckbox("Collect Dust", 50, $top)
	GUICtrlSetOnEvent($chbTresureRwdDust, "checkboxTreasureCollectDust")
	$treasure_collect_dust = (IniRead(@ScriptDir & "/H2C2.ini", "events", "treasureDust", "0") == "1")
  If $treasure_collect_dust Then
		GUICtrlSetState($chbTresureRwdDust, $GUI_CHECKED)
	Else
		GUICtrlSetState($chbTresureRwdDust, $GUI_UNCHECKED)
	EndIf
  $top = $top + 18

  $chbTresureRwdBook  = GUICtrlCreateCheckbox("Collect Book", 50, $top)
	GUICtrlSetOnEvent($chbTresureRwdBook, "checkboxTreasureCollectBook")
	$treasure_collect_book = (IniRead(@ScriptDir & "/H2C2.ini", "events", "treasureBook", "0") == "1")
  If $treasure_collect_book Then
		GUICtrlSetState($chbTresureRwdBook, $GUI_CHECKED)
	Else
		GUICtrlSetState($chbTresureRwdBook, $GUI_UNCHECKED)
	EndIf
  $top = $top + 18

  $chbTresureRwdLoupe  = GUICtrlCreateCheckbox("Collect Loupe", 50, $top)
	GUICtrlSetOnEvent($chbTresureRwdLoupe, "checkboxTreasureCollectLoupe")
	$treasure_collect_loupe = (IniRead(@ScriptDir & "/H2C2.ini", "events", "treasureLoupe", "0") == "1")
  If $treasure_collect_loupe Then
		GUICtrlSetState($chbTresureRwdLoupe, $GUI_CHECKED)
	Else
		GUICtrlSetState($chbTresureRwdLoupe, $GUI_UNCHECKED)
	EndIf
  $top = $top + 18

  $chbTresureRwdDiams  = GUICtrlCreateCheckbox("Collect Diams", 50, $top)
	GUICtrlSetOnEvent($chbTresureRwdDiams, "checkboxTreasureCollectDiams")
	$treasure_collect_diams = (IniRead(@ScriptDir & "/H2C2.ini", "events", "treasureDiams", "0") == "1")
  If $treasure_collect_diams Then
		GUICtrlSetState($chbTresureRwdDiams, $GUI_CHECKED)
	Else
		GUICtrlSetState($chbTresureRwdDiams, $GUI_UNCHECKED)
	EndIf
  $top = $top + 18

  $chbTresureRwdTickets  = GUICtrlCreateCheckbox("Collect Tickets", 50, $top)
	GUICtrlSetOnEvent($chbTresureRwdTickets, "checkboxTreasureCollectTickets")
	$treasure_collect_tickets = (IniRead(@ScriptDir & "/H2C2.ini", "events", "treasureTickets", "0") == "1")
  If $treasure_collect_tickets Then
		GUICtrlSetState($chbTresureRwdTickets, $GUI_CHECKED)
	Else
		GUICtrlSetState($chbTresureRwdTickets, $GUI_UNCHECKED)
	EndIf
  $top = $top + 18

	$Treasure_Max_Loupe = IniRead(@ScriptDir & "/H2C2.ini", "events", "treasureMaxLoupe", "6")
	$label = GUICtrlCreateLabel("Max loupe per level", 50, $top+6)
	$edtTresureMaxLoupePerLevel = GUICtrlCreateInput($Treasure_Max_Loupe, 180, $top, 30, 25, $ES_NUMBER)
  $top = $top + 25

  GUICtrlCreateLabel("Nb min reward per Loupe", 50, $top+6)
	$cmbTresureMinBoxPerLoupe = GUICtrlCreateCombo("3", 180, $top+2, 40, 25)
	GUICtrlSetData($cmbTresureMinBoxPerLoupe, "2|1")
	GUICtrlSetOnEvent($cmbTresureMinBoxPerLoupe, "comboTresureMinBoxPerLoupe")
	$TresureMinBoxPerLoupe = IniRead(@ScriptDir & "/H2C2.ini", "events", "treasureMinBoxPerLoupe", "3")
	_GUICtrlComboBox_SelectString($cmbTresureMinBoxPerLoupe, $TresureMinBoxPerLoupe)
  $top = $top + 25

  $Treasure_Stop_Max_Gold = IniRead(@ScriptDir & "/H2C2.ini", "events", "treasureStopMaxGold", "12")
	$label = GUICtrlCreateLabel("Stop after X Gold collected", 50, $top+6)
	$edtTresureStopMaxGold = GUICtrlCreateInput($Treasure_Stop_Max_Gold, 190, $top, 30, 25, $ES_NUMBER)
  GUICtrlCreateLabel("(0=Disabled)", 230, $top+6)
  $top = $top + 25

  $Treasure_Stop_Max_Apple = IniRead(@ScriptDir & "/H2C2.ini", "events", "treasureStopMaxApple", "12")
	$label = GUICtrlCreateLabel("Stop after X Apple collected", 50, $top+6)
	$edtTresureStopMaxApple = GUICtrlCreateInput($Treasure_Stop_Max_Apple, 190, $top, 30, 25, $ES_NUMBER)
  GUICtrlCreateLabel("(0=Disabled)", 230, $top+6)
  $top = $top + 25

  $Treasure_Stop_Max_Mana = IniRead(@ScriptDir & "/H2C2.ini", "events", "treasureStopMaxMana", "10")
	$label = GUICtrlCreateLabel("Stop after X Mana collected", 50, $top+6)
	$edtTresureStopMaxMana = GUICtrlCreateInput($Treasure_Stop_Max_Mana, 190, $top, 30, 25, $ES_NUMBER)
  GUICtrlCreateLabel("(0=Disabled)", 230, $top+6)
  
  $top = $top + 30
  
  
  GUICtrlCreateLabel("Excavation:", 10, $top)
  $top = $top + 18

  $btnEventExcavationAuto = GUICtrlCreateButton("Excavation Auto", 50, $top, 120, 25)
	GUICtrlSetOnEvent($btnEventExcavationAuto, "DoExcavationAuto")

EndFunc


Func CreateParamsGui()
	$top = 50

	$chbRuneEvent  = GUICtrlCreateCheckbox("Rune Event", 100, $top)
	GUICtrlSetOnEvent($chbRuneEvent, "checkboxRuneEvent")
	$isRuneEvent = (IniRead(@ScriptDir & "/H2C2.ini", "params", "runeevent", "0") == "1")
	If $isRuneEvent Then
		GUICtrlSetState($chbRuneEvent, $GUI_CHECKED)
	Else
		GUICtrlSetState($chbRuneEvent, $GUI_UNCHECKED)
	EndIf

	$top = $top + 40

	$chbQuickFight  = GUICtrlCreateCheckbox("Quick Fight", 100, $top)
	GUICtrlSetOnEvent($chbQuickFight, "checkboxQuickFight")
	$local_isQuickFight = (IniRead(@ScriptDir & "/H2C2.ini", "params", "quickfight", "0") == "1")
	If $local_isQuickFight Then
		GUICtrlSetState($chbQuickFight, $GUI_CHECKED)
	Else
		GUICtrlSetState($chbQuickFight, $GUI_UNCHECKED)
	EndIf

	$top = $top + 60
  GUICtrlCreateLabel("Discord params:", 10, $top+6)
  $top = $top + 20
  $discordUserId = IniRead(@ScriptDir & "/H2C2.ini", "params", "discordUserId", "1")
	GUICtrlCreateLabel("UserID", 50, $top+6)
	$edtDiscordUserId = GUICtrlCreateInput($discordUserId, 100, $top, 130, 25, $ES_NUMBER)
  $btnDiscordUserIdTest = GUICtrlCreateButton("UID Test", 250, $top, 50, 25)
	GUICtrlSetOnEvent($btnDiscordUserIdTest, "DoDiscordUserIdTest")

  $top = $top + 30
  $chbDiscordEnableCapcha  = GUICtrlCreateCheckbox("Notif Capcha", 50, $top)
	GUICtrlSetOnEvent($chbDiscordEnableCapcha, "checkboxDiscordEnableCapcha")
	$discordEnableCapcha = (IniRead(@ScriptDir & "/H2C2.ini", "params", "discordEnableCapcha", "1") == "1")
	If $discordEnableCapcha Then
		GUICtrlSetState($chbDiscordEnableCapcha, $GUI_CHECKED)
	Else
		GUICtrlSetState($chbDiscordEnableCapcha, $GUI_UNCHECKED)
	EndIf
  $chbDiscordEnableNotif  = GUICtrlCreateCheckbox("Notif Stop Bot", 150, $top)
	GUICtrlSetOnEvent($chbDiscordEnableNotif, "checkboxDiscordEnableNotif")
	$discordEnableNotif = (IniRead(@ScriptDir & "/H2C2.ini", "params", "discordEnableNotif", "1") == "1")
	If $discordEnableNotif Then
		GUICtrlSetState($chbDiscordEnableNotif, $GUI_CHECKED)
	Else
		GUICtrlSetState($chbDiscordEnableNotif, $GUI_UNCHECKED)
	EndIf



EndFunc

Func CreateGUI($title)
	; ########## Début de la création de la GUI 1 ##########
	$winwidth = 320
	$winheight = 500
	$GUI1 = GUICreate($title, $winwidth, $winheight, 1580, 10, default)

	GUISetOnEvent($GUI_EVENT_CLOSE, "ExitApplication");
	;$Lb1 = GUICtrlCreateLabel("", 10, 30, 280, 24) ; Création du label1
	;$btnBay = GUICtrlCreateButton("Bay", 10, 10, 100, 25)
	;$btnArena = GUICtrlCreateButton("Arena", 10, 50, 100, 25)

	$tab1 = GUICtrlCreateTab(0, 0, $winwidth, $winheight-20)

	$tabitem1 = GUICtrlCreateTabItem("Raider")

		CreateRaidersGui()

	$tabitem1 = GUICtrlCreateTabItem("Hunter")

		CreateHuntersGui()

	$tabitem1 = GUICtrlCreateTabItem("Worker")

		CreateWorkersGui()

	$tabitem1 = GUICtrlCreateTabItem("Ads")

		CreateAdsGui()

	$tabitem1 = GUICtrlCreateTabItem("Params")

		CreateParamsGui()

	$tabitem1 = GUICtrlCreateTabItem("X.")

		CreateExperimentalGui($winwidth, $winheight)

	$tabitem1 = GUICtrlCreateTabItem("Event")

		CreateEventGui()

	GUICtrlCreateTabItem("")
	;$Btn1 = GUICtrlCreateButton("Passer...", 130, 110, 100, 25) ; Création d'un bouton simple
	;GUICtrlSetState($Btn1, $GUI_DISABLE);

	$label = GUICtrlCreateLabel("", 0, $winheight-20, 400, 20, $SS_SUNKEN)


	GUISetState(@SW_SHOW, $GUI1) ; On affiche la GUI1
EndFunc



Func SwitchGuiState($activation)
	GUICtrlSetState($btnArena, $activation)
	GUICtrlSetState($btnAds, $activation)
	GUICtrlSetState($btnMapAds, $activation)
	GUICtrlSetState($btnResourceAds, $activation)
	GUICtrlSetState($chbAutocloseAds, $activation)
	GUICtrlSetState($btnFreeRes, $activation)
	GUICtrlSetState($btnAllAds, $activation)
	GUICtrlSetState($cmbAdsDuration, $activation)
	GUICtrlSetState($btnPortal, $activation)
	GUICtrlSetState($btnPortalChests, $activation)
	GUICtrlSetState($btnLighthouse, $activation)
	GUICtrlSetState($btnTitanTower, $activation)
	GUICtrlSetState($btnTitanColiseum, $activation)
	GUICtrlSetState($chbTitanBest, $activation)
	GUICtrlSetState($btnCathedral, $activation)
	GUICtrlSetState($cmbCathedralPay, $activation)
	GUICtrlSetState($cmbCathedralLevel, $activation)
	GUICtrlSetState($chbCathedralDangerousGame, $activation)
	GUICtrlSetState($chbCathedralBossesOnly, $activation)
	GUICtrlSetState($cmbCathedralNbToRez, $activation)
	GUICtrlSetState($btnTournament10, $activation)
	GUICtrlSetState($btnContinueTournament10, $activation)
	GUICtrlSetState($chbTournamentGetReward, $activation)
	GUICtrlSetState($cmbTournament10, $activation)
	GUICtrlSetState($cmbTournamentPay, $activation)
	GUICtrlSetState($btnInvasions, $activation)
	GUICtrlSetState($btnClanBoss, $activation)
	GUICtrlSetState($cmbClanBoss, $activation)
	GUICtrlSetState($btnBay, $activation)
	GUICtrlSetState($btnContinueBay, $activation)
	GUICtrlSetState($btnRuneAdds, $activation)
	GUICtrlSetState($btnIsland, $activation)
	GUICtrlSetState($btnDungeonM, $activation)
	GUICtrlSetState($btnDungeonS, $activation)
	GUICtrlSetState($btnDungeonBoss, $activation)
	GUICtrlSetState($chbRuneEvent, $activation)
	GUICtrlSetState($chbQuickFight, $activation)
	GUICtrlSetState($edtCathedralCount, $activation)
	GUICtrlSetState($cmbCathedralSpeed, $activation)
	GUICtrlSetState($btnDungeonLoop, $activation)
	GUICtrlSetState($edtDungeonCount, $activation)
	GUICtrlSetState($cmbDungeonPay, $activation)
	GUICtrlSetState($chbDungeonRandomRestart, $activation)
	GUICtrlSetState($btnEventExcavationAuto, $activation)
	GUICtrlSetState($edtDiscordUserId, $activation)
	GUICtrlSetState($btnDiscordUserIdTest, $activation)
	GUICtrlSetState($chbDiscordEnableCapcha, $activation)
	GUICtrlSetState($chbDiscordEnableNotif, $activation)
	GUICtrlSetState($chbTresureRwdApple, $activation)
	GUICtrlSetState($chbTresureRwdGold, $activation)
	GUICtrlSetState($chbTresureRwdMana, $activation)
	GUICtrlSetState($chbTresureRwdDiams, $activation)
	GUICtrlSetState($chbTresureRwdTickets, $activation)
	GUICtrlSetState($chbTresureRwdDust, $activation)
	GUICtrlSetState($chbTresureRwdFragHero, $activation)
	GUICtrlSetState($chbTresureRwdBook, $activation)
	GUICtrlSetState($chbTresureRwdLoupe, $activation)
	GUICtrlSetState($btnTresureHuntRun, $activation)
	GUICtrlSetState($cmbTresureMinBoxPerLoupe, $activation)
	GUICtrlSetState($edtTresureMaxLoupePerLevel, $activation)
	GUICtrlSetState($edtTresureStopMaxGold, $activation)
	GUICtrlSetState($edtTresureStopMaxApple, $activation)
	GUICtrlSetState($edtTresureStopMaxMana, $activation)
EndFunc



Func DoTest()
	BeforeLaunch("test")
  initBlueStacks()

  ;test_capcha()
  ;$Buffer = ""
  ;_OCR_Screen(470, 285, 1070-470, 485-285, $Buffer )

  _Function_finished()
EndFunc

Func PrintGUIStatus($message)
	GUICtrlSetData($label, $message)
EndFunc

Func Runtime_In_Hour_Min_Sec(ByRef $str_output)
  Local $fDiff = TimerDiff($hScriptRuntime)

  $fDiff = Int( $fDiff / 1000 ) ;In sec

  $iHours = Int( $fDiff / 3600 )
  $fDiff = Mod( $fDiff, 3600 )

  $iMin = Int( $fDiff / 60 )
  $fDiff = Mod( $fDiff, 60 )

  $iSec = $fDiff

  $str_output = StringFormat("[Run: %dh%dmin%dsec]", $iHours, $iMin, $iSec)
EndFunc

Func BeforeLaunch($message)
	Global $fInterrupt
	$fInterrupt = 0

  ; Enable periodic captcha test by default
  $enablePeriodicCapchaTest = True

  ; Read only one time the quick attack test
  $isQuickFight = (IniRead(@ScriptDir & "/H2C2.ini", "params", "quickfight", "0") == "1")

  ; Logger init
  _log_init()

  ; Init script runtime time reference
  $hScriptRuntime = TimerInit()

  ; Discord params
  $discordScriptNotif = False
  $discordUserId = GUICtrlRead($edtDiscordUserId)
	IniWrite(@ScriptDir & "/H2C2.ini", "params", "discordUserId", $discordUserId)
  $discordEnableCapcha = (IniRead(@ScriptDir & "/H2C2.ini", "params", "discordEnableCapcha", "1") == "1")

	PrintGUIStatus($message)
	SwitchGuiState($GUI_DISABLE)
EndFunc

Func DoDiscordUserIdTest()
	BeforeLaunch("run Discord UserID test ...")
	DiscordMessage($discordUserId, "Houston, do you hear me ?")
  _Function_finished()
EndFunc

Func DoDailyAds()
	BeforeLaunch("run Daily Ads script...")
	DailyAds(IsChecked($chbAutocloseAds))
EndFunc

Func DoMapAds()
	BeforeLaunch("run Map Ads script...")
	MapAds(IsChecked($chbAutocloseAds))
EndFunc

Func DoResourceAds()
	BeforeLaunch("run Resource Ads script...")
	ResourceAds(IsChecked($chbAutocloseAds))
EndFunc

Func DoFreeResources()
	BeforeLaunch("run Free Resources script...")
	FreeResources()
EndFunc

Func DoAllAds()
	Global $fInterrupt
	DoDailyAds()
	If $fInterrupt Then
		Return
	EndIf
	DoResourceAds()
	If $fInterrupt Then
		Return
	EndIf
	DoResourceAds() ;2x
	If $fInterrupt Then
		Return
	EndIf
	DoMapAds()
	If $fInterrupt Then
		Return
	EndIf
	DoFreeResources()
EndFunc

Func DoTournament10()
	BeforeLaunch("run Tournament (10) script...")
	Local $strategy = GUICtrlRead($cmbTournament10)
	Local $payment = GUICtrlRead($cmbTournamentPay)
	Local $getReward = IsChecked($chbTournamentGetReward)
	Local $count = Number(GUICtrlRead($edtTournamentCount))
	IniWrite(@ScriptDir & "/H2C2.ini", "game", "tournamentCount", $count)

  $discordEnableNotif = (IniRead(@ScriptDir & "/H2C2.ini", "params", "discordEnableNotif", "1") == "1")
  If $discordEnableNotif Then
    ;DiscordMessage($discordUserId, "Start arena script ...")
    $discordScriptNotif = True
  EndIf

	Tournament10($strategy, $payment, $getReward, $count)
EndFunc

Func DoTournament()
	BeforeLaunch("run Tournament  script...")
	Local $strategy = GUICtrlRead($cmbTournament10)
	Local $payment = GUICtrlRead($cmbTournamentPay)
	Local $getReward = IsChecked($chbTournamentGetReward)
	Local $count = Number(GUICtrlRead($edtTournamentCount))
	IniWrite(@ScriptDir & "/H2C2.ini", "game", "tournamentCount", $count)

  $discordEnableNotif = (IniRead(@ScriptDir & "/H2C2.ini", "params", "discordEnableNotif", "1") == "1")
  If $discordEnableNotif Then
    ;DiscordMessage($discordUserId, "Start arena script ...")
    $discordScriptNotif = True
  EndIf

	Tournament($strategy, $payment, $getReward)
EndFunc

Func DoContinueTournament10()
	BeforeLaunch("run Tournament (10) script...")
	Local $strategy = GUICtrlRead($cmbTournament10)
	Local $payment = GUICtrlRead($cmbTournamentPay)
	Local $getReward = IsChecked($chbTournamentGetReward)
	Local $count = Number(GUICtrlRead($edtTournamentCount))
	IniWrite(@ScriptDir & "/H2C2.ini", "game", "tournamentCount", $count)

  $discordEnableNotif = (IniRead(@ScriptDir & "/H2C2.ini", "params", "discordEnableNotif", "1") == "1")
  If $discordEnableNotif Then
    ;DiscordMessage($discordUserId, "Start arena script ...")
    $discordScriptNotif = True
  EndIf

	ContinueTournament10($strategy, $payment, $getReward, $count)
EndFunc

Func DoInvasions()
	BeforeLaunch("run Invasions script...")
	Invasions()
EndFunc

Func DoPortal()
	BeforeLaunch("run Portal script...")
	Portal()
EndFunc

Func DoPortalChests()
	BeforeLaunch("run Portal script...")
	PortalChestLevels()
EndFunc

Func Dolighthouse()
	BeforeLaunch("run Lighthouse script...")
	Lighthouse()
EndFunc

Func DoTitanTower()
	BeforeLaunch("run Titan tower script...")
	$isTitanBest = (IniRead(@ScriptDir & "/H2C2.ini", "game", "titanBest", "0") == "1")
	TitanTower($isTitanBest)
EndFunc

Func DoTitanColiseum()
	BeforeLaunch("run Titan coliseum script...")
	$isTitanBest = (IniRead(@ScriptDir & "/H2C2.ini", "game", "titanBest", "0") == "1")
	TitanColiseum($isTitanBest)
EndFunc

Func DoCathedral()
	BeforeLaunch("run Cathedral script...")
	Local $count = Number(GUICtrlRead($edtCathedralCount))
	IniWrite(@ScriptDir & "/H2C2.ini", "game", "cathedralCount", $count)

  $discordEnableNotif = (IniRead(@ScriptDir & "/H2C2.ini", "params", "discordEnableNotif", "1") == "1")
  If $discordEnableNotif Then
    ;DiscordMessage($discordUserId, "Start cathedral script ...")
    $discordScriptNotif = True
  EndIf

  ; Disable periodic capcha test -> Capcha occurs on cathedral end !!!
  ; -> Dedicated test somewhere else !
  $enablePeriodicCapchaTest = False

	Cathedral($count)
EndFunc

Func DoCathedralBigChestLoop()
	BeforeLaunch("run Cathedral big chest buy loop script...")
  Local $count = Number(GUICtrlRead($edtChestBuyNumber))
	IniWrite(@ScriptDir & "/H2C2.ini", "game", "chestBuyNumber", $count)
	Cathedral_BuyManuscriptsChest($count)
  _Function_Finished()
EndFunc

Func DoExcavationAuto()
	BeforeLaunch("run Excavation auto script...")
	Pioche_Run()
EndFunc

Func DoTreasureHunt()
	BeforeLaunch("run TresureHunt script...")

  Local $count = Number(GUICtrlRead($edtTresureMaxLoupePerLevel))
	IniWrite(@ScriptDir & "/H2C2.ini", "events", "treasureMaxLoupe", $count)
  $count = Number(GUICtrlRead($edtTresureStopMaxGold))
	IniWrite(@ScriptDir & "/H2C2.ini", "events", "treasureStopMaxGold", $count)
  $count = Number(GUICtrlRead($edtTresureStopMaxApple))
	IniWrite(@ScriptDir & "/H2C2.ini", "events", "treasureStopMaxApple", $count)
  $count = Number(GUICtrlRead($edtTresureStopMaxMana))
	IniWrite(@ScriptDir & "/H2C2.ini", "events", "treasureStopMaxMana", $count)

  $discordEnableNotif = (IniRead(@ScriptDir & "/H2C2.ini", "params", "discordEnableNotif", "1") == "1")
  If $discordEnableNotif Then
    ;DiscordMessage($discordUserId, "Start cathedral script ...")
    $discordScriptNotif = True
  EndIf

  ; Disable periodic capcha test -> Capcha occurs on cathedral end !!!
  ; -> Dedicated test somewhere else !
  $enablePeriodicCapchaTest = False

	Treasure_Run()

  _Function_Finished()
EndFunc

Func DoBay()
	BeforeLaunch("run Bay script...")
	Bay()
EndFunc

Func DoContinueBay()
	BeforeLaunch("run Bay script...")
	ContinueBay()
EndFunc


Func DoRuneEventAds()
	BeforeLaunch("run Runes Adds script...")
	RuneEventAds(IsChecked($chbAutocloseAds))
EndFunc

Func DoTheIsland()
	BeforeLaunch("run Island script...")
	TheIsland()
EndFunc


Func doClanBoss()
	Local $clanBossValue = GUICtrlRead($cmbClanBoss)
	;ConsoleWrite ("Boss : "& $clanBossValue & @CRLF)
	BeforeLaunch("run Clan Boss script...")
	ClanBoss($clanBossValue)
EndFunc

Func doDungeonLeader()
	BeforeLaunch("run Dungeon Leader script...")
	DungeonLeader()
  _Function_Finished()
EndFunc

Func doDungeonFollower()
	BeforeLaunch("run Dungeon Follower script...")
	DungeonFollower()
  _Function_Finished()
EndFunc

Func doDungeonBoss()
	BeforeLaunch("run Boss Dungeon script...")
	DungeonBoss()
EndFunc

Func doDungeonLoop()
	BeforeLaunch("run Dungeon Loop script...")
  Local $count = Number(GUICtrlRead($edtDungeonCount))
	IniWrite(@ScriptDir & "/H2C2.ini", "game", "dungeonCount", $count)

  $discordEnableNotif = (IniRead(@ScriptDir & "/H2C2.ini", "params", "discordEnableNotif", "1") == "1")
  If $discordEnableNotif Then
    ;DiscordMessage($discordUserId, "Start dungeon script ...")
    $discordScriptNotif = True
  EndIf

  ; Disable periodic capcha test -> Capcha occurs on dungeon end !!!
  ; -> Dedicated test somewhere else !
  $enablePeriodicCapchaTest = False

	DungeonLoop($count)

EndFunc


Func DoGetColor()
	initBlueStacks()

	Local $x = GUICtrlRead($edtXCoord)
	Local $y = GUICtrlRead($edtYCoord)

	$color = PixelGetColor($x, $y)
	MouseMove($x, $y)

	GUICtrlSetData($edtGetColor, "0x"&hex($color, 6))
EndFunc

Func comboTournament()
	IniWrite(@ScriptDir & "/H2C2.ini", "game", "tournamentStrategy", GUICtrlRead($cmbTournament10))
EndFunc

Func comboTournamentPay()
	IniWrite(@ScriptDir & "/H2C2.ini", "game", "tournamentPayment", GUICtrlRead($cmbTournamentPay))
EndFunc

Func comboDungeonPay()
	IniWrite(@ScriptDir & "/H2C2.ini", "game", "dungeonPayment", GUICtrlRead($cmbDungeonPay))
EndFunc

Func checkboxDungeonRandomRestart()
	IniWrite(@ScriptDir & "/H2C2.ini", "game", "dungeonrandomrestart", IsChecked($chbDungeonRandomRestart))
EndFunc

Func checkboxDiscordEnableCapcha()
	IniWrite(@ScriptDir & "/H2C2.ini", "params", "discordEnableCapcha", IsChecked($chbDiscordEnableCapcha))
EndFunc

Func checkboxDiscordEnableNotif()
	IniWrite(@ScriptDir & "/H2C2.ini", "params", "discordEnableNotif", IsChecked($chbDiscordEnableNotif))
EndFunc

Func checkboxTournamentGetReward()
	IniWrite(@ScriptDir & "/H2C2.ini", "game", "tournamentGetReward", IsChecked($chbTournamentGetReward))
EndFunc
Func comboClanBoss()
	IniWrite(@ScriptDir & "/H2C2.ini", "game", "clanBoss", GUICtrlRead($cmbClanBoss))
EndFunc

Func comboAdsDuration()
	IniWrite(@ScriptDir & "/H2C2.ini", "ads", "duration", GUICtrlRead($cmbAdsDuration))
EndFunc

Func IsChecked($nCtrl)
    If BitAND(GUICtrlRead($nCtrl), $GUI_CHECKED) = $GUI_CHECKED Then Return "1"
    Return "0"
EndFunc

Func checkboxAutocloseAds()
	IniWrite(@ScriptDir & "/H2C2.ini", "game", "autocloseads", IsChecked($chbAutocloseAds))
EndFunc

Func checkboxCathedralDangerousGame()
	IniWrite(@ScriptDir & "/H2C2.ini", "game", "cathedraldangerousgame", IsChecked($chbCathedralDangerousGame))
EndFunc

Func checkboxCathedralBossesOnly()
	IniWrite(@ScriptDir & "/H2C2.ini", "game", "cathedralbossesonly", IsChecked($chbCathedralBossesOnly))
EndFunc

Func checkboxTitanBest()
	IniWrite(@ScriptDir & "/H2C2.ini", "game", "titanBest", IsChecked($chbTitanBest))
EndFunc

Func comboCathedralPay()
	IniWrite(@ScriptDir & "/H2C2.ini", "game", "cathedralPayment", GUICtrlRead($cmbCathedralPay))
EndFunc

Func comboLogLevel()
	IniWrite(@ScriptDir & "/H2C2.ini", "game", "logLevel", GUICtrlRead($cmbLogLevel))
  $log_level_CB = GUICtrlRead($cmbLogLevel)
EndFunc

Func comboCathedralLevel()
	IniWrite(@ScriptDir & "/H2C2.ini", "game", "cathedralLevel", GUICtrlRead($cmbCathedralLevel))
EndFunc

Func comboCathedralSpeed()
	IniWrite(@ScriptDir & "/H2C2.ini", "game", "cathedralSpeed", GUICtrlRead($cmbCathedralSpeed))
EndFunc

Func comboCathedralNbToRez()
	IniWrite(@ScriptDir & "/H2C2.ini", "game", "cathedralRez", GUICtrlRead($cmbCathedralNbToRez))
EndFunc


Func checkboxRuneEvent()
	IniWrite(@ScriptDir & "/H2C2.ini", "params", "runeevent", IsChecked($chbRuneEvent))
EndFunc

Func checkboxLogCathe()
	IniWrite(@ScriptDir & "/H2C2.ini", "params", "log_cathe", IsChecked($chbLogCathe))
  $cath_log_enable = IsChecked($chbLogCathe)
EndFunc

Func checkboxLogDungeon()
	IniWrite(@ScriptDir & "/H2C2.ini", "params", "log_dungeon", IsChecked($chbLogDungeon))
  $dungeon_log_enable = IsChecked($chbLogDungeon)
EndFunc

Func checkboxQuickFight()
	IniWrite(@ScriptDir & "/H2C2.ini", "params", "quickfight", IsChecked($chbQuickFight))
EndFunc

Func checkboxTreasureCollectApples()
	IniWrite(@ScriptDir & "/H2C2.ini", "events", "treasureApples", IsChecked($chbTresureRwdApple))
  $treasure_collect_apples = IsChecked($chbTresureRwdApple)
EndFunc

Func checkboxTreasureCollectGold()
	IniWrite(@ScriptDir & "/H2C2.ini", "events", "treasureGold", IsChecked($chbTresureRwdGold))
  $treasure_collect_gold = IsChecked($chbTresureRwdGold)
EndFunc

Func checkboxTreasureCollectMana()
	IniWrite(@ScriptDir & "/H2C2.ini", "events", "treasureMana", IsChecked($chbTresureRwdMana))
  $treasure_collect_mana = IsChecked($chbTresureRwdMana)
EndFunc

Func checkboxTreasureCollectFragHero()
	IniWrite(@ScriptDir & "/H2C2.ini", "events", "treasureFragHero", IsChecked($chbTresureRwdFragHero))
  $treasure_collect_fraghero = IsChecked($chbTresureRwdFragHero)
EndFunc

Func checkboxTreasureCollectDust()
	IniWrite(@ScriptDir & "/H2C2.ini", "events", "treasureDust", IsChecked($chbTresureRwdDust))
  $treasure_collect_dust = IsChecked($chbTresureRwdDust)
EndFunc

Func checkboxTreasureCollectBook()
	IniWrite(@ScriptDir & "/H2C2.ini", "events", "treasureBook", IsChecked($chbTresureRwdBook))
  $treasure_collect_book = IsChecked($chbTresureRwdBook)
EndFunc

Func checkboxTreasureCollectLoupe()
	IniWrite(@ScriptDir & "/H2C2.ini", "events", "treasureLoupe", IsChecked($chbTresureRwdLoupe))
  $treasure_collect_loupe = IsChecked($chbTresureRwdLoupe)
EndFunc

Func checkboxTreasureCollectDiams()
	IniWrite(@ScriptDir & "/H2C2.ini", "events", "treasureDiams", IsChecked($chbTresureRwdDiams))
  $treasure_collect_diams = IsChecked($chbTresureRwdDiams)
EndFunc

Func checkboxTreasureCollectTickets()
	IniWrite(@ScriptDir & "/H2C2.ini", "events", "treasureTickets", IsChecked($chbTresureRwdTickets))
  $treasure_collect_tickets = IsChecked($chbTresureRwdTickets)
EndFunc

Func comboTresureMinBoxPerLoupe()
	IniWrite(@ScriptDir & "/H2C2.ini", "events", "treasureMinBoxPerLoupe", GUICtrlRead($cmbTresureMinBoxPerLoupe))
EndFunc

Func ExitApplication()
    Exit
EndFunc   ;==>Terminate

Func Pass()
    $pass = 1
EndFunc   ;==>Terminate

Func Wait($timeout)
	GUICtrlSetState($Btn1, $GUI_ENABLE);
	For $i = 1 to $timeout
		$min = Floor(($timeout-$i) / 60);
		$sec = mod(($timeout-$i), 60);
		GUICtrlSetData ($Lb1, "Attente : " & $min & "'" & $sec &"''");

		If $pass = 1 Then
			ExitLoop
		EndIf
		Sleep(1000)
	Next
	GUICtrlSetData ($Lb1, "");
	$pass = 0;
	GUICtrlSetState($Btn1, $GUI_DISABLE);
	WinActivate("BlueStacks")
EndFunc

Func _Interrupt()
	Global $fInterrupt
  Local $sRuntime

	ConsoleWrite("interrupt")

  Runtime_In_Hour_Min_Sec($sRuntime)
  PrintGUIStatus("Script interrupted ... " & $sRuntime)

	;if not in a loop, do not capture the signal
	If $fInterrupt == 1 Then
		HotKeySet("{ESC}") ;disable to prevent infinite loop
		Send("{E& sRuntimeSC}")
		HotKeySet("{ESC}", "_Interrupt")
	EndIf

    ; set the flag
    $fInterrupt = 1
EndFunc

Func _Interrupt_Sleep($iDelay)
	Global $fInterrupt

	Local $iBegin = TimerInit()
	Do
		Sleep(10)
		; Look for the interrupt
		If $fInterrupt Then
			; And return True if set
			Return True
		EndIf

	Until TimerDiff($iBegin) > $iDelay

  If $enablePeriodicCapchaTest Then
    If test_capcha() Then
      Return True
    EndIf
  EndIf

	; Return False if timed out and no interrupt was set
	Return False
EndFunc

Func _MouseClickApprox($button, $x, $y, $clicks)
	$x = $x+Random(-10, 10)
	$y = $y+Random(-10, 10)
	MouseClick($button, $x, $y, $clicks)
EndFunc

Func _GetEndFightButtonColor()
	initBlueStacks()

	$color = PixelGetColor($end_fight_button[0], $end_fight_button[1])
	MouseMove($end_fight_button[0], $end_fight_button[1])

	IniWrite("/H2C2.ini", "EndFightButton", "Color", $color)

EndFunc

Func _GetGoToMapButtonColor()
	initBlueStacks()

	For $i = 0 to 2
		$n = $i*3
		$color = PixelGetColor($go_to_map_button[$n], $go_to_map_button[$n+1])
		MouseMove($go_to_map_button[$n], $go_to_map_button[$n+1])

		IniWrite("/H2C2.ini", "GoToMapButtonColors", "color"&($i+1), $color)
	Next

	init()

EndFunc


Func _Function_finished()
	Global $fInterrupt
  Local $sRuntime

	If $fInterrupt == 0 Then ;fin de fonction, on sonne
		SoundPlay("levelup2.mp3")
		ConsoleWrite("play")

    Runtime_In_Hour_Min_Sec($sRuntime)
    PrintGUIStatus("Script complete !!! " & $sRuntime )

    ; Push message on discord
    If $discordScriptNotif Then
      DiscordMessage($discordUserId, "Script complete !!! " & $sRuntime)
    EndIf
	EndIf

	$fInterrupt = 1
	; reactivate all buttons and disable stop button
	SwitchGuiState($GUI_ENABLE)
EndFunc



Func _OnAutoItExit()
	_OpenCV_Unregister_And_Close()
EndFunc

Func DiscordNotif_IsEnabled()
  return $discordScriptNotif
EndFunc

Func DiscordGetUserId()
  return $discordUserId
EndFunc

Func _log($message)
  $msg = @HOUR & "h" & @MIN & "min" & @SEC & "s" & @MSEC & "ms -> " & $message
  ConsoleWrite($msg)
  If $logging Then ; global variable where you can globally switch OFF/ON logging
    FileWriteLine(@ScriptDir & '\file.log', $msg)
  EndIf
EndFunc

Func _log_init()
  If $log_level_CB == "ERROR" Then
    $log_level = 1
  ElseIf $log_level_CB == "INFO" Then
    $log_level = 2
  ElseIf $log_level_CB == "TRACE" Then
    $log_level = 3
  Else
    $log_level = 0
  EndIf
  _log("INIT LOG LEVEL = " & $log_level & @CRLF)
EndFunc

Func _cath_log_ERROR($message)
  If $cath_log_enable Then
    _Generic_log_ERROR($message)
  EndIf
EndFunc

Func _cath_log_INFO($message)
  If $cath_log_enable Then
    _Generic_log_INFO($message)
  EndIf
EndFunc

Func _cath_log_TRACE($message)
  If $cath_log_enable Then
    _Generic_log_TRACE($message)
  EndIf
EndFunc

Func _dungeon_log_ERROR($message)
  If $dungeon_log_enable Then
    _Generic_log_ERROR($message)
  EndIf
EndFunc

Func _dungeon_log_INFO($message)
  If $dungeon_log_enable Then
    _Generic_log_INFO($message)
  EndIf
EndFunc

Func _dungeon_log_TRACE($message)
  If $dungeon_log_enable Then
    _Generic_log_TRACE($message)
  EndIf
EndFunc

Func _Generic_log_ERROR($message)
  If $log_level >= 1 Then
    _log("ERROR: " & $message)
  EndIf
EndFunc

Func _Generic_log_INFO($message)
  If $log_level >= 2 Then
    _log("INFO: " & $message)
  EndIf
EndFunc

Func _Generic_log_TRACE($message)
  If $log_level >= 3 Then
    _log("TRACE: " & $message)
  EndIf
EndFunc

Func test_capcha()
	Local $aRect[4] = [300, 250, 700-300, 600-250]
	Local $img = _OpenCV_GetDesktopScreenMat($aRect)
	Local $aMatches = _OpenCV_FindTemplate($img, $capcha_img, 0.8)

	If UBound($aMatches) Then
		_Generic_log_INFO("Captcha detected !!"& @CRLF)
		SoundPlay("data/22296.mp3")
    PrintGUIStatus("Captcha detected !!")
    If $discordEnableCapcha Then
      DiscordCapcha($discordUserId)
    EndIf
		Return True
	EndIf

	Return False
EndFunc

;#RequireAdmin
#include <AutoItConstants.au3>
#include <MsgBoxConstants.au3>
#include <GUIConstantsEx.au3>
#include <StaticConstants.au3>
#include <WindowsConstants.au3>

#include "hc_actions.au3"
#include "gui.au3"

CreateGUI("HC Destroy Equipment")
HotKeySet("{ESC}", "Terminate")

Local $eventype = "tuiles"  ; "runes", "animal"

initBlueStacks()

;TODO avant de lancer le script :
; 1) ouvrir hustle castle
; 2) se mettre sur l'accueil de bluestacks. Les paramètres doivent être en 6e position

;!!! Attention, ça marche pas si y'a une invasion. Le texte de Donald s'enlève pas... STFU, Donald!!

MouseClick("primary", 243, 23 , 1) ; accueil de bluestacks
Sleep(1500)
;MouseClick("primary",883, 239 , 1) ; application systeme (6e icône, sinon ça marche pas)
;MouseClick("primary",735, 220 , 1) ; application systeme (5e icône, sinon ça marche pas)
MouseClick("primary",598, 226 , 1) ; application systeme (5e icône, sinon ça marche pas)
Sleep(1500)
MouseClick("primary",701, 473 , 1) ; paramètres android
Sleep(1500)
MouseClick("primary",286, 292 , 1) ; stockage
Sleep(1500)
MouseClick("primary",287, 404 , 1) ; applications
Sleep(4000)
MouseClick("primary",255, 296 , 1) ; hustle castle (c'est la plus grosse, sinon ça marche pas)
Sleep(1500)


Local $count = 10
Do
	MouseClick("primary", 348, 472 , 1) ; effacer les données
	Sleep(500)
	MouseClick("primary", 1171, 560 , 1) ; confirmer
	Sleep(2000)
	MouseClick("primary", 426, 20 , 1) ; 2e onglet de bluestacks (hustle castle)
	Sleep(15000) ;15 secondes

	MouseClick("primary", 771, 608 , 1) ;accept
	Sleep(50000) ;attend 50 secondes
	MouseClick("primary", 1470, 659 , 1) ;parametres
	Sleep(2000)
	MouseClick("primary", 755, 476 , 1) ;gestion du compte
	Sleep(1000)
	MouseClick("primary", 775, 423 , 1) ;google
	Sleep(3000)
	MouseClick("primary", 542, 576 , 1) ;compte G
	Sleep(3000)
	MouseClick("primary", 1002, 595 , 1) ;confirme G
	Sleep(1000)
	MouseClick("primary", 652, 663 , 1) ;confirm agaiiiin!
	Sleep(15000)

	;todo voir si c'est suffisant
	back_to_castle()
	Sleep(3000)
	back_to_castle()
	Sleep(1000)

	MouseClickDrag("primary", 184, 382, 1353, 382)
	Sleep(1000)
	MouseClickDrag("primary", 184, 382, 1353, 382)
	Sleep(1000)
	MouseClickDrag("primary", 184, 382, 1353, 382)
	Sleep(3000) ;si jamais 'faut cliquer vite vite ;)
	if( $eventype == "rune" ) Then
		MouseClick("primary", 700, 866 , 1) ; (à changer en fct des events et de la hauteur de la caserne)
		Sleep(2000)
		MouseClick("primary", 1310, 253 , 1) ; magasin d'event rune
		Sleep(1000)
		MouseClick("primary", 621, 694 , 1) ; pub rune


	ElseIf ($eventype == "animal") Then
		MouseClick("primary", 700, 866 , 1) ;  (à changer en fct des events et de la hauteur de la caserne)
		Sleep(2000)
		MouseClick("primary", 775, 831 , 1) ; magasin d'event animal
		Sleep(1000)
		MouseClick("primary", 414, 467 , 1) ; pub animal


	ElseIf ($eventype == "tuiles") Then
		MouseClickDrag("primary", 309, 709, 309, 188)
		Sleep(3000)
		MouseClick("primary", 1203, 426 , 1) ;  le bonhomme (à changer en fct des events et de la hauteur de la caserne)
		Sleep(2000)
		MouseClick("primary", 755, 223 , 1) ; quêtes
		Sleep(1000)
		MouseClick("primary", 1155, 488 , 1) ; pub
    EndIf

	wait_and_exit_add()
	Sleep(20000) ;attend la récompense

	If ($eventype == "tuiles") Then ;il faut récupérer la récompense
		MouseClick("primary", 1155, 488 , 1)
		Sleep(1000)
    EndIf

	MouseClick("primary", 622, 23 , 1) ; 3e onglet de bluestacks. Doit être ouvert préalablement sur applications => hustle => stockage
	Sleep(3000)


Until $count <0